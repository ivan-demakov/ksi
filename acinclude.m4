dnl ------------------------------------------------------------------------
dnl Find a file (or one of more files in a list of dirs)
dnl ------------------------------------------------------------------------
dnl
AC_DEFUN([AS_FIND_FILE],
[

$3=NO
for i in $2;
do
  for j in $1;
  do
    if test -r "$i/$j"; then
      $3=$i
      break 2
    fi
  done
done

])

AC_DEFUN([AS_CHECK_GETHOSTNAME],
[
  if test "$ac_cv_func_gethostbyname_r" = "yes"; then
    AC_LANG_SAVE
    AC_MSG_CHECKING([whether gethostbyname_r has 6 args])
    AC_TRY_COMPILE([#include <netdb.h>], [struct hostent *hp; char buf[1024]; int res; (void)gethostbyname_r("127.0.0.1", hp, buf, 1024, &hp, &res);],
      [AC_DEFINE(HAVE_GETHOSTBYNAME_R6, 1, [Do we have gethostbyname_r6])
       AC_MSG_RESULT(yes)
       ac_have_gethostbyname_r6="yes"],
      [AC_MSG_RESULT(no)])
    if test ! "$ac_have_gethostbyname_r6" = "yes"; then
      AC_MSG_CHECKING([whether gethostbyname_r has 5 args])
      AC_TRY_COMPILE([#include <netdb.h>], [struct hostent *hp; char buf[1024]; int res; (void)gethostbyname_r("127.0.0.1", hp, buf, 1024, &res);],
        [AC_DEFINE(HAVE_GETHOSTBYNAME_R5, 1, [Do we have gethostbyname_r5])
         AC_MSG_RESULT(yes)],
        [AC_MSG_RESULT(no)])
    fi
    AC_LANG_RESTORE
  fi
])

