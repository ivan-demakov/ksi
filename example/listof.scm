;;;
;;; listof.scm
;;; Haskell-like list comprehension
;;;
;;; derived from example in "The Scheme Programming Language, 2ed"
;;; by R. Kent Dybvig
;;;
;;; Author:        Ivan Demakov <demakov@users.sourceforge.net>
;;; Creation date: Sat Jun 10 14:58:18 2000
;;; Last Update:   Sat Jun 10 16:02:22 2000
;;;
;;; $Id: listof.scm,v 1.1 2000/07/25 18:59:42 demakov Exp $
;;;

;;;
;;; Examples.
;;;
;;; (list-of (cons x y) (x <- '(1 2)) (y <- '(3 4)))
;;;     ==> ((1 . 3) (1 . 4) (2 . 3) (2 . 4))
;;;
;;; (list-of y (x <- '(1 2 3 4)) (y is (* x x))) 
;;;     ==> (1 4 9 16)
;;;
;;; (define (quicksort l)
;;;   (if (null? l)
;;;       '()
;;;       (let ((x (car l)) (xs (cdr l)))
;;;         (append (quicksort (list-of y (y <- xs) (< y x)))
;;;                 (cons x
;;;                       (quicksort (list-of y (y <- xs) (>= y x))))))))
;;;

(define-syntax list-of
  (syntax-rules (<- is)
    ((list-of "help" e base)
     (cons e base))
    ((list-of "help" e base (x <- s) m ...)
     (let loop ((xs s))
       (if (null? xs)
           base
           (let ((x (car xs)))
             (list-of "help" e (loop (cdr xs)) m ...)))))
    ((list-of "help" e base (x is y) m ...)
     (let ((x y)) (list-of "help" e base m ...)))
    ((list-of "help" e base p m ...)
     (if p (list-of "help" e base m ...) base))
    ((list-of e m ...)
     (list-of "help" e '() m ...))))


;;; End of code