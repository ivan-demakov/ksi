#
# main makefile for Visual C
#

SUBDIRS=si interp lib
#SUBDIRS=si interp lib doc ext

!include visual.mif

CFLAGS=$(EXE_CFLAGS)

all: do_subdirs
	@echo done all

clean: do_clean do_subdirs_clean
	@echo done clean

topdir_dist:
	@if exist $(DISTDIR).rar del $(DISTDIR).rar
	@if exist $(DISTDIR) rmdir /Q /S $(DISTDIR)
	@if not exist $(DISTDIR) mkdir $(DISTDIR)
	@if not exist $(DISTDIR)\bin mkdir $(DISTDIR)\bin
	@if not exist $(DISTDIR)\include mkdir $(DISTDIR)\include
	@if not exist $(DISTDIR)\include\ksi mkdir $(DISTDIR)\include\ksi
	@if not exist $(DISTDIR)\lib mkdir $(DISTDIR)\lib
#	@if not exist $(DISTDIR)\doc mkdir $(DISTDIR)\doc
	copy $(TOPDIR)\AUTHORS $(DISTDIR)
	copy $(TOPDIR)\COPYING $(DISTDIR)
	copy $(TOPDIR)\NEWS $(DISTDIR)
	copy $(TOPDIR)\README $(DISTDIR)

dist: topdir_dist do_subdirs_dist
	@for %i in ($(DLLS)) do copy %i $(DISTDIR)\bin
	@echo -----------------------------------------
	@echo !!! Distribution `$(DISTDIR)' created !!!
	@echo -----------------------------------------

distdev: dist
	@for %i in ($(LIBS)) do copy %i $(DISTDIR)\lib
	@if not exist $(DISTDIR)\include\gc mkdir $(DISTDIR)\include\gc
	copy $(LIBSDIR)\include\gc\* $(DISTDIR)\include\gc
	copy $(LIBSDIR)\include\gmp.h $(DISTDIR)\include
#	@rar a -r $(DISTDIR) $(DISTDIR)
#	@del /S $(DISTDIR)
	@echo -----------------------------------------
	@echo !!! Libs copied to '$(DISTDIR)'       !!!
	@echo -----------------------------------------
