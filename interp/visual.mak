#
# makefile for Visual C
#

BUILD_TARGETS=$(BUILDDIR)\ksi.exe

!include $(TOPDIR)\visual.mif
!include <win32.mak>

TARGET_CFLAGS=$(EXE_CFLAGS)

KSI_LIB=$(BUILDDIR)\ksi-$(KSI_RELEASE).lib

initdir = $(TOPDIR)\$(DISTDIR)\$(KSI_VERSION)

all: do_curdir

$(BUILDDIR)\ksi.exe: ksi.obj $(KSI_LIB)
	$(link) $(lflags) -MACHINE:$(CPU) -out:$@ ksi.obj $(KSI_LIB) $(guilibsmt)

clean: do_curdir_clean

dist:
	copy $(BUILDDIR)\ksi.exe $(TOPDIR)\$(DISTDIR)\bin
	if not exist $(initdir) mkdir $(initdir)
	copy $(SRCDIR)\Boot.scm $(initdir)
