/*
 * ksi.c
 * stand alone ksi interpreter
 *
 * Note that this file can be linked with GNU readline library and so,
 * it is released under GPL as required by GNU readline library.
 *
 * Copyright (C) 1997-2010, 2015, ivan demakov
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Sun Aug 17 20:55:20 1997
 *
 */

#include <ksi_type.h>
#include <ksi_port.h>
#include <ksi_proc.h>
#include <ksi_jump.h>
#include <ksi_comp.h>
#include <ksi_printf.h>
#include <ksi_util.h>
#include <ksi_gc.h>

#include <signal.h>

#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif

#ifdef HAVE_FCNTL_H
#  include <fcntl.h>
#endif

#ifdef _MSC_VER
#  include <io.h>
#endif


#if defined(WIN32) || defined(OS2) || defined(MSDOS) || defined(__CYGWIN32__) || defined(__CYGWIN__)

#  define EXE_SUFFIX ".exe"
#  define IS_DIR(x)  ((x) == '/' || (x) == '\\' || (x) == ':')

#else

#  define EXE_SUFFIX ".static"
#  define IS_DIR(x)  ((x) == '/')

#endif


struct Ksi
{
    int interactive;
    char *app_name;
#if HAVE_LIBREADLINE
    char *prompt;
#else
    wchar_t *prompt;
#endif

    ksi_env top_level_env;

#if HAVE_LIBREADLINE
    ksi_obj complit_list;
    char *history_file;
    int history_size;
    char *readline;
#endif
};


static struct Ksi *ksi;


#if HAVE_LIBREADLINE

typedef int Function ();

extern char *readline (char *prompt);
extern int  rl_parse_and_bind (char *line);
extern int  rl_variable_bind (char *var, char *val);

extern void add_history (char *line);
extern int  read_history (char *filename);
extern int  write_history (char *filename);
extern int  append_history (int nelements, char *filename);
extern void *remove_history (int which);
extern int  where_history ();
extern int  history_search (char *string, int direction);
extern int  history_truncate_file (char *filename, int nlines);
extern void stifle_history (int max);

extern char *rl_readline_name;
extern char *rl_basic_word_break_characters;
extern char *rl_basic_quote_characters;
extern char *rl_completer_word_break_characters;
extern char *rl_completer_quote_characters;
extern Function *rl_completion_entry_function;
extern Function *rl_event_hook;


static int
rl_close (ksi_byte_port x)
{
    return 0;
}

static int
rl_read (ksi_byte_port port, char *buf, int num)
{
    int i;

    while (ksi->readline == 0) {
        ksi->readline = readline(ksi->prompt);
        if (ksi->readline == 0) {
            port->kp.eof = 1;
            return 0;
        }

        if (ksi->readline[0] == 0) {
            free(ksi->readline);
            ksi->readline = 0;
        } else {
            add_history(ksi->readline);
        }
    }

    for (i = 0; i < num; i++) {
        if (ksi->readline[i] != 0) {
            buf[i] = ksi->readline[i];
        } else {
            free(ksi->readline);
            ksi->readline = 0;
            buf[i++] = '\n';
            break;
        }
    }

    if (ksi->readline) {
        num = strlen(ksi->readline) - i;
        memmove(ksi->readline, ksi->readline + i, num + 1);
    }

    return i;
}

static struct Ksi_Byte_Port_Ops rl_port_ops =
{
    rl_close,
    0,
    rl_read,
    0,
    0,
    0
};

static ksi_obj
make_rl_port (void)
{
    struct Ksi_Byte_Port *port;

    port = ksi_malloc(sizeof *port);
    port->kp.o.itag = KSI_TAG_PORT;
    port->kp.port_id = L"readline";
    port->kp.binary = 1;
    port->kp.input = 1;
    port->kp.can_getpos = 0;
    port->kp.can_setpos = 0;
    port->ops = &rl_port_ops;

    return (ksi_obj) port;
}


static char*
ksi_compliter (char *text, int num)
{
    if (!text)
        text = "";

    if (num == 0) {
        wchar_t *ptr = ksi_utf(text);
        ksi->complit_list = ksi_abbrev(ptr, wcslen(ptr));
    }

    if (ksi->complit_list != ksi_nil) {
        char *ptr = ksi_local(ksi_obj2str(KSI_CAR(ksi->complit_list)));
        int len = strlen(ptr);
        ksi->complit_list = KSI_CDR(ksi->complit_list);

        text = malloc(len + 1);
        memcpy(text, ptr, len + 1);
        return text;
    }

    return 0;
}

static void
init_readline (void)
{
    /* Allow conditional parsing of the ~/.inputrc file. */
    rl_readline_name = ksi->app_name;

    rl_basic_word_break_characters = " \n\t()[]{}'`,;\"";
    rl_completion_entry_function = (Function*) ksi_compliter;

    stifle_history(ksi->history_size);
    read_history(ksi->history_file);
}

#endif


static ksi_obj
repl_proc (struct Ksi_Context *ctx, void *data)
{
    double t1 = 0.0, t2 = 0.0, t3 = 0.0, t4 = 0.0, t5 = 0.0, t6 = 0.0;
    ksi_obj val;
    const wchar_t *buf;

    for (;;) {
#if !HAVE_LIBREADLINE
        if (ksi->interactive) {
            ksi_port_write(ctx->output_port, ksi->prompt, wcslen(ksi->prompt));
            ksi_flush_output_port((ksi_obj) ctx->output_port);
        }
#endif

        val = ksi_get_datum((ksi_obj) ctx->input_port);
        if (val == ksi_eof)
            return val;

        if (ksi->interactive) {
            t5 = ksi_real_time ();
            t3 = ksi_eval_time ();
            t1 = ksi_cpu_time ();
        }

        val = ksi_eval(val, ksi->top_level_env);

        if (ksi->interactive) {
            t2 = ksi_cpu_time();
            t4 = ksi_eval_time();
            t6 = ksi_real_time();

            if (val != ksi_void) {
                buf = ksi_obj2str(val);
                ksi_port_write(ctx->output_port, L"\n", 1);
                ksi_port_write(ctx->output_port, buf, wcslen(buf));
                ksi_port_write(ctx->output_port, L"\n", 1);
                ksi_flush_output_port((ksi_obj) ctx->output_port);
            }

            buf = ksi_aprintf ("\n; cpu/eval/real time: %.4f/%.4f/%.4f sec\n; heap/free size: %d/%d bytes\n",
                               t2-t1, t4-t3, t6-t5, ksi_get_heap_size(), ksi_get_heap_free());
            ksi_port_write(ctx->error_port, buf, wcslen(buf));
            ksi_flush_output_port((ksi_obj) ctx->error_port);
        }
    }
}

static ksi_obj
ksi_interactive (ksi_obj x)
{
    int i = ksi->interactive;
    if (x)
        ksi->interactive = (x == ksi_false ? 0 : 1);
    return i ? ksi_true : ksi_false;
}

int
main (int argc, char* argv[])
{
    ksi_context_t ksi_ctx;
    ksi_obj val;
    ksi_env env;
    char *ptr, *app;
    int i;

#if defined(WIN32)
    ksi_init_gc();
#endif

    ptr = argv[0];
    for (i = strlen(ptr); i > 0; --i) {
        if (IS_DIR(ptr[i-1])) {
            ptr += i;
            break;
        }
    }

    app = (char*) alloca(strlen(ptr) + 1);
    strcpy(app, ptr);
#ifdef EXE_SUFFIX
    if (ksi_has_suffix(app, EXE_SUFFIX))
        app[strlen(app) - strlen(EXE_SUFFIX)] = '\0';
#endif

    ksi_ctx = ksi_malloc_eternal(sizeof *ksi_ctx);

    ksi_init_std_ports(0, 1, 2);
    ksi_init_context(ksi_ctx, &ksi_ctx);
    ksi_set_current_context(ksi_ctx);

    ksi = (struct Ksi*) ksi_malloc_eternal(sizeof *ksi);
    ksi->interactive   = isatty(0);
    ksi->app_name      = app;
#if HAVE_LIBREADLINE
    ksi->prompt        = ksi_local(ksi_aprintf("%s> ", app));
#else
    ksi->prompt        = ksi_aprintf("%s> ", app);
#endif
    ksi->top_level_env = ksi_top_level_env();

    env = ksi_get_lib_env(L"ksi", L"core", L"system", 0);
    ksi_defun(L"interactive", ksi_interactive, KSI_CALL_ARG1, 0, env);
    ksi_defsym(L"application", ksi_str02string(ksi_utf(app)), env);

    /* build arg-list */
    val = ksi_nil;
    for (i = argc; --i > 0; )
        val = ksi_cons(ksi_str02string(ksi_utf(argv[i])), val);
    val = ksi_cons(ksi_str02string(ksi_utf(app)), val);
    ksi_defsym(L"argv", val, env);

    if (ksi->interactive) {
#if HAVE_LIBREADLINE
        ptr = getenv ("HISTSIZE");
        ksi->history_file = ksi_expand_file_name("~/.ksi_history");
        ksi->history_size = (ptr ? atoi(ptr) : 200);
        ksi->complit_list = ksi_nil;
        ksi_set_current_input_port(ksi_transcoded_port(make_rl_port(), 0, 0, 0));
        init_readline();
#endif
    }

    if (argc >= 2 && strcmp(argv[1], "-b") == 0) {
        ksi_setlocale(ksi_str02sym(L"LC_ALL"), ksi_str02string(L""));
        if (argc >= 3) {
            ksi_load_boot_file(argv[2], ksi->top_level_env);
        }
    } else {
        ksi_load_boot_file("ksi/Boot.scm", ksi->top_level_env);
    }

    for (;;) {
        ksi_obj x = ksi_call_in_context(ksi_ctx, 0, repl_proc, ksi_handle_error);
        if (x == ksi_eof)
            break;
    }

    if (ksi->interactive) {
#if HAVE_LIBREADLINE
        write_history (ksi->history_file);
        history_truncate_file (ksi->history_file, ksi->history_size);
#endif
        ksi_port_write(ksi_ctx->error_port, L"; EXIT\n", 7);
        ksi_flush_output_port((ksi_obj) ksi_ctx->error_port);
    }

    ksi_term();
    exit(0);
}

/* End of code */
