;;;
;;; compiler.scm
;;; compiler
;;;
;;; Copyright (C) 1999-2009, Ivan Demakov.
;;;
;;; The software is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as published by
;;; the Free Software Foundation; either version 2.1 of the License, or (at your
;;; option) any later version.
;;;
;;; The software is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
;;; License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public License
;;; along with the software; see the file COPYING.LIB.  If not, write to
;;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
;;; MA 02110-1301, USA.
;;;
;;;
;;; Author:        Ivan Demakov <ksion@users.sourceforge.net>
;;; Creation date: Tue Jan 26 22:11:16 1999
;;;
;;;
;;; $Id: compiler.scm,v 1.10 2009/09/19 04:13:38 ksion Exp $
;;;
;;;

;;; compilation options

(define verbose? #f)
(define debug? #f)
(define need-expand? #t)
(define need-eval? #f)
(define need-compile? #t)
(define need-write? #t)
(define compile-load-path '())


;;; exported procedures

(define (set-compile-option! opt val)
  (case opt
    ((verbose)
     (set! verbose? val))
    ((debug)
     (if val (set! verbose? #t))
     (set! debug? val))
    ((expand)
     (set! need-expand? val))
    ((eval)
     (set! need-eval? val))
    ((compile)
     (set! need-compile? val))
    ((test-only)
     (set! need-write? (not val)))
    ((load-path)
     (set! compile-load-path val))))


(define (compile-expression sexp)
  (let ((module (@current-module)))
    (if need-expand?
	(set! sexp (expand-sexp sexp module)))
    (if need-compile?
	(set! sexp (compile-sexp sexp module)))
    sexp))


(define (compile-expression-list sexp-list)
  (let ((module (init-compilation-module)))
    (if need-expand?
	(set! sexp-list (expand-sexp-list sexp-list module)))
    (if need-compile?
	(set! sexp-list (compile-sexp-list sexp-list module)))
    sexp-list))


(define (compile-port in-port)
  (info "Reading   ")
  (let loop ((x (read in-port))
	     (n 0)
	     (r '()))
    (if (eof-object? x)
	(begin (info " ok~%")
	       (compile-expression-list (reverse! r)))
	(begin
	  (info-progress n)
	  (loop (read in-port) (+ n 1) (cons x r))))))


(define (compile-file input-path . output-path)
  (if (and (not need-expand?) (not need-compile?))
      (error 'compile-file "nothing to do"))

  (let* ((input (path-list->file-name input-path))
	 (stamp (file-time-stamp input))
	 (header (read-script-header input))
	 (output (if (pair? output-path)
		     (path-list->file-name (car output-path))
		     (make-output-file-name input))))

    (info "~s -> ~s~%" input-path output)

    (let ((in-port (open-input-file input)))
      (dynamic-wind
       (lambda () #f)

       (lambda ()
	 (let ((sexp-list (compile-port in-port)))
	   (if need-write?
	       (let ((out-port (open-file output "wb")))
		 (info "Writing   ")
		 (write-compiled-header header stamp out-port)
		 (let loop ((n 0) (xs sexp-list))
		   (if (null? xs)
		       (void)
		       (begin (info-progress n)
			      (write-compiled-sexp (car xs) out-port)
			      (loop (+ n 1) (cdr xs)))))
		 (info " ok~%")
		 (close-port out-port)))
	   sexp-list))

       (lambda ()
	 (close-port in-port))))))


(define (compile-dir dir-name . recur)
  (let ((dir (opendir dir-name)) (scm-dir '()) (subdir '()))

    ; collect all `.scm' files
    (let loop ((fn (readdir dir)))
      (if (eof-object? fn)
	  (closedir dir)
	  (begin
	    (cond ((directory? dir-name fn)
		   (if (and (pair? recur) (car recur))
		       (set! subdir (cons fn subdir))))
		  ((file-name-has-suffix? fn ".scm")
		   (set! scm-dir (cons fn scm-dir))))
	    (loop (readdir dir)))))

    (let loop ((res '()) (scm-dir scm-dir))
      (if (null? scm-dir)
	  (for-each (lambda (x)
		      (compile-file (list dir-name x))
		      (info "~%"))
		    res)

	  ; (pair? scm-dir)
	  (let* ((scm-file (car scm-dir))
		 (ks-file  (replace-suffix scm-file ".scm" ".ko")))
	    (if (or (not (file-exists? (list dir-name ks-file)))
		    (need-recompile? dir-name ks-file scm-file))
		(loop (cons scm-file res) (cdr scm-dir))
		(loop res (cdr scm-dir))))))

    ; compile subdirectories
    (let loop ((subdir subdir))
      (if (pair? subdir)
	  (begin (compile-dir (list dir-name (car subdir)) #t)
		 (loop (cdr subdir)))))))


;;; utils

(define (info fmt . args)
  (if verbose?
      (begin
	(apply format (current-output-port) fmt args)
	(flush-port (current-output-port)))))

(define (info-progress n)
  (info "."))

(define (debug fmt . args)
  (if debug?
      (begin
	(apply format (current-output-port) fmt args)
	(flush-port (current-output-port)))))

(define (read-script-header file-name)
  (call-with-input-file file-name
    (lambda (port)
      (if (and (eqv? (read-char port) #\#) (eqv? (read-char port) #\!))
	  (let loop ((c1 (read-char port)) (l '()))
            (if (eqv? c1 #\\)
                (let ((c2 (read-char port)))
                  (loop (read-char port) (cons c2 (cons c1 l))))
                (if (or (eof-object? c1) (eqv? c1 #\newline))
                    (string-append "#!" (list->string (reverse! l)) "\n")
                    (loop (read-char port) (cons c1 l)))))
          #f))))

(define (replace-suffix file in-suffix out-suffix)
  (string-append (if (file-name-has-suffix? file in-suffix)
		     (let ((fl (string-length file))
			   (sl (string-length in-suffix)))
		       (substring file 0 (- fl sl)))
		     file)
		 out-suffix))

(define (make-output-file-name file-name)
  (replace-suffix file-name ".scm" (if need-compile? ".ko" ".ks")))

(define (file-time-stamp fn)
  (strftime "%Y/%m/%d %H:%M:%S" (localtime (stat:mtime (stat fn)))))

(define (need-recompile? dir ks-fn scm-fn)
  (call-with-input-file (path-list->file-name (list dir ks-fn))
    (lambda (p)
      (not (equal? (read p) (file-time-stamp (list dir scm-fn)))))))

(define (directory? dir fn)
  (and (not (char=? #\. (string-ref fn 0)))
       (eq? (stat:type (stat (list dir fn))) 'directory)))


(define (make-compiled-header header)
  (define (find pred? pos)
    (cond ((not pos) pos)
	  ((>= pos (string-length header)) #f)
	  ((and (char? pred?) (char=? (string-ref header pos) pred?)) pos)
	  ((pred? (string-ref header pos)) pos)
	  (else (find pred? (+ pos 1)))))

  (define ws? char-whitespace?)

  (define (nws? x) (not (char-whitespace? x)))

  (let loop ((i (find nws? (find ws? 0))))
    (cond ((not i)
	   header)

	  ((and (char=? (string-ref header i) #\-)
		(< (+ 1 i) (string-length header)))
	   (cond ((char=? (string-ref header (+ 1 i)) #\-)
		  (loop (find nws? (find ws? (+ i 1)))))

		 ((ws? (string-ref header (+ 1 i)))
		  (loop (find nws? header (+ 1 i))))

		 (else
		  (let next ((i (+ 1 i)))
		    (cond ((>= i (string-length header))
			   header)
			  ((char=? (string-ref header i) #\s)
			   (string-set! header i #\S)
			   header)
			  ((ws? (string-ref header i))
			   (loop (find nws? i)))
			  (else
			   (next (+ i 1))))))))

	  (else
	   (loop (find nws? (find ws? i)))))))


(define (write-compiled-header header stamp port)
  (if need-compile?
      (begin
	(if header
	    (display (make-compiled-header header) port)
	    (format port "#! ~a/ksi -S~%" (ksi:install-bin-dir)))
	(write stamp port)
	(newline port)
	(write-char (integer->char 0) port)
	(write-char (integer->char (ksi:major-version)) port)
	(write-char (integer->char (ksi:minor-version)) port)
	(write-char (integer->char (ksi:patch-level)) port))
      (if header
	  (begin (display header port)
		 (newline port)))))

(define (write-compiled-sexp sexp port)
  (if need-compile?
      (write-dump sexp port)
      (begin
	(write sexp port)
	(newline port))))


(define (init-compilation-module)
  (let* ((name 'temporary-compilation-module)
	 (module (@find-module name)))
    (if module
	(@clear-module module)
	(set! module (@make-module name #t)))
    (@set-module-option! module 'syntax-env (push-syntax-case-state))
    (pop-syntax-case-state)
    module))


;;; expanding

(define (expand-sexp-list sexp-list module)
  (info "Expanding ")
  (let loop ((sexp-list sexp-list)
	     (n 0)
	     (res '()))
    (if (null? sexp-list)
	(begin (info " ok~%")
	       (reverse! res))
	(begin (info-progress n)
	       (loop (cdr sexp-list)
		     (+ n 1)
		     (cons (expand-sexp (car sexp-list) module) res))))))

(define (expand-sexp sexp module)
  (let ((prev-module (@current-module))
	(prev-load-path *load-path*))
    (dynamic-wind
     (lambda ()
       (set-load-path! compile-load-path)
       (@set-current-module! module)
       (push-syntax-case-state (@get-module-option module 'syntax-env)))
     (lambda ()
       (sc-expand3 sexp (if need-eval? 'c&e 'c) '(compile load eval)))
     (lambda ()
       (set-load-path! prev-load-path)
       (pop-syntax-case-state)
       (@set-current-module! prev-module)))))


;;; compiling

(define (compile-sexp-list sexp-list module)
  (info "Compiling ")
  (let loop ((sexp-list sexp-list)
	     (n 0)
	     (res '()))
    (if (null? sexp-list)
	(begin (info " ok~%")
	       (reverse! res))
	(begin (info-progress n)
	       (+ n 1)
	       (loop (cdr sexp-list)
		     (+ n 1)
		     (cons (compile-sexp (car sexp-list) module) res))))))

(define (compile-sexp sexp module)
  (@compile sexp (@module-environment module)))


;;; End of code
