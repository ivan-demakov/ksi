;;;
;;; syncase.scm
;;; syntax-case macros
;;;
;;; Copyright (C) 1998-2009, Ivan Demakov.
;;;
;;; The software is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as published by
;;; the Free Software Foundation; either version 2.1 of the License, or (at your
;;; option) any later version.
;;;
;;; The software is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
;;; License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public License
;;; along with the software; see the file COPYING.LIB.  If not, write to
;;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
;;; MA 02110-1301, USA.
;;;
;;;
;;; Author:        Ivan Demakov <ksion@users.sourceforge.net>
;;; Creation date: Tue Apr 14 19:00:58 1998
;;;
;;;
;;; $Id: syncase.scm,v 1.6 2009/09/19 04:13:38 ksion Exp $


;;; Exported variables

(define sc-expand #f)
(define sc-expand3 #f)
(define install-global-transformer #f)
(define syntax-dispatch #f)
(define syntax-error #f)

(define bound-identifier=? #f)
(define datum->syntax-object #f)
(define free-identifier=? #f)
(define generate-temporaries #f)
(define identifier? #f)
(define syntax-object->datum #f)


;;; Hooks needed by module system

(define save-syntax-case-state #f)
(define push-syntax-case-state #f)
(define pop-syntax-case-state #f)
(define copy-syntax-case-state #f)
(define copy-syntax-case-expr #f)


;(define (annotation-expression x) (errlog #f errlog/debug "~s" x) x)


;;; Load the preprocessed code
(load "psyntax")

(define *default-system-expander* sc-expand)
(set! *system-expander* sc-expand)


;; now define some useful macros

(define-syntax fluid-let
  (lambda (orig-x)
    (syntax-case orig-x ()
      ((_ () body0 body ...)
       (syntax (begin body0 body ...)))

      ((_ ((id val) ...) body0 body ...)
       (with-syntax (((old-val ...) (generate-temporaries (syntax (id ...))))
		     ((new-val ...) (generate-temporaries (syntax (id ...)))))
	 (syntax
	  (let ((old-val #f) ... (new-val val) ...)
	    (dynamic-wind
	     (lambda ()
	       (set! old-val id) ...
	       (set! id new-val) ...)
	     (lambda () body0 body ...)
	     (lambda ()
	       (set! new-val id) ...
	       (set! id old-val) ...)))))))))


(define-syntax define-macro
  (lambda (x)
    (syntax-case x ()
      ((_ (name . args) body0 body ...) (identifier? (syntax name))
       (syntax
	(define-syntax name
	  (lambda (x)
	    (define expander
	      (lambda args body0 body ...))
	    (syntax-case x ()
	      ((name . args)
	       (let ((exp (datum->syntax-object
			   (syntax name)
			   (apply expander
				  (syntax-object->datum (syntax args))))))
		 (with-syntax ((e exp))
		   (syntax e))))))))))))

					
(define-syntax when
  (syntax-rules ()
    ((when test)
     ((let ((t test)) (if t t))))
    ((when test exp0)
     (if test exp0))
    ((when test exp0 exp1 exp2 ...)
     (if test (begin exp0 exp1 exp2 ...)))))

(define-syntax unless
  (syntax-rules ()
    ((unless test)
     (let ((t (not test))) (if t t)))
    ((unless test exp0)
     (if (not test) exp0))
    ((unless test exp0 exp1 exp2 ...)
     (if (not test) (begin exp0 exp1 exp2 ...)))))

(define-syntax while
  (syntax-rules ()
    ((while test exp ...)
     (let loop ((t test))
       (if t (begin exp ... (loop test)))))))

(define-syntax until
  (syntax-rules ()
    ((until test exp ...)
     (let loop ((t test))
       (if (not t) (begin exp ... (loop test)))))))

(define-syntax dotimes
  (syntax-rules ()
    ((dotimes (var count) body ...)
     (let ((num count))
       (do ((var 0 (+ var 1)))
	   ((>= var num))
	 body ...)))
    ((dotimes (var count result) body ...)
     (let ((num count))
       (do ((var 0 (+ var 1)))
	   ((>= var num) result)
	 body ...)))))

(define-syntax begin0
  (syntax-rules ()
    ((begin0 expr0 expr ...)
     (let ((tmp expr0))
       expr ...
       tmp))))

(define-syntax receive
  (syntax-rules ()
    ((receive formals expression body ...)
     (call-with-values (lambda () expression)
		       (lambda formals body ...)))))

(define-syntax and-let*
  (syntax-rules ()
    ((and-let* () body ...)
     (begin body ...))

    ((and-let* ((var exp) clause ...) body ...)
     (let ((var exp))
       (and var (and-let* (clause ...) body ...))))

    ((and-let* ((exp) clause ...) body ...)
     (and exp (and-let* (clause ...) body ...)))

    ((and-let* (var clause ...) body ...)
     (and var (and-let* (clause ...) body ...)))))


;;; End of code 
