#! ../interp/ksi -b
;;;
;;; precompile init file
;;;

;(errlog-priority errlog/debug)

(set! *load-path*
      (let ((lib (getenv "KSI_LIBRARY")))
	(if lib (split-path lib) (list "../lib"))))

(set! *print-closure-body* #t)

(load "Boot")

(@set-current-module! (@make-module 'top-module #t))
(load "comp")

(compile-main (cons "precomp.scm" (cdddr *argv*)))
(exit)
