#! @-bindir-@/ksi \
-e compile-main -s
;;;
;;; comp.scm
;;; compilation
;;;
;;; Copyright (C) 1998,1999, Ivan Demakov.
;;; All rights reserved.
;;;
;;; Permission to use, copy, modify, and distribute this software and its
;;; documentation for any purpose, without fee, and without a written
;;; agreement is hereby granted, provided that the above copyright notice
;;; and this paragraph and the following two paragraphs appear in all copies.
;;; Modifications to this software may be copyrighted by their authors
;;; and need not follow the licensing terms described here, provided that
;;; the new terms are clearly indicated on the first page of each file where
;;; they apply.
;;;
;;; IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
;;; FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
;;; INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
;;; DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
;;; POSSIBILITY OF SUCH DAMAGE.
;;;
;;; THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
;;; INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
;;; AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
;;; ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
;;; TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
;;;
;;;
;;; Author:        Ivan Demakov <demakov@users.sourceforge.net>
;;; Creation date: Fri Sep 11 18:30:25 1998
;;; Last Update:   Sat Dec  4 04:35:13 1999
;;;
;;;
;;; $Id: comp.scm,v 1.3 2009/09/19 04:13:38 ksion Exp $
;;;
;;;
;;; Comments:
;;;
;;;

(use-module 'getopt)
(use-module 'compiler)


;;; compilation options

(define compile-load-path
  (let ((lib (getenv "KSI_LIBRARY")))
    (if lib (append *load-path* (split-path lib)) *load-path*)))

(define output-file-name #f)


;;; compilation entry point

(define (compile-main argv)
  (parse-options (cdr argv)
		 '(e: expand: noexpand: eval: noeval: v: q: d:)
		 '(i: o: l:)

		 (lambda (kw arg rest-opts)
		   (case kw
		     ((noexpand:)
		      (set-compile-option! 'expand #f))

		     ((expand:)
		      (set-compile-option! 'expand #t))

		     ((noeval:)
		      (set-compile-option! 'eval #f))

		     ((eval:)
		      (set-compile-option! 'eval #t))

		     ((e:)
		      (set-compile-option! 'compile #f))

		     ((i:)
		      (set! compile-load-path (cons arg compile-load-path)))

		     ((l:)
		      (load arg))

		     ((o:)
		      (set! output-file-name arg))

		     ((v:)
		      (set-compile-option! 'verbose #t))

		     ((q:)
		      (set-compile-option! 'verbose #f))

		     ((d:)
		      (set-compile-option! 'debug #t))

		     ((normal-arg)
		      (set-compile-option! 'load-path compile-load-path)
		      (if output-file-name
			  (compile-file arg output-file-name)
			  (compile-file arg))
		      rest-opts)

		     ((end-args)
		      (exit))

		     (else
		      (format #t "~s ~s~%" kw arg))))))


;;; End of code
