#
# makefile for Visual C
#

!include $(SRCDIR)\makedefs.mak

BUILD_TARGETS=makedefs.mak

!include $(TOPDIR)\visual.mif

bindir = $(TOPDIR)\$(DISTDIR)\bin
initdir = $(TOPDIR)\$(DISTDIR)\$(KSI_VERSION)
ksidir = $(initdir)\ksi
rnrsdir = $(initdir)\rnrs
iodir = $(rnrsdir)\io

all: do_curdir

clean: do_curdir_clean

dist:
	@if not exist $(initdir) mkdir $(initdir)
	@if not exist $(ksidir) mkdir $(ksidir)
	@if not exist $(rnrsdir) mkdir $(rnrsdir)
	@if not exist $(iodir) mkdir $(iodir)
#	@for %i in ($(init_DATA)) do copy $(SRCDIR)\%i $(initdir)
	@for %i in ($(ksi_DATA)) do copy $(SRCDIR)\%i $(ksidir)
	@for %i in ($(rnrs_DATA)) do copy $(SRCDIR)\%i $(rnrsdir)
	@for %i in ($(io_DATA)) do copy $(SRCDIR)\%i $(iodir)
