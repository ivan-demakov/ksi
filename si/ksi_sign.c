/*
 * ksi_sign.c
 * signals
 *
 * Copyright (C) 1998-2010, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Thu Jun 18 22:35:25 1998
 * Last Update:   Thu Jul 29 19:02:03 2010
 *
 */

#include "ksi_int.h"
#include "ksi_util.h"

#include <signal.h>

#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif

#ifdef HAVE_FCNTL_H
#  include <fcntl.h>
#endif

#if defined(HAVE_SYS_POLL_H)
#  include <sys/poll.h>
#elif defined(HAVE_SYS_SELECT_H)
#  include <sys/select.h>
#endif


#if defined(WIN32)
#  define FD_VALID(fd)	((fd) > 0)
#elif !defined(HAVE_SYS_POLL_H) && defined(HAVE_SYS_SELECT_H)
#  define FD_VALID(fd)	((fd) >= 0 && (fd) < FD_SETSIZE)
#else
#  define FD_VALID(fd)	((fd) >= 0)
#endif

#ifndef RETSIGTYPE
#  ifdef STDC_HEADERS
#    define RETSIGTYPE void
#  else
#    define RETSIGTYPE int
#  endif
#endif


#if defined(SIGIO) && !defined(sun)
#  define INOUT_BY_SIGIO	1
#else
#  define INOUT_BY_ALRM		1
#  define INOUT_POLL_TICK	0.1
#endif

#if defined(WIN32)

void
ksi_init_signals (ksi_env env)
{
}

void
ksi_term_signals (void)
{
}

#else

struct Timer_Event
{
  struct Timer_Event *next, *prev;
  ksi_event evt;
  double time;
  double tick;
  int restart;
};

struct Port_Event
{
  struct Port_Event *next, *prev;
  ksi_event evt;
  int fd;
  int restart;
};

struct Signal_Event
{
  struct Signal_Event *next, *prev;
  ksi_event evt;
  int sig;
  int restart;
};

struct Idle_Event
{
  struct Idle_Event *next, *prev;
  ksi_event evt;
  int restart;
};

struct Def_Event_Mgr
{
  struct Ksi_Event_Mgr ops;
  struct Timer_Event *timers;
  struct Port_Event *inputs;
  struct Port_Event *outputs;
  struct Signal_Event *signals[_NSIG];
  struct Idle_Event *idles;
#if defined(HAVE_SYS_POLL_H)
  int poll_num;
#endif
};

static void
append_timer (struct Def_Event_Mgr *mgr, struct Timer_Event *item)
{
  if (mgr->timers == 0) {
    item->next  = item;
    item->prev  = item;
    mgr->timers = item;
  } else if (item->time <= mgr->timers->time) {
    item->next       = mgr->timers;
    item->prev       = mgr->timers->prev;
    item->next->prev = item;
    item->prev->next = item;
    mgr->timers = item;
  } else {
    struct Timer_Event *curr;
    for (curr = mgr->timers->next; /*curr*/; curr = curr->next) {
      if (item->time <= curr->time || curr == mgr->timers) {
        item->next       = curr;
        item->prev       = curr->prev;
        curr->prev->next = item;
        curr->prev       = item;
        break;
      }
    }
  }
}

static void
remove_timer (struct Def_Event_Mgr *mgr, struct Timer_Event *item)
{
  if (item == mgr->timers) {
    if (item->next == mgr->timers) {
      mgr->timers = 0;
    } else {
      mgr->timers = item->next;
      item->next->prev = item->prev;
      item->prev->next = item->next;
    }
  } else {
    item->next->prev = item->prev;
    item->prev->next = item->next;
  }

  item->next = 0;
  item->prev = 0;
}

static void
append_input (struct Def_Event_Mgr *mgr, struct Port_Event *item)
{
  if (mgr->inputs == 0) {
    item->next  = item;
    item->prev  = item;
    mgr->inputs = item;
  } else {
    item->next       = mgr->inputs;
    item->prev       = mgr->inputs->prev;
    item->next->prev = item;
    item->prev->next = item;
    mgr->inputs      = item;
  }

#if defined(HAVE_SYS_POLL_H)
  mgr->poll_num++;
#endif
}

static void
remove_input (struct Def_Event_Mgr *mgr, struct Port_Event *item)
{
  if (item == mgr->inputs) {
    if (item->next == mgr->inputs) {
      mgr->inputs = 0;
    } else {
      mgr->inputs = item->next;
      item->next->prev = item->prev;
      item->prev->next = item->next;
    }
  } else {
    item->next->prev = item->prev;
    item->prev->next = item->next;
  }

  item->next = 0;
  item->prev = 0;

#if defined(HAVE_SYS_POLL_H)
  mgr->poll_num--;
#endif
}

static void
append_output (struct Def_Event_Mgr *mgr, struct Port_Event *item)
{
  if (mgr->outputs == 0) {
    item->next   = item;
    item->prev   = item;
    mgr->outputs = item;
  } else {
    item->next       = mgr->outputs;
    item->prev       = mgr->outputs->prev;
    item->next->prev = item;
    item->prev->next = item;
    mgr->outputs     = item;
  }

#if defined(HAVE_SYS_POLL_H)
  mgr->poll_num++;
#endif
}

static void
remove_output (struct Def_Event_Mgr *mgr, struct Port_Event *item)
{
  if (item == mgr->outputs) {
    if (item->next == mgr->outputs) {
      mgr->outputs = 0;
    } else {
      mgr->outputs     = item->next;
      item->next->prev = item->prev;
      item->prev->next = item->next;
    }
  } else {
    item->next->prev = item->prev;
    item->prev->next = item->next;
  }

  item->next = 0;
  item->prev = 0;

#if defined(HAVE_SYS_POLL_H)
  mgr->poll_num--;
#endif
}

static void
append_signal (struct Def_Event_Mgr *mgr, struct Signal_Event *item, int sig)
{
  if (mgr->signals[sig] == 0) {
    item->next        = item;
    item->prev        = item;
    mgr->signals[sig] = item;
  } else {
    item->next        = mgr->signals[sig];
    item->prev        = mgr->signals[sig]->prev;
    item->next->prev  = item;
    item->prev->next  = item;
    mgr->signals[sig] = item;
  }
}

static void
remove_signal (struct Def_Event_Mgr *mgr, struct Signal_Event *item, int sig)
{
  if (item == mgr->signals[sig]) {
    if (item->next == mgr->signals[sig]) {
      mgr->signals[sig] = 0;
    } else {
      mgr->signals[sig] = item->next;
      item->next->prev  = item->prev;
      item->prev->next  = item->next;
    }
  } else {
    item->next->prev = item->prev;
    item->prev->next = item->next;
  }

  item->next = 0;
  item->prev = 0;
}

static void
append_idle (struct Def_Event_Mgr *mgr, struct Idle_Event *item)
{
  if (mgr->idles == 0) {
    item->next = item;
    item->prev = item;
    mgr->idles = item;
  } else {
    item->next       = mgr->idles;
    item->prev       = mgr->idles->prev;
    item->next->prev = item;
    item->prev->next = item;
    mgr->idles = item;
  }
}

static void
remove_idle (struct Def_Event_Mgr *mgr, struct Idle_Event *item)
{
  if (item == mgr->idles) {
    if (item->next == mgr->idles) {
      mgr->idles = 0;
    } else {
      mgr->idles = item->next;
      item->next->prev = item->prev;
      item->prev->next = item->next;
    }
  } else {
    item->next->prev = item->prev;
    item->prev->next = item->next;
  }

  item->next = 0;
  item->prev = 0;
}


/*******************************************************************/

static struct Def_Event_Mgr	*event_mgr;
static volatile int		sig_ready[_NSIG], has_sig_ready;
static int			sig_installed[_NSIG];
static sigset_t			block_sig_set, old_sig_set;

#ifdef INOUT_BY_ALRM
static void			*inout_timer;
#endif

#ifdef HAVE_SIGACTION
static struct sigaction		old_sig_action[_NSIG];
#else
static RETSIGTYPE		(*old_sig_handler[_NSIG]) (int);
#endif

static void install_timer (double tm);
static int  install_signal (int sig);
static void uninstall_signal (int sig);
#ifdef INOUT_BY_SIGIO
static void install_inout (void);
static void uninstall_inout (void);
#endif

static void
run_timers (struct Def_Event_Mgr *mgr, double curr_time)
{
  struct Timer_Event *curr = mgr->timers, *next;
  do {
    if (curr_time < curr->time)
      break;
    if ((next = curr->next) == mgr->timers)
      next = 0;

    if (curr->evt)
      ksi_run_event (curr->evt, curr, 0);

    remove_timer (mgr, curr);
    if (curr->restart) {
      curr->time = curr_time + curr->tick;
      append_timer (mgr, curr);
    }
  } while ((curr = next) != 0);
}

static void
run_signals (struct Def_Event_Mgr *mgr, int sig)
{
  struct Signal_Event *curr = mgr->signals[sig], *next;
  do {
    if ((next = curr->next) == mgr->signals[sig])
      next = 0;

    if (curr->evt)
      ksi_run_event (curr->evt, curr, 0);

    if (!curr->restart) {
      uninstall_signal (sig);
      remove_signal (mgr, curr, sig);
    }
  } while ((curr = next) != 0);
}

static int
run_inout (struct Def_Event_Mgr *mgr, double timeout)
{
#if defined(HAVE_POLL)

  int res, num = mgr->poll_num;
  struct pollfd *fds = (struct pollfd*) ksi_malloc_data (num * sizeof *fds);

  num = 0;
  if (mgr->inputs) {
    struct Port_Event *curr = mgr->inputs;
    do {
#ifdef INOUT_BY_SIGIO
      fcntl (curr->fd, F_SETFL, fcntl (curr->fd, F_GETFL, 0) & ~O_ASYNC);
#endif
      fds[num].fd      = curr->fd;
      fds[num].events  = POLLIN;
      num++;
    } while ((curr = curr->next) != mgr->inputs);
  }

  if (mgr->outputs) {
    struct Port_Event *curr = mgr->outputs;
    do {
#ifdef INOUT_BY_SIGIO
      fcntl (curr->fd, F_SETFL, fcntl (curr->fd, F_GETFL, 0) & ~O_ASYNC);
#endif
      fds[num].fd      = curr->fd;
      fds[num].events  = POLLOUT;
      num++;
    } while ((curr = curr->next) != mgr->outputs);
  }

  res = poll (fds, num, (timeout < 0.0 ? -1 : (int) (timeout * 1000)));

  if (res <= 0)
    return res;

#define READY_IN(curr)	fds[num].revents != 0
#define READY_OUT(curr) fds[num].revents != 0
#define READY_START	num = 0
#define READY_NEXT      num++

#elif defined(HAVE_SELECT)

  int res;
  fd_set r_set, w_set;

  FD_ZERO (&r_set);
  FD_ZERO (&w_set);

  if (mgr->inputs) {
    struct Port_Event *curr = mgr->inputs;
    do {
#ifdef INOUT_BY_SIGIO
      fcntl (curr->fd, F_SETFL, fcntl (curr->fd, F_GETFL, 0) & ~O_ASYNC);
#endif
      FD_SET (curr->fd, &r_set);
    } while ((curr = curr->next) != mgr->inputs);
  }

  if (mgr->outputs) {
    struct Port_Event *curr = mgr->outputs;
    do {
#ifdef INOUT_BY_SIGIO
      fcntl (curr->fd, F_SETFL, fcntl (curr->fd, F_GETFL, 0) & ~O_ASYNC);
#endif
      FD_SET (curr->fd, &w_set);
    } while ((curr = curr->next) != mgr->outputs);
  }

  if (timeout < 0.0) {
    res = select (FD_SETSIZE, &r_set, &w_set, 0, 0);
  } else {
    struct timeval tv;
    tv.tv_sec  = (long) timeout;
    tv.tv_usec = (long) ((timeout - tv.tv_sec) * 1000000);
    if (tv.tv_usec >= 1000000) {
      tv.tv_sec  += 1;
      tv.tv_usec -= 1000000;
    }

    res = select (FD_SETSIZE, &r_set, &w_set, 0, &tv);
  }

  if (res <= 0)
    return res;

#define READY_IN(curr)	FD_ISSET (curr->fd, &r_set)
#define READY_OUT(curr) FD_ISSET (curr->fd, &w_set)
#define READY_START
#define READY_NEXT

#endif

  READY_START;
  if (mgr->inputs) {
    struct Port_Event *curr = mgr->inputs, *next;
    do {
      if ((next = curr->next) == mgr->inputs)
        next = 0;

      if (READY_IN (curr)) {
        if (curr->evt)
          ksi_run_event (curr->evt, curr, 0);

        if (!curr->restart)
          remove_input (mgr, curr);
      }

      READY_NEXT;
    } while ((curr = next) != 0);
  }

  if (mgr->outputs) {
    struct Port_Event *curr = mgr->outputs, *next;
    do {
      if ((next = curr->next) == mgr->outputs)
        next = 0;

      if (READY_OUT (curr)) {
        if (curr->evt)
          ksi_run_event (curr->evt, curr, 0);

        if (!curr->restart)
          remove_output (mgr, curr);
      }

      READY_NEXT;
    } while ((curr = next) != 0);
  }

#undef READY_IN
#undef READY_OUT
#undef READY_START
#undef READY_NEXT

  return 0;
}


static RETSIGTYPE
def_alrm_handler (int sig)
{
  if (event_mgr && event_mgr->timers) {
    double curr_time = ksi_real_time ();
    run_timers (event_mgr, curr_time);

    if (event_mgr->timers)
      install_timer (event_mgr->timers->time - curr_time);
    else
      install_timer (-1.0);

#ifdef INOUT_BY_ALRM
    run_inout (event_mgr, 0.0);
#endif
  }
}

static void
install_timer (double tm)
{
  struct itimerval it;

  if (event_mgr == 0 || tm < 0.0) {
    if (sig_installed[SIGALRM]) {
      sig_installed[SIGALRM] = 0;

      it.it_interval.tv_sec  = 0;
      it.it_interval.tv_usec = 0;
      it.it_value.tv_sec     = 0;
      it.it_value.tv_usec    = 0;

      setitimer (ITIMER_REAL, &it, 0);
      sigaction (SIGALRM, &old_sig_action[SIGALRM], 0);
    }
  } else {
    if (sig_installed[SIGALRM]++ == 0) {
      struct sigaction new_action;
      sigemptyset (&new_action.sa_mask);
      new_action.sa_handler = def_alrm_handler;
      new_action.sa_flags   = SA_RESTART;
      sigaction (SIGALRM, &new_action, &old_sig_action[SIGALRM]);
    }

    it.it_interval.tv_sec  = 0;
    it.it_interval.tv_usec = 0;
    it.it_value.tv_sec     = (long) tm;
    it.it_value.tv_usec    = (long) ((tm - it.it_value.tv_sec) * 1000000);
    if (it.it_value.tv_usec >= 1000000) {
      it.it_value.tv_sec  += 1;
      it.it_value.tv_usec -= 1000000;
    }

    setitimer (ITIMER_REAL, &it, 0);
  }
}


#ifdef INOUT_BY_SIGIO

static RETSIGTYPE
def_poll_handler (int sig)
{
  if (event_mgr) {
    run_inout (event_mgr, 0.0);
    install_inout ();
  }
}

static void
install_inout (void)
{
  if (!event_mgr)
    return;

  if (sig_installed[SIGIO] == 0) {
    struct sigaction new_action;
    sigemptyset (&new_action.sa_mask);
    new_action.sa_handler = def_poll_handler;
    new_action.sa_flags   = SA_RESTART;
    sigaction (SIGIO, &new_action, &old_sig_action[SIGIO]);
    sig_installed[SIGIO]++;
  }

  if (event_mgr->inputs) {
    struct Port_Event *curr = event_mgr->inputs;
    do {
      fcntl (curr->fd, F_SETFL, fcntl (curr->fd, F_GETFL, 0) | O_ASYNC);
    } while ((curr = curr->next) != event_mgr->inputs);
  }

  if (event_mgr->outputs) {
    struct Port_Event *curr = event_mgr->outputs;
    do {
      fcntl (curr->fd, F_SETFL, fcntl (curr->fd, F_GETFL, 0) | O_ASYNC);
    } while ((curr = curr->next) != event_mgr->outputs);
  }
}

static void
uninstall_inout (void)
{
  if (sig_installed[SIGIO]) {
    if (event_mgr) {
      if (event_mgr->inputs) {
        struct Port_Event *curr = event_mgr->inputs;
        do {
          fcntl (curr->fd, F_SETFL, fcntl (curr->fd, F_GETFL, 0) & ~O_ASYNC);
        } while ((curr = curr->next) != event_mgr->inputs);
      }

      if (event_mgr->outputs) {
        struct Port_Event *curr = event_mgr->outputs;
        do {
          fcntl (curr->fd, F_SETFL, fcntl (curr->fd, F_GETFL, 0) & ~O_ASYNC);
        } while ((curr = curr->next) != event_mgr->outputs);
      }
    }

    sig_installed[SIGIO] = 0;
    sigaction (SIGIO, &old_sig_action[SIGIO], 0);
  }
}
#endif


static RETSIGTYPE
def_sig_handler (int sig)
{
  if (event_mgr == 0) {
    sig_ready[sig] = 1;
    has_sig_ready  = 1;
  } else {
    if (event_mgr->signals[sig])
      run_signals (event_mgr, sig);
  }
}

static int
install_signal (int sig)
{
  if (sig < 0 || sig >= _NSIG || sig == SIGALRM)
    return 0;

#ifdef INOUT_BY_SIGIO
  if (sig == SIGIO)
    return 0;
#endif

  if (sig_installed[sig]++ == 0) {
#ifdef HAVE_SIGACTION
    struct sigaction new_action;
    sigemptyset (&new_action.sa_mask);
    new_action.sa_handler = def_sig_handler;
    new_action.sa_flags   = 0;
    sigaction (sig, &new_action, &old_sig_action[sig]);
#else
    old_sig_handler[sig] = signal (sig, def_sig_handler);
#endif
  }

  return 1;
}

static void
uninstall_signal (int sig)
{
  if (sig_installed[sig] == 0)
    return;

  if (--sig_installed[sig] == 0) {
#ifdef HAVE_SIGACTION
    sigaction (sig, &old_sig_action[sig], 0);
#else
    signal (sig, old_sig_handler[sig]);
#endif
  }
}


static void*
def_wait_timer (ksi_event_mgr _mgr, ksi_event evt, double tm, int restart)
{
  struct Def_Event_Mgr *mgr = (struct Def_Event_Mgr*) _mgr;
  struct Timer_Event   *item;

  if (tm <= 0.0)
    return 0;

  item = ksi_malloc (sizeof *item);
  item->evt     = evt;
  item->time    = tm + ksi_real_time ();
  item->tick    = tm;
  item->restart = restart;

  append_timer (mgr, item);
  install_timer (mgr->timers->time - ksi_real_time ());
  return item;
}

static void
def_cancel_timer (ksi_event_mgr _mgr, ksi_event evt, void *data)
{
  struct Def_Event_Mgr *mgr = (struct Def_Event_Mgr*) _mgr;
  struct Timer_Event   *item = (struct Timer_Event*) data;

  if (item->next == 0 || item->evt != evt)
    return;

  remove_timer (mgr, item);
  install_timer (mgr->timers ? mgr->timers->time - ksi_real_time () : -1.0);
}

static void*
def_wait_input (ksi_event_mgr _mgr, ksi_event evt, int fd, int restart)
{
  struct Def_Event_Mgr *mgr = (struct Def_Event_Mgr*) _mgr;
  struct Port_Event    *item;

  if (!FD_VALID (fd))
    return 0;

  item = ksi_malloc(sizeof *item);
  item->evt     = evt;
  item->fd      = fd;
  item->restart = restart;

  append_input (mgr, item);

#ifdef INOUT_BY_SIGIO
  install_inout ();
#endif

#ifdef INOUT_BY_ALRM
  if (!inout_timer)
    inout_timer = def_wait_timer (_mgr, 0, INOUT_POLL_TICK, 1);
#endif

  return item;
}

static void
def_cancel_input (ksi_event_mgr _mgr, ksi_event evt, void *data)
{
  struct Def_Event_Mgr *mgr = (struct Def_Event_Mgr*) _mgr;
  struct Port_Event    *item = (struct Port_Event*) data;

  if (item->next == 0 || item->evt != evt)
    return;

  remove_input (mgr, item);

#ifdef INOUT_BY_SIGIO
  fcntl (item->fd, F_SETFL, fcntl (item->fd, F_GETFL, 0) & ~O_ASYNC);
  install_inout ();
#endif

#ifdef INOUT_BY_ALRM
  if (inout_timer && !mgr->inputs && !mgr->outputs)
    {
      def_cancel_timer (_mgr, 0, inout_timer);
      inout_timer = 0;
    }
#endif
}

static void*
def_wait_output (ksi_event_mgr _mgr, ksi_event evt, int fd, int restart)
{
  struct Def_Event_Mgr *mgr = (struct Def_Event_Mgr*) _mgr;
  struct Port_Event    *item;

  if (!FD_VALID (fd))
    return 0;

  item = ksi_malloc(sizeof *item);
  item->evt     = evt;
  item->fd      = fd;
  item->restart = restart;

  append_output (mgr, item);

#ifdef INOUT_BY_SIGIO
  install_inout ();
#endif

#ifdef INOUT_BY_ALRM
  if (!inout_timer)
    inout_timer = def_wait_timer (_mgr, 0, INOUT_POLL_TICK, 1);
#endif

  return item;
}

static void
def_cancel_output (ksi_event_mgr _mgr, ksi_event evt, void *data)
{
  struct Def_Event_Mgr *mgr = (struct Def_Event_Mgr*) _mgr;
  struct Port_Event    *item = (struct Port_Event*) data;

  if (item->next == 0 || item->evt != evt)
    return;

  remove_output (mgr, item);

#ifdef INOUT_BY_SIGIO
  fcntl (item->fd, F_SETFL, fcntl (item->fd, F_GETFL, 0) & ~O_ASYNC);
  install_inout ();
#endif

#ifdef INOUT_BY_ALRM
  if (inout_timer && !mgr->inputs && !mgr->outputs) {
    def_cancel_timer (_mgr, 0, inout_timer);
    inout_timer = 0;
  }
#endif
}


static void*
def_wait_signal (ksi_event_mgr _mgr, ksi_event evt, int sig, int restart)
{
  struct Def_Event_Mgr *mgr = (struct Def_Event_Mgr*) _mgr;
  struct Signal_Event  *item;

  if (!install_signal (sig))
    return 0;

  item = ksi_malloc(sizeof *item);
  item->evt     = evt;
  item->sig     = sig;
  item->restart = restart;

  append_signal (mgr, item, sig);
  return item;
}

static void
def_cancel_signal (ksi_event_mgr _mgr, ksi_event evt, void *data)
{
  struct Def_Event_Mgr *mgr  = (struct Def_Event_Mgr*) _mgr;
  struct Signal_Event  *item = (struct Signal_Event*) data;

  if (item->next == 0 || item->evt != evt)
    return;

  remove_signal (mgr, item, item->sig);
  uninstall_signal (item->sig);
}

static void*
def_wait_idle (ksi_event_mgr _mgr, ksi_event evt, int restart)
{
  struct Def_Event_Mgr *mgr = (struct Def_Event_Mgr*) _mgr;
  struct Idle_Event    *item;

  item = ksi_malloc(sizeof *item);
  item->evt     = evt;
  item->restart = restart;

  append_idle (mgr, item);
  return item;
}

static void
def_cancel_idle (ksi_event_mgr _mgr, ksi_event evt, void *data)
{
  struct Def_Event_Mgr *mgr  = (struct Def_Event_Mgr*) _mgr;
  struct Idle_Event    *item = (struct Idle_Event*) data;

  if (item->next == 0 || item->evt != evt)
    return;

  remove_idle (mgr, item);
}

static void
def_wait_event (ksi_event_mgr _mgr, double timeout)
{
  int i;
  struct Def_Event_Mgr *mgr  = (struct Def_Event_Mgr*) _mgr;

  if (mgr->timers) {
    double t = mgr->timers->time - ksi_real_time ();
    if (t <= 0.0)
      timeout = 0.0;
    else if (timeout < 0.0 || t < timeout)
      timeout = t;
  }

  if (has_sig_ready || mgr->idles)
    timeout = 0.0;

  if (run_inout (mgr, timeout) < 0 && errno != EINTR) {
#ifdef INOUT_BY_SIGIO
    install_inout ();
#endif
    ksi_exn_error(0, 0, "wait-event: %m");
  }

  if (mgr->timers)
    run_timers (mgr, ksi_real_time ());

  has_sig_ready = 0;
  for (i = 0; i < _NSIG; i++) {
    if (sig_ready[i] && mgr->signals[i])
      run_signals (mgr, i);
    sig_ready[i] = 0;
  }

  if (mgr->idles && !ksi_has_pending_events ()) {
    struct Idle_Event *curr = mgr->idles, *next;
    do {
      if ((next = curr->next) == mgr->idles)
        next = 0;

      ksi_run_event (curr->evt, curr, 0);
      if (!curr->restart)
        remove_idle (mgr, curr);
    } while ((curr = next) != 0);
  }

#ifdef INOUT_BY_SIGIO
  install_inout ();
#endif
}

static void
def_enable_async_wait (ksi_event_mgr _mgr)
{
  event_mgr = (struct Def_Event_Mgr*) _mgr;

  if (event_mgr->timers)
    install_timer (event_mgr->timers->time - ksi_real_time ());
  else
    install_timer (-1.0);

#ifdef INOUT_BY_SIGIO
  install_inout ();
#endif
}

static void
def_disable_async_wait (ksi_event_mgr _mgr)
{
  install_timer (-1.0);

#ifdef INOUT_BY_SIGIO
  uninstall_inout ();
#endif

  event_mgr = 0;
}

static void
def_block_wait (ksi_event_mgr _mgr)
{
  sigprocmask (SIG_SETMASK, &block_sig_set, 0);
}

static void
def_unblock_wait (ksi_event_mgr _mgr)
{
  sigprocmask (SIG_SETMASK, &old_sig_set, 0);
}

static void
def_init (ksi_event_mgr _mgr)
{
}

static void
def_term (ksi_event_mgr _mgr)
{
}


void
ksi_init_signals (void)
{
    struct Def_Event_Mgr *mgr;
    ksi_env env = ksi_get_lib_env(L"ksi", L"core", L"event", 0);

    sigprocmask (SIG_SETMASK, 0, &old_sig_set);
    sigfillset (&block_sig_set);
#ifdef SIGSEGV
    sigdelset (&block_sig_set, SIGSEGV);
#endif
#ifdef SIGILL
    sigdelset (&block_sig_set, SIGILL);
#endif
#ifdef SIGQUIT
    sigdelset (&block_sig_set, SIGQUIT);
#endif
#ifdef SIGBUS
    sigdelset (&block_sig_set, SIGBUS);
#endif
#ifdef SIGIOT
    sigdelset (&block_sig_set, SIGIOT);
#endif
#ifdef SIGEMT
    sigdelset (&block_sig_set, SIGEMT);
#endif
#ifdef SIGTRAP
    sigdelset (&block_sig_set, SIGTRAP);
#endif

#ifdef SIGPIPE
    sig_installed[SIGPIPE] = 1;
#ifdef HAVE_SIGACTION
    {
        struct sigaction new_action;
        new_action.sa_handler = def_sig_handler;
        sigemptyset (&new_action.sa_mask);
        new_action.sa_flags = 0;
        sigaction (SIGPIPE, &new_action, &old_sig_action[SIGPIPE]);
    }
#else
    old_sig_handler[SIGPIPE] = signal (SIGPIPE, def_sig_handler);
#endif
#endif

#ifdef SIGHUP
    ksi_defsym (L"signal/hup", ksi_long2num (SIGHUP), env);
#endif
#ifdef SIGINT
    ksi_defsym (L"signal/int", ksi_long2num (SIGINT), env);
#endif
#ifdef SIGQUIT
    ksi_defsym (L"signal/quit", ksi_long2num (SIGQUIT), env);
#endif
#ifdef SIGILL
    ksi_defsym (L"signal/ill", ksi_long2num (SIGILL), env);
#endif
#ifdef SIGTRAP
    ksi_defsym (L"signal/trap", ksi_long2num (SIGTRAP), env);
#endif
#ifdef SIGABRT
    ksi_defsym (L"signal/abrt", ksi_long2num (SIGABRT), env);
#endif
#ifdef SIGIOT
    ksi_defsym (L"signal/iot", ksi_long2num (SIGIOT), env);
#endif
#ifdef SIGBUS
    ksi_defsym (L"signal/bus", ksi_long2num (SIGBUS), env);
#endif
#ifdef SIGFPE
    ksi_defsym (L"signal/fpe", ksi_long2num (SIGFPE), env);
#endif
#ifdef SIGKILL
    ksi_defsym (L"signal/kill", ksi_long2num (SIGKILL), env);
#endif
#ifdef SIGSEGV
    ksi_defsym (L"signal/segv", ksi_long2num (SIGSEGV), env);
#endif
#ifdef SIGUSR1
    ksi_defsym (L"signal/usr1", ksi_long2num (SIGUSR1), env);
#endif
#ifdef SIGUSR2
    ksi_defsym (L"signal/usr2", ksi_long2num (SIGUSR2), env);
#endif
#ifdef SIGPIPE
    ksi_defsym (L"signal/pipe", ksi_long2num (SIGPIPE), env);
#endif
#ifdef SIGALRM
    ksi_defsym (L"signal/alrm", ksi_long2num (SIGALRM), env);
#endif
#ifdef SIGTERM
    ksi_defsym (L"signal/term", ksi_long2num (SIGTERM), env);
#endif
#ifdef SIGSTKFLT
    ksi_defsym (L"signal/stkflt", ksi_long2num (SIGSTKFLT), env);
#endif
#ifdef SIGCHLD
    ksi_defsym (L"signal/chld", ksi_long2num (SIGCHLD), env);
#endif
#ifdef SIGCONT
    ksi_defsym (L"signal/cont", ksi_long2num (SIGCONT), env);
#endif
#ifdef SIGSTOP
    ksi_defsym (L"signal/stop", ksi_long2num (SIGSTOP), env);
#endif
#ifdef SIGTSTP
    ksi_defsym (L"signal/tstp", ksi_long2num (SIGTSTP), env);
#endif
#ifdef SIGTTIN
    ksi_defsym (L"signal/ttin", ksi_long2num (SIGTTIN), env);
#endif
#ifdef SIGTTOU
    ksi_defsym (L"signal/ttou", ksi_long2num (SIGTTOU), env);
#endif
#ifdef SIGURG
    ksi_defsym (L"signal/urg", ksi_long2num (SIGURG), env);
#endif
#ifdef SIGXCPU
    ksi_defsym (L"signal/xcpu", ksi_long2num (SIGXCPU), env);
#endif
#ifdef SIGXFSZ
    ksi_defsym (L"signal/xfsz", ksi_long2num (SIGXFSZ), env);
#endif
#ifdef SIGVTALRM
    ksi_defsym (L"signal/vtalrm", ksi_long2num (SIGVTALRM), env);
#endif
#ifdef SIGPROF
    ksi_defsym (L"signal/prof", ksi_long2num (SIGPROF), env);
#endif
#ifdef SIGWINCH
    ksi_defsym (L"signal/winch", ksi_long2num (SIGWINCH), env);
#endif
#ifdef SIGIO
    ksi_defsym (L"signal/io", ksi_long2num (SIGIO), env);
#endif
#ifdef SIGPOLL
    ksi_defsym (L"signal/poll", ksi_long2num (SIGPOLL), env);
#endif
#ifdef SIGPWR
    ksi_defsym (L"signal/pwr", ksi_long2num (SIGPWR), env);
#endif

    mgr = (struct Def_Event_Mgr*) ksi_malloc (sizeof *mgr);
    mgr->ops.init                = def_init;
    mgr->ops.term                = def_term;
    mgr->ops.wait_timer          = def_wait_timer;
    mgr->ops.cancel_timer        = def_cancel_timer;
    mgr->ops.wait_input          = def_wait_input;
    mgr->ops.cancel_input        = def_cancel_input;
    mgr->ops.wait_output         = def_wait_output;
    mgr->ops.cancel_output       = def_cancel_output;
    mgr->ops.wait_signal         = def_wait_signal;
    mgr->ops.cancel_signal       = def_cancel_signal;
    mgr->ops.wait_idle           = def_wait_idle;
    mgr->ops.cancel_idle         = def_cancel_idle;
    mgr->ops.wait_event          = def_wait_event;
    mgr->ops.enable_async_wait   = def_enable_async_wait;
    mgr->ops.disable_async_wait  = def_disable_async_wait;
    mgr->ops.block_wait          = def_block_wait;
    mgr->ops.unblock_wait        = def_unblock_wait;

    ksi_register_event_mgr ((ksi_event_mgr) mgr);
}

void
ksi_term_signals (void)
{
  int i;

  sigprocmask (SIG_SETMASK, &old_sig_set, 0);
  for (i = 0; i < _NSIG; i++) {
    if (sig_installed[i]) {
#ifdef HAVE_SIGACTION
      sigaction (i, &old_sig_action[i], 0);
#else
      signal (i, old_sig_handler[i]);
#endif
      sig_installed[i] = 0;
    }
  }
}
#endif

 /* End of code */
