/*
 * ksi_char.c
 * chars
 *
 * Copyright (C) 1997-2010, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Fri Jan 24 23:11:51 1997
 * Last Update:   Tue Aug 17 13:12:00 2010
 *
 */

#include "ksi_type.h"
#include "ksi_printf.h"
#include "ksi_util.h"
#include "ksi_gc.h"


static struct
{
    const wchar_t *name;
    wchar_t code;
} ksi_chars[] =
{
    { L"#\\nul", 0x0000 },
    { L"#\\alarm", 0x0007 },
    { L"#\\backspace", 0x0008 },
    { L"#\\tab", 0x0009 },
    { L"#\\linefeed", 0x000A },
    { L"#\\newline", 0x000A },
    { L"#\\vtab", 0x000B },
    { L"#\\page", 0x000C },
    { L"#\\return", 0x000D },
    { L"#\\esc", 0x001B },
    { L"#\\space", 0x0020 },
    { L"#\\delete", 0x007F }
};

#define KSI_CHARS_NUM (sizeof(ksi_chars)/sizeof(ksi_chars[0]))

ksi_obj
ksi_int2char (wint_t code)
{
    ksi_char x = 0;

    if (code < 0 || (0xd800 <= code && code <= 0xdfff) || 0x10ffff < code) {
        ksi_exn_error(0, ksi_long2num(code), "integer->char: character code out of range");
    } else {
        x = ksi_malloc(sizeof *x);
        x->o.itag = KSI_TAG_CHAR;
        x->o.ilen = code;
    }
    return (ksi_obj) x;
}


ksi_obj
ksi_str2char (const wchar_t *name, int len)
{
    int i, j;
    wint_t code;

    if (len > 2 && name[0] == L'#' && name[1] == L'\\') {
        name += 2;
        len -= 2;
    }

    for (i = 0; i < KSI_CHARS_NUM; ++i) {
        if (wcslen(ksi_chars[i].name) == len + 2) {
            for (j = 0; j < len; j++)
                if (ksi_chars[i].name[j+2] != name[j])
                    goto next;
            return ksi_int2char(ksi_chars[i].code);
        }
    next:;
    }

    if (name[0] == L'x') {
        for (code = i = 0; ++i < len; )
            if (L'0' <= name[i] && name[i] <= L'9')
                code = code * 16 + name[i] - L'0';
            else if (L'a' <= name[i] && name[i] <= L'f')
                code = code * 16 + (name[i] - L'a' + 10);
            else if (L'A' <= name[i] && name[i] <= L'F')
                code = code * 16 + (name[i] - L'A' + 10);
            else
                return ksi_false;

        return ksi_int2char(code);
    }

    return ksi_false;
}

const wchar_t *
ksi_char2str (ksi_char x)
{
    int i;
    wchar_t *ptr;
    wint_t code;

    code = KSI_CHAR_CODE(x);
    for (i = 0; i < KSI_CHARS_NUM; ++i)
        if (ksi_chars[i].code == code)
            return ksi_chars[i].name;

    if (iswgraph(code)) {
        ptr = ksi_malloc_data(4 * sizeof(*ptr));
        ptr[0] = L'#'; ptr[1] = L'\\'; ptr[2] = code; ptr[3] = L'\0';
    } else {
        ptr = ksi_aprintf("#\\x%04x", code);
    }

    return ptr;
}

ksi_obj
ksi_char_p (ksi_obj x)
{
    return KSI_CHAR_P(x) ? ksi_true : ksi_false;
}


#define CONCAT(a,b) a##b

#define DEF_COMP(op, test, name)                                        \
ksi_obj                                                                 \
CONCAT (ksi_char_, op) (int ac, ksi_obj* av)                            \
{                                                                       \
    wint_t c1, c2;                                                      \
    int i;                                                              \
    if (ac < 1)                                                         \
        return ksi_true;                                                \
    KSI_CHECK (av[0], KSI_CHAR_P (av[0]), name ": invalid char");       \
    c1 = KSI_CHAR_CODE(av[0]);                                          \
    for (i = 1; i < ac; i++) {                                          \
        KSI_CHECK(av[i], KSI_CHAR_P(av[i]), name ": invalid char");     \
        c2 = KSI_CHAR_CODE(av[i]);                                      \
        if (!(test))                                                    \
            return ksi_false;                                           \
        c1 = c2;                                                        \
    }                                                                   \
    return ksi_true;                                                    \
}

DEF_COMP (eq_p, c1 == c2, "char=?")
DEF_COMP (lt_p, c1 < c2, "char<?")
DEF_COMP (gt_p, c1 > c2, "char>?")
DEF_COMP (le_p, c1 <= c2, "char<=?")
DEF_COMP (ge_p, c1 >= c2, "char>=?")
DEF_COMP (ci_eq_p, towlower(c1) == towlower(c2), "char-ci=?")
DEF_COMP (ci_lt_p, towlower(c1) < towlower(c2), "char-ci<?")
DEF_COMP (ci_gt_p, towlower(c1) > towlower(c2), "char-ci>?")
DEF_COMP (ci_le_p, towlower(c1) <= towlower(c2), "char-ci<=?")
DEF_COMP (ci_ge_p, towlower(c1) >= towlower(c2), "char-ci>=?")


#define DEF_TEST(op, test, name)                                \
ksi_obj                                                         \
CONCAT (ksi_char_, op) (ksi_obj o)                              \
{                                                               \
    wint_t c;                                                   \
    KSI_CHECK (o, KSI_CHAR_P (o), name ": invalid char");       \
    c = KSI_CHAR_CODE (o);                                      \
    if (test)                                                   \
        return ksi_true;                                        \
    return ksi_false;                                           \
}

DEF_TEST (digit_p, iswdigit(c), "char-numeric?")
DEF_TEST (space_p, iswspace(c), "char-whitespace?")
DEF_TEST (alpha_p, iswalpha(c), "char-alphabetic?")
DEF_TEST (upper_p, iswupper(c), "char-upper-case?")
DEF_TEST (lower_p, iswlower(c), "char-lower-case?")


ksi_obj
ksi_char2integer (ksi_obj x)
{
    KSI_CHECK(x, KSI_CHAR_P(x), "char->integer: invalid char");
    return ksi_long2num(KSI_CHAR_CODE(x));
}

ksi_obj
ksi_integer2char (ksi_obj x)
{
    unsigned i = 0;

    KSI_CHECK(x, ksi_integer_p(x) == ksi_true, "integer->char: invalid integer");
    i = ksi_num2ulong(x, "integer->char");
    return ksi_int2char(i);
}

ksi_obj
ksi_char_upcase (ksi_obj x)
{
    int c;
    KSI_CHECK(x, KSI_CHAR_P(x), "char-upcase: invalid char");
    c = KSI_CHAR_CODE(x);
    return ksi_int2char(towupper(c));
}

ksi_obj
ksi_char_downcase (ksi_obj x)
{
    int c;
    KSI_CHECK(x, KSI_CHAR_P(x), "char-downcase: invalid char");
    c = KSI_CHAR_CODE(x);
    return ksi_int2char(towlower(c));
}


 /* End of code */
