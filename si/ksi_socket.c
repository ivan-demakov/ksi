/*
 * ksi_socket.c
 * sockets
 *
 * Copyright (C) 1999-2010, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Fri May  7 18:19:06 1999
 * Last Update:   Fri Jan 22 21:39:39 2010
 *
 */

#include "ksi_int.h"

#if defined(NO_SOCKETS)

void
ksi_init_socket (void)
{
}

void
ksi_term_socket (void)
{
}

#else

#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif

#ifdef HAVE_FCNTL_H
#  include <fcntl.h>
#endif

#if defined(unix) || defined(__CYGWIN32__) || defined(__CYGWIN__)
#  include <sys/socket.h>
#  include <netinet/in.h>
#  include <arpa/inet.h>
#  include <netdb.h>
#elif defined(MSWIN32)
#  include <winsock.h>
#endif

#ifndef INVALID_SOCKET
#  define INVALID_SOCKET -1
#endif

#ifndef MSWIN32
#  define SOCKET int
#endif

#define SOCKET_P(x)	(KSI_PORT_P (x) && (((ksi_port) x) -> is_inet))


#ifdef MSWIN32

#define INIT_SOCKETS(x) win_init_sockets (x)

#ifndef WSAAPI
#  define WSAAPI FAR PASCAL
#endif

#ifdef __WATCOMC__

static struct Win_Sock
{
  int		inited;
  HINSTANCE	hlib;

  SOCKET WSAAPI (*pf_accept) (SOCKET s, struct sockaddr FAR *addr,
			      int FAR *addrlen);

  int WSAAPI (*pf_bind) (SOCKET s, const struct sockaddr FAR *addr,
			 int namelen);

  int WSAAPI (*pf_closesocket) (SOCKET s);

  int WSAAPI (*pf_connect) (SOCKET s, const struct sockaddr FAR *name,
			    int namelen);

  int WSAAPI (*pf_ioctlsocket) (SOCKET s, long cmd, u_long FAR *argp);
  
  int WSAAPI (*pf_getpeername) (SOCKET s, struct sockaddr FAR *name,
				int FAR * namelen);
  
  int WSAAPI (*pf_getsockname) (SOCKET s, struct sockaddr FAR *name,
				int FAR * namelen);

  int WSAAPI (*pf_getsockopt) (SOCKET s, int level, int optname,
			       char FAR * optval, int FAR *optlen);

  u_long WSAAPI (*pf_htonl) (u_long hostlong);

  u_short WSAAPI (*pf_htons) (u_short hostshort);

  unsigned long WSAAPI (*pf_inet_addr) (const char FAR * cp);

  char FAR * WSAAPI (*pf_inet_ntoa) (struct in_addr in);

  int WSAAPI (*pf_listen) (SOCKET s, int backlog);

  u_long WSAAPI (*pf_ntohl) (u_long netlong);

  u_short WSAAPI (*pf_ntohs) (u_short netshort);

  int WSAAPI (*pf_recv) (SOCKET s, char FAR * buf, int len, int flags);

  int WSAAPI (*pf_recvfrom) (SOCKET s, char FAR * buf, int len,
			     int flags, struct sockaddr FAR *from,
			     int FAR * fromlen);

  int WSAAPI (*pf_select) (int nfds, fd_set FAR *readfds,
			   fd_set FAR *writefds, fd_set FAR *exceptfds,
			   const struct timeval FAR *timeout);

  int WSAAPI (*pf_send) (SOCKET s, const char FAR * buf, int len,
			 int flags);

  int WSAAPI (*pf_sendto) (SOCKET s, const char FAR * buf, int len,
			   int flags, const struct sockaddr FAR *to,
			   int tolen);

  int WSAAPI (*pf_setsockopt) (SOCKET s, int level, int optname,
			       const char FAR * optval, int optlen);

  int WSAAPI (*pf_shutdown) (SOCKET s, int how);

  SOCKET WSAAPI (*pf_socket) (int af, int type, int protocol);

  struct hostent FAR* WSAAPI (*pf_gethostbyaddr) (const char FAR *addr,
						  int len, int type);

  struct hostent FAR* WSAAPI (*pf_gethostbyname) (const char FAR *name);

  int WSAAPI (*pf_gethostname) (char FAR * name, int namelen);

  struct servent FAR* WSAAPI (*pf_getservbyport) (int port,
						  const char FAR *proto);

  struct servent FAR* WSAAPI (*pf_getservbyname)(const char FAR *name,
						 const char FAR *proto);

  struct protoent FAR* WSAAPI (*pf_getprotobynumber) (int proto);

  struct protoent FAR* WSAAPI (*pf_getprotobyname)(const char FAR *name);

  int WSAAPI (*pf_WSAStartup) (WORD wVersionRequired,
			       LPWSADATA lpWSAData);

  int WSAAPI (*pf_WSACleanup) (void);

  int WSAAPI (*pf_WSAGetLastError) (void);

  int WSAAPI (*pf__WSAFDIsSet) (SOCKET, fd_set FAR *);
} wsa;

#else

static struct Win_Sock
{
  int		inited;
  HINSTANCE	hlib;

  SOCKET (WSAAPI *pf_accept) (SOCKET s, struct sockaddr FAR *addr,
			      int FAR *addrlen);

  int (WSAAPI *pf_bind) (SOCKET s, const struct sockaddr FAR *addr,
			 int namelen);

  int (WSAAPI *pf_closesocket) (SOCKET s);

  int (WSAAPI *pf_connect) (SOCKET s, const struct sockaddr FAR *name,
			    int namelen);

  int (WSAAPI *pf_ioctlsocket) (SOCKET s, long cmd, u_long FAR *argp);

  int (WSAAPI *pf_getpeername) (SOCKET s, struct sockaddr FAR *name,
				int FAR * namelen);

  int (WSAAPI *pf_getsockname) (SOCKET s, struct sockaddr FAR *name,
				int FAR * namelen);

  int (WSAAPI *pf_getsockopt) (SOCKET s, int level, int optname,
			       char FAR * optval, int FAR *optlen);

  u_long (WSAAPI *pf_htonl) (u_long hostlong);

  u_short (WSAAPI *pf_htons) (u_short hostshort);

  unsigned long (WSAAPI *pf_inet_addr) (const char FAR * cp);

  char FAR * (WSAAPI *pf_inet_ntoa) (struct in_addr in);

  int (WSAAPI *pf_listen) (SOCKET s, int backlog);

  u_long (WSAAPI *pf_ntohl) (u_long netlong);

  u_short (WSAAPI *pf_ntohs) (u_short netshort);

  int (WSAAPI *pf_recv) (SOCKET s, char FAR * buf, int len, int flags);

  int (WSAAPI *pf_recvfrom) (SOCKET s, char FAR * buf, int len,
			     int flags, struct sockaddr FAR *from,
			     int FAR * fromlen);

  int (WSAAPI *pf_select) (int nfds, fd_set FAR *readfds,
			   fd_set FAR *writefds, fd_set FAR *exceptfds,
			   const struct timeval FAR *timeout);

  int (WSAAPI *pf_send) (SOCKET s, const char FAR * buf, int len,
			 int flags);

  int (WSAAPI *pf_sendto) (SOCKET s, const char FAR * buf, int len,
			   int flags, const struct sockaddr FAR *to,
			   int tolen);

  int (WSAAPI *pf_setsockopt) (SOCKET s, int level, int optname,
			       const char FAR * optval, int optlen);

  int (WSAAPI *pf_shutdown) (SOCKET s, int how);

  SOCKET (WSAAPI *pf_socket) (int af, int type, int protocol);

  struct hostent FAR* (WSAAPI *pf_gethostbyaddr) (const char FAR *addr,
						  int len, int type);

  struct hostent FAR* (WSAAPI *pf_gethostbyname) (const char FAR *name);

  int (WSAAPI *pf_gethostname) (char FAR * name, int namelen);

  struct servent FAR* (WSAAPI *pf_getservbyport) (int port,
						  const char FAR *proto);

  struct servent FAR* (WSAAPI *pf_getservbyname)(const char FAR *name,
						 const char FAR *proto);

  struct protoent FAR* (WSAAPI *pf_getprotobynumber) (int proto);

  struct protoent FAR* (WSAAPI *pf_getprotobyname)(const char FAR *name);

  int (WSAAPI *pf_WSAStartup) (WORD wVersionRequired,
			       LPWSADATA lpWSAData);

  int (WSAAPI *pf_WSACleanup) (void);

  int (WSAAPI *pf_WSAGetLastError) (void);

  int (WSAAPI *pf__WSAFDIsSet) (SOCKET, fd_set FAR *);
} wsa;

#endif


#define accept			wsa.pf_accept
#define bind			wsa.pf_bind
#define closesocket		wsa.pf_closesocket
#define connect			wsa.pf_connect
#define getpeername		wsa.pf_getpeername
#define getsockname		wsa.pf_getsockname
#define getsockopt		wsa.pf_getsockopt
#define htonl			wsa.pf_htonl
#define htons			wsa.pf_htons
#define inet_addr		wsa.pf_inet_addr
#define inet_ntoa		wsa.pf_inet_ntoa
#define ioctlsocket		wsa.pf_ioctlsocket
#define listen			wsa.pf_listen
#define ntohl			wsa.pf_ntohl
#define ntohs			wsa.pf_ntohs
#define recv			wsa.pf_recv
#define recvfrom		wsa.pf_recvfrom
#define select			wsa.pf_select
#define send			wsa.pf_send
#define sendto			wsa.pf_sendto
#define setsockopt		wsa.pf_setsockopt
#define shutdown		wsa.pf_shutdown
#define socket			wsa.pf_socket
#define gethostbyaddr		wsa.pf_gethostbyaddr
#define gethostbyname		wsa.pf_gethostbyname
#define getprotobyname		wsa.pf_getprotobyname
#define getprotobynumber	wsa.pf_getprotobynumber
#define getservbyname		wsa.pf_getservbyname
#define getservbyport		wsa.pf_getservbyport
#define gethostname		wsa.pf_gethostname

#define WSAGetLastError		wsa.pf_WSAGetLastError
#define WSAStartup		wsa.pf_WSAStartup
#define WSACleanup		wsa.pf_WSACleanup
#define __WSAFDIsSet		wsa.pf__WSAFDIsSet


static char*
sock_errstr (void)
{
  int err = WSAGetLastError ();
  return ksi_aprintf ("error code %d", err);
}


static void
winsock_init (char *name, char *proc)
{
  wsa.hlib = LoadLibrary (name);
  if (wsa.hlib == NULL)
    ksi_exn_error ("system", ksi_str02string (name),
		   "%s: winsock dll not found", proc);

  accept		= GetProcAddress (wsa.hlib, (LPCSTR) 1);
  bind			= GetProcAddress (wsa.hlib, (LPCSTR) 2);
  closesocket		= GetProcAddress (wsa.hlib, (LPCSTR) 3);
  connect		= GetProcAddress (wsa.hlib, (LPCSTR) 4);
  getpeername		= GetProcAddress (wsa.hlib, (LPCSTR) 5);
  getsockname		= GetProcAddress (wsa.hlib, (LPCSTR) 6);
  getsockopt		= GetProcAddress (wsa.hlib, (LPCSTR) 7);
  htonl			= GetProcAddress (wsa.hlib, (LPCSTR) 8);
  htons			= GetProcAddress (wsa.hlib, (LPCSTR) 9);
  inet_addr		= GetProcAddress (wsa.hlib, (LPCSTR) 10);
  inet_ntoa		= GetProcAddress (wsa.hlib, (LPCSTR) 11);
  ioctlsocket		= GetProcAddress (wsa.hlib, (LPCSTR) 12);
  listen		= GetProcAddress (wsa.hlib, (LPCSTR) 13);
  ntohl			= GetProcAddress (wsa.hlib, (LPCSTR) 14);
  ntohs			= GetProcAddress (wsa.hlib, (LPCSTR) 15);
  recv			= GetProcAddress (wsa.hlib, (LPCSTR) 16);
  recvfrom		= GetProcAddress (wsa.hlib, (LPCSTR) 17);
  select		= GetProcAddress (wsa.hlib, (LPCSTR) 18);
  send			= GetProcAddress (wsa.hlib, (LPCSTR) 19);
  sendto		= GetProcAddress (wsa.hlib, (LPCSTR) 20);
  setsockopt		= GetProcAddress (wsa.hlib, (LPCSTR) 21);
  shutdown		= GetProcAddress (wsa.hlib, (LPCSTR) 22);
  socket		= GetProcAddress (wsa.hlib, (LPCSTR) 23);
  gethostbyaddr		= GetProcAddress (wsa.hlib, (LPCSTR) 51);
  gethostbyname		= GetProcAddress (wsa.hlib, (LPCSTR) 52);
  getprotobyname	= GetProcAddress (wsa.hlib, (LPCSTR) 53);
  getprotobynumber	= GetProcAddress (wsa.hlib, (LPCSTR) 54);
  getservbyname		= GetProcAddress (wsa.hlib, (LPCSTR) 55);
  getservbyport		= GetProcAddress (wsa.hlib, (LPCSTR) 56);
  gethostname		= GetProcAddress (wsa.hlib, (LPCSTR) 5);

  WSAGetLastError	= GetProcAddress (wsa.hlib, (LPCSTR) 111);
  WSAStartup		= GetProcAddress (wsa.hlib, (LPCSTR) 115);
  WSACleanup		= GetProcAddress (wsa.hlib, (LPCSTR) 116);
  __WSAFDIsSet		= GetProcAddress (wsa.hlib, (LPCSTR) 151);

  wsa.inited = 1;
}


static void
win_init_sockets (char *proc)
{
  WSADATA	data;
  int		rc, opt;
  char		*str;

  if (wsa.inited)
    return;

  winsock_init ("wsock32.dll", proc);

  rc = WSAStartup (MAKEWORD (1,1), &data);
  if (rc != 0)
    {
      str = sock_errstr ();
      ksi_term_socket ();
      ksi_exn_error ("system", 0, "%s: initialization failed (%s)", proc, str);
    }

  opt = SO_OPENTYPE;
  rc = setsockopt (INVALID_SOCKET, SOL_SOCKET, SO_OPENTYPE,
		   (char*) &opt, sizeof opt);
  if (rc != 0)
    {
      str = sock_errstr ();
      ksi_term_socket ();
      ksi_exn_error ("system", 0,
		     "%s: synchronous socket option setting failed (%s)",
		     proc, str);
    }
}

static inline void
sys_err (char *proc, char *msg)
{
  ksi_exn_error ("system", 0, "%s: %s (%s)", proc, msg, sock_errstr ());
}

static inline void
sock_err (char *proc, struct Ksi_FdPort *x)
{
  ksi_exn_error ("i/o", (ksi_obj) x, "%s: %s", proc, sock_errstr ());
}

static const char*
socket_error (struct Ksi_FdPort *sock)
{
  return sock_errstr ();
}

static int
socket_r_ready (struct Ksi_FdPort *sock, int wait)
{
  struct timeval tm;
  fd_set set;
  int res;

  FD_ZERO (&set);
  FD_SET ((SOCKET) sock->fd, &set);

  tm.tv_sec  = 0;
  tm.tv_usec = 0;

  res = select (FD_SETSIZE, &set, 0, 0, wait ? 0 : &tm);
  return res; /*FD_ISSET (port->fd, &set);*/
}

static int
socket_w_ready (struct Ksi_FdPort *sock, int wait)
{
  struct timeval tm;
  fd_set set;
  int res;

  FD_ZERO (&set);
  FD_SET ((SOCKET) sock->fd, &set);

  tm.tv_sec  = 0;
  tm.tv_usec = 0;

  res = select (FD_SETSIZE, 0, &set, 0, wait ? 0 : &tm);
  return res; /*FD_ISSET (port->fd, &set);*/
}

static int
socket_close (struct Ksi_FdPort* sock)
{
  return closesocket (sock->fd);
}

static int
socket_read (struct Ksi_FdPort* sock, char *buf, int len)
{
  return recv (sock->fd, buf, len, 0);
}

static int
socket_write (struct Ksi_FdPort* sock, const char *buf, int len)
{
  return send (sock->fd, buf, len, 0);
}

static int
socket_set_async (struct Ksi_FdPort* sock, int async)
{
  u_long arg = async;
  return ioctlsocket (sock->fd, FIONBIO, &arg);
}

#else

#define INIT_SOCKETS(x)

static inline void
sys_err (char *proc, char *msg)
{
  ksi_exn_error ("system", 0, "%s: %s (%s)", proc, msg, strerror (errno));
}

static inline void
sock_err (char *proc, struct Ksi_FdPort *x)
{
  ksi_exn_error ("i/o", (ksi_obj) x, "%s: %m", proc);
}

#endif


static ksi_obj
ksi_new_socket_port (char *hostname, int portnum, SOCKET fd)
{
  ksi_port port;
  char *name;

  if (hostname)
    name = ksi_aprintf ("socket %s:%d", hostname, portnum);
  else
    name = ksi_aprintf ("socket listen %d", portnum);

  port = ksi_new_fd_port (fd, name, 1);
  port->input   = 1;
  port->output  = 1;
  port->is_sock = 1;
  port->is_inet = 1;

#ifdef MSWIN32
  ((struct Ksi_FdPort*) port) -> error     = socket_error;
  ((struct Ksi_FdPort*) port) -> close     = socket_close;
  ((struct Ksi_FdPort*) port) -> r_ready   = socket_r_ready;
  ((struct Ksi_FdPort*) port) -> w_ready   = socket_w_ready;
  ((struct Ksi_FdPort*) port) -> read      = socket_read;
  ((struct Ksi_FdPort*) port) -> write     = socket_write;
  ((struct Ksi_FdPort*) port) -> set_async = socket_set_async;
#endif

  return (ksi_obj) port;
}

static ksi_obj
ksi_open_client_socket (ksi_obj host, ksi_obj port, ksi_obj async)
{
  SOCKET sock;
  int portnum;
  char *hostname;
  struct sockaddr_in server;
  struct hostent *hostinfo;

  INIT_SOCKETS ("open-client-socket");

  TYPE_CHECK (host, KSI_STR_P (host),
	      "open-client-socket: invalid string in arg1");
  TYPE_CHECK (port, KSI_SINT_P (port),
	      "open-client-socket: invalid integer in arg2");

  hostname = KSI_STR_PTR (host);
  portnum = ksi_num2int (port);

  hostinfo = gethostbyname (hostname);
  if (!hostinfo)
    ksi_exn_error ("system", host, "open-client-socket: unknown host");

  /* Create the socket. */
  sock = socket (PF_INET, SOCK_STREAM, 0);
  if (sock == INVALID_SOCKET)
    sys_err ("open-client-socket", "cannot create socket");

  if (async && async != ksi_false)
    {
#if defined(MSWIN32)
      u_long arg = 1;
      if (ioctlsocket (sock, FIONBIO, &arg) != 0)
	sys_err ("open-client-socket", "cannot set nonblocking mode");
#elif defined(O_NONBLOCK)
      int flg = fcntl (sock, F_GETFL, 0);
      if (flg == -1)
	sys_err ("open-clinet-socket", "cannot set nonblocking mode");
      if (fcntl (sock, F_SETFL, flg | O_NONBLOCK) == -1)
	sys_err ("open-clinet-socket", "cannot set nonblocking mode");
#else
      sys_err  ("open-clinet-socket", "nonblocking mode not supported");
#endif
    }

  /* Connect to the server. */
  bzero (&server, sizeof server);
  server.sin_family = AF_INET;
  server.sin_port   = htons (portnum);
  server.sin_addr   = * ((struct in_addr*) hostinfo->h_addr);

#ifdef MSWIN32
  if (connect (sock, (struct sockaddr*) &server, sizeof server) == SOCKET_ERROR)
    {
      if (WSAGetLastError () != WSAEWOULDBLOCK)
	{
	  char *err = sock_errstr ();
	  closesocket (sock);
	  ksi_exn_error ("system", ksi_void,
			 "open-client-socket: %s: %s:%d",
			 err, hostname, portnum);
	}
    }
#else
  if (connect (sock, (struct sockaddr*) &server, sizeof server) < 0)
    {
      if (errno != EINPROGRESS)
	{
	  char *err = strerror (errno);
	  close (sock);
	  ksi_exn_error ("system", ksi_void,
			 "open-client-socket: %s: %s:%d",
			 err, hostname, portnum);
	}
    }
#endif

  return ksi_new_socket_port (hostname, portnum, sock);
}

static ksi_obj
ksi_open_server_socket (ksi_obj port, ksi_obj qlen)
{
  SOCKET sock;
  int portnum, len;
  struct sockaddr_in server;

  INIT_SOCKETS ("open-server-socket");

  if (!qlen)
    qlen = ksi_five;

  TYPE_CHECK (port, KSI_SINT_P (port),
	      "open-server-socket: invalid integer in arg1");
  TYPE_CHECK (port, KSI_SINT_P (qlen),
	      "open-server-socket: invalid integer in arg2");

  portnum = ksi_num2int (port);

  /* Create the socket. */
  sock = socket (PF_INET, SOCK_STREAM, 0);
  if (sock == INVALID_SOCKET)
    sys_err ("open-server-socket", "cannot create socket");

#if defined(SO_REUSEADDR) && !defined(MSWIN32)
  len = 1;
  setsockopt (sock, SOL_SOCKET, SO_REUSEADDR, &len, sizeof len);
#endif

  /* Bind the socket to a name */
  bzero (&server, sizeof server);
  server.sin_family = AF_INET;
  server.sin_port = htons (portnum);
  server.sin_addr.s_addr = htonl (INADDR_ANY);

  if (bind (sock, (struct sockaddr*) &server, sizeof server) < 0)
    {
#ifdef MSWIN32
      char *err = sock_errstr ();
      closesocket (sock);
#else
      char *err = strerror (errno);
      close (sock);
#endif
      ksi_exn_error ("system", port, "open-server-socket: %s", err);
    }

  len = sizeof server;
  if (getsockname (sock, (struct sockaddr*) &server, &len) < 0)
    {
#ifdef MSWIN32
      char *err = sock_errstr ();
      closesocket (sock);
#else
      char *err = strerror (errno);
      close (sock);
#endif
      ksi_exn_error ("system", port, "open-server-socket: %s", err);
    }

  portnum = ntohs (server.sin_port);
  len = ksi_num2int (qlen);

#ifdef MSWIN32
  if (listen (sock, len) == SOCKET_ERROR)
    {
      char *err = sock_errstr ();
      closesocket (sock);
      ksi_exn_error ("system", port, "open-server-socket: %s", err);
    }
#else
  if (listen (sock, len) < 0)
    {
      char *err = strerror (errno);
      close (sock);
      ksi_exn_error ("system", port, "open-server-socket: %s", err);
    }
#endif

  return ksi_new_socket_port (0, portnum, sock);
}

static ksi_obj
ksi_accept_connection (ksi_obj x)
{
  struct Ksi_FdPort *port = (struct Ksi_FdPort*) x;
  struct sockaddr_in client;
  int fd, len;

  TYPE_CHECK (x, SOCKET_P (x), "accept-connection: invalid socket in arg1");
  INIT_SOCKETS ("accept-connection");

  len = sizeof client;
  fd = accept (port->fd, (struct sockaddr *) &client, &len);
  if (fd == INVALID_SOCKET)
    sock_err ("accept-connection", port);

  len = sizeof client;
  if (getsockname (fd, (struct sockaddr*) &client, &len))
    sock_err ("accept-connection", port);

  return ksi_new_socket_port (inet_ntoa (client.sin_addr),
			      ntohs (client.sin_port),
			      fd);
}

static ksi_obj
ksi_socket_p (ksi_obj x)
{
  return (SOCKET_P (x) ? ksi_true : ksi_false);
}

static ksi_obj
ksi_socket_portnum (ksi_obj x)
{
  struct Ksi_FdPort *port = (struct Ksi_FdPort*) x;
  struct sockaddr_in addr;
  int len;

  TYPE_CHECK (x, SOCKET_P (x), "socket-port-number: invalid socket in arg1");

  len = sizeof addr;
  if (getsockname (port->fd, (struct sockaddr*) &addr, &len) < 0)
    sock_err ("socket-port-number", port);

  return ksi_int2num (ntohs (addr.sin_port));
}

static ksi_obj
ksi_socket_address (ksi_obj x)
{
  struct Ksi_FdPort *port = (struct Ksi_FdPort*) x;
  struct sockaddr_in sin;
  int len;

  TYPE_CHECK (x, SOCKET_P (x), "socket-address: invalid socket in arg1");
  INIT_SOCKETS ("socket-address");

  len = sizeof sin;
  if (getsockname (port->fd, (struct sockaddr*) &sin, &len))
    sock_err ("socket-address", port);

  return ksi_str02string (inet_ntoa (sin.sin_addr));
}

static ksi_obj
ksi_shutdown_socket (ksi_obj sock, ksi_obj how)
{
  struct Ksi_FdPort *port = (struct Ksi_FdPort*) sock;
  int mode;

  if (!how) how = ksi_two;

  TYPE_CHECK (sock, SOCKET_P (sock),
	      "shutdown-socket: invalid socket in arg1");
  TYPE_CHECK (how, KSI_SINT_P (how),
	      "shutdown-socket: invalid integer in arg2");

  mode = KSI_SINT_COD (how);
  RANGE_CHECK (how, 0 <= mode <= 2,
	       "shutdown-socket: integer out of range in arg2");

  INIT_SOCKETS ("shutdown-socket");

#ifdef MSWIN32
  if (shutdown (port->fd, mode) == SOCKET_ERROR)
    sock_err ("shutdown-socket", port);
#else
  if (shutdown (port->fd, mode) < 0)
    sock_err ("shutdown-socket", port);
#endif

  if (mode == 0 || mode == 2)
    port->r_pos = port->r_len = 0;
  if (mode == 1 || mode == 2)
    port->w_num = 0;

  return ksi_void;
}


static struct Ksi_Prim_Def defs [] =
{
  {"open-client-socket",  ksi_open_client_socket,	KSI_CALL_ARG3, 2},
  {"open-server-socket",  ksi_open_server_socket,	KSI_CALL_ARG2, 1},
  {"accept-connection",   ksi_accept_connection,	KSI_CALL_ARG1, 1},
  {"shutdown-socket",	  ksi_shutdown_socket,		KSI_CALL_ARG2, 1},

  {"socket?",		  ksi_socket_p,			KSI_CALL_ARG1, 1},
  {"socket-port-number",  ksi_socket_portnum,		KSI_CALL_ARG1, 1},
  {"socket-address",	  ksi_socket_address,		KSI_CALL_ARG1, 1},

  { 0 }
};


void
ksi_init_socket (void)
{
  ksi_ignore (rcsid);
  ksi_reg_unit (defs);

#ifdef MSWIN32
  bzero (&wsa, sizeof wsa);
#endif
}

void
ksi_term_socket (void)
{
#ifdef MSWIN32
  if (wsa.hlib)
    {
      WSACleanup ();
      FreeLibrary (wsa.hlib);
      bzero (&wsa, sizeof wsa);
    }
#endif
}

#endif /* defined(NO_SOCKETS) */

 /* End of code */
