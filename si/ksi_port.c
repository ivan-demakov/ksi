/*
 * ksi_port.c
 * ports
 *
 * Copyright (C) 1997-2011, 2014, 2015, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Fri Jan 24 21:48:27 1997
 * Last Update:   Mon May 19 22:09:14 2014
 *
 */

#include "ksi_port.h"
#include "ksi_comp.h"
#include "ksi_proc.h"
#include "ksi_env.h"
#include "ksi_klos.h"
#include "ksi_printf.h"
#include "ksi_util.h"
#include "ksi_gc.h"


#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif

#ifdef HAVE_FCNTL_H
#  include <fcntl.h>
#endif

#ifdef HAVE_SYS_STAT_H
#  include <sys/stat.h>
#endif

#if defined(HAVE_SYS_POLL_H)
#  include <sys/poll.h>
#elif defined(HAVE_SYS_SELECT_H)
#  include <sys/select.h>
#endif

#ifdef HAVE_ICONV_H
#  include <iconv.h>
#endif

#ifdef _MSC_VER
#  include <io.h>
#  define F_OK 0
#  define W_OK 2
#  define R_OK 4
#  define X_OK R_OK
#endif

#if defined(_MSC_VER)
#  define S_ISDIR(x)  (((x) & _S_IFMT) == _S_IFDIR)
#  define S_ISCHR(x)  (((x) & _S_IFMT) == _S_IFCHR)
#  define S_ISREG(x)  (((x) & _S_IFMT) == _S_IFREG)
#  define S_ISFIFO(x) (((x) & _S_IFMT) == _S_IFIFO)
#endif

#ifndef O_ACCMODE
#  define O_ACCMODE (O_RDONLY | O_WRONLY | O_RDWR)
#endif

#ifndef EWOULDBLOCK
#  ifdef EAGAIN
#    define EWOULDBLOCK EAGAIN
#  endif
#endif


/* ---------- null port ---------------------------------------------- */

static int
nul_close (ksi_char_port x)
{
    return 0;
}

static int
nul_flush (ksi_char_port x)
{
    return 0;
}

static int
nul_read (ksi_char_port x, wchar_t *buf, int max_len)
{
    return 0;
}

static int
nul_lookahead (ksi_char_port x)
{
    return -1;
}

static int
nul_write (ksi_char_port x, const wchar_t *buf, int len)
{
    return len;
}

static int
nul_getpos (ksi_char_port x, long *pos)
{
    *pos = 0;
    return 0;
}

static int
nul_setpos (ksi_char_port x, long pos)
{
    return 0;
}


static struct Ksi_Char_Port_Ops nul_port_ops =
{
    nul_close,
    nul_flush,
    nul_read,
    nul_lookahead,
    nul_write,
    nul_getpos,
    nul_setpos
};

ksi_char_port
ksi_new_null_port (void)
{
    struct Ksi_Char_Port *x = ksi_malloc_data(sizeof *x);
    memset(x, 0, sizeof(*x));

    x->kp.o.itag = KSI_TAG_PORT;
    x->kp.port_id = L"null";
    x->kp.input = 1;
    x->kp.output = 1;
    x->kp.eof = 1;
    x->ops = &nul_port_ops;

    return x;
}

ksi_obj
ksi_null_port (void)
{
    return (ksi_obj) ksi_data->null_port;
}


/* ---------- bytevector port -------------------------------------------- */

struct Ksi_Bytevector_Port
{
    struct Ksi_Byte_Port p;
    ksi_obj data;
    unsigned int curpos, curlen;
};

static int
byte_close (ksi_byte_port _port)
{
    struct Ksi_Bytevector_Port *port = (struct Ksi_Bytevector_Port *) _port;
    port->p.kp.closed = 1;
    return 0;
}

static int
byte_flush (ksi_byte_port x)
{
    return 0;
}

static int
byte_read (ksi_byte_port _port, char *buf, int len)
{
    struct Ksi_Bytevector_Port *port = (struct Ksi_Bytevector_Port *) _port;
    if (port->curpos + len > KSI_BVEC_LEN(port->data)) {
        len = KSI_BVEC_LEN(port->data) - port->curpos;
    }
    if (len > 0) {
        int i;
        char *ptr;

        ptr = KSI_BVEC_PTR(port->data) + port->curpos;
        for (i = 0; i < len; i++) {
            buf[i] = ptr[i];
        }
        port->curpos += len;
        port->curlen = port->curpos;
        if (port->curlen >= KSI_BVEC_LEN(port->data))
            port->p.kp.eof = 1;

        return len;
    }

    return 0;
}

static int
byte_lookahead (ksi_byte_port _port)
{
    struct Ksi_Bytevector_Port *port = (struct Ksi_Bytevector_Port *) _port;
    if (port->curpos >= KSI_BVEC_LEN(port->data)) {
        port->p.kp.eof = 1;
        return -1;
    } else {
        unsigned char *ptr = (unsigned char *) (KSI_BVEC_PTR(port->data) + port->curpos);
        return *ptr;
    }
}

static int
byte_write (ksi_byte_port _port, const char *buf, int len)
{
    struct Ksi_Bytevector_Port *port = (struct Ksi_Bytevector_Port *) _port;
    if (port->curpos + len > KSI_BVEC_LEN(port->data)) {
        int newlen = (port->curpos + len + 63) & ~63;
        KSI_BVEC_LEN(port->data) = newlen;
        KSI_BVEC_PTR(port->data) = ksi_realloc(KSI_BVEC_PTR(port->data), newlen);
    }
    if (len > 0) {
        memcpy(KSI_BVEC_PTR(port->data) + port->curpos, buf, len);
        port->curpos += len;
        if (port->curlen < port->curpos)
            port->curlen = port->curpos;
        return len;
    }

    return 0;
}

static int
byte_getpos (ksi_byte_port _port, long *pos)
{
    struct Ksi_Bytevector_Port *port = (struct Ksi_Bytevector_Port *) _port;
    *pos = port->curpos;
    return 0;
}

static int
byte_setpos (ksi_byte_port _port, long pos)
{
    struct Ksi_Bytevector_Port *port = (struct Ksi_Bytevector_Port *) _port;
    if (pos < 0)
        return -1;

    if (port->p.kp.input) {
        if (KSI_BVEC_LEN(port->data) < pos)
            return -1;
    }

    port->curpos = pos;
    port->p.kp.eof = 0;
    return 0;
}

static struct Ksi_Byte_Port_Ops byte_port_ops =
{
    byte_close,
    byte_flush,
    byte_read,
    byte_lookahead,
    byte_write,
    byte_getpos,
    byte_setpos
};


ksi_byte_port
ksi_new_bytevector_input_port (ksi_obj vec)
{
    struct Ksi_Bytevector_Port *port;
    KSI_CHECK(vec, KSI_BVEC_P(vec), "ksi_new_bytevector_input_port(): invalid bytevector");

    port = ksi_malloc(sizeof *port);
    port->p.kp.o.itag = KSI_TAG_PORT;
    port->p.kp.port_id = L"bytevector";
    port->p.kp.binary = 1;
    port->p.kp.input = 1;
    port->p.kp.can_getpos = 1;
    port->p.kp.can_setpos = 1;
    port->p.kp.is_bytevector = 1;
    port->p.ops = &byte_port_ops;
    port->data = vec;

    return (ksi_byte_port) port;
}

ksi_obj
ksi_open_bytevector_input_port (ksi_obj vec)
{
    KSI_CHECK(vec, KSI_BVEC_P(vec), "open-bytevector-input-port: invalid bytevector in arg1");
    return (ksi_obj) ksi_new_bytevector_input_port(vec);
}

ksi_obj
ksi_open_bytevector_output_port (void)
{
    struct Ksi_Bytevector_Port *port;

    port = ksi_malloc(sizeof *port);
    port->p.kp.o.itag = KSI_TAG_PORT;
    port->p.kp.port_id = L"bytevector";
    port->p.kp.binary = 1;
    port->p.kp.output  = 1;
    port->p.kp.can_getpos = 1;
    port->p.kp.can_setpos = 1;
    port->p.kp.is_bytevector = 1;
    port->p.ops = &byte_port_ops;
    port->data = ksi_str2bytevector(0, 64);

    return (ksi_obj) port;
}

ksi_obj
ksi_extract_bytevector_port_data (ksi_obj p)
{
    ksi_obj x;
    struct Ksi_Bytevector_Port *port = (struct Ksi_Bytevector_Port *) p;

    KSI_CHECK(p, KSI_BIN_OUTPUT_PORT_P(p), "extract-bytevector-port-data: invalid bytevector output port in arg1");
    KSI_CHECK(p, ((struct Ksi_Port *) p)->is_bytevector, "extract-bytevector-port-data: invalid bytevector output port in arg1");

    x = ksi_str2bytevector(KSI_BVEC_PTR(port->data), port->curlen);
    port->curlen = 0;
    port->curpos = 0;

    return x;
}


/* ---------- string port -------------------------------------------- */

struct Ksi_String_Port
{
    struct Ksi_Char_Port p;
    ksi_obj data;
    int curpos, curlen;
    int init_file_line;
};

static int
str_close (ksi_char_port _port)
{
    struct Ksi_String_Port *port = (struct Ksi_String_Port *) _port;
    port->p.kp.closed = 1;
    return 0;
}

static int
str_flush (ksi_char_port x)
{
    return 0;
}

static int
str_read (ksi_char_port _port, wchar_t *buf, int len)
{
    struct Ksi_String_Port *port = (struct Ksi_String_Port *) _port;
    if (port->curpos + len > KSI_STR_LEN(port->data)) {
        len = KSI_STR_LEN(port->data) - port->curpos;
    }
    if (len > 0) {
        int i;
        wchar_t *ptr;

        if (port->curpos != port->curlen) {
            ptr = KSI_STR_PTR(port->data);
            port->p.file_line = port->init_file_line;
            port->p.line_pos = 0;
            for (i = 0; i < port->curpos; i++) {
                if (ptr[i] == L'\n') {
                    port->p.file_line += 1;
                    port->p.line_pos = 0;
                } else if (ptr[i] == L'\t') {
                    port->p.line_pos += 8;
                } else {
                    port->p.line_pos += 1;
                }
            }
        }

        ptr = KSI_STR_PTR(port->data) + port->curpos;
        for (i = 0; i < len; i++) {
            buf[i] = ptr[i];
            if (buf[i] == L'\n') {
                port->p.file_line += 1;
                port->p.line_pos = 0;
            } else if (buf[i] == L'\t') {
                port->p.line_pos += 8;
            } else {
                port->p.line_pos += 1;
            }
        }
        port->curpos += i;
        port->curlen = port->curpos;
        if (port->curlen >= KSI_STR_LEN(port->data))
            port->p.kp.eof = 1;

        return i;
    }

    return 0;
}

static int
str_lookahead (ksi_char_port _port)
{
    struct Ksi_String_Port *port = (struct Ksi_String_Port *) _port;
    if (port->curpos >= KSI_STR_LEN(port->data)) {
        port->p.kp.eof = 1;
        return -1;
    } else {
        wchar_t *ptr = (wchar_t *) (KSI_STR_PTR(port->data) + port->curpos);
        return *ptr;
    }
}

static int
str_write (ksi_char_port _port, const wchar_t *buf, int len)
{
    struct Ksi_String_Port *port = (struct Ksi_String_Port *) _port;
    if (port->curpos + len > KSI_STR_LEN(port->data)) {
        int newlen = (port->curpos + len + 15) & ~15;
        KSI_STR_LEN(port->data) = newlen;
        KSI_STR_PTR(port->data) = ksi_realloc(KSI_STR_PTR(port->data), newlen * sizeof(*buf));
    }
    if (len > 0) {
        wmemcpy(KSI_STR_PTR(port->data) + port->curpos, buf, len);
        port->p.last_write_char = buf[len-1];

        port->curpos += len;
        if (port->curlen < port->curpos)
            port->curlen = port->curpos;
        return len;
    }

    return 0;
}

static int
str_getpos (ksi_char_port _port, long *pos)
{
    struct Ksi_String_Port *port = (struct Ksi_String_Port *) _port;
    *pos = port->curpos;
    return 0;
}

static int
str_setpos (ksi_char_port _port, long pos)
{
    struct Ksi_String_Port *port = (struct Ksi_String_Port *) _port;
    if (pos < 0)
        return -1;

    if (port->p.kp.input) {
        if (KSI_STR_LEN(port->data) < pos)
            return -1;
    }

    port->curpos = pos;
    port->p.kp.eof = 0;
    return 0;
}

static struct Ksi_Char_Port_Ops str_port_ops =
{
    str_close,
    str_flush,
    str_read,
    str_lookahead,
    str_write,
    str_getpos,
    str_setpos
};


ksi_char_port
ksi_new_string_input_port (ksi_obj str, const char *file_name, int file_line)
{
    struct Ksi_String_Port *port;
    KSI_CHECK(str, KSI_STR_P(str), "ksi_new_string_input_port: invalid string");

    port = ksi_malloc(sizeof *port);
    port->p.kp.o.itag = KSI_TAG_PORT;
    port->p.kp.port_id = L"string";
    port->p.kp.input = 1;
    port->p.kp.can_getpos = 1;
    port->p.kp.can_setpos = 1;
    port->p.kp.is_string = 1;
    port->p.ops = &str_port_ops;
    port->p.file_name = (file_name ? ksi_strdup(file_name) : 0);
    port->p.file_line = file_line;
    port->p.line_pos = 0;
    port->data = str;
    port->init_file_line = file_line;

    return (ksi_char_port) port;
}

ksi_obj
ksi_open_string_input_port (ksi_obj str)
{
    KSI_CHECK(str, KSI_STR_P(str), "open-string-input-port: invalid string in arg1");
    return (ksi_obj) ksi_new_string_input_port(str, 0, 0);
}

ksi_obj
ksi_open_string_output_port (void)
{
    struct Ksi_String_Port *port;

    port = ksi_malloc(sizeof *port);
    port->p.kp.o.itag = KSI_TAG_PORT;
    port->p.kp.port_id = L"string";
    port->p.kp.output = 1;
    port->p.kp.can_getpos = 1;
    port->p.kp.can_setpos = 1;
    port->p.kp.is_string = 1;
    port->p.ops = &str_port_ops;
    port->data = ksi_str2string(0, 16);

    return (ksi_obj) port;
}

ksi_obj
ksi_extract_string_port_data(ksi_obj p)
{
    ksi_obj x;
    struct Ksi_String_Port *port = (struct Ksi_String_Port *) p;

    KSI_CHECK(p, KSI_TEXT_OUTPUT_PORT_P(p), "extract-string-port-data: invalid string output port in arg1");
    KSI_CHECK(p, ((struct Ksi_Port *) p)->is_string, "extract-string-port-data: invalid string output port in arg1");

    x = ksi_str2string(KSI_STR_PTR(port->data), port->curlen);
    port->curlen = 0;
    port->curpos = 0;

    return x;
}


/* ---------- file port ---------------------------------------------- */

struct Ksi_Fd_Port
{
    struct Ksi_Byte_Port p;
    int fd;                     /**< file descriptor */
    int pg_size;                /**< buffer size */
    char *buf;                  /**< buffer */
    FILE *fp;                   /**< FILE pointer  */
};

static int
fd_close (ksi_byte_port _port)
{
    struct Ksi_Fd_Port *port = (struct Ksi_Fd_Port *) _port;
    port->p.kp.closed = 1;

    ksi_unregister_finalizer(port);
    {
        int res = fclose(port->fp);
	port->fp = 0;
        if (res < 0) {
            ksi_exn_error(L"i/o-port", (ksi_obj) port, "%m");
        }
    }
    if (port->buf) {
        ksi_free(port->buf);
        port->buf = 0;
    }
    return 0;
}

static int
fd_flush (ksi_byte_port _port)
{
    struct Ksi_Fd_Port *port = (struct Ksi_Fd_Port *) _port;
    int res = fflush(port->fp);
    if (res < 0) {
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "%m");
    }
    return res;
}

static int
fd_read (ksi_byte_port _port, char *buf, int len)
{
    struct Ksi_Fd_Port *port = (struct Ksi_Fd_Port *) _port;
    int res = (int) fread(buf, 1, len, port->fp);
    if (feof(port->fp)) {
        port->p.kp.eof = 1;
    } else if (res <= 0) {
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "%m");
    }
    return res;
}

static int
fd_lookahead (ksi_byte_port _port)
{
    struct Ksi_Fd_Port *port = (struct Ksi_Fd_Port *) _port;
    int res = getc(port->fp);
    if (res < 0) {
        if (feof(port->fp)) {
            port->p.kp.eof = 1;
            return -1;
        } else {
            ksi_exn_error(L"i/o-port", (ksi_obj) port, "%m");
        }
    }
    ungetc(res, port->fp);
    return res;
}

static int
fd_write (ksi_byte_port _port, const char *buf, int len)
{
    struct Ksi_Fd_Port *port = (struct Ksi_Fd_Port *) _port;
    int res =  (int) fwrite(buf, 1, len, port->fp);
    if (res <= 0) {
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "%m");
    }
    return res;
}

static int
fd_getpos (ksi_byte_port _port, long *pos)
{
    struct Ksi_Fd_Port *port = (struct Ksi_Fd_Port *) _port;
    long res = ftell(port->fp);
    if (res < 0) {
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "%m");
    }
    *pos = res;
    return 0;
}

static int
fd_setpos (ksi_byte_port _port, long pos)
{
    struct Ksi_Fd_Port *port = (struct Ksi_Fd_Port *) _port;
    long res = fseek(port->fp, pos, SEEK_SET);
    if (res < 0) {
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "%m");
    }

    port->p.kp.eof = 0;
    return res;
}

static struct Ksi_Byte_Port_Ops fd_port_ops =
{
    fd_close,
    fd_flush,
    fd_read,
    fd_lookahead,
    fd_write,
    fd_getpos,
    fd_setpos
};

static void
file_finalizer (void *obj, void *data)
{
    struct Ksi_Fd_Port *port = (struct Ksi_Fd_Port *) obj;
    if (port->fp) {
	fclose(port->fp);
	port->fp = 0;
    }
}

static ksi_byte_port
ksi_new_fd_port (int fd, const char *file_name, int in, int out, ksi_obj bufmode)
{
    struct Ksi_Fd_Port *port;
    char *mode;

    port = ksi_malloc(sizeof *port);
    port->p.kp.o.itag = KSI_TAG_PORT;
    port->p.kp.binary = 1;
    port->p.kp.is_fd = 1;
    port->p.ops = &fd_port_ops;
    port->fd = fd;

    if (in)
        port->p.kp.input = 1;
    if (out)
        port->p.kp.output = 1;

    port->pg_size = BUFSIZ;

    if (isatty(fd)) {
        port->p.kp.port_id = L"tty";
        if (!bufmode)
            bufmode = ksi_data->sym_line;
    } else {
        struct stat st;
        fstat(fd, &st);

#ifdef S_ISCHR
        if (S_ISCHR(st.st_mode)) {
            if (file_name)
                port->p.kp.port_id = ksi_aprintf("char-device %s", file_name);
            else
                port->p.kp.port_id = L"char-device";
        }
#endif
#ifdef S_ISBLK
        if (S_ISBLK(st.st_mode)) {
            if (file_name)
                port->p.kp.port_id = ksi_aprintf("block-device %s", file_name);
            else
                port->p.kp.port_id = L"block-device";
        }
#endif
#ifdef S_ISFIFO
        if (S_ISFIFO(st.st_mode)) {
            if (file_name)
                port->p.kp.port_id = ksi_aprintf("fifo %s", file_name);
            else
                port->p.kp.port_id = L"fifo";
        }
#endif
#ifdef S_ISLNK
        if (S_ISLNK(st.st_mode)) {
            if (file_name)
                port->p.kp.port_id = ksi_aprintf("link %s", file_name);
            else
                port->p.kp.port_id = L"link";
        }
#endif
#ifdef S_ISSOCK
        if (S_ISSOCK(st.st_mode)) {
            if (file_name)
                port->p.kp.port_id = ksi_aprintf("socket %s", file_name);
            else
                port->p.kp.port_id = L"socket";
        }
#endif
#ifdef S_ISREG
        if (S_ISREG(st.st_mode)) {
#if defined(F_GETFL)
            int fd_flags = fcntl(fd, F_GETFL, 0);
            if ((fd_flags & O_APPEND) == 0) {
                port->p.kp.can_getpos = 1;
                port->p.kp.can_setpos = 1;
            } else {
                out = 2;
            }
#else
            if (out != 2) {
                port->p.kp.can_getpos = 1;
                port->p.kp.can_setpos = 1;
            }
#endif
#ifdef HAVE_STRUCT_STAT_ST_BLKSIZE
            port->pg_size = st.st_blksize;
#endif
            if (file_name)
                port->p.kp.port_id = ksi_aprintf("file %s", file_name);
            else
                port->p.kp.port_id = L"file";
        }
#endif
    }

    if (out == 2)
        mode = (in ? "a+b" : "ab");
    else if (out)
        mode = (in ? "w+b" : "wb");
    else
        mode = "rb";

    port->fp = fdopen(port->fd, mode);
    if (bufmode == ksi_data->sym_none) {
        port->p.kp.unbuf = 1;
        setvbuf(port->fp, 0, _IONBF, 0);
    } else {
        port->buf = ksi_malloc_data(port->pg_size);
        if (bufmode == ksi_data->sym_line) {
            port->p.kp.linebuf = 1;
            setvbuf(port->fp, port->buf, _IOLBF, port->pg_size);
        } else {
            setvbuf(port->fp, port->buf, _IOFBF, port->pg_size);
        }
    }

    ksi_register_finalizer(port, file_finalizer, 0);
    return (ksi_byte_port) port;
}

ksi_byte_port
ksi_new_fd_input_port (int fd, const char *file_name)
{
    return ksi_new_fd_port(fd, file_name, 1, 0, 0);
}

ksi_byte_port
ksi_new_fd_output_port (int fd, const char *file_name)
{
    return ksi_new_fd_port(fd, file_name, 0, 1, 0);
}

ksi_byte_port
ksi_new_fd_inout_port (int fd, const char *file_name)
{
    return ksi_new_fd_port(fd, file_name, 1, 1, 0);
}

ksi_byte_port
ksi_new_fd_append_port (int fd, const char *file_name)
{
    return ksi_new_fd_port(fd, file_name, 0, 2, 0);
}

ksi_obj
ksi_open_file_input_port (ksi_obj filename, ksi_obj mode, ksi_obj bufmode)
{
    const char *fname = ksi_mk_filename(filename, "open-file-input-port");
    int flags = O_RDONLY, fd;
    ksi_byte_port port;

#if 0
    while (KSI_PAIR_P(mode)) {
        mode = KSI_CDR(mode);
    }
#endif

    fd = open(fname, flags);
    if (fd < 0) {
        ksi_exn_error(0, filename, "open-file-input-port: cannot open file: %m");
    }

    port = ksi_new_fd_port(fd, fname, 1, 0, bufmode);
    return (ksi_obj) port;
}

ksi_obj
ksi_open_file_output_port (ksi_obj filename, ksi_obj mode, ksi_obj bufmode)
{
    const char *fname = ksi_mk_filename(filename, "open-file-input-port");
    int flags = O_WRONLY, fd;
    int no_create = 0, no_fail = 0, no_trunc = 0;
    ksi_byte_port port;

    while (KSI_PAIR_P(mode)) {
        if (KSI_CAR(mode) == ksi_data->sym_no_create) {
            no_create = 1;
        } else if (KSI_CAR(mode) == ksi_data->sym_no_fail) {
            no_fail = 1;
        } else if (KSI_CAR(mode) == ksi_data->sym_no_truncate) {
            no_trunc = 1;
        }
        mode = KSI_CDR(mode);
    }

    if (!no_create) {
        flags |= O_CREAT;
        if (!no_fail) {
            flags |= O_EXCL;
        }
    }
    if (!no_trunc)
        flags |= O_TRUNC;

    fd = open(fname, flags, 0666);
    if (fd < 0) {
        ksi_exn_error(0, filename, "open-file-output-port: cannot open file: %m");
    }

    port = ksi_new_fd_port(fd, fname, 0, 1, bufmode);
    return (ksi_obj) port;
}

ksi_obj
ksi_open_file_inout_port (ksi_obj filename, ksi_obj mode, ksi_obj bufmode)
{
    const char *fname = ksi_mk_filename(filename, "open-file-input/output-port");
    int flags = O_RDWR, fd;
    int no_create = 0, no_fail = 0, no_trunc = 0;
    ksi_byte_port port;

    while (KSI_PAIR_P(mode)) {
        if (KSI_CAR(mode) == ksi_data->sym_no_create) {
            no_create = 1;
        } else if (KSI_CAR(mode) == ksi_data->sym_no_fail) {
            no_fail = 1;
        } else if (KSI_CAR(mode) == ksi_data->sym_no_truncate) {
            no_trunc = 1;
        }
        mode = KSI_CDR(mode);
    }

    if (!no_create) {
        flags |= O_CREAT;
        if (!no_fail) {
            flags |= O_EXCL;
        }
    }
    if (!no_trunc)
        flags |= O_TRUNC;

    fd = open(fname, flags, 0666);
    if (fd < 0) {
        ksi_exn_error(0, filename, "open-file-input/output-port: cannot open file: %m");
    }

    port = ksi_new_fd_port(fd, fname, 1, 1, bufmode);
    return (ksi_obj) port;
}


/* ---------- transcoded port ---------------------------------------------- */

static unsigned short sys_endianess = 0xfffe;

struct Ksi_Tr_Port
{
    struct Ksi_Char_Port p;
    ksi_byte_port port;
    ksi_obj codec, eol, handling;
    wint_t unget, unget2;
    unsigned short endianess;
#ifdef HAVE_ICONV_H
    iconv_t in_ic;
    iconv_t out_ic;
#endif
};

static int
tr_read (ksi_char_port _port, wchar_t *buf, int len);

static int
tr_close (ksi_char_port _port)
{
    struct Ksi_Tr_Port *port = (struct Ksi_Tr_Port *) _port;
    port->p.kp.closed = 1;

    ksi_unregister_finalizer(port);

#ifdef HAVE_ICONV_H
    if (port->in_ic != (iconv_t) -1) {
	iconv_close(port->in_ic);
	port->in_ic = (iconv_t) -1;
    }
    if (port->out_ic != (iconv_t) -1) {
	iconv_close(port->out_ic);
	port->out_ic = (iconv_t) -1;
    }
#endif
    return port->port->ops->close(port->port);
}

static int
tr_flush (ksi_char_port _port)
{
    struct Ksi_Tr_Port *port = (struct Ksi_Tr_Port *) _port;
    return port->port->ops->flush(port->port);
}

static int
tr_nl (struct Ksi_Tr_Port *port, wchar_t *buf, int len)
{
    int pos = 0, num = 0;
    while (pos < len) {
	if (port->eol == ksi_data->sym_none) {
	    ++pos;
	} else {
	    if (port->unget == L'\r') {
		if (buf[pos] == L'\n' || buf[pos] == 0x85) {
		    port->unget = WEOF;
		    ++pos;
		    buf[num] = L'\n';
		} else {
		    port->unget = buf[pos++];
		    buf[num] = L'\r';
		}
	    } else if (port->unget == L'\n' || port->unget == 0x85) {
		if (buf[pos] == L'\r') {
		    port->unget = WEOF;
		    ++pos;
		    buf[num] = L'\n';
		} else {
		    port->unget = buf[pos++];
		    buf[num] = L'\n';
		}
	    } else if (port->unget == 0x2028) {
		port->unget = buf[pos++];
		buf[num] = L'\n';
	    } else if (port->unget != WEOF) {
		/* BEWARE: pos can be equal num */
		wchar_t tmp = port->unget;
		port->unget = buf[pos++];
		buf[num] = tmp;
	    } else {
		if (buf[pos] == L'\n' || buf[pos] == 0x85 || buf[pos] == 0x2028) {
		    buf[num] = L'\n';
		    ++pos;
		} else if (buf[pos] == L'\r') {
		    port->unget = buf[pos++];
		    continue;
		} else {
		    buf[num] = buf[pos++];
		}
	    }
	}

	if (buf[num] == L'\n') {
	    port->p.file_line += 1;
	    port->p.line_pos = 0;
	} else if (buf[num] == L'\t') {
	    port->p.line_pos = (port->p.line_pos & ~7) + 8;
	} else {
	    port->p.line_pos += 1;
	}
	++num;
    }

    /* special case: function should ne called with space for the last char */
    if (port->p.kp.eof && port->unget != WEOF) {
	if (port->eol == ksi_data->sym_none) {
	    buf[num] = port->unget;
	} else {
	    if (port->unget == L'\r' || port->unget == L'\n' || port->unget == 0x85 || port->unget == 0x2028) {
		buf[num] = L'\n';
	    } else {
		buf[num] = port->unget;
	    }
	}
	port->unget = WEOF;
    }

    return num;
}

static int
tr_lookahead (ksi_char_port port)
{
    struct Ksi_Tr_Port *tr = (struct Ksi_Tr_Port *) port;
    wchar_t ch[1];

    if (tr->unget != WEOF) {
        ch[0] = tr->unget;
    } else {
        if (tr_read(port, ch, 1) > 0) {
            tr->unget = ch[0];
        } else {
	    ch[0] = tr->unget;
	}
    }

    if (tr->eol != ksi_data->sym_none) {
        if (ch[0] == 0x2028 || ch[0] == 0x85 || ch[0] == L'\r') {
            ch[0] = L'\n';
        }
    }

    return ch[0];
}

static int
tr_read (ksi_char_port _port, wchar_t *buf, int len)
{
    struct Ksi_Tr_Port *port = (struct Ksi_Tr_Port *) _port;
    int i, n, u, num = 0, fill;
    char tmp[MB_LEN_MAX];

    if (!buf || len <= 0)
	return 0;

    if (port->unget != WEOF) {
	buf[0] = port->unget;
	port->unget = port->unget2;
	port->unget2 = WEOF;
	num = tr_nl(port, buf, 1);
    }

    if (port->codec == ksi_data->sym_utf8) {
        while (num < len) {
	    if (port->unget != WEOF) {
		buf[num] = port->unget;
		port->unget = WEOF;
		num = tr_nl(port, buf+num, 1);
	    } else {
		n = port->port->ops->read(port->port, tmp, 1);
		if (n <= 0) {
		    port->p.kp.eof = 1;
		    num += tr_nl(port, buf+num, 0);
		    break;
		}

		fill = 1;
		if (tmp[0] < 0x80) {
		    u = (unsigned char) tmp[0];
		} else if (tmp[0] < 0xc2) {
		invalid_seq:
		    if (port->handling == ksi_data->sym_raise) {
			ksi_buffer_t bf;
		    raise:
			bf = ksi_new_buffer(0, 0);
			for (i = 0; i < fill; i++) {
			    if (i > 0)
				ksi_buffer_put(bf, L' ');
			    ksi_bufprintf(bf, "0x%02x", tmp[i]);
			}
			ksi_buffer_put(bf, L'\0');

			ksi_exn_error(L"i/o-decoding", (ksi_obj) port,
				      "byte sequence (%ls) cannot be decoded by codec %ls",
				      ksi_buffer_data(bf), KSI_SYM_PTR(port->codec));
		    }
		    if (port->handling == ksi_data->sym_replace)
			u = 0xfffd;
		    else
			u = -1;
		} else if (tmp[0] < 0xe0) {
		    n = port->port->ops->read(port->port, tmp+1, 1);
		    fill = 2;
		    if (n <= 0)
			goto invalid_seq;
		    if (tmp[1] < 0x80 || tmp[1] > 0xbf)
			goto invalid_seq;
		    u = ((tmp[0] & 0x1f) << 6) | (tmp[1] & 0x3f);
		    if (u < 0x80 || u > 0x7ff)
			goto invalid_seq;
		} else if (tmp[0] < 0xf0) {
		    n = port->port->ops->read(port->port, tmp+1, 2);
		    fill = 3;
		    if (n <= 0)
			goto invalid_seq;
		    if (tmp[1] < 0x80 || tmp[1] > 0xbf)
			goto invalid_seq;
		    if (tmp[2] < 0x80 || tmp[2] > 0xbf)
			goto invalid_seq;
		    u = ((tmp[0] & 0x0f) << 12) | ((tmp[1] & 0x3f) << 6) | (tmp[2] & 0x3f);
		    if (u < 0x800 || u > 0xffff)
			goto invalid_seq;
		    if (u >= 0xd800 && u <= 0xdfff)
			goto invalid_seq;
		} else if (tmp[0] < 0xf8) {
		    n = port->port->ops->read(port->port, tmp+1, 3);
		    fill = 4;
		    if (n <= 0)
			goto invalid_seq;
		    if (tmp[1] < 0x80 || tmp[1] > 0xbf)
			goto invalid_seq;
		    if (tmp[2] < 0x80 || tmp[2] > 0xbf)
			goto invalid_seq;
		    if (tmp[3] < 0x80 || tmp[3] > 0xbf)
			goto invalid_seq;
		    u = ((tmp[0] & 0x07) << 18) | ((tmp[1] & 0x3f) << 12) | ((tmp[2] & 0x3f) << 6) | (tmp[3] & 0x3f);
		    if (u < 0x10000 || u > 0x10fff)
			goto invalid_seq;
		} else {
		    goto invalid_seq;
		}
		if (u >= 0) {
		    if (sizeof(wchar_t) >= 4) {
			buf[num] = u;
			num += tr_nl(port, buf+num, 1);
		    } else {
			if (u > 0xffff) {
			    buf[num] = (u >> 10) + 0xd7c0;
			    u = (u % 0x400) + 0xdc00;
			    num += tr_nl(port, buf+num, 1);
			    if (num < len) {
				buf[num] = u;
				num += tr_nl(port, buf+num, 1);
			    } else {
				if (port->unget == WEOF) {
				    port->unget = u;
				} else {
				    port->unget2 = u;
				}
			    }
			} else {
			    buf[num] = u;
			    num += tr_nl(port, buf+num, 1);
			}
		    }
		}
	    }
	}
    } else if (port->codec == ksi_data->sym_utf16) {
        unsigned short *tmp_short = (unsigned short *) tmp;
        while (num < len) {
	    n = port->port->ops->read(port->port, tmp, 2);
	    if (n <= 0) {
		port->p.kp.eof = 1;
		num += tr_nl(port, buf+num, 0);
		break;
	    }
	    if (n != 2) {
	    invalid_utf16:
		if (port->handling == ksi_data->sym_raise) {
		    ksi_exn_error(L"i/o-decoding", (ksi_obj) port, "utf16 sequence (0x%04x) cannot be decoded by codec %ls",
				  *tmp_short, KSI_SYM_PTR(port->codec));
		}
		if (port->handling == ksi_data->sym_replace)
		    u = 0xfffd;
		else
		    u = -1;
	    } else {
		if (port->endianess != 0 && port->endianess != sys_endianess) {
		    tmp[2] = tmp[0];
		    tmp[0] = tmp[1];
		    tmp[1] = tmp[2];
		}
		u = *tmp_short;
		if (sizeof(wchar_t) >= 4) {
		    if ((u & 0xfc00) == 0xd800) {
			n = port->port->ops->read(port->port, tmp, 2);
			if (n != 2)
			    goto invalid_utf16;
			if (port->endianess != 0 && port->endianess != sys_endianess) {
			    tmp[2] = tmp[0];
			    tmp[0] = tmp[1];
			    tmp[1] = tmp[2];
			}
			n = *tmp_short;
			if ((n & 0xfc00) != 0xdc00)
			    goto invalid_utf16;
			u = (u << 10) + n - 0x35fdc00;
		    }
		    if (u >= 0xd800 && u <= 0xdfff)
			goto invalid_utf16;
		}
            }
            if (u >= 0) {
                if (u == 0xfffe || u == 0xfeff) {
                    if (port->endianess == 0)
                        port->endianess = u;
                } else {
                    buf[num] = u;
                    num += tr_nl(port, buf+num, 1);
                }
            }
        }
    } else if (port->codec == ksi_data->sym_latin1) {
        while (num < len) {
	    n = port->port->ops->read(port->port, tmp, 1);
	    if (n <= 0) {
		port->p.kp.eof = 1;
		num += tr_nl(port, buf+num, 0);
		break;
	    }
	    buf[num] = (unsigned char) tmp[0];
	    num += tr_nl(port, buf+num, 1);
        }
    } else if (port->codec == ksi_data->sym_native) {
        mbstate_t state;
        memset(&state, 0, sizeof(state));

        while (num < len) {
	    fill = 0;
	read_more_native:
	    n = port->port->ops->read(port->port, tmp+fill, 1);
	    if (n <= 0) {
		port->p.kp.eof = 1;
		if (fill > 0) {
		    if (port->handling == ksi_data->sym_raise)
			goto raise;
		    if (port->handling == ksi_data->sym_replace)
			buf[num++] = 0xfffd;
		}
		num += tr_nl(port, buf+num, 0);
		break;
	    }
	    fill += 1;

	    n = (int) mbrtowc(buf+num, tmp, fill, &state);

	    if (n == (size_t) -2) {
		memset(&state, 0, sizeof(state));
		goto read_more_native;
	    }

	    if (n == (size_t) -1) {
		if (port->handling == ksi_data->sym_raise)
		    goto raise;

		memset(&state, 0, sizeof(state));
		if (port->handling == ksi_data->sym_replace) {
		    buf[num++] = 0xfffd;
		} else {
		    /* ignore byte sequence */
		    goto read_more_native;
		}
	    } else {
		num += tr_nl(port, buf+num, 1);
	    }
        }
    } else {
#ifdef HAVE_ICONV_H
	char *in_ptr, *out_ptr = (char*) &buf[0];
	size_t in_size, out_size = len * sizeof(wchar_t);

        while (num < len) {
	    fill = 0;
	read_more_iconv:
	    n = port->port->ops->read(port->port, tmp+fill, 1);
	    if (n <= 0) {
		port->p.kp.eof = 1;
		if (fill > 0) {
		    if (port->handling == ksi_data->sym_raise)
			goto raise;
		    if (port->handling == ksi_data->sym_replace) {
			buf[num++] = 0xfffd;
			num += tr_nl(port, buf+num, 1);
		    }
		}
		num += tr_nl(port, buf+num, 0);
		break;
	    }
	    ++fill;

	    in_ptr = (char*) &tmp[0];
	    in_size = fill * sizeof(wchar_t);
	    n = iconv(port->in_ic, &in_ptr, &in_size, &out_ptr, &out_size);

	    if (n == (size_t) -1) {
		if (port->handling == ksi_data->sym_raise)
		    goto raise;

		iconv(port->in_ic, 0, 0, 0, 0);
		if (port->handling == ksi_data->sym_replace) {
		    buf[num] = 0xfffd;
		    num += tr_nl(port, buf+num, 1);
		} else {
		    /* ignore byte sequence */
		    goto read_more_iconv;
		}
	    } else {
		num += tr_nl(port, buf+num, n);
	    }
        }
#endif
    }

    return num;
}

static unsigned short
tr_utf16_code (struct Ksi_Tr_Port *port, unsigned short code)
{
    if (port->endianess != sys_endianess) {
        char *ptr = (char *) &code;
        char c0 = ptr[0], c1 = ptr[1];
        ptr[0] = c1;
        ptr[1] = c0;
    }
    return code;
}

static int
tr_write (ksi_char_port _port, const wchar_t *buf, int len)
{
    int i, n = 0, num = 0;
    struct Ksi_Tr_Port *port = (struct Ksi_Tr_Port *) _port;

    if (port->codec == ksi_data->sym_utf8) {
        for (i = 0; i < len; i++) {
            char tmp[8];
            unsigned u = buf[i];

            n = 0;
            if (u == L'\n') {
                if (port->eol == ksi_data->sym_crlf) {
                    tmp[n++] = '\r';
                    tmp[n++] = '\n';
                } else if (port->eol == ksi_data->sym_cr) {
                    tmp[n++] = '\r';
                } else if (port->eol == ksi_data->sym_nel) {
                    tmp[n++] = 0xc2;
                    tmp[n++] = 0x85;
                } else if (port->eol == ksi_data->sym_crnel) {
                    tmp[n++] = '\r';
                    tmp[n++] = 0xc2;
                    tmp[n++] = 0x85;
                } else if (port->eol == ksi_data->sym_ls) {
                    tmp[n++] = 0xe2;
                    tmp[n++] = 0x80;
                    tmp[n++] = 0xa8;
                } else {
                    tmp[n++] = '\n';
                }
            } else if (u < 0x80) {
                tmp[n++] = (char) u;
            } else {
                if (u < 0x800) {
                    tmp[n++] = (char) (0xc0 | (u >> 6));
                } else {
                    if (u > 0xffff) {
                        tmp[n++] = (char) (0xf0 | (u >> 18));
                        tmp[n++] = (char) (0x80 | ((u >> 12) & 0x3f));
                    } else {
                        tmp[n++] = (char) (0xe0 | (u >> 12));
                    }
                    tmp[n++] = (char) (0x80 | ((u >> 6) & 0x3f));
                }
                tmp[n++] = (char) (0x80 | (u & 0x3f));
            }
            port->port->ops->write(port->port, tmp, n);
            port->p.last_write_char = u;
            num += 1;
        }
    } else if (port->codec == ksi_data->sym_utf16) {
        unsigned short *tmp = alloca((len+1) * 2 * sizeof(*tmp));

        n = 0;
        if (port->endianess == 0) {
            port->endianess = sys_endianess;
            tmp[n++] = sys_endianess;
        }
        for (i = 0; i < len; i++) {
            unsigned u = buf[i];

            if (u == L'\n') {
                if (port->eol == ksi_data->sym_crlf) {
                    tmp[n++] = tr_utf16_code(port, '\r');
                    tmp[n++] = tr_utf16_code(port, '\n');
                } else if (port->eol == ksi_data->sym_cr) {
                    tmp[n++] = tr_utf16_code(port, '\r');
                } else if (port->eol == ksi_data->sym_nel) {
                    tmp[n++] = tr_utf16_code(port, 0x85);
                } else if (port->eol == ksi_data->sym_crnel) {
                    tmp[n++] = tr_utf16_code(port, '\r');
                    tmp[n++] = tr_utf16_code(port, 0x85);
                } else if (port->eol == ksi_data->sym_ls) {
                    tmp[n++] = tr_utf16_code(port, 0x2028);
                } else {
                    tmp[n++] = tr_utf16_code(port, '\n');
                }
            } else if (u < 0x10000) {
                tmp[n++] = tr_utf16_code(port, u);
            } else {
                tmp[n++] = tr_utf16_code(port, (u >> 10) + 0xd7c0);
                tmp[n++] = tr_utf16_code(port, (u % 0x400) + 0xdc00);
            }
            port->p.last_write_char = u;
            num += 1;
        }
        port->port->ops->write(port->port, (char *)tmp, n * sizeof(*tmp));
    } else if (port->codec == ksi_data->sym_latin1) {
        char *tmp = alloca(2*len);
        n = 0;
        for (i = 0; i < len; i++) {
            if (buf[i] == L'\n') {
                if (port->eol == ksi_data->sym_crlf) {
                    tmp[n++] = '\r';
                    tmp[n++] = '\n';
                } else if (port->eol == ksi_data->sym_cr) {
                    tmp[n++] = '\r';
                } else if (port->eol == ksi_data->sym_none || port->eol == ksi_data->sym_lf) {
                    tmp[n++] = '\n';
                } else {
                lfraise:
                    ksi_exn_error(L"i/o-encoding", (ksi_obj) port,
                                  "character #\\linefeed cannot be encoded by codec %ls with eol-style %ls",
                                  KSI_SYM_PTR(port->codec), KSI_SYM_PTR(port->eol));
                    return 0;
                }
            } else if (buf[i] <= 0xff) {
                tmp[n++] = (char) buf[i];
            } else {
                if (port->handling == ksi_data->sym_raise) {
                raise:
                    ksi_exn_error(L"i/o-encoding", (ksi_obj) port,
                                  "character #\\x%04x cannot be encoded by codec %ls",
                                  buf[i], KSI_SYM_PTR(port->codec));
                    return 0;
                }
                if (port->handling == ksi_data->sym_replace)
                    tmp[n++] = '?';
            }
            port->p.last_write_char = buf[i];
            num += 1;
        }
        port->port->ops->write(port->port, tmp, n);
    } else if (port->codec == ksi_data->sym_native) {
        mbstate_t state;
        memset(&state, 0, sizeof(state));

        for (i = 0; i < len; i++) {
            if (buf[i] == L'\n') {
                if (port->eol == ksi_data->sym_crlf) {
                    port->port->ops->write(port->port, "\r\n", 2);
                } else if (port->eol == ksi_data->sym_cr) {
                    port->port->ops->write(port->port, "\r", 1);
                } else if (port->eol == ksi_data->sym_none || port->eol == ksi_data->sym_lf) {
                    port->port->ops->write(port->port, "\n", 1);
                } else {
                    goto lfraise;
                }
            } else {
                char tmp[MB_LEN_MAX];
                size_t nx = wcrtomb(tmp, buf[i], &state);
                if (nx == (size_t) -1) {
                    if (port->handling == ksi_data->sym_raise) {
                        goto raise;
                    }

                    memset(&state, 0, sizeof(state));
                    if (port->handling == ksi_data->sym_replace) {
                        nx = wcrtomb(tmp, L'?', &state);
                    } else {
                        /* ignore char */
                        num += 1;
                        continue;
                    }
                }
                port->port->ops->write(port->port, tmp, (int) nx);
            }
            port->p.last_write_char = buf[i];
            num += 1;
        }
    } else {
#ifdef HAVE_ICONV_H
	if (port->out_ic != (iconv_t) -1) {
	    for (i = 0; i < len; i++) {
		wchar_t in_buf[8];
		size_t in_pos = 0, in_size;
		char out_buf[MB_LEN_MAX * 8];
		size_t out_size = sizeof(out_buf);
		char *in_ptr = (char*) &in_buf[0], *out_ptr = &out_buf[0];
		size_t n;

	    try_again:
		if (buf[i] == L'\n') {
		    if (port->eol == ksi_data->sym_crlf) {
			in_buf[in_pos++] = L'\r';
			in_buf[in_pos++] = L'\n';
		    } else if (port->eol == ksi_data->sym_cr) {
			in_buf[in_pos++] = L'\r';
		    } else if (port->eol == ksi_data->sym_none || port->eol == ksi_data->sym_lf) {
			in_buf[in_pos++] = L'\n';
		    } else {
			goto lfraise;
		    }
		} else {
		    in_buf[in_pos++] = buf[i];
		}

	    do_iconv:
		in_size = in_pos * sizeof(wchar_t);
		n = iconv(port->out_ic, &in_ptr, &in_size, &out_ptr, &out_size);
		if (n == (size_t) -1) {
		    if (errno == EINVAL && in_pos < sizeof(in_buf)/sizeof(in_buf[0]) && i+1 < len) {
			++i;
			goto try_again;
		    }

		    if (port->handling == ksi_data->sym_raise) {
			goto raise;
		    }

		    iconv(port->out_ic, 0, 0, 0, 0);
		    if (port->handling == ksi_data->sym_replace) {
			in_buf[0] = L'?';
			in_pos = 1;
			goto do_iconv;
		    } else {
			/* ignore char */
			num += 1;
			continue;
		    }
		}
		port->port->ops->write(port->port, out_buf, out_ptr - out_buf);
		port->p.last_write_char = buf[i];
		num += 1;
	    }
	}
#endif
    }

    return num;
}

static int
tr_getpos (ksi_char_port _port, long *pos)
{
    struct Ksi_Tr_Port *tr = (struct Ksi_Tr_Port *) _port;
    return tr->port->ops->getpos(tr->port, pos);
}

static int
tr_setpos (ksi_char_port _port, long pos)
{
    struct Ksi_Tr_Port *tr = (struct Ksi_Tr_Port *) _port;
    tr->p.kp.eof = 0;
    tr->unget = WEOF;
    return tr->port->ops->setpos(tr->port, pos);
}

static struct Ksi_Char_Port_Ops tr_port_ops =
{
    tr_close,
    tr_flush,
    tr_read,
    tr_lookahead,
    tr_write,
    tr_getpos,
    tr_setpos
};

static void
set_tr_codec (struct Ksi_Tr_Port *tr, ksi_obj codec, const char *name)
{
#ifdef HAVE_ICONV_H
    if (tr->in_ic != (iconv_t) -1) {
	iconv_close(tr->in_ic);
	tr->in_ic = (iconv_t) -1;
    }
    if (tr->out_ic != (iconv_t) -1) {
	iconv_close(tr->out_ic);
	tr->out_ic = (iconv_t) -1;
    }
#endif

    if (codec == ksi_data->sym_latin1) {
    } else if (codec == ksi_data->sym_utf8) {
    } else if (codec == ksi_data->sym_utf16) {
        tr->endianess = 0;
    } else if (codec == ksi_data->sym_native) {
    } else {
#ifdef HAVE_ICONV_H
	char *codec_name = ksi_local(KSI_SYM_PTR(codec));

	if (tr->p.kp.input) {
	    tr->in_ic = iconv_open("wchar_t", codec_name);
	    if (tr->in_ic == (iconv_t) -1) {
		ksi_exn_error(L"i/o", codec, "%s: codec is not supported for input", name);
	    }
	}
	if (tr->p.kp.output) {
	    tr->out_ic = iconv_open(codec_name, "wchar_t");
	    if (tr->out_ic == (iconv_t) -1) {
		ksi_exn_error(L"i/o", codec, "%s: codec is not supported for output", name);
	    }
	}
#endif

        ksi_exn_error(L"i/o", codec, "%s: codec is not supported", name);
    }

    tr->codec = codec;
}

static void
tr_finalizer (void *obj, void *data)
{
    struct Ksi_Tr_Port *port = (struct Ksi_Tr_Port *) obj;

#ifdef HAVE_ICONV_H
    if (port->in_ic != (iconv_t) -1) {
	iconv_close(port->in_ic);
	port->in_ic = (iconv_t) -1;
    }
    if (port->out_ic != (iconv_t) -1) {
	iconv_close(port->out_ic);
	port->out_ic = (iconv_t) -1;
    }
#endif
}

ksi_char_port
ksi_new_transcoded_port (ksi_byte_port p, ksi_obj codec, ksi_obj eol, ksi_obj handling, const char *file_name, int file_line)
{
    struct Ksi_Tr_Port *tr;

    if (!codec)
        codec = ksi_data->sym_native;
    if (!eol)
        eol = ksi_data->sym_native;
    if (!handling)
        handling = ksi_data->sym_replace;

    /* KSI_CHECK(p, KSI_PORT_P(p) && ((ksi_port) p)->binary, "transcoded-port: invalid binary port in arg1"); */
    KSI_CHECK(codec, KSI_SYM_P(codec), "transcoded-port: invalid codec in arg2");
    KSI_CHECK(eol, KSI_SYM_P(eol), "transcoded-port: invalid eol-style in arg3");
    KSI_CHECK(handling, KSI_SYM_P(handling), "transcoded-port: invalid error-handling-mode in arg4");

#if defined(WIN32) || defined(OS2) || defined(MSDOS)
    if (eol == ksi_data->sym_native)
        eol = ksi_data->sym_crlf;
#else
    if (eol == ksi_data->sym_native)
        eol = ksi_data->sym_lf;
#endif

    tr = ksi_malloc(sizeof *tr);
    tr->p.kp.o.itag = KSI_TAG_PORT;
    tr->p.kp.port_id = ksi_aprintf("transcoded(%ls,%ls) %ls", KSI_SYM_PTR(codec), KSI_SYM_PTR(eol), p->kp.port_id);
    tr->p.kp.input = p->kp.input;
    tr->p.kp.output = p->kp.output;
    tr->p.kp.can_getpos = p->kp.can_getpos;
    tr->p.kp.can_setpos = p->kp.can_setpos;
    tr->p.kp.is_transcoded = 1;
    tr->p.ops = &tr_port_ops;
    tr->p.file_name = (file_name ? ksi_strdup(file_name) : 0);
    tr->p.file_line = file_line;
    tr->p.line_pos = 0;
    tr->port = p;
    tr->codec = codec;
    tr->eol = eol;
    tr->handling = handling;
    tr->unget = WEOF;
    tr->unget2 = WEOF;

#ifdef HAVE_ICONV_H
    tr->in_ic = (iconv_t) -1;
    tr->out_ic = (iconv_t) -1;
#endif

    set_tr_codec(tr, codec, "transcoded-port");

    ksi_register_finalizer(tr, tr_finalizer, 0);
    return (ksi_char_port) tr;
}

ksi_obj
ksi_transcoded_port (ksi_obj p, ksi_obj codec, ksi_obj eol, ksi_obj handling)
{
    KSI_CHECK(p, KSI_PORT_P(p) && (((struct Ksi_Port *) p)->binary), "transcoded-port: invalid binary port in arg1");
    return (ksi_obj) ksi_new_transcoded_port((ksi_byte_port) p, codec, eol, handling, 0, 0);
}

ksi_obj
ksi_port_codec (ksi_obj p)
{
    struct Ksi_Tr_Port *tr = (struct Ksi_Tr_Port *) p;

    KSI_CHECK(p, KSI_PORT_P(p), "port-codec: invalid port in arg1");
    KSI_CHECK(p, ((struct Ksi_Port *) p)->is_transcoded, "port-codec: not transcoded port in arg1");

    return tr->codec;
}

ksi_obj
ksi_set_port_codec_x (ksi_obj p, ksi_obj codec)
{
    struct Ksi_Tr_Port *tr = (struct Ksi_Tr_Port *) p;

    KSI_CHECK(p, KSI_PORT_P(p), "set-port-codec!: invalid port in arg1");
    KSI_CHECK(p, ((struct Ksi_Port *) p)->is_transcoded, "set-port-codec!: not transcoded port in arg1");
    KSI_CHECK(codec, KSI_SYM_P(codec), "set-port-codec!: invalid symbol in arg2");

    set_tr_codec(tr, codec, "set-port-codec!");
    return ksi_void;
}

ksi_obj
ksi_port_eol_style (ksi_obj p)
{
    struct Ksi_Tr_Port *tr = (struct Ksi_Tr_Port *) p;

    KSI_CHECK(p, KSI_PORT_P(p), "port-eol-style: invalid port in arg1");
    KSI_CHECK(p, ((struct Ksi_Port *) p)->is_transcoded, "port-eol-style: not transcoded port in arg1");

    return tr->eol;
}

ksi_obj
ksi_port_error_handling_mode (ksi_obj p)
{
    struct Ksi_Tr_Port *tr = (struct Ksi_Tr_Port *) p;

    KSI_CHECK(p, KSI_PORT_P(p), "port-error-handling-mode: invalid port in arg1");
    KSI_CHECK(p, ((struct Ksi_Port *) p)->is_transcoded, "port-error-handling-mode: not transcoded port in arg1");

    return tr->handling;
}


/* ---------- scheme port utils -------------------------------------- */

ksi_obj
ksi_eof_object (void)
{
    return ksi_eof;
}

ksi_obj
ksi_eof_object_p (ksi_obj x)
{
    return (x == ksi_eof ? ksi_true : ksi_false);
}

ksi_obj
ksi_port_p (ksi_obj port)
{
    return (KSI_PORT_P(port) ? ksi_true : ksi_false);
}

ksi_obj
ksi_text_port_p (ksi_obj port)
{
    return (KSI_PORT_P(port) && !(((struct Ksi_Port *) port)->binary) ? ksi_true : ksi_false);
}

ksi_obj
ksi_bin_port_p (ksi_obj port)
{
    return (KSI_PORT_P(port) && (((struct Ksi_Port *) port)->binary) ? ksi_true : ksi_false);
}

ksi_obj
ksi_input_port_p (ksi_obj port)
{
    return (KSI_PORT_P(port) && (((struct Ksi_Port *) port)->input) ? ksi_true : ksi_false);
}

ksi_obj
ksi_output_port_p (ksi_obj port)
{
    return (KSI_PORT_P(port) && (((struct Ksi_Port *) port)->output) ? ksi_true : ksi_false);
}

ksi_obj
ksi_fd_port_p (ksi_obj port)
{
    return (KSI_PORT_P(port) && (((struct Ksi_Port *) port)->is_fd) ? ksi_true : ksi_false);
}

ksi_obj
ksi_transcoded_port_p (ksi_obj port)
{
    return (KSI_PORT_P(port) && (((struct Ksi_Port *) port)->is_transcoded) ? ksi_true : ksi_false);
}

ksi_obj
ksi_port_can_getpos_p (ksi_obj port)
{
    return (KSI_PORT_P(port) && (((struct Ksi_Port *) port)->can_getpos) ? ksi_true : ksi_false);
}

ksi_obj
ksi_port_can_setpos_p (ksi_obj port)
{
    return (KSI_PORT_P(port) && (((struct Ksi_Port *) port)->can_setpos) ? ksi_true : ksi_false);
}

ksi_obj
ksi_port_getpos (ksi_obj port)
{
    long pos;
    int res;

    KSI_CHECK(port, KSI_PORT_P(port), "port-position: invalid port in arg1");
    KSI_CHECK(port, (((struct Ksi_Port *) port)->can_getpos), "port-position: port does not support the operation");
    if (((struct Ksi_Port *) port)->closed)
        ksi_exn_error(L"i/o-port", port, "port-position: port closed");

    if (((struct Ksi_Port *) port)->binary) {
        res = ((ksi_byte_port) port)->ops->getpos((ksi_byte_port) port, &pos);
    } else {
        res = ((ksi_char_port) port)->ops->getpos((ksi_char_port) port, &pos);
    }
    if (res < 0)
        ksi_exn_error(L"i/o-port", port, "port-position: i/o error");

    return ksi_long2num(pos);
}

ksi_obj
ksi_port_setpos (ksi_obj port, ksi_obj pos)
{
    long posval;
    int retval;

    KSI_CHECK(port, KSI_PORT_P(port), "set-port-position!: invalid port in arg1");
    KSI_CHECK(port, (((struct Ksi_Port *) port)->can_setpos), "set-port-position!: port does not support the operation");
    if (((struct Ksi_Port *) port)->closed)
        ksi_exn_error(L"i/o-port", port, "set-port-position!: port closed");

    KSI_CHECK(pos, KSI_EINT_P(pos), "set-port-position!: invalid exact integer in arg2");
    posval = ksi_num2long(pos, "set-port-position!");
    if (((struct Ksi_Port *) port)->binary) {
        retval = ((ksi_byte_port) port)->ops->setpos((ksi_byte_port) port, posval);
    } else {
        retval = ((ksi_char_port) port)->ops->setpos((ksi_char_port) port, posval);
    }
    if (retval < 0)
        ksi_exn_error(L"i/o-invalid-position", pos, "set-port-position!: invalid position");

    return ksi_long2num(retval);
}

ksi_obj
ksi_close_port (ksi_obj port)
{
    int retval;

    KSI_CHECK(port, KSI_PORT_P(port), "close-port: invalid port in arg1");

    if (((struct Ksi_Port *) port)->closed)
        return ksi_void;

    if (((struct Ksi_Port *) port)->binary) {
        retval = ((ksi_byte_port) port)->ops->close((ksi_byte_port) port);
    } else {
        retval = ((ksi_char_port) port)->ops->close((ksi_char_port) port);
    }
    if (retval < 0)
        ksi_exn_error(L"i/o-port", port, "close-port: i/o port error");

    return ksi_void;
}

ksi_obj
ksi_flush_output_port (ksi_obj port)
{
    int retval;

    KSI_CHECK(port, KSI_PORT_P(port) && (((struct Ksi_Port *) port)->output), "flush-output-port: invalid output port in arg1");

    if (((struct Ksi_Port *) port)->closed)
        return ksi_void;

    if (((struct Ksi_Port *) port)->binary) {
        retval = ((ksi_byte_port) port)->ops->flush((ksi_byte_port) port);
    } else {
        retval = ((ksi_char_port) port)->ops->flush((ksi_char_port) port);
    }
    if (retval < 0)
        ksi_exn_error(L"i/o-port", port, "flush-output-port: i/o port error");

    return ksi_void;
}

ksi_obj
ksi_port_buffer_mode (ksi_obj port)
{
    KSI_CHECK(port, KSI_PORT_P(port), "port-buffer-mode: invalid port in arg1");
    if (((struct Ksi_Port *) port)->unbuf)
        return ksi_data->sym_none;
    if (((struct Ksi_Port *) port)->linebuf)
        return ksi_data->sym_line;
    return ksi_data->sym_block;
}

ksi_obj
ksi_port_eof_p (ksi_obj port)
{
    KSI_CHECK(port, KSI_PORT_P(port), "port-eof?: invalid input port in arg1");
    KSI_CHECK(port, ((struct Ksi_Port *) port)->input, "port-eof?: invalid input port in arg1");
    return (((struct Ksi_Port *) port)->closed || (((struct Ksi_Port *) port)->eof) ? ksi_true : ksi_false);
}

int
ksi_get_byte (ksi_byte_port port)
{
    char buf[1];
    int res;

    KSI_CHECK((ksi_obj) port, port->kp.input, "get-u8: invalid binary input port in arg1");
    if (port->kp.closed)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "get-u8: port closed");

    res = port->ops->read(port, buf, 1);
    if (res < 0)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "get-u8: i/o port error");
    if (res <= 0)
        return -1;
    return (unsigned char) buf[0];
}

int
ksi_lookahead_byte (ksi_byte_port port)
{
    KSI_CHECK((ksi_obj) port, port->kp.input, "lookahead-u8: invalid binary input port in arg1");
    if (port->kp.closed)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "lookahead-u8: port closed");

    return port->ops->lookahead(port);
}

ksi_obj
ksi_get_bytevector (ksi_obj port, ksi_obj bv, ksi_obj start, ksi_obj count)
{
    int beg, len, res;

    KSI_CHECK(port, KSI_PORT_P(port), "get-bytevector-n!: invalid binary input port in arg1");
    KSI_CHECK(port, ((struct Ksi_Port *) port)->input, "get-bytevector-n!: invalid binary input port in arg1");
    KSI_CHECK(port, ((struct Ksi_Port *) port)->binary, "get-bytevector-n!: invalid binary input port in arg1");
    if (((struct Ksi_Port *) port)->closed)
        ksi_exn_error(L"i/o-port", port, "get-bytevector-n!: port closed");

    KSI_CHECK(bv, KSI_BVEC_P(bv), "get-bytevector-n!: invalid bytevector in arg2");

    KSI_CHECK(start, KSI_UINT_P(start), "get-bytevector-n!: invalid exact non-negative integer in arg3");
    beg = ksi_num2ulong(start, "get-bytevector-n!");

    KSI_CHECK(count, KSI_UINT_P(count), "get-bytevector-n!: invalid exact non-negative integer in arg4");
    len = ksi_num2ulong(count, "get-bytevector-n!");

    if (beg + len > KSI_BVEC_LEN(bv)) {
        ksi_exn_error(0, 0, "get-bytevector-n!: length of bytevector too small: %d+%d>%d", beg, len, KSI_BVEC_LEN(bv));
    }

    res = ((ksi_byte_port) port)->ops->read((ksi_byte_port) port, KSI_BVEC_PTR(bv) + beg, len);
    if (res < 0)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "get-bytevector-n!: i/o port error");
    if (res <= 0)
        return ksi_eof;
    return ksi_long2num(res);
}

void
ksi_put_byte (ksi_byte_port port, int byte)
{
    char buf[1];
    int res;

    KSI_CHECK((ksi_obj) port, port->kp.output, "put-u8: invalid binary output port in arg1");
    if (port->kp.closed)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "put-u8: port closed");

    buf[0] = (char) byte;
    res = port->ops->write(port, buf, 1);
    if (res <= 0)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "put-u8: i/o port error");
}

ksi_obj
ksi_put_bytevector (ksi_obj port, ksi_obj bv, ksi_obj start, ksi_obj count)
{
    int beg, len, res;

    KSI_CHECK(port, KSI_PORT_P(port), "put-bytevector: invalid binary output port in arg1");
    KSI_CHECK(port, ((struct Ksi_Port *) port)->output, "put-bytevector: invalid binary output port in arg1");
    KSI_CHECK(port, ((struct Ksi_Port *) port)->binary, "put-bytevector: invalid binary output port in arg1");
    if (((struct Ksi_Port *) port)->closed)
        ksi_exn_error(L"i/o-port", port, "put-bytevector: port closed");

    KSI_CHECK(bv, KSI_BVEC_P(bv), "put-bytevector: invalid bytevector in arg2");

    if (!start) {
        beg = 0;
    } else {
        KSI_CHECK(start, KSI_UINT_P(start), "put-bytevector: invalid exact non-negative integer in arg3");
        beg = ksi_num2ulong(start, "put-bytevector");
    }

    if (!count) {
        len = KSI_BVEC_LEN(bv) - beg;
    } else {
        KSI_CHECK(count, KSI_UINT_P(count), "put-bytevector: invalid exact non-negative integer in arg4");
        len = ksi_num2ulong(count, "put-bytevector");
    }

    if (beg >= KSI_BVEC_LEN(bv) || beg + len > KSI_BVEC_LEN(bv)) {
        ksi_exn_error(0, 0, "put-bytevector: length of bytevector too small: %d+%d>%d", beg, len, KSI_BVEC_LEN(bv));
    }

    res = ((ksi_byte_port) port)->ops->write((ksi_byte_port) port, KSI_BVEC_PTR(bv) + beg, len);
    if (res <= 0)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "put-bytevector: i/o port error");

    return ksi_long2num(res);
}


wint_t
ksi_get_char (ksi_char_port port)
{
    wchar_t buf[1];
    int res;

    KSI_CHECK((ksi_obj) port, port->kp.input, "get-char: invalid text input port in arg1");
    if (port->kp.closed)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "get-char: port closed");

    if (port->kp.eof)
        return WEOF;

    res = port->ops->read(port, buf, 1);
    if (res < 0)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "get-char: i/o port error");
    if (res <= 0)
        return WEOF;
    return (wint_t) buf[0];
}

wint_t
ksi_lookahead_char (ksi_char_port port)
{
    KSI_CHECK((ksi_obj) port, port->kp.input, "lookahead-char: invalid text input port in arg1");
    if (port->kp.closed)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "lookahead-char: port closed");

    if (port->kp.eof)
        return WEOF;

    return port->ops->lookahead(port);
}

ksi_obj
ksi_get_string (ksi_obj port, ksi_obj bv, ksi_obj start, ksi_obj count)
{
    int beg, len, res;

    KSI_CHECK(port, KSI_PORT_P(port), "get-string-n!: invalid textual input port in arg1");
    KSI_CHECK(port, ((struct Ksi_Port *) port)->input, "get-string-n!: invalid textual input port in arg1");
    KSI_CHECK(port, !((struct Ksi_Port *) port)->binary, "get-string-n!: invalid textual input port in arg1");
    if (((struct Ksi_Port *) port)->closed)
        ksi_exn_error(L"i/o-port", port, "get-string-n!: port closed");

    KSI_CHECK(bv, KSI_STR_P(bv), "get-string-n!: invalid string in arg2");

    KSI_CHECK(start, KSI_UINT_P(start), "get-string-n!: invalid exact non-negative integer in arg3");
    beg = ksi_num2ulong(start, "get-string-n!");

    KSI_CHECK(count, KSI_UINT_P(count), "get-string-n!: invalid exact non-negative integer in arg4");
    len = ksi_num2ulong(count, "get-string-n!");

    if (beg + len > KSI_STR_LEN(bv)) {
        ksi_exn_error(0, 0, "get-string-n!: length of string too small: %d+%d>%d", beg, len, KSI_BVEC_LEN(bv));
    }

    res = ((ksi_char_port) port)->ops->read((ksi_char_port) port, KSI_STR_PTR(bv) + beg, len);
    if (res < 0)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "get-string-n!: i/o port error");
    if (res <= 0)
        return ksi_eof;
    return ksi_long2num(res);
}

ksi_obj
ksi_get_line (ksi_obj port)
{
    wchar_t buf[1];
    ksi_buffer_t out;

    KSI_CHECK(port, KSI_PORT_P(port), "get-line: invalid textual input port in arg1");
    KSI_CHECK(port, ((struct Ksi_Port *) port)->input, "get-line: invalid textual input port in arg1");
    KSI_CHECK(port, !((struct Ksi_Port *) port)->binary, "get-line: invalid textual input port in arg1");
    if (((struct Ksi_Port *) port)->closed)
        ksi_exn_error(L"i/o-port", port, "get-line: port closed");

    out = ksi_new_buffer(64, 0);
    for (;;) {
        int res = ((ksi_char_port) port)->ops->read((ksi_char_port) port, buf, 1);
        if (res < 0)
            ksi_exn_error(L"i/o-port", (ksi_obj) port, "get-line: i/o port error");
        if (res <= 0) {
            int len = ksi_buffer_len(out);
            if (len == 0)
                return ksi_eof;
            return ksi_str2string(ksi_buffer_data(out), len);
        }
        if (buf[0] == L'\n') {
            return ksi_str2string(ksi_buffer_data(out), ksi_buffer_len(out));
        }
        ksi_buffer_put(out, buf[0]);
    }
}

void
ksi_put_char (ksi_char_port port, wchar_t ch)
{
    wchar_t buf[1];
    int res;

    KSI_CHECK((ksi_obj) port, port->kp.output, "put-char: invalid text output port in arg1");
    if (port->kp.closed)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "put-char: port closed");

    buf[0] = ch;
    res = port->ops->write(port, buf, 1);
    if (res <= 0)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "put-char: i/o port error");
}

ksi_obj
ksi_put_string (ksi_obj port, ksi_obj str, ksi_obj start, ksi_obj count)
{
    int beg, len, res;

    KSI_CHECK(port, KSI_PORT_P(port), "put-string: invalid textual output port in arg1");
    KSI_CHECK(port, ((struct Ksi_Port *) port)->output, "put-string: invalid textual output port in arg1");
    KSI_CHECK(port, !((struct Ksi_Port *) port)->binary, "put-string: invalid textual output port in arg1");
    if (((struct Ksi_Port *) port)->closed)
        ksi_exn_error(L"i/o-port", port, "put-string: port closed");

    KSI_CHECK(str, KSI_STR_P(str), "put-string: invalid string in arg2");

    if (!start) {
        beg = 0;
    } else {
        KSI_CHECK(start, KSI_UINT_P(start), "put-string: invalid exact non-negative integer in arg3");
        beg = ksi_num2ulong(start, "put-string");
    }

    if (!count) {
        len = KSI_STR_LEN(str) - beg;
    } else {
        KSI_CHECK(count, KSI_UINT_P(count), "put-string: invalid exact non-negative integer in arg4");
        len = ksi_num2ulong(count, "put-string");
    }

    if (beg >= KSI_STR_LEN(str) || beg + len > KSI_STR_LEN(str)) {
        ksi_exn_error(0, 0, "put-string: length of string too small: %d+%d>%d", beg, len, KSI_BVEC_LEN(str));
    }

    res = ((ksi_char_port) port)->ops->write((ksi_char_port) port, KSI_STR_PTR(str) + beg, len);
    if (res <= 0)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "put-string: i/o port error");

    return ksi_long2num(res);
}

/* ---------- get-datum ---------------------------------------------- */

#define DELIMITERS  L"()[]{}\";#'`,"

typedef struct
{
    ksi_char_port port;
    wchar_t *tok_buf;
    unsigned tok_size, tok_len;
    unsigned case_sens : 1;
    unsigned r6rs_mode : 1;
    unsigned kw_prefix : 1;
    unsigned kw_sharp : 1;
} read_state;


static ksi_obj ksi_read_obj (read_state* st, wint_t prev_ch, int close_char);


static const wchar_t *
src_name (read_state *st, int tok_line, int tok_pos)
{
    if (st->port->file_name) {
        return ksi_aprintf("%s (%d:%d)", st->port->file_name, tok_line, tok_pos);
    }

    return ksi_obj2str((ksi_obj) st->port);
}

static void
annotate (ksi_obj x, read_state *st, int tok_line, int tok_pos)
{
    x->annotation = src_name(st, tok_line, tok_pos);
}

static void
expand_tok_buf (read_state *st, int sz)
{
    wchar_t *p;

    if (sz >= st->tok_size) {
        sz = ((sz + 127) & ~127) + 128;

        /* cannot use ksi_realloc(), because initial buffer allocated on the stack */
        p = ksi_malloc_data(sz * sizeof(*p));
        if (st->tok_len)
            wmemcpy(p, st->tok_buf, st->tok_len);

        st->tok_buf = p;
        st->tok_size = sz;
    }
}

static wint_t
skip_ws (read_state *st)
{
    for (;;) {
        wint_t c = ksi_get_char(st->port);
        if (c == WEOF)
            return c;

        if (iswspace(c))
            continue;

        if (!iswgraph(c))
            ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos), "get-datum: illegal char #\\x%04x", c);

        return c;
    }
}

static ksi_obj
read_not_impl (read_state *st, wint_t ch, int tok_line, int tok_pos)
{
    ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: invalid char `%lc' (syntax not implemented)", ch);
    return ksi_void;
}

static int
read_token (read_state *st, wint_t prev_ch1, wint_t prev_ch2, int use_esc)
{
    int esc = 0;
    wint_t c;

    st->tok_len = 0;
    for (;;) {
    skip:
        if (prev_ch1 > 0) {
            c = prev_ch1;
            prev_ch1 = 0;
        } else if (prev_ch2 > 0) {
            c = prev_ch2;
            prev_ch2 = 0;
        } else {
            c = ksi_lookahead_char(st->port);
            if (c == WEOF || iswspace(c) || wcschr(DELIMITERS, c))
                break;
            c = ksi_get_char(st->port);
        }
        if (use_esc && c == '\\') {
            c = ksi_get_char(st->port);
            if (c == WEOF)
                ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos), "get-datum: unexpected eof after `\\'");

            if (c == 'x') {
                int n = 0;
                for (;;) {
                    c = ksi_get_char(st->port);
                    if (c == WEOF)
                        ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos), "get-datum: unexpected eof after `\\x'");
                    if ('0' <= c && c <= '9')
                        n = (n * 16) + (c - '0');
                    else if ('a' <= c && c <= 'f')
                        n = (n * 16) + (c - 'a' + 0x10);
                    else if ('A' <= c && c <= 'F')
                        n = (n * 16) + (c - 'A' + 0x10);
                    else if (c == ';' || iswspace(c))
                        break;
                    else
                        ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos),  "get-datum: invalid sequence after `\\x'");
                }
                if (st->tok_len == 0)
                    esc = 1;

                c = n;
            } else {
                if (st->r6rs_mode || !iswgraph(c))
                    ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos), "get-datum: invalid sequence `\\x%04x'", c);
            }
        } else if (use_esc && c == L'|') {
            if (st->r6rs_mode)
                ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos), "get-datum: illegal char `|'");

            if (st->tok_len == 0)
                esc = 1;

            for (;;) {
                c = ksi_get_char(st->port);
                if (c == WEOF)
                    ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos), "get-datum: unexpected eof after `|'");
                if (c == L'|')
                    goto skip;

                expand_tok_buf(st, st->tok_len);
                st->tok_buf[st->tok_len++] = c;
            }
        } else {
            if (!iswgraph(c))
                ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos), "get-datum: illegal char #\\x%04x", c);

            if (!st->case_sens)
                c = towlower(c);
        }

        expand_tok_buf(st, st->tok_len);
        st->tok_buf[st->tok_len++] = c;
    }

    expand_tok_buf(st, st->tok_len);
    st->tok_buf[st->tok_len] = 0;

    return esc;
}

static ksi_obj
read_ch (read_state *st, int tok_line, int tok_pos)
{
    ksi_obj x;
    wint_t c;

    st->tok_len = 0;

    c = ksi_get_char(st->port);
    if (c == WEOF)
        ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected eof while reading a `#\' syntax");
    if (c == L'\n')
        ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected linefeed while reading a `#\' syntax");
    if (!iswprint(c))
        ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos), "get-datum: illegal char #\\x%04x", c);

    expand_tok_buf(st, st->tok_len);
    st->tok_buf[st->tok_len++] = c;

    if (!iswspace(c) && wcschr(DELIMITERS, c) == 0) {
        for (;;) {
            c = ksi_lookahead_char(st->port);
            if (c == WEOF || iswspace(c) || wcschr(DELIMITERS, c))
                break;
            c = ksi_get_char(st->port);
            if (!iswgraph(c))
                ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos), "get-datum: illegal char #\\x%04x", c);

            expand_tok_buf(st, st->tok_len);
            st->tok_buf[st->tok_len++] = c;
        }
    }

    expand_tok_buf(st, st->tok_len);
    st->tok_buf[st->tok_len] = 0;

    if (st->tok_len == 1)
        return ksi_int2char(st->tok_buf[0]);

    x = ksi_str2char(st->tok_buf, st->tok_len);
    if (x != ksi_false)
        return x;

    ksi_src_error (src_name(st, tok_line, tok_pos), "get-datum: invalid character name `#\\%ls'", st->tok_buf);
    return 0;
}

static wchar_t *
read_c_string (read_state *st, int end_char, char* tok_name, int tok_line, int tok_pos)
{
    st->tok_len = 0;
    for (;;) {
        int n, c = ksi_get_char(st->port);
        if (c == WEOF) {
        eof_err:
            ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected eof while reading a %s", tok_name);
        }

    next_char:
        if (c == end_char)
            break;

        if (c == '\\') {
            if ((c = ksi_get_char(st->port)) == WEOF)
                goto eof_err;

            switch (c) {
            case 'a' : c = '\a'; break; /* alarm  */
            case 'b' : c = '\b'; break; /* bs   */
            case 'n' : c = '\n'; break; /* lf   */
            case 'r' : c = '\r'; break; /* cr   */
            case 't' : c = '\t'; break; /* tab  */
            case 'f' : c = '\f'; break; /* ff   */
            case 'v' : c = '\v'; break; /* vtab */
            case '\\': c = '\\'; break;
            case '\"': c = '\"'; break;

            case 'x' :
                n = 0;
                for (;;) {
                    c = ksi_get_char(st->port);
                    if (c == WEOF)
                        goto eof_err;
                    if (L'0' <= c && c <= L'9')
                        n = (n * 16) + (c - L'0');
                    else if (L'a' <= c && c <= L'f')
                        n = (n * 16) + (c - L'a' + 10);
                    else if (L'A' <= c && c <= L'F')
                        n = (n * 16) + (c - L'A' + 10);
                    else if (c == L';')
                        break;
                    else
                        ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos), "get-datum: invalid sequence after `\\x' while reading a %s", tok_name);
                }
                c = n;
                break;

            default:
                if (!iswprint(c))
                    ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos), "get-datum: illegal char #\\x%04x", c);

                if (iswspace(c)) {
                    while (c != '\n') {
                        if ((c = ksi_get_char(st->port)) == WEOF)
                            goto eof_err;
                        if (!iswspace(c))
                            goto esc_err;
                    }
                    while (iswspace (c)) {
                        if ((c = ksi_get_char(st->port)) == WEOF)
                            goto eof_err;
                    }
                    goto next_char;
                }
            esc_err:
                ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos), "get-datum: invalid sequence `\\%lc' while reading a %s", c, tok_name);
                break;
            } /* switch */
        } /* if (c == '\\') */

        expand_tok_buf (st, st->tok_len);
        st->tok_buf [st->tok_len++] = c;
    } /* for */

    expand_tok_buf (st, st->tok_len);
    st->tok_buf [st->tok_len] = '\0';
    return st->tok_buf;
}

static ksi_obj
read_list (read_state *st, int close_char, int improper_list, char* tok_name, int tok_line, int tok_pos)
{
    ksi_obj  list = ksi_nil, tmp;
    ksi_obj* last = &list;
    int cur_line, cur_pos;

    for (;;) {
        int c = skip_ws(st);
        if (c == WEOF) {
        eof_err:
            ksi_src_error (src_name (st, tok_line, tok_pos), "get-datum: unexpected eof while reading a %s", tok_name);
        }
        cur_line = st->port->file_line;
        cur_pos = st->port->line_pos;

        if (c == close_char)
            return list;

        if (c == L'.') {
            wint_t c1 = ksi_lookahead_char(st->port);
            if (c1 == WEOF)
                goto eof_err;

            if (iswspace(c1) || wcschr(DELIMITERS, c1)) {
                if (!improper_list)
                    ksi_src_error(src_name(st, st->port->file_line, st->port->line_pos), "get-datum: unexpected '.' while reading a %s", tok_name);

                *last = ksi_read_obj(st, WEOF, 0);
                if (*last == ksi_eof)
                    goto eof_err;

                c = skip_ws(st);
                if (c != close_char)
                    ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: missing `%lc' while reading %s", close_char, tok_name);

                return list;
            }
        }

        tmp = ksi_read_obj(st, c, close_char);
        if (tmp == 0) /* close char readed */
            return list;

        if (tmp == ksi_eof)
            goto eof_err;

        *last = ksi_cons(tmp, ksi_nil);
        annotate(*last, st, cur_line, cur_pos);

        last = &KSI_CDR(*last);
    }

    ksi_exn_error(0, 0, "get-datum: internal error");
    return ksi_void;
}

static ksi_obj
read_vector (read_state *st, int close_char, int tok_line, int tok_pos)
{
    ksi_obj x = read_list(st, close_char, 0, "vector", tok_line, tok_pos);
    return ksi_list2vector(x);
}

static ksi_obj
list2bytevector(ksi_obj ls, read_state *st, int tok_line, int tok_pos)
{
    int i, len = ksi_list_len(ls);
    char *buf = alloca(len);
    for (i = 0; i < len; i++) {
        ksi_obj x = KSI_CAR(ls);
        ls = KSI_CDR(ls);
        if (!KSI_EINT_P(x)) {
            ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: invalid bytevector element");
        }
        buf[i] = (char) ksi_num2long(x, "get-datum");
    }
    return ksi_str2bytevector(buf, len);
}

static ksi_obj
read_sharp (read_state *st, int tok_line, int tok_pos)
{
    int c1, c2, depth;
    ksi_obj x;

    c1 = ksi_get_char(st->port);
    if (c1 == WEOF)
        ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected eof while reading `#' syntax");
    if (iswspace(c1))
        ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected space while reading `#' syntax");

    switch (c1) {
    case '!':
        read_token(st, 0, 0, 0);
        if (wcscmp(st->tok_buf, L"r6rs") == 0) {
            st->r6rs_mode = 1;
            return 0;
        }
        if (wcscmp(st->tok_buf, L"ksi") == 0) {
            st->r6rs_mode = 0;
            return 0;
        }
        if (!st->r6rs_mode) {
            if (wcscmp(st->tok_buf, L"code-page") == 0) {
                c1 = skip_ws(st);
                if (c1 == WEOF)
                    return ksi_eof;
                read_token(st, c1, 0, 0);
                if (st->port->kp.is_transcoded)
                    ksi_set_port_codec_x((ksi_obj) st->port, ksi_str2sym(st->tok_buf, st->tok_len));
                return 0;
            }
            if (wcscmp(st->tok_buf, L"fold-case") == 0) {
                st->case_sens = 0;
                return 0;
            }
            if (wcscmp(st->tok_buf, L"no-fold-case") == 0) {
                st->case_sens = 1;
                return 0;
            }
            if (wcscmp(st->tok_buf, L"keyword-prefix") == 0) {
                st->kw_prefix = 1;
                return 0;
            }
            if (wcscmp(st->tok_buf, L"keyword-postfix") == 0) {
                st->kw_prefix = 0;
                return 0;
            }
            if (wcscmp(st->tok_buf, L"keyword-sharp") == 0) {
                st->kw_sharp = 1;
                return 0;
            }
            if (wcscmp(st->tok_buf, L"no-keyword-sharp") == 0) {
                st->kw_sharp = 0;
                return 0;
            }
            if (wcscmp(st->tok_buf, L"eof") == 0) {
                return ksi_eof;
            }
            if (wcscmp(st->tok_buf, L"void") == 0) {
                return ksi_void;
            }
        }
        ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: invalid `#!%ls' syntax", st->tok_buf);
        break;

    case ';':
        /* skip datum comment, i.e.  #; <datum> */
        ksi_read_obj(st, WEOF, 0);
        return 0;

    case '|':
        /* skip balanced comment, i.e.  #| ... |# */
        depth = 1;
        c1 = ' ';
        for (;;) {
            c2 = ksi_get_char(st->port);
            if (c2 == WEOF)
                ksi_src_error (src_name (st, tok_line, tok_pos), "get-datum: unexpected eof while reading `#|' syntax");

            if (c1 == '#' && c2 == '|') {
                ++depth;
                c1 = ' ';
            } else if (c1 == '|' && c2 == '#') {
                if (--depth == 0)
                    return 0;
                c1 = ' ';
            } else {
                c1 = c2;
            }
        }
        break;

    case 'f': case 'F':
        c2 = ksi_lookahead_char(st->port);
        if (c2 == WEOF || iswspace(c2) || wcschr(DELIMITERS, c2))
            return ksi_false;
        ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: invalid `#%lc%lc' syntax", c1, c2);

    case 't': case 'T':
        c2 = ksi_lookahead_char(st->port);
        if (c2 == WEOF || iswspace(c2) || wcschr(DELIMITERS, c2))
            return ksi_true;
        ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: invalid `#%lc%lc' syntax", c1, c2);

    case '\\':
        return read_ch(st, tok_line, tok_pos);

    case ':':
        if (st->kw_sharp) {
            read_token(st, 0, 0, 1);
            return ksi_str2key(st->tok_buf, st->tok_len);
        }
        break;

    case 'b': case 'B': case 'o': case 'O':
    case 'd': case 'D': case 'x': case 'X':
    case 'i': case 'I': case 'e': case 'E':
        read_token(st, '#', c1, 0);
        x = ksi_str02num(st->tok_buf, 0);
        if (x == ksi_false)
            ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: invalid number syntax `%ls'", st->tok_buf);
        return x;

    case '(':
        x = read_vector(st, ')', tok_line, tok_pos);
        return x;

    case '[':
        x = read_vector(st, ']', tok_line, tok_pos);
        return x;

    case '\'':
        x = ksi_read_obj(st, WEOF, 0);
        if (x == ksi_eof)
            ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected eof while reading a syntax");
        x = KSI_LIST2(ksi_data->sym_syntax, x);
        annotate(x, st, tok_line, tok_pos);
        return x;

    case '`':
        x = ksi_read_obj(st, WEOF, 0);
        if (x == ksi_eof)
            ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected eof while reading a quasisyntax");
        x = KSI_LIST2(ksi_data->sym_quasisyntax, x);
        annotate(x, st, tok_line, tok_pos);
        return x;

    case ',':
        c2 = ksi_get_char(st->port);
        if (c2 == WEOF)
            ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected eof while reading a unsyntax");

        if (c2 == '@') {
            x = ksi_read_obj(st, WEOF, 0);
            if (x == ksi_eof)
                ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected eof while reading a #,@");

            x = KSI_LIST2(ksi_data->sym_unsyntax_splicing, x);
        } else {
            x = ksi_read_obj(st, c2, 0);
            if (x == ksi_eof)
                ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected eof while reading a unsyntax");

            x = KSI_LIST2(ksi_data->sym_unsyntax, x);
        }
        annotate (x, st, tok_line, tok_pos);
        return x;

    case '@':
        read_token(st, 0, 0, 0);
        if (wcscmp(st->tok_buf, L"cons") == 0)
            return ksi_data->cons_proc;
        if (wcscmp(st->tok_buf, L"car") == 0)
            return ksi_data->car_proc;
        if (wcscmp(st->tok_buf, L"cdr") == 0)
            return ksi_data->cdr_proc;
        if (wcscmp(st->tok_buf, L"not") == 0)
            return ksi_data->not_proc;
        if (wcscmp(st->tok_buf, L"eq?") == 0)
            return ksi_data->eq_proc;
        if (wcscmp(st->tok_buf, L"eqv?") == 0)
            return ksi_data->eqv_proc;
        if (wcscmp(st->tok_buf, L"equal?") == 0)
            return ksi_data->equal_proc;
        if (wcscmp(st->tok_buf, L"memq") == 0)
            return ksi_data->memq_proc;
        if (wcscmp(st->tok_buf, L"memv") == 0)
            return ksi_data->memv_proc;
        if (wcscmp(st->tok_buf, L"member") == 0)
            return ksi_data->member_proc;
        if (wcscmp(st->tok_buf, L"null?") == 0)
            return ksi_data->nullp_proc;
        if (wcscmp(st->tok_buf, L"pair?") == 0)
            return ksi_data->pairp_proc;
        if (wcscmp(st->tok_buf, L"list?") == 0)
            return ksi_data->listp_proc;
        if (wcscmp(st->tok_buf, L"vector?") == 0)
            return ksi_data->vectorp_proc;
        if (wcscmp(st->tok_buf, L"list") == 0)
            return ksi_data->list_proc;
        if (wcscmp(st->tok_buf, L"apply") == 0)
            return ksi_data->apply_proc;
        if (wcscmp(st->tok_buf, L"call/cc") == 0)
            return ksi_data->call_cc_proc;
        if (wcscmp(st->tok_buf, L"call/vs") == 0)
            return ksi_data->call_vs_proc;
        break;

    default:
        if (iswalpha(c1)) {
            read_token(st, c1, 0, 0);
            if (wcscmp(st->tok_buf, L"vu8") == 0) {
                c1 = ksi_get_char(st->port);
                if (c1 == L'(')
                    x = read_list(st, ')', 0, "bytevector", tok_line, tok_pos);
                else if (c1 == L'[')
                    x = read_list(st, ']', 0, "bytevector", tok_line, tok_pos);
                else
                    x = ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: invalid `#%ls%lc' syntax", st->tok_buf, c1);

                x = list2bytevector(x, st, tok_line, tok_pos);
                annotate(x, st, tok_line, tok_pos);
                return x;
            }
            ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: invalid `#%ls' syntax", st->tok_buf);
        }
        break;
    }

    ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: invalid `#%lc' syntax", c1);
    return ksi_void;
}

static ksi_obj
ksi_read_obj (read_state *st, wint_t prev_ch, int close_char)
{
    int c, esc, tok_line, tok_pos;

    if (prev_ch != WEOF && !iswspace(prev_ch)) {
        c = prev_ch;
    } else {
    again:
        c = skip_ws(st);
    }

    tok_line = st->port->file_line;
    tok_pos = st->port->line_pos;

/*    ksi_debug("read char=`%x' pos=%d:%d>", c, tok_line, tok_pos);
 */

    if (c != WEOF) {
        ksi_obj res = ksi_void;
        switch (c) {
        case '{': case '}':
            return read_not_impl(st, c, tok_line, tok_pos);

        case';': /* line comment */
            for (;;) {
                c = ksi_get_char(st->port);
                if (c == WEOF)
                    return ksi_eof;
                if (c == '\n')
                    goto again;
            }
            break;

        case '#':
            res = read_sharp(st, tok_line, tok_pos);
            if (!res)
                goto again;
            break;

        case '"':
            read_c_string(st, '"', "string", tok_line, tok_pos);
            res = ksi_str2string (st->tok_buf, st->tok_len);
            break;

        case '(':
            res = read_list (st, ')', 1, "list", tok_line, tok_pos);
            if (KSI_PAIR_P(res))
                annotate(res, st, tok_line, tok_pos);
            break;

        case '[':
            res = read_list (st, ']', 1, "list", tok_line, tok_pos);
            if (KSI_PAIR_P(res))
                annotate(res, st, tok_line, tok_pos);
            break;

        case ')':
        case ']':
            if (c != close_char)
                ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected `%lc'", c);
            return 0;

        case '.':
            c = ksi_lookahead_char(st->port);
            if (c == WEOF || iswspace(c) || wcschr(DELIMITERS, c))
                ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected `.'");

            c = '.';
            goto token;

        case '\'':
            res = ksi_read_obj(st, WEOF, 0);
            if (res == ksi_eof)
                ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected eof while reading a quote");

            res = KSI_LIST2(ksi_data->sym_quote, res);
            annotate(res, st, tok_line, tok_pos);
            break;

        case '`':
            res = ksi_read_obj(st, WEOF, 0);
            if (res == ksi_eof)
                ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected eof while reading a quasiquote");

            res = KSI_LIST2(ksi_data->sym_quasiquote, res);
            annotate(res, st, tok_line, tok_pos);
            break;

        case ',':
            c = ksi_get_char(st->port);
            if (c == WEOF)
                ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected eof while reading a unquote");

            if (c == '@') {
                res = ksi_read_obj(st, WEOF, 0);
                if (res == ksi_eof)
                    ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected eof while reading a ,@");

                res = KSI_LIST2(ksi_data->sym_unquote_splicing, res);
            } else {
                res = ksi_read_obj(st, c, 0);
                if (res == ksi_eof)
                    ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: unexpected eof while reading a unquote");

                res = KSI_LIST2(ksi_data->sym_unquote, res);
            }
            annotate(res, st, tok_line, tok_pos);
            break;

        case '|':
        default:
        token:
            esc = read_token(st, c, 0, 1);

            if (esc || st->tok_len == 0)
                return ksi_str2sym (st->tok_buf, st->tok_len);

            if (st->r6rs_mode) {
                if (st->tok_len == 1) {
                    if (st->tok_buf[0] == '+')
                        return ksi_data->sym_plus;
                    else if (st->tok_buf[0] == '-')
                        return ksi_data->sym_minus;
                } else if (st->tok_len >= 2) {
                    if (st->tok_buf[0] == '-' && st->tok_buf[1] == '>')
                        return ksi_str2sym (st->tok_buf, st->tok_len);
                    else if (st->tok_len == 2 && st->tok_buf[0] == '=' && st->tok_buf[1] == '>')
                        return ksi_data->sym_arrow;
                    else if (st->tok_len == 3 && st->tok_buf[0] == '.' && st->tok_buf[1] == '.' && st->tok_buf[2] == '.')
                        return ksi_data->sym_dots;
                }
                if (st->tok_buf[0] == '-' || st->tok_buf[0] == '+' || st->tok_buf[0] == '.' || iswdigit(st->tok_buf[0])) {
                    res = ksi_str02num(st->tok_buf, 0);
                    if (res == ksi_false)
                        ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: invalid number syntax `%ls'", st->tok_buf);
                } else {
                    res = ksi_str2sym(st->tok_buf, st->tok_len);
                }
            } else {
                if (st->kw_prefix && st->tok_buf[0] == ':') {
                    res = ksi_str2key(st->tok_buf+1, st->tok_len-1);
                } else if (!st->kw_prefix && st->tok_buf[st->tok_len-1] == ':') {
                    res = ksi_str2key(st->tok_buf, st->tok_len-1);
                } else {
                    wchar_t *p = st->tok_buf;
                    if (iswdigit(p[0])
                        || ((p[0] == '-' || p[0] == '+' || p[0] == '.') && iswdigit(p[1]))
                        || ((p[0] == 'n' || p[0] == 'N') && (p[1] == 'a' || p[1] == 'A') && (p[2] == 'n' || p[2] == 'N') && (p[3] == '.'))
                        || ((p[0] == '-' || p[0] == '+') && (p[1] == 'n' || p[1] == 'N') && (p[2] == 'a' || p[2] == 'A') && (p[3] == 'n' || p[3] == 'N') && (p[4] == '.'))
                        || ((p[0] == '-' || p[0] == '+') && (p[1] == 'i' || p[1] == 'I') && (p[2] == 'n' || p[2] == 'N') && (p[3] == 'f' || p[3] == 'F') && (p[4] == '.'))
                        ) {
                        res = ksi_str02num(st->tok_buf, 0);
                        if (res == ksi_false)
                            ksi_src_error(src_name(st, tok_line, tok_pos), "get-datum: invalid number syntax `%ls'", st->tok_buf);
                    } else {
                        res = ksi_str2sym(st->tok_buf, st->tok_len);
                    }
                }
            }
            break;
        }

/*    ksi_debug ("read res=%s at %d:%d", ksi_obj2str(res), tok_line, tok_pos);
 */
        return res;
    }

    return ksi_eof;
}

ksi_obj
ksi_get_datum (ksi_obj p)
{
    read_state st;
    wchar_t buf[256];
    ksi_obj res;

    KSI_CHECK(p, KSI_TEXT_INPUT_PORT_P(p), "get-datum: invalid textual input port");

    st.port = (ksi_char_port) p;
    st.tok_buf = buf;
    st.tok_size = sizeof(buf)/sizeof(buf[0]);
    st.tok_len = 0;
    st.case_sens = 1;
    st.r6rs_mode = 0;
    st.kw_prefix = 0;
    st.kw_sharp = 0;

    //ksi_warn (">>> %s %d:%d", p->file_name, p->file_line, p->line_pos);
    res = ksi_read_obj(&st, WEOF, 0);
    //ksi_warn ("<<< %s %d:%d\n    res=%ls", p->file_name, p->file_line, p->line_pos, ksi_obj2str(res));

    return res;
}


/* ---------- put-datum ---------------------------------------------- */

static int put_obj (ksi_char_port port, ksi_obj x);

int
ksi_port_write (ksi_char_port port, const wchar_t *ptr, int len)
{
    KSI_CHECK((ksi_obj) port, port->kp.output, "port-write: invalid text output port in arg1");
    if (port->kp.closed)
        ksi_exn_error(L"i/o-port", (ksi_obj) port, "port-write: port closed");

    if (len > 0) {
        len = port->ops->write(port, ptr, len);
        if (len <= 0)
            ksi_exn_error(L"i/o-port", (ksi_obj) port, "port-write: i/o port error");
    }

    return len;
}


static int
put_imm (ksi_char_port port, ksi_obj o)
{
    wchar_t *ptr;

    if (o == ksi_nil)
        return port->ops->write(port, L"()", 2);
    if (o == ksi_false)
        return port->ops->write(port, L"#f", 2);
    if (o == ksi_true)
        return port->ops->write(port, L"#t", 2);
    if (o == ksi_void)
        return port->ops->write(port, L"#!void", 6);
    if (o == ksi_eof)
        return port->ops->write(port, L"#!eof", 5);
    if (o == ksi_err)
        return port->ops->write(port, L"#!error", 7);

    ptr = ksi_aprintf("#!imm-%p", o);
    return (int) port->ops->write(port, ptr, (int) wcslen(ptr));
}

static int
put_core (ksi_char_port port, ksi_core o)
{
    wchar_t *ptr;

    switch (o->o.ilen) {
    case KSI_TAG_BEGIN:
        return port->ops->write(port, L"#@begin", 7);
    case KSI_TAG_AND:
        return port->ops->write(port, L"#@and", 5);
    case KSI_TAG_OR:
        return port->ops->write(port, L"#@or", 3);
    case KSI_TAG_IF:
        return port->ops->write(port, L"#@if", 3);
    case KSI_TAG_LAMBDA:
        return port->ops->write(port, L"#@lambda", 8);
    case KSI_TAG_DEFINE:
        return port->ops->write(port, L"#@define", 8);
    case KSI_TAG_DEFSYNTAX:
        return port->ops->write(port, L"#@defsyntax", 11);
    case KSI_TAG_SET:
        return port->ops->write(port, L"#@set!", 6);
    case KSI_TAG_QUOTE:
        return port->ops->write(port, L"#@quote", 7);
    default:
        ptr = ksi_aprintf("#@{core-%x}", o->o.ilen);
        break;
    }
    return port->ops->write(port, ptr, (int)wcslen(ptr));
}

static int
put_one_pair (ksi_char_port port, ksi_obj *x, int (*print) (ksi_char_port, ksi_obj))
{
    int res = print(port, KSI_CAR(*x));

    *x = KSI_CDR(*x);
    if (*x == ksi_nil)
        return res;

    if (!KSI_PAIR_P(*x)) {
        port->ops->write(port, L" . ", 3);
        return print(port, *x);
    }

    return port->ops->write(port, L" ", 1);
}

static int
put_pair (ksi_char_port port, wchar_t *prefix, ksi_obj x, wchar_t *suffix, int (*print) (ksi_char_port, ksi_obj))
{
    ksi_obj z = x;

    if (KSI_PAIR_P(KSI_CDR(x)) && KSI_CDR(KSI_CDR(x)) == ksi_nil) {
        if (KSI_CAR(x) == ksi_data->sym_quote) {
            port->ops->write(port, L"'", 1);
        quote:
            x = KSI_CDR(x);
            return put_one_pair(port, &x, print);
        }
        if (KSI_CAR(x) == ksi_data->sym_quasiquote) {
            port->ops->write(port, L"`", 1);
            goto quote;
        }
        if (KSI_CAR(x) == ksi_data->sym_unquote) {
            port->ops->write(port, L",", 1);
            goto quote;
        }
        if (KSI_CAR(x) == ksi_data->sym_unquote_splicing) {
            port->ops->write(port, L",@", 2);
            goto quote;
        }

        if (KSI_CAR(x) == ksi_data->sym_syntax) {
            port->ops->write(port, L"#'", 2);
            goto quote;
        }
        if (KSI_CAR(x) == ksi_data->sym_quasisyntax) {
            port->ops->write(port, L"#`", 2);
            goto quote;
        }
        if (KSI_CAR(x) == ksi_data->sym_unsyntax) {
            port->ops->write(port, L"#,", 2);
            goto quote;
        }
        if (KSI_CAR(x) == ksi_data->sym_unsyntax_splicing) {
            port->ops->write(port, L"#,@", 3);
            goto quote;
        }
    }

    port->ops->write(port, prefix, (int)wcslen(prefix));

    for (;;) {
        put_one_pair(port, &x, print);
        if (!KSI_PAIR_P(x))
            break;
        put_one_pair(port, &x, print);
        if (!KSI_PAIR_P(x))
            break;

        z = KSI_CDR(z);
        if (z == x) {
            /* cycle found */
            port->ops->write(port, L". ...", 5);
            break;
        }
    }

    return port->ops->write(port, suffix, (int)wcslen(suffix));
}

static int
put_vec (ksi_char_port port, wchar_t *beg, ksi_obj x, wchar_t *end, int (*print) (ksi_char_port, ksi_obj))
{
    int n;

    port->ops->write(port, beg, (int)wcslen(beg));

    if (KSI_VEC_LEN(x) > 0) {
        for (n = 0;;) {
            print(port, KSI_VEC_REF(x, n));
            if (++n >= KSI_VEC_LEN(x))
                break;
            port->ops->write(port, L" ", 1);
        }
    }

    return port->ops->write(port, end, (int)wcslen(end));
}

static int
put_bytevec (ksi_char_port port, wchar_t *beg, ksi_obj x, wchar_t *end)
{
    int n;

    port->ops->write(port, beg, (int)wcslen(beg));

    if (KSI_BVEC_LEN(x) > 0) {
        unsigned char *ptr = (unsigned char *) KSI_BVEC_PTR(x);
        for (n = 0;;) {
            wchar_t *p = ksi_aprintf("%d", ptr[n]);
            port->ops->write(port, p, (int)wcslen(p));
            if (++n >= KSI_BVEC_LEN(x))
                break;
            port->ops->write(port, L" ", 1);
        }
    }

    return port->ops->write(port, end, (int)wcslen(end));
}

static int
put_values (ksi_char_port port, ksi_obj x)
{
    if (KSI_VALUES_VALS(x) == ksi_nil) {
        return port->ops->write(port, L"#@<values ()>", 13);
    } else {
        port->ops->write(port, L"#@<values ", 10);
        put_obj(port, KSI_VALUES_VALS(x));
        return port->ops->write(port, L">", 1);
    }
}

static int
put_port (ksi_char_port port, ksi_obj o)
{
    port->ops->write(port, L"#@<port ", 8);

    if (((struct Ksi_Port *) o)->closed)
        port->ops->write(port, L"closed,", 7);

    if (((struct Ksi_Port *) o)->input)
        port->ops->write(port, L"input,", 6);

    if (((struct Ksi_Port *) o)->output)
        port->ops->write(port, L"output,", 7);

    if (((struct Ksi_Port *) o)->binary)
        port->ops->write(port, L"binary ", 7);
    else
        port->ops->write(port, L"textual ", 8);

    port->ops->write(port, ((struct Ksi_Port *) o)->port_id, (int)wcslen(((struct Ksi_Port *) o)->port_id));
    return port->ops->write(port, L">", 1);
}

static int
put_prim (ksi_char_port port, ksi_obj x)
{
    wchar_t *ptr;

    if (KSI_PRIM_NAME(x))
        ptr = ksi_aprintf("#@<procedure %ls >", KSI_PRIM_NAME(x));
    else
        ptr = ksi_aprintf ("#@<procedure %p>", x);

    return port->ops->write(port, ptr, (int)wcslen(ptr));
}

static int
put_env (ksi_char_port port, ksi_obj x)
{
    wchar_t *ptr;

    if (((ksi_env) x)->name == ksi_void)
        ptr = L"#@<top-level-env>";
    else if (((ksi_env) x)->name == ksi_nil)
        ptr = ksi_aprintf("#@<env %p>", x);
    else
        ptr = ksi_aprintf("#@<env %ls>", ksi_obj2str(((ksi_env) x)->name));

    return port->ops->write(port, ptr, (int)wcslen(ptr));
}

static int
put_hashtab (ksi_char_port port, ksi_obj x)
{
    wchar_t *ptr = ksi_aprintf("#@<hashtable %p>", x);
    return port->ops->write(port, ptr, (int)wcslen(ptr));
}

static int
put_code (ksi_char_port port, wchar_t *beg, ksi_obj x, wchar_t *end)
{
    int n;

    port->ops->write(port, beg, (int)wcslen(beg));
    for (n = 0;;) {
        put_obj(port, KSI_CODE_VAL(x, n));
        if (++n >= KSI_CODE_NUM(x) + 1)
            break;
        port->ops->write(port, L" ", 1);
    }

    return port->ops->write(port, end, (int)wcslen(end));
}

static int
put_closure (ksi_char_port port, ksi_obj x)
{
    const wchar_t *name = 0;
    ksi_obj doc = KSI_CLOS_DOC(x);
    ksi_obj env = (ksi_obj) KSI_CLOS_FRM(x)->env;

    if (KSI_SYM_P(doc)) {
        name = KSI_SYM_PTR(doc);
    } else {
        name = ksi_aprintf("%p", x);
    }
    if (KSI_PAIR_P(((ksi_env) env)->name)) {
        env = ((ksi_env) env)->name;
    }

    port->ops->write(port, L"#@<closure ", 11);
    port->ops->write(port, name, (int)wcslen(name));
    port->ops->write(port, L" ", 1);
    put_obj(port, env);
    return port->ops->write(port, L">", 1);
}

static int
put_prim_closure (ksi_char_port port, ksi_obj x)
{
    port->ops->write(port, L"#@<closure ", 11);
    put_obj(port, ((ksi_prim_closure) x)->proc);
    return port->ops->write(port, L">", 1);
}

static int
put_varbox (ksi_char_port port, const char *p, ksi_obj x)
{
    wchar_t *ptr = ksi_aprintf("#@{%s %d %d}", p, KSI_VARBOX_LEV(x), KSI_VARBOX_NUM(x));
    return port->ops->write(port, ptr, (int)wcslen(ptr));
}

static int
put_freevar (ksi_char_port port, const char* p, ksi_obj x)
{
    wchar_t *ptr = ksi_aprintf("#@{%s %ls %ls}", p, ksi_obj2str(KSI_FREEVAR_SYM(x)), ksi_obj2str(KSI_FREEVAR_ENV(x)->name));
    return port->ops->write(port, ptr, (int)wcslen(ptr));
}

int
put_obj (ksi_char_port port, ksi_obj o)
{
    const wchar_t *ptr = 0;

    if (!o) {
        return port->ops->write(port, L"#@null", 6);
    }

    switch (o->itag) {
    case KSI_TAG_IMM:
        return put_imm(port, o);

    case KSI_TAG_BIGNUM:
    case KSI_TAG_FLONUM:
        ptr = ksi_num2str(o, 10);
        break;

    case KSI_TAG_SYMBOL:
        ptr = ksi_symbol2str((ksi_symbol) o);
        break;

    case KSI_TAG_KEYWORD:
        ptr = ksi_key2str((ksi_keyword) o);
        break;

    case KSI_TAG_PAIR:
    case KSI_TAG_CONST_PAIR:
        return put_pair(port, L"(", o, L")", put_obj);

    case KSI_TAG_VECTOR:
    case KSI_TAG_CONST_VECTOR:
        return put_vec(port, L"#(", o, L")", put_obj);

    case KSI_TAG_BYTEVECTOR:
    case KSI_TAG_CONST_BYTEVECTOR:
        return put_bytevec(port, L"#vu8(", o, L")");

    case KSI_TAG_STRING:
    case KSI_TAG_CONST_STRING:
        ptr = ksi_string2str(o);
        break;

    case KSI_TAG_CHAR:
        ptr = ksi_char2str((ksi_char) o);
        break;

    case KSI_TAG_VAR0:
    case KSI_TAG_VAR1:
    case KSI_TAG_VAR2:
    case KSI_TAG_VARN:
        return put_varbox(port, "varbox", o);

    case KSI_TAG_FREEVAR:
        return put_freevar(port, "freevar", o);

    case KSI_TAG_LOCAL:
        return put_freevar(port, "local", o);

    case KSI_TAG_IMPORTED:
        return put_freevar(port, "imported", o);

    case KSI_TAG_PRIM:
    case KSI_TAG_PRIM_0:
    case KSI_TAG_PRIM_1:
    case KSI_TAG_PRIM_2:
    case KSI_TAG_PRIM_r:
        return put_prim(port, o);

    case KSI_TAG_CONS:
        return port->ops->write(port, L"#@cons", 6);

    case KSI_TAG_CAR:
        return port->ops->write(port, L"#@car", 5);

    case KSI_TAG_CDR:
        return port->ops->write(port, L"#@cdr", 5);

    case KSI_TAG_NOT:
        return port->ops->write(port, L"#@not", 5);

    case KSI_TAG_EQP:
        return port->ops->write(port, L"#@eq?", 5);

    case KSI_TAG_EQVP:
        return port->ops->write(port, L"#@eq?", 6);

    case KSI_TAG_EQUALP:
        return port->ops->write(port, L"#@equal?", 8);

    case KSI_TAG_MEMQ:
        return port->ops->write(port, L"#@memq", 6);

    case KSI_TAG_MEMV:
        return port->ops->write(port, L"#@memv", 6);

    case KSI_TAG_MEMBER:
        return port->ops->write(port, L"#@member", 8);

    case KSI_TAG_NULLP:
        return port->ops->write(port, L"#@null?", 7);

    case KSI_TAG_PAIRP:
        return port->ops->write(port, L"#@pair?", 7);

    case KSI_TAG_LISTP:
        return port->ops->write(port, L"#@list?", 7);

    case KSI_TAG_LIST:
        return port->ops->write(port, L"#@list", 6);

    case KSI_TAG_MK_VECTOR:
        return port->ops->write(port, L"#@vector", 8);

    case KSI_TAG_LIST2VECTOR:
        return port->ops->write(port, L"#@list->vector", 14);

    case KSI_TAG_APPEND:
        return port->ops->write(port, L"#@append", 8);

    case KSI_TAG_APPLY:
        return port->ops->write(port, L"#@apply", 7);

    case KSI_TAG_CALL_CC:
        return port->ops->write(port, L"#@call/cc", 9);

    case KSI_TAG_CALL_WITH_VALUES:
        return port->ops->write(port, L"#@call/vs", 9);

    case KSI_TAG_VECTORP:
        return port->ops->write(port, L"#@vector?", 9);

    case KSI_TAG_CLOSURE:
        return put_closure(port, o);

    case KSI_TAG_PRIM_CLOSURE:
        return put_prim_closure(port, o);

    case KSI_TAG_QUOTE:
        return put_code(port, L"#@{quote ", o, L"}");

    case KSI_TAG_AND:
        return put_code(port, L"#@{and ", o, L"}");

    case KSI_TAG_OR:
        return put_code(port, L"#@{or ", o, L"}");

    case KSI_TAG_BEGIN:
        return put_code(port, L"#@{begin ", o, L"}");

    case KSI_TAG_IF:
        return put_code(port, L"#@{if ", o, L"}");

    case KSI_TAG_DEFINE:
        return put_code(port, L"#@{define ", o, L"}");

    case KSI_TAG_SET:
        return put_code(port, L"#@{set! ", o, L"}");

    case KSI_TAG_CALL:
        return put_code(port, L"#@{call ", o, L"}");

    case KSI_TAG_FRAME:
        return put_code(port, L"#@{frame ", o, L"}");

    case KSI_TAG_LAMBDA:
        return put_code(port, L"#@{lambda ", o, L"}");

    case KSI_TAG_IMPORT:
        return put_code(port, L"#@{import ", o, L"}");

    case KSI_TAG_SYNTAX:
        return put_code(port, L"#@{syntax ", o, L"}");

    case KSI_TAG_INSTANCE:
        ptr =  ksi_inst2str((ksi_instance) o);
        break;

    case KSI_TAG_NEXT_METHOD:
        return port->ops->write(port, L"#@{next-method}", 15);

    case KSI_TAG_PORT:
        return put_port(port, o);

    case KSI_TAG_VALUES:
        return put_values(port, o);

    case KSI_TAG_ENVIRON:
        return put_env(port, o);

    case KSI_TAG_HASHTAB:
        return put_hashtab(port, o);

    case KSI_TAG_EXN:
        return put_vec(port, L"#@<exception ", o, L">", put_obj);

    case KSI_TAG_CORE:
        return put_core(port, (ksi_core) o);

    case KSI_TAG_EXTENDED:
    case KSI_TAG_BROKEN:
        ptr = KSI_EXT_TAG(o)->print((struct Ksi_EObj *) o, 1);
        break;
    }

    if (ptr) {
        return port->ops->write(port, ptr, (int)wcslen(ptr));
    }

    ksi_exn_error (0, 0, "object->string: invalid object tag: 0x%x", o->itag);
    return 0;
}

ksi_obj
ksi_put_datum (ksi_obj port, ksi_obj x)
{
    KSI_CHECK(port, KSI_TEXT_OUTPUT_PORT_P(port), "put-datum: invalid textual output port");
    put_obj((ksi_char_port) port, x);
    return ksi_void;
}


/* ---------- simple io ---------------------------------------------- */

static ksi_obj
coerce2num (ksi_obj x)
{
    if (KSI_NUM_P (x))
        return x;
    if (KSI_CHAR_P (x))
        return ksi_uint2num(KSI_CHAR_CODE(x));
    if (x == ksi_false)
        return ksi_int2num(0);
    if (x == ksi_true)
        return ksi_int2num(1);
    return ksi_ulonglong2num((KSI_WORD) x);
}

int
ksi_internal_format (ksi_char_port port, const wchar_t *fmt, int argc, ksi_obj* argv, char* nm)
{
    int nums;
    ksi_obj tmp;
    wchar_t *buf;

    for (nums = 0; *fmt; fmt++) {
        if (*fmt == L'~' && *(fmt+1) != '\0') {
            fmt++;
            switch (*fmt) {
            case L'a': case L'A':
                if (nums >= argc) {
                too_many:
                    ksi_exn_error (0, 0, "%s: too many ~ spec's in format string", nm);
                }
                ksi_display(argv[nums++], (ksi_obj) port);
                break;

            case L's': case L'S':
            case L'c': case L'C':
                if (nums >= argc)
                    goto too_many;
                ksi_write(argv[nums++], (ksi_obj) port);
                break;

            case L'd': case L'D':
                if (nums >= argc)
                    goto too_many;
                tmp = coerce2num(argv[nums++]);
                buf = ksi_num2str(tmp, 10);
                ksi_port_write(port, buf, (int)wcslen(buf));
                break;

            case L'b': case L'B':
                if (nums >= argc)
                    goto too_many;
                tmp = coerce2num(argv[nums++]);
                buf = ksi_num2str(tmp, 2);
                ksi_port_write(port, buf, (int)wcslen(buf));
                break;

            case L'o': case L'O':
                if (nums >= argc)
                    goto too_many;
                tmp = coerce2num(argv[nums++]);
                buf = ksi_num2str(tmp, 8);
                ksi_port_write(port, buf, (int)wcslen(buf));
                break;

            case L'x': case L'X':
                if (nums >= argc)
                    goto too_many;
                tmp = coerce2num(argv[nums++]);
                buf = ksi_num2str(tmp, 16);
                ksi_port_write(port, buf, (int)wcslen(buf));
                break;

            case L'?':
                if (nums+1 >= argc)
                    goto too_many;
                {
                    ksi_obj f = argv[nums++];
                    ksi_obj x = argv[nums++];
                    ksi_obj *av;
                    int ac, i;

                    if (!KSI_STR_P(f))
                        ksi_exn_error(0, 0, "%s: invalid format string for ~? spec: %ls", nm, ksi_obj2str(f));
                    if (KSI_LIST_P(x))
                        x = ksi_list2vector(x);
                    else if (!KSI_VEC_P(x))
                        ksi_exn_error(0, 0, "%s: invalid list for ~? spec: %ls", nm, ksi_obj2str(x));

                    ac = KSI_VEC_LEN(x);
                    av = KSI_VEC_ARR(x);
                    while (ac > 0) {
                        i = ksi_internal_format(port, KSI_STR_PTR(f), ac, av, nm);
                        if (i == 0)
                            break;
                        ac -= i;
                        av += i;
                    }
                }
                break;

            case L'%':
                ksi_port_write(port, L"\n", 1);
                break;

            case L'&':
                if (port->last_write_char != '\n')
                    ksi_port_write(port, L"\n", 1);
                break;

            case L'_':
                ksi_port_write(port, L" ", 1);
                break;

            case L'/':
                ksi_port_write(port, L"\t", 1);
                break;

            case L'|':
                ksi_port_write(port, L"\f", 1);
                break;

            case L'!':
                ksi_flush_output_port((ksi_obj) port);
                break;

            default:
                ksi_port_write(port, L"~", 1);
                ksi_port_write(port, fmt, 1);
                break;
            }
        } else {
            ksi_port_write(port, fmt, 1);
        }
    }

    return nums;
}

ksi_obj
ksi_format (ksi_obj port, const wchar_t *fmt, int argc, ksi_obj* argv)
{
    int retstring = 0;

    if (port == ksi_true) {
        port = ksi_current_output_port();
    } else if (port == ksi_false) {
        port = ksi_open_string_output_port();
        retstring = 1;
    } else {
        KSI_CHECK(port, KSI_TEXT_OUTPUT_PORT_P(port), "format: invalid textual output port in arg1");
    }

    ksi_internal_format((ksi_char_port) port, fmt, argc, argv, "format");
    return retstring ? ksi_extract_string_port_data(port) : ksi_void;
}


ksi_obj
ksi_newline (ksi_obj p)
{
    if (!p)
        p = ksi_current_output_port();

    KSI_CHECK(p, KSI_TEXT_OUTPUT_PORT_P(p), "newline: invalid textual output port");

    ksi_port_write((ksi_char_port) p, L"\n", 1);
    return ksi_void;
}

ksi_obj
ksi_write (ksi_obj o, ksi_obj p)
{
    if (!p)
        p = ksi_current_output_port();

    KSI_CHECK(p, KSI_TEXT_OUTPUT_PORT_P(p), "write: invalid textual output port");
    return ksi_put_datum(p, o);
}

ksi_obj
ksi_display (ksi_obj o, ksi_obj p)
{
    if (!p)
        p = ksi_current_output_port();

    KSI_CHECK(p, KSI_TEXT_OUTPUT_PORT_P(p), "display: invalid textual output port");

    if (!o)
        goto genobj;

    if (KSI_CHAR_P(o)) {
        wchar_t buf[1];
        buf[0] = KSI_CHAR_CODE(o);
        ksi_port_write((ksi_char_port) p, buf, 1);
    } else {
        int len, i;
        const wchar_t *ptr;
        ksi_obj x;

        if (!o)
            goto genobj;

        if (KSI_SYM_P(o)) {
            ptr = KSI_SYM_PTR(o);
            len = KSI_SYM_LEN(o);
        } else if (KSI_STR_P(o)) {
            ptr = KSI_STR_PTR(o);
            len = KSI_STR_LEN(o);
        } else if (KSI_VEC_P(o)) {
            ksi_port_write((ksi_char_port) p, L"#(", 2);
            for (i = 0; ;) {
                if (i < KSI_VEC_LEN(o))
                    ksi_display(KSI_VEC_REF(o, i), p);
                ++i;
                if (i >= KSI_VEC_LEN(o))
                    break;
                ksi_port_write((ksi_char_port) p, L" ", 1);
            }
            ksi_port_write((ksi_char_port) p, L")", 1);
            return ksi_void;
        } else if (KSI_PAIR_P(o)) {
            if (KSI_PAIR_P(KSI_CDR(o)) && KSI_CDR(KSI_CDR(o)) == ksi_nil) {
                if (KSI_CAR(o) == ksi_data->sym_quote) {
                    ksi_port_write((ksi_char_port) p, L"\'", 1);
                    ksi_display(KSI_CAR(KSI_CDR(o)), p);
                    return ksi_void;
                }
                if (KSI_CAR(o) == ksi_data->sym_quasiquote) {
                    ksi_port_write((ksi_char_port) p, L"`", 1);
                    ksi_display(KSI_CAR(KSI_CDR(o)), p);
                    return ksi_void;
                }
                if (KSI_CAR(o) == ksi_data->sym_unquote) {
                    ksi_port_write((ksi_char_port) p, L",", 1);
                    ksi_display(KSI_CAR(KSI_CDR(o)), p);
                    return ksi_void;
                }
                if (KSI_CAR(o) == ksi_data->sym_unquote_splicing) {
                    ksi_port_write((ksi_char_port) p, L",@", 2);
                    ksi_display(KSI_CAR(KSI_CDR(o)), p);
                    return ksi_void;
                }
            }

            ksi_port_write((ksi_char_port) p, L"(", 1);
            x = o;
            while (1) {
                ksi_display(KSI_CAR(o), p);
                o = KSI_CDR(o);
                if (o == ksi_nil)
                    break;
                if (!KSI_PAIR_P(o)) {
                    ksi_port_write((ksi_char_port) p, L" . ", 3);
                    ksi_display(o, p);
                    break;
                }

                ksi_port_write((ksi_char_port) p, L" ", 1);
                ksi_display(KSI_CAR(o), p);
                o = KSI_CDR(o);
                if (o == ksi_nil)
                    break;
                if (!KSI_PAIR_P(o)) {
                    ksi_port_write((ksi_char_port) p, L" . ", 3);
                    ksi_display(o, p);
                    break;
                }

                ksi_port_write((ksi_char_port) p, L" ", 1);
                x = KSI_CDR(x);
                if (o == x) {
                    ksi_port_write((ksi_char_port) p, L". . .", 5);
                    break;
                }
            }
            ksi_port_write((ksi_char_port) p, L")", 1);
            return ksi_void;
        } else if (KSI_INST_P(o)) {
            ksi_write_inst(o, (ksi_char_port) p, 0);
            return ksi_void;
        } else {
        genobj:
            ptr = ksi_obj2str(o);
            len = (int)wcslen(ptr);
        }

        ksi_port_write((ksi_char_port) p, ptr, len);
    }

    return ksi_void;
}

ksi_obj
ksi_write_char (ksi_obj o, ksi_obj p)
{
    wchar_t buf[1];

    if (!p)
        p = ksi_current_output_port();

    KSI_CHECK(o, KSI_CHAR_P(o), "write-char: invalid char in arg1");
    KSI_CHECK(p, KSI_TEXT_OUTPUT_PORT_P(p), "write-char: invalid textual output port in arg2");

    buf[0] = KSI_CHAR_CODE(o);
    ksi_port_write((ksi_char_port) p, buf, 1);
    return ksi_void;
}

ksi_obj
ksi_read_char (ksi_obj p)
{
    int c;

    if (!p)
        p = ksi_current_input_port();

    KSI_CHECK(p, KSI_TEXT_INPUT_PORT_P(p), "read-char: invalid textual input port");
    c = ksi_get_char((ksi_char_port) p);
    if (c == WEOF)
        return ksi_eof;

    return ksi_int2char(c);
}

ksi_obj
ksi_peek_char (ksi_obj p)
{
    wint_t c;

    if (!p)
        p = ksi_current_input_port();

    KSI_CHECK(p, KSI_TEXT_INPUT_PORT_P(p), "peek-char: invalid textual input port");
    c = ksi_lookahead_char((ksi_char_port) p);
    if (c == WEOF)
        return ksi_eof;
    return ksi_int2char(c);
}

ksi_obj
ksi_read (ksi_obj p)
{
    if (!p)
        p = ksi_current_input_port();

    KSI_CHECK(p, KSI_TEXT_INPUT_PORT_P(p), "read: invalid textual input port");
    return ksi_get_datum(p);
}


/* End of code */
