/*
 * ksi_evt.c
 * async events
 *
 * Copyright (C) 2000-2010, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Tue Mar 28 02:51:26 2000
 * Last Update:   Sat Jul 31 10:02:26 2010
 *
 */

#include "ksi_int.h"
#include "ksi_evt.h"
#include "ksi_printf.h"
#include "ksi_util.h"


#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif

#ifdef HAVE_FCNTL_H
#  include <fcntl.h>
#endif


static int events_in_progress = 0;
static int events_disabled = 1;
static int events_blocked = 0;


#define BLOCK_WAIT                                                      \
  do {                                                                  \
    if (events_blocked == 0)                                            \
      if (ksi_int_data->event_mgr->block_wait)                          \
        ksi_int_data->event_mgr->block_wait (ksi_int_data->event_mgr);  \
    events_blocked++;                                                   \
  } while (0)


#define UNBLOCK_WAIT                                                    \
  do {                                                                  \
    if (--events_blocked == 0)                                          \
      if (ksi_int_data->event_mgr->unblock_wait)                        \
        ksi_int_data->event_mgr->unblock_wait (ksi_int_data->event_mgr); \
  } while (0)


/* append_event, remove_event -- добавление/удаление событий в очередь
 */

static void
append_event (ksi_event x, ksi_event *list)
{
  if (*list)
    (*list)->prev = x;

  x->prev = 0;
  x->next = *list;
  *list = x;
}

static void
remove_event (ksi_event x, ksi_event *list)
{
  if (x->prev)
    x->prev->next = x->next;
  else
    *list = x->next;
  if (x->next)
    x->next->prev = x->prev;

  x->prev = x->next = 0;
}


ksi_obj
ksi_event_p (ksi_obj x)
{
  return (KSI_EVT_P (x) ? ksi_true : ksi_false);
}

ksi_obj
ksi_event_state (ksi_obj x)
{
  KSI_CHECK (x, KSI_EVT_P (x), "event-state: invalid event in arg1");
  return KSI_EVT_STATE (x);
}

ksi_obj
ksi_event_result (ksi_obj x)
{
  KSI_CHECK (x, KSI_EVT_P (x), "event-result: invalid event in arg1");
  return KSI_EVT_RESULT (x);
}

ksi_obj
ksi_event_procedure (ksi_obj x)
{
  KSI_CHECK (x, KSI_EVT_P (x), "event-procedure: invalid event in arg1");
  return KSI_EVT_ACTION (x);
}


ksi_obj
ksi_start_event (ksi_obj x)
{
  KSI_CHECK (x, KSI_EVT_P (x), "start-event: invalid event in arg1");
  if (!ksi_int_data || !ksi_int_data->event_mgr)
    ksi_exn_error (0, 0, "start-event: events is not supported or not initialized");

  BLOCK_WAIT;
  if (((ksi_event) x) -> waiting || ((ksi_event) x) -> pending) {
    ((ksi_event) x) -> start = 1;
    ((ksi_event) x) -> stop  = 0;
    ((ksi_event) x) -> ops -> init ((ksi_event) x);
  } else {
    if (((ksi_event) x) -> active) {
      ((ksi_event) x) -> active = 0;
      remove_event ((ksi_event) x, &ksi_int_data->active_events);
    }

    ((ksi_event) x) -> start = 1;
    ((ksi_event) x) -> stop  = 0;
    ((ksi_event) x) -> ops -> init ((ksi_event) x);

    if (!((ksi_event) x) -> waiting && !((ksi_event) x) -> pending)
    {
      ((ksi_event) x) -> waiting = 1;
      append_event ((ksi_event) x, &ksi_int_data->waiting_events);
    }
  }
  UNBLOCK_WAIT;

  if (!events_in_progress && !events_disabled)
    ksi_run_pending_events ();
  else
    ksi_int_data->have_event = 1;

  return x;
}

ksi_obj
ksi_stop_event (ksi_obj x)
{
  KSI_CHECK (x, KSI_EVT_P (x), "stop-event: invalid event in arg1");
  if (!ksi_int_data || ksi_int_data->event_mgr == 0)
    ksi_exn_error (0, 0, "stop-event: not supported");

  BLOCK_WAIT;
  if (((ksi_event) x) -> waiting || ((ksi_event) x) -> pending) {
    ((ksi_event) x) -> start = 0;
    ((ksi_event) x) -> stop  = 1;
  } else {
    if (((ksi_event) x) -> active) {
      ((ksi_event) x) -> active  = 0;
      ((ksi_event) x) -> start   = 0;
      ((ksi_event) x) -> stop    = 1;
      ((ksi_event) x) -> waiting = 1;
      remove_event ((ksi_event) x, &ksi_int_data->active_events);
      append_event ((ksi_event) x, &ksi_int_data->waiting_events);
    }
  }
  UNBLOCK_WAIT;

  if (!events_in_progress && !events_disabled)
    ksi_run_pending_events ();
  else
    ksi_int_data->have_event = 1;

  return x;
}

void
ksi_run_event (ksi_event evt, void *data, int invoke)
{
  if (evt) {
    BLOCK_WAIT;
    evt->data = data;

    if (evt->active) {
      evt->active = 0;
      remove_event (evt, &ksi_int_data->active_events);
    }

    evt->ready = 1;
    if (!evt->waiting && !evt->pending) {
      evt->waiting = 1;
      append_event (evt, &ksi_int_data->waiting_events);
    }
    UNBLOCK_WAIT;
  }

  if (invoke)
    ksi_run_pending_events ();
  else
    ksi_int_data->have_event = 1;
}

int
ksi_run_pending_events (void)
{
  volatile ksi_wind old_exit, new_exit;

  if (!ksi_int_data || ksi_int_data->event_mgr == 0)
    return 0;

  if (events_in_progress)
    return 0;

  events_in_progress++;
  ksi_disable_evt ();

  old_exit = ksi_int_data->exit_catch;
  new_exit = ksi_add_catch (ksi_true, 0, 0);

  ksi_int_data->exit_catch = new_exit;
  FLUSH_REGISTER_WINDOWS;
  if (setjmp (new_exit->the_catch->jmp.j_buf) == 0) {
    do {
      while (ksi_int_data->pending_events) {
        ksi_event curr = ksi_int_data->pending_events;

        BLOCK_WAIT;
        curr->pending = 0;
        curr->waiting = 1;
        curr->ready   = 0;
        curr->state   = ksi_data->sym_ready;
        remove_event (curr, &ksi_int_data->pending_events);
        append_event (curr, &ksi_int_data->waiting_events);
        UNBLOCK_WAIT;

        if (curr->ops->invoke (curr, curr->data)) {
          if (!curr->start)
            curr->stop = 1;
        }
      }

      while (ksi_int_data->waiting_events) {
        ksi_event curr = ksi_int_data->waiting_events;

        BLOCK_WAIT;
        curr->waiting = 0;
        remove_event (curr, &ksi_int_data->waiting_events);
        UNBLOCK_WAIT;

        if (curr->start) {
          if (curr->inited) {
            curr->inited = 0;
            curr->ops->cancel (curr);
          }

          curr->inited = 1;
          curr->start  = 0;
          curr->stop   = 0;
          curr->ready  = 0;
          curr->state  = ksi_data->sym_wait;
          curr->ops->setup (curr);
        }

        if (curr->stop) {
          if (curr->inited) {
            curr->inited = 0;
            curr->ops->cancel (curr);
          }

          curr->start  = 0;
          curr->stop   = 0;
          curr->ready  = 0;
          curr->state  = ksi_data->sym_inactive;
        }

        if (curr->ready) {
          BLOCK_WAIT;
          if (!curr->waiting && !curr->pending) {
            curr->pending = 1;
            append_event (curr, &ksi_int_data->pending_events);
          }
          UNBLOCK_WAIT;
        }

        if (curr->inited) {
          BLOCK_WAIT;
          if (!curr->waiting && !curr->pending) {
            curr->active = 1;
            append_event (curr, &ksi_int_data->active_events);
          }
          UNBLOCK_WAIT;
        }
      }
    } while (ksi_int_data->pending_events);

    ksi_del_catch (new_exit);
    new_exit = 0;
  }

  ksi_int_data->exit_catch = old_exit;
  ksi_int_data->have_event = 0;

  ksi_enable_evt ();
  events_in_progress--;

  if (new_exit)
    ksi_rethrow (new_exit);
  return 0;
}

int
ksi_has_pending_events (void)
{
  if (!ksi_int_data)
    return 0;

  if (ksi_int_data->pending_events || ksi_int_data->waiting_events)
    return 1;
  return ksi_int_data->have_event;
}

int
ksi_do_events (void)
{
  if (!ksi_int_data)
    return 0;

  if (!events_in_progress && !events_disabled && (ksi_int_data->pending_events || ksi_int_data->waiting_events))
    ksi_run_pending_events ();

  ksi_int_data->have_event = 0;
  return 0;
}

ksi_obj
ksi_wait_event (ksi_obj tm)
{
  volatile ksi_wind old_exit, new_exit;
  volatile ksi_obj retval;
  double timeout, wait_time;

  if (!ksi_int_data || ksi_int_data->event_mgr == 0 || ksi_int_data->event_mgr->wait_event == 0)
    return ksi_false;

  if (tm == ksi_true)
    tm = 0;

  if (tm == 0 || tm == ksi_false || ksi_zero_p (tm) == ksi_true) {
    timeout = 0.0;
  } else {
    KSI_CHECK (tm, KSI_RATIONAL_P(tm), "wait-event: invalid real in arg1");
    timeout = ksi_real_part(tm);
    KSI_CHECK (tm, timeout >= 0.0, "wait-event: negative real in arg1");
  }

  wait_time = ksi_real_time () + timeout;
  retval    = ksi_false;

  ksi_disable_evt ();

  old_exit = ksi_int_data->exit_catch;
  new_exit = ksi_add_catch (ksi_true, 0, 0);

  ksi_int_data->exit_catch = new_exit;
  FLUSH_REGISTER_WINDOWS;
  if (setjmp (new_exit->the_catch->jmp.j_buf) == 0) {
    do {
      ksi_int_data->event_mgr->wait_event (ksi_int_data->event_mgr, tm ? timeout : -1.0);
      if (ksi_int_data->pending_events || ksi_int_data->waiting_events) {
        retval = ksi_true;
        break;
      }
    } while (tm == 0 || (timeout = wait_time - ksi_real_time ()) > 0.0);

    ksi_del_catch (new_exit);
    new_exit = 0;
  }
  ksi_int_data->exit_catch = old_exit;

  if (ksi_enable_evt () == ksi_false)
    ksi_run_pending_events ();

  if (new_exit)
    ksi_rethrow (new_exit);
  return retval;
}

ksi_obj
ksi_sleep (ksi_obj tm)
{
  double timeout;

  KSI_CHECK (tm, KSI_RATIONAL_P (tm), "sleep: invalid real in arg1");
  timeout = ksi_real_part(tm);
  KSI_CHECK (tm, timeout >= 0.0, "sleep: negative real in arg1");

  if (ksi_int_data && ksi_int_data->event_mgr && ksi_int_data->event_mgr->wait_event) {
    double wait_time = ksi_real_time () + timeout;
    while ((timeout = wait_time - ksi_real_time ()) > 0.0)
      ksi_wait_event (ksi_double2num (timeout));
  } else {
    double wait_time = ksi_real_time () + timeout;
    while ((timeout = wait_time - ksi_real_time ()) > 0.0) {
#if defined(HAVE_NANOSLEEP)
      struct timespec tv;
      tv.tv_sec  = (time_t) timeout;
      tv.tv_nsec = (long) ((timeout - tv.tv_sec) * 1000000000);
      nanosleep (&tv, 0);
#elif defined(HAVE_SLEEP) && defined(HAVE_USLEEP)
      unsigned long sec  = (unsigned long) timeout;
      unsigned long usec = (unsigned long) ((timeout - sec) * 1000000);
      if (sec)  sleep (sec);
      if (usec) usleep (usec);
#elif defined(HAVE_SLEEP)
      unsigned long sec  = (unsigned long) (timeout + 0.5);
      if (sec) sleep (sec);
#endif
    }
  }

  return ksi_void;
}


ksi_obj
ksi_disable_evt (void)
{
  if (!ksi_int_data || ksi_int_data->event_mgr == 0)
    ksi_exn_error (0, 0, "disable-async-event: events is not supported or not initialized");

  if (events_disabled++ == 0) {
    if (ksi_int_data->event_mgr->disable_async_wait)
      ksi_int_data->event_mgr->disable_async_wait (ksi_int_data->event_mgr);
  }

  return ksi_false;
}

ksi_obj
ksi_enable_evt (void)
{
  if (!ksi_int_data || ksi_int_data->event_mgr == 0)
    ksi_exn_error (0, 0, "enable-async-event: events is not supported or not initialized");

  if (events_disabled == 0)
    ksi_exn_error (0, 0, "enable-async-event: async events already enabled");

  if (--events_disabled == 0) {
    if (ksi_int_data->event_mgr->enable_async_wait)
      ksi_int_data->event_mgr->enable_async_wait (ksi_int_data->event_mgr);

    if (!events_in_progress && (ksi_int_data->pending_events || ksi_int_data->waiting_events))
      ksi_run_pending_events ();
    return ksi_true;
  }

  return ksi_false;
}


ksi_event_mgr
ksi_current_event_mgr (void)
{
  return ksi_int_data ? ksi_int_data->event_mgr : 0;
}

ksi_event_mgr
ksi_register_event_mgr (ksi_event_mgr mgr)
{
  if (ksi_int_data) {
    ksi_event_mgr old_mgr = ksi_int_data->event_mgr;

    if (ksi_int_data->event_mgr) {
      BLOCK_WAIT;
      ksi_disable_evt ();
      ksi_run_pending_events ();

      while (ksi_int_data->active_events) {
        ksi_event curr = ksi_int_data->active_events;

        curr->ops->cancel (curr);
        curr->active  = 0;
        curr->inited  = 0;
        curr->waiting = 1;
        curr->start   = 1;
        curr->ready   = 0;
        remove_event (curr, &ksi_int_data->active_events);
        append_event (curr, &ksi_int_data->waiting_events);
      }

      UNBLOCK_WAIT;

      if (ksi_int_data->event_mgr->term)
        ksi_int_data->event_mgr->term (mgr);
    }

    ksi_int_data->event_mgr = mgr;

    if (ksi_int_data->event_mgr) {
      if (ksi_int_data->event_mgr->init)
        ksi_int_data->event_mgr->init (mgr);

      if (ksi_enable_evt () == ksi_false)
        ksi_run_pending_events ();
    }

    return old_mgr;
  }

  return 0;
}

void*
ksi_wait_timer (ksi_event evt, double tm, int restart)
{
  if (!ksi_int_data || ksi_int_data->event_mgr == 0 || ksi_int_data->event_mgr->wait_timer == 0)
    ksi_exn_error (0, 0, "ksi_wait_timer: events is not supported or not initialized");
  return ksi_int_data->event_mgr->wait_timer (ksi_int_data->event_mgr, evt, tm, restart);
}

void
ksi_cancel_timer (ksi_event evt, void *data)
{
  if (!ksi_int_data || ksi_int_data->event_mgr == 0 || ksi_int_data->event_mgr->cancel_timer == 0)
    ksi_exn_error (0, 0, "ksi_cancel_timer: events is not supported or not initialized");
  ksi_int_data->event_mgr->cancel_timer (ksi_int_data->event_mgr, evt, data);
}

void*
ksi_wait_input (ksi_event evt, int fd, int restart)
{
  if (!ksi_int_data || ksi_int_data->event_mgr == 0 || ksi_int_data->event_mgr->wait_input == 0)
    ksi_exn_error (0, 0, "ksi_wait_input: events is not supported or not initialized");
  return ksi_int_data->event_mgr->wait_input (ksi_int_data->event_mgr, evt, fd, restart);
}

void
ksi_cancel_input (ksi_event evt, void *data)
{
  if (!ksi_int_data || ksi_int_data->event_mgr == 0 || ksi_int_data->event_mgr->cancel_input == 0)
    ksi_exn_error (0, 0, "ksi_cancel_input: events is not supported or not initialized");
  ksi_int_data->event_mgr->cancel_input (ksi_int_data->event_mgr, evt, data);
}

void*
ksi_wait_output (ksi_event evt, int fd, int restart)
{
  if (!ksi_int_data || ksi_int_data->event_mgr == 0 || ksi_int_data->event_mgr->wait_output == 0)
    ksi_exn_error (0, 0, "ksi_wait_output: events is not supported or not initialized");
  return ksi_int_data->event_mgr->wait_output (ksi_int_data->event_mgr, evt, fd, restart);
}

void
ksi_cancel_output (ksi_event evt, void *data)
{
  if (!ksi_int_data || ksi_int_data->event_mgr == 0 || ksi_int_data->event_mgr->cancel_output == 0)
    ksi_exn_error (0, 0, "ksi_cancel_output: events is not supported or not initialized");
  ksi_int_data->event_mgr->cancel_output (ksi_int_data->event_mgr, evt, data);
}

void*
ksi_wait_signal (ksi_event evt, int sig, int restart)
{
  if (!ksi_int_data || ksi_int_data->event_mgr == 0 || ksi_int_data->event_mgr->wait_signal == 0)
    ksi_exn_error (0, 0, "ksi_wait_signal: events is not supported or not initialized");
  return ksi_int_data->event_mgr->wait_signal (ksi_int_data->event_mgr, evt, sig, restart);
}

void
ksi_cancel_signal (ksi_event evt, void *data)
{
  if (!ksi_int_data || ksi_int_data->event_mgr == 0 || ksi_int_data->event_mgr->cancel_signal == 0)
    ksi_exn_error (0, 0, "ksi_cancel_signal: events is not supported or not initialized");
  ksi_int_data->event_mgr->cancel_signal (ksi_int_data->event_mgr, evt, data);
}

void*
ksi_wait_idle (ksi_event evt, int restart)
{
  if (!ksi_int_data || ksi_int_data->event_mgr == 0 || ksi_int_data->event_mgr->wait_idle == 0)
    ksi_exn_error (0, 0, "ksi_wait_idle: events is not supported or not initialized");
  return ksi_int_data->event_mgr->wait_idle (ksi_int_data->event_mgr, evt, restart);
}

void
ksi_cancel_idle (ksi_event evt, void *data)
{
  if (!ksi_int_data || ksi_int_data->event_mgr == 0 || ksi_int_data->event_mgr->cancel_idle == 0)
    ksi_exn_error (0, 0, "ksi_cancel_idle: events is not supported or not initialized");
  ksi_int_data->event_mgr->cancel_idle (ksi_int_data->event_mgr, evt, data);
}


/*------------------------------------------------------------------
 * ready
 */

static const char*
ready_name (ksi_event x)
{
  return "ready";
}

static void
ready_init (ksi_event x)
{
}

static void
ready_setup (ksi_event x)
{
  ksi_run_event (x, 0, 0);
}

static void
ready_cancel (ksi_event x)
{
}

static int
ready_invoke (ksi_event x, void *data)
{
  x->result = ksi_apply_1_with_catch (x->action, (ksi_obj) x);
  return 1;
}

static struct Ksi_Event_Tag ready_ops =
{
  ready_name,
  ready_init,
  ready_setup,
  ready_cancel,
  ready_invoke
};

ksi_obj
ksi_ready_event (ksi_obj proc)
{
  ksi_event x;

  KSI_CHECK (proc, KSI_PROC_P (proc), "make-ready-event: invalid procedure in arg1");

  x = (ksi_event) ksi_malloc (sizeof *x);
  x->o.itag   = KSI_TAG_EVENT;
  x->ops    = &ready_ops;
  x->state  = ksi_data->sym_inactive;
  x->action = proc;
  x->result = ksi_void;

  return (ksi_obj) x;
}


/* -----------------------------------------------------------------
 * timer
 */

typedef struct Ksi_Timer
{
  struct Ksi_Event	evt;
  void			*timer_data;
  double		time;               /* время срабатывания таймера */
  double		timeout;            /* величина таймаута */
} *ksi_timer;


static const char*
timer_name (ksi_event x)
{
  double tm;

  if (x->state != ksi_data->sym_wait)
      return ksi_local(ksi_aprintf ("timer %ls", ksi_obj2str(x->state)));

  tm = ((ksi_timer) x) -> time - ksi_real_time ();
  return ksi_local(ksi_aprintf ("timer %f", tm));
}

static void
timer_init (ksi_event x)
{
  ((ksi_timer) x) -> time = ksi_real_time () + ((ksi_timer) x) -> timeout;
}

static void
timer_setup (ksi_event x)
{
  double tm = ((ksi_timer) x) -> time - ksi_real_time ();
  if (tm <= 0.0)
    ksi_run_event (x, 0, 0);
  else
    ((ksi_timer) x) -> timer_data = ksi_wait_timer (x, tm, 0);
}

static void
timer_cancel (ksi_event x)
{
  if (((ksi_timer) x) -> timer_data)
    {
      ksi_cancel_timer (x, ((ksi_timer) x) -> timer_data);
      ((ksi_timer) x) -> timer_data = 0;
    }
}

static int
timer_invoke (ksi_event x, void *data)
{
  if (((ksi_timer) x) -> timer_data == data)
    ((ksi_timer) x) -> timer_data = 0;

  x->state  = ksi_data->sym_timeout;
  x->result = ksi_apply_1_with_catch (x->action, (ksi_obj) x);
  return 1;
}

static struct Ksi_Event_Tag timer_ops =
{
  timer_name,
  timer_init,
  timer_setup,
  timer_cancel,
  timer_invoke
};

ksi_obj
ksi_timer_event (ksi_obj tm, ksi_obj proc)
{
  ksi_timer x;
  double timeout;

  KSI_CHECK (tm, KSI_RATIONAL_P (tm), "make-timer-event: invalid rational in arg1");
  timeout = ksi_real_part(tm);
  KSI_CHECK (tm, timeout >= 0.0, "make-timer-event: negative in arg1");
  KSI_CHECK (proc, KSI_PROC_P (proc), "make-timer-event: invalid procedure in arg2");

  x = (ksi_timer) ksi_malloc (sizeof *x);
  x->evt.o.itag = KSI_TAG_EVENT;
  x->evt.ops    = &timer_ops;
  x->evt.state  = ksi_data->sym_inactive;
  x->evt.action = proc;
  x->evt.result = ksi_void;
  x->timeout    = timeout;

  return (ksi_obj) x;
}


/* -----------------------------------------------------------------
 * port events
 */

typedef struct Ksi_PortReady
{
  struct Ksi_Event evt;
  ksi_port port;
  void *timer_data;
  void *r_data;
  void *w_data;
  double tick;               /* период проверки готовности */
  double timeout;            /* величина таймаута */
  double time;               /* время наступления таймаута */
  unsigned r_wait : 1;       /* wait for read */
  unsigned w_wait : 1;       /* wait for write */
  unsigned r_ready : 1;      /* read ready */
  unsigned w_ready : 1;      /* write ready */
  unsigned no_timeout : 1;
} *ksi_port_ready;


static const char*
port_name (ksi_event x)
{
  ksi_port_ready e = ((ksi_port_ready) x);

  if (e->r_wait)
      return ksi_local(ksi_aprintf ("input-ready %ls", ksi_obj2str((ksi_obj) e->port)));
  if (e->w_wait)
      return ksi_local(ksi_aprintf ("output-ready %ls", ksi_obj2str((ksi_obj) e->port)));

  return "port-ready";
}

static void
port_init (ksi_event e)
{
  ((ksi_port_ready) e) -> time = ksi_real_time () + ((ksi_port_ready) e) -> timeout;
}

static void
port_setup (ksi_event x)
{
  ksi_port_ready e = ((ksi_port_ready) x);
  double tm;

  if (e->port->closed) {
    ksi_run_event ((ksi_event) e, 0, 0);
    return;
  }

  if (e->r_wait) {
    int fd = e->port->ops->input_fd (e->port);
    e->r_data = ksi_wait_input ((ksi_event) e, fd, 0);
  }
  if (e->w_wait) {
    int fd = e->port->ops->output_fd (e->port);
    e->w_data = ksi_wait_output ((ksi_event) e, fd, 0);
  }

  if ((e->r_data || !e->r_wait) && (e->w_data || !e->w_wait)) {
    if (!e->no_timeout) {
      tm = e->time - ksi_real_time ();
      if (tm <= 0.0)
        ksi_run_event ((ksi_event) e, 0, 0);
      else
        e->timer_data = ksi_wait_timer ((ksi_event) e, tm, 0);
    }
  } else {
    if (e->r_wait && (e->r_ready || e->port->ops->input_ready (e->port))) {
      e->r_ready = 1;
      ksi_run_event ((ksi_event) e, 0, 0);
      return;
    }

    if (e->w_wait && (e->w_ready || e->port->ops->output_ready (e->port))) {
      e->w_ready = 1;
      ksi_run_event ((ksi_event) e, 0, 0);
      return;
    }

    tm = ksi_real_time () + e->tick;
    if (!e->no_timeout && e->time < tm)
      tm = e->time;

    tm = tm - ksi_real_time ();
    if (tm <= 0.0)
      ksi_run_event ((ksi_event) e, 0, 0);
    else
      e->timer_data = ksi_wait_timer ((ksi_event) e, tm, 0);
  }
}

static void
port_cancel (ksi_event x)
{
  ksi_port_ready e = ((ksi_port_ready) x);

  if (e->timer_data) {
    ksi_cancel_timer ((ksi_event) e, e->timer_data);
    e->timer_data = 0;
  }
  if (e->r_data) {
    ksi_cancel_input ((ksi_event) e, e->r_data);
    e->r_data = 0;
  }
  if (e->w_data) {
    ksi_cancel_output ((ksi_event) e, e->w_data);
    e->w_data = 0;
  }
}

static int
port_invoke (ksi_event x, void *data)
{
  ksi_port_ready e = ((ksi_port_ready) x);

  if (e->port->closed)
    return 1;

  if (data) {
    if (data == e->r_data) {
      e->r_ready = 1;
      e->r_data  = 0;
    }
    if (data == e->w_data) {
      e->w_ready = 1;
      e->w_data  = 0;
    }
    if (data == e->timer_data) {
      e->timer_data = 0;
    }
  }

  if (e->r_data) {
    ksi_cancel_input ((ksi_event) e, e->r_data);
    e->r_data = 0;
  }
  if (e->w_data) {
    ksi_cancel_output ((ksi_event) e, e->w_data);
    e->w_data = 0;
  }
  if (e->timer_data) {
    ksi_cancel_timer ((ksi_event) e, e->timer_data);
    e->timer_data = 0;
  }

  if (e->r_wait && (e->r_ready || e->port->ops->input_ready (e->port))) {
    e->r_ready = 0;
    x->result  = ksi_apply_1_with_catch (x->action, (ksi_obj) e);
  } else if (e->w_wait && (e->w_ready || e->port->ops->output_ready (e->port))) {
    e->w_ready = 0;
    x->result  = ksi_apply_1_with_catch (x->action, (ksi_obj) e);
  } else if (!e->no_timeout && ksi_real_time () <= e->time) {
    x->state  = ksi_data->sym_timeout;
    x->result = ksi_apply_1_with_catch (x->action, (ksi_obj) x);
    return 1;
  }

  if (KSI_EXN_P (x->result))
    return 1;

  e->time  = ksi_real_time () + e->timeout;
  port_setup ((ksi_event) e);
  return 0;
}


static struct Ksi_Event_Tag port_ops =
{
  port_name,
  port_init,
  port_setup,
  port_cancel,
  port_invoke
};


ksi_obj
ksi_input_event (ksi_obj tm, ksi_obj port, ksi_obj proc)
{
  double time;
  ksi_port_ready x;

  if (tm == ksi_true || tm == ksi_false)
    time = 0.0;
  else
    {
      KSI_CHECK (tm, KSI_RATIONAL_P (tm), "make-input-event: invalid real in arg1");
      time = ksi_real_part (tm);
      KSI_CHECK (tm, time >= 0.0, "make-input-event: negative in arg1");
    }

  KSI_CHECK (port, KSI_INPUT_PORT_P (port), "make-input-event: invalid input port in arg2");
  KSI_CHECK (proc, KSI_PROC_P (proc), "make-input-event: invalid procedure in arg3");

  x = (ksi_port_ready) ksi_malloc (sizeof *x);
  x->evt.o.itag = KSI_TAG_EVENT;
  x->evt.ops    = &port_ops;
  x->evt.state  = ksi_data->sym_inactive;
  x->evt.action = proc;
  x->evt.result = ksi_void;
  x->port       = (ksi_port) port;
  x->r_wait     = 1;
  x->tick       = 0.1;
  x->timeout    = time;
  x->no_timeout = (tm == ksi_true ? 1 : 0);

  return (ksi_obj) x;
}

ksi_obj
ksi_output_event (ksi_obj tm, ksi_obj port, ksi_obj proc)
{
  double time;
  ksi_port_ready x;

  if (tm == ksi_true || tm == ksi_false)
    time = 0.0;
  else
    {
      KSI_CHECK (tm, KSI_RATIONAL_P (tm), "make-output-event: invalid real in arg1");
      time = ksi_real_part (tm);
      KSI_CHECK (tm, time >= 0.0, "make-output-event: negative in arg1");
    }
  KSI_CHECK (port, KSI_OUTPUT_PORT_P (port), "make-output-event: invalid output port in arg2");
  KSI_CHECK (proc, KSI_PROC_P (proc), "make-output-event: invalid procedure in arg3");

  x = (ksi_port_ready) ksi_malloc (sizeof *x);
  x->evt.o.itag = KSI_TAG_EVENT;
  x->evt.ops    = &port_ops;
  x->evt.state  = ksi_data->sym_inactive;
  x->evt.action = proc;
  x->evt.result = ksi_void;
  x->port       = (ksi_port) port;
  x->w_wait     = 1;
  x->tick       = 0.1;
  x->timeout    = time;
  x->no_timeout = (tm == ksi_true ? 1 : 0);

  return (ksi_obj) x;
}

static ksi_obj
ksi_event_port (ksi_obj x)
{
  KSI_CHECK (x, KSI_EVT_P (x), "event-port: invalid event in arg1");
  KSI_CHECK (x, ((ksi_event) x) -> ops == &port_ops, "event-port: invalid event in arg1");
  return (ksi_obj) ((ksi_port_ready) x) -> port;
}

static ksi_obj
ksi_event_port_tick (ksi_obj x, ksi_obj tm)
{
  KSI_CHECK (x, KSI_EVT_P (x), "event-port-tick: invalid event in arg1");
  KSI_CHECK (x, ((ksi_event) x) -> ops == &port_ops, "event-port-tick: invalid event in arg1");

  if (tm)
    {
      double tick;
      KSI_CHECK (tm, KSI_RATIONAL_P (tm), "event-port-tick: invalid event in arg2");
      tick = ksi_real_part (tm);
      KSI_CHECK (tm, tick > 0.0, "event-port-tick: nonpositive in arg2");
      ((ksi_port_ready) x) -> tick = tick;
    }

  return ksi_double2num (((ksi_port_ready) x) -> tick);
}

void
ksi_cancel_port_events (ksi_port port)
{
  ksi_event curr, next;

  if (!ksi_int_data)
    return;

  for (curr = ksi_int_data->active_events; curr; curr = next) {
    next = curr->next;
    if (curr->ops == &port_ops && ((ksi_port_ready) curr) -> port == port)
      ksi_stop_event ((ksi_obj) curr);
  }

  ksi_run_pending_events ();
}


/* -----------------------------------------------------------------
 * idle events
 */

typedef struct Ksi_Idle
{
  struct Ksi_Event evt;
  void *data;
} *ksi_idle;

static const char*
idle_name (ksi_event x)
{
  return "idle";
}

static void
idle_init (ksi_event x)
{
}

static void
idle_setup (ksi_event x)
{
  ((ksi_idle) x) -> data = ksi_wait_idle (x, 1);
}

static void
idle_cancel (ksi_event x)
{
  if (((ksi_idle) x) -> data)
    {
      ksi_cancel_idle (x, ((ksi_idle) x) -> data);
      ((ksi_idle) x) -> data = 0;
    }
}

static int
idle_invoke (ksi_event x, void *data)
{
  x->result = ksi_apply_1_with_catch (x->action, (ksi_obj) x);
  if (KSI_EXN_P (x->result))
    return 1;
  return 0;
}

static struct Ksi_Event_Tag idle_ops =
{
  idle_name,
  idle_init,
  idle_setup,
  idle_cancel,
  idle_invoke
};

ksi_obj
ksi_idle_event (ksi_obj proc)
{
  ksi_idle x;
  KSI_CHECK (proc, KSI_PROC_P (proc), "make-idle-event: invalid procedure in arg1");

  x = (ksi_idle) ksi_malloc (sizeof *x);
  x->evt.o.itag = KSI_TAG_EVENT;
  x->evt.ops    = &idle_ops;
  x->evt.state  = ksi_data->sym_inactive;
  x->evt.action = proc;
  x->evt.result = ksi_void;

  return (ksi_obj) x;
}

/* -----------------------------------------------------------------
 * signal events
 */

typedef struct Ksi_Signal
{
  struct Ksi_Event	evt;
  void			*data;
  int			sig;
} *ksi_signal;

static const char*
signal_name (ksi_event x)
{
    return ksi_local(ksi_aprintf ("signal %d", ((ksi_signal) x) -> sig));
}

static void
signal_init (ksi_event x)
{
}

static void
signal_setup (ksi_event x)
{
  ((ksi_signal) x) -> data = ksi_wait_signal (x, ((ksi_signal) x) -> sig, 1);
}

static void
signal_cancel (ksi_event x)
{
  if (((ksi_signal) x) -> data)
    {
      ksi_cancel_signal (x, ((ksi_signal) x) -> data);
      ((ksi_idle) x) -> data = 0;
    }
}

static int
signal_invoke (ksi_event x, void *data)
{
  x->result = ksi_apply_1_with_catch (x->action, (ksi_obj) x);
  if (KSI_EXN_P (x->result))
    return 1;
  return 0;
}

static struct Ksi_Event_Tag signal_ops =
{
  signal_name,
  signal_init,
  signal_setup,
  signal_cancel,
  signal_invoke
};

ksi_obj
ksi_signal_event (ksi_obj sig, ksi_obj proc)
{
  ksi_signal x;
  KSI_CHECK (sig, KSI_UINT_P (sig), "make-signal-event: invalid signal in arg2");
  KSI_CHECK (proc, KSI_PROC_P (proc), "make-signal-event: invalid procedure in arg2");

  x = (ksi_signal) ksi_malloc (sizeof *x);
  x->evt.o.itag = KSI_TAG_EVENT;
  x->evt.ops    = &signal_ops;
  x->evt.state  = ksi_data->sym_inactive;
  x->evt.action = proc;
  x->evt.result = ksi_void;
  x->sig        = ksi_num2ulong (sig, "make-signal-event");

  return (ksi_obj) x;
}


static struct Ksi_Prim_Def defs [] =
{
  { L"event?",			ksi_event_p,		KSI_CALL_ARG1, 1 },
  { L"event-state",		ksi_event_state,	KSI_CALL_ARG1, 1 },
  { L"event-result",		ksi_event_result,	KSI_CALL_ARG1, 1 },
  { L"event-procedure",		ksi_event_procedure,	KSI_CALL_ARG1, 1 },

  { L"start-event",		ksi_start_event,	KSI_CALL_ARG1, 1 },
  { L"stop-event",		ksi_stop_event,		KSI_CALL_ARG1, 1 },
  { L"wait-event",		ksi_wait_event,		KSI_CALL_ARG1, 0 },
  { L"sleep",			ksi_sleep,		KSI_CALL_ARG1, 1 },
  { L"disable-async-events",	ksi_disable_evt,	KSI_CALL_ARG0, 0 },
  { L"enable-async-events",	ksi_enable_evt,		KSI_CALL_ARG0, 0 },

  { L"make-ready-event",	ksi_ready_event,	KSI_CALL_ARG1, 1 },
  { L"make-timer-event",	ksi_timer_event,	KSI_CALL_ARG2, 2 },
  { L"make-idle-event",		ksi_idle_event,		KSI_CALL_ARG1, 1 },
  { L"make-signal-event",	ksi_signal_event,	KSI_CALL_ARG2, 2 },

  { L"make-input-event",	ksi_input_event,	KSI_CALL_ARG3, 3 },
  { L"make-output-event",	ksi_output_event,	KSI_CALL_ARG3, 3 },
  { L"event-port",		ksi_event_port,		KSI_CALL_ARG1, 1 },
  { L"event-port-tick",		ksi_event_port_tick,	KSI_CALL_ARG2, 1 },

  { 0 }
};


void
ksi_init_events (void)
{
    ksi_env env = ksi_get_lib_env(L"ksi", L"core", L"event", 0);
    ksi_reg_unit (defs, env);
}

void
ksi_term_events (void)
{
    ksi_register_event_mgr (0);
}

 /* End of code */
