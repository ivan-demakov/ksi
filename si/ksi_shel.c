/*
 * ksi_shel.c
 * shell utils
 *
 * Copyright (C) 1997-2010, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Fri Dec  5 12:56:37 1997
 * Last Update:   Sun Aug 29 22:22:38 2010
 *
 */

#include "ksi_type.h"
#include "ksi_util.h"
#include "ksi_gc.h"


#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif
#ifdef HAVE_PWD_H
#  include <pwd.h>
#endif
#ifdef HAVE_SYS_STAT_H
#  include <sys/stat.h>
#endif
#ifdef HAVE_STDIO_H
#  include <stdio.h>
#endif

#ifdef __WATCOMC__
#  include <direct.h>
#  define NAMLEN(dirent) strlen((dirent)->d_name)
#elif defined(_MSC_VER)
#  include <direct.h>
#elif defined(HAVE_DIRENT_H)
#  include <dirent.h>
#  define NAMLEN(dirent) strlen((dirent)->d_name)
#else
#  define dirent direct
#  define NAMLEN(dirent) (dirent)->d_namlen
#  if HAVE_SYS_NDIR_H
#    include <sys/ndir.h>
#  endif
#  if HAVE_SYS_DIR_H
#    include <sys/dir.h>
#  endif
#  if HAVE_NDIR_H
#    include <ndir.h>
#  endif
#endif

#ifdef _MSC_VER
#  include <io.h>
#  define F_OK 0
#  define W_OK 2
#  define R_OK 4
#  define X_OK R_OK
#endif

#if defined(_MSC_VER)
#  define S_ISDIR(x)  (((x) & _S_IFMT) == _S_IFDIR)
#  define S_ISCHR(x)  (((x) & _S_IFMT) == _S_IFCHR)
#  define S_ISREG(x)  (((x) & _S_IFMT) == _S_IFREG)
#  define S_ISFIFO(x) (((x) & _S_IFMT) == _S_IFIFO)
#endif

#ifdef __GNUC__
extern char **environ;
#endif


ksi_obj
ksi_getenv (ksi_obj str)
{
    int i;
    char *ptr;
    ksi_obj res, x, y;

    if (str) {
        KSI_CHECK(str, KSI_STR_P(str), "getenv: invalid string in arg1");

        ptr = ksi_local(KSI_STR_PTR(str));
        if (ptr)
            ptr = getenv (ptr);
        res = (ptr ? ksi_str02string(ksi_utf(ptr)) : ksi_false);
    } else {
        res = ksi_nil;
        for (i = 0; environ[i] != 0; i++) {
            ptr = strchr (environ[i], '=');
            if (ptr) {
                x = ksi_str02string(ksi_to_utf(environ[i], (int)(ptr - environ[i])));
                y = ksi_str02string(ksi_utf(ptr + 1));
                res = ksi_cons(ksi_cons(x, y), res);
            }
        }
    }

    return res;
}

ksi_obj
ksi_syscall (ksi_obj str)
{
    char *ptr;
    KSI_CHECK (str, KSI_STR_P (str), "system: invalid string in arg1");

    ptr = ksi_local(KSI_STR_PTR(str));
    return ksi_long2num(system(ptr));
}


ksi_obj
ksi_getcwd (void)
{
  char *ptr = ksi_get_cwd ();

#if defined(WIN32) || defined(OS2) || defined(MSDOS)
  {
    int i;
    for (i = 0; ptr [i]; i++)
      if (ptr [i] == '/')
        ptr [i] = '\\';
  }
#endif

  return ksi_str02string(ksi_utf(ptr));
}

ksi_obj
ksi_chdir (ksi_obj dir)
{
  const char *ptr = ksi_mk_filename(dir, "chdir");
  ptr = ksi_tilde_expand(ptr);

  if (chdir(ptr) == 0)
    return ksi_true;

  ksi_exn_error(0, dir, "chdir: %m");
  return ksi_false;
}

ksi_obj
ksi_mkdir (ksi_obj dir, ksi_obj msk)
{
  const char *ptr = ksi_mk_filename(dir, "mkdir");
  ptr = ksi_tilde_expand(ptr);

#if defined(WIN32) || defined(OS2) || defined(MSDOS)
  if (mkdir (ptr) == 0)
    return ksi_true;
#else
  KSI_CHECK (msk, !msk || KSI_UINT_P (msk), "mkdir: invalid mode in arg2");
  if (mkdir (ptr, msk ? ksi_num2ulong (msk, "mkdir") : 0777) == 0)
    return ksi_true;
#endif

  ksi_exn_error(0, dir, "mkdir: %m");
  return ksi_false;
}

ksi_obj
ksi_rmdir (ksi_obj dir)
{
  const char *ptr = ksi_mk_filename(dir, "rmdir");
  ptr = ksi_tilde_expand(ptr);

  if (rmdir (ptr) == 0)
    return ksi_true;

  ksi_exn_error(0, dir, "rmdir: %m");
  return ksi_false;
}

ksi_obj
ksi_file_exists (ksi_obj fn)
{
  const char *ptr = ksi_mk_filename(fn, "file-exists?");
  ptr = ksi_tilde_expand(ptr);

  if (access(ptr, F_OK) == 0)
    return ksi_true;

  return ksi_false;
}

ksi_obj
ksi_delete_file (ksi_obj fn)
{
  const char *ptr = ksi_mk_filename(fn, "delete-file");
  ptr = ksi_tilde_expand(ptr);

  if (unlink(ptr) == 0)
    return ksi_true;

  ksi_exn_error(0, fn, "delete-file: %m");
  return ksi_false;
}

ksi_obj
ksi_rename_file (ksi_obj old, ksi_obj new)
{
  const char *ptr_old = ksi_mk_filename(old, "rename-file");
  const char *ptr_new = ksi_mk_filename(new, "rename-file");
  ptr_old = ksi_tilde_expand(ptr_old);
  ptr_new = ksi_tilde_expand(ptr_new);

#ifdef HAVE_RENAME
  if (rename(ptr_old, ptr_new) == 0)
    return ksi_true;
#else
  if (link(ptr_old, ptr_new) == 0) {
    if (unlink(ptr_old) == 0)
      return ksi_true;
    /* unlink failed. remove new filename */
    unlink(ptr_new);
  }
#endif

  ksi_exn_error(0, old, "rename-file: %m");
  return ksi_false;
}


ksi_obj
ksi_stat (ksi_obj x)
{
  struct stat st;
  const char *fname;

  fname = ksi_mk_filename(x, "stat");
  fname = ksi_tilde_expand(fname);

  if (stat (fname, &st) == 0) {
    ksi_obj r = (ksi_obj) ksi_alloc_vector (15, KSI_TAG_VECTOR);

    KSI_VEC_REF (r, 0)  = ksi_ulong2num (st.st_dev);
    KSI_VEC_REF (r, 1)  = ksi_ulong2num (st.st_ino);
    KSI_VEC_REF (r, 2)  = ksi_ulong2num (st.st_mode);
    KSI_VEC_REF (r, 3)  = ksi_ulong2num (st.st_nlink);
    KSI_VEC_REF (r, 4)  = ksi_ulong2num (st.st_uid);
    KSI_VEC_REF (r, 5)  = ksi_ulong2num (st.st_gid);
#ifdef HAVE_STRUCT_STAT_ST_RDEV
    KSI_VEC_REF (r, 6)  = ksi_ulong2num (st.st_rdev);
#else
    KSI_VEC_REF (r, 6)  = ksi_false;
#endif
    KSI_VEC_REF (r, 7)  = ksi_ulong2num (st.st_size);
    KSI_VEC_REF (r, 8)  = ksi_ulonglong2num (st.st_atime);
    KSI_VEC_REF (r, 9)  = ksi_ulonglong2num (st.st_mtime);
    KSI_VEC_REF (r, 10) = ksi_ulonglong2num (st.st_ctime);
#ifdef HAVE_STRUCT_STAT_ST_BLKSIZE
    KSI_VEC_REF (r, 11) = ksi_ulong2num (st.st_blksize);
#else
    KSI_VEC_REF (r, 11) = ksi_ulong2num (4096);
#endif
#ifdef HAVE_STRUCT_STAT_ST_BLOCKS
    KSI_VEC_REF (r, 12) = ksi_ulong2num (st.st_blocks);
#else
    KSI_VEC_REF (r, 12) = ksi_ulong2num ((st.st_size + 511) / 512);
#endif

    if (S_ISREG (st.st_mode))
      KSI_VEC_REF (r, 13) = ksi_str02sym (L"regular");
#ifdef S_ISDIR
    else if (S_ISDIR (st.st_mode))
      KSI_VEC_REF (r, 13) = ksi_str02sym (L"directory");
#endif
#ifdef S_ISLNK
    else if (S_ISLNK (st.st_mode))
      KSI_VEC_REF (r, 13) = ksi_str02sym (L"symlink");
#endif
#ifdef S_ISBLK
    else if (S_ISBLK (st.st_mode))
      KSI_VEC_REF (r, 13) = ksi_str02sym (L"block-special");
#endif
#ifdef S_ISCHR
    else if (S_ISCHR (st.st_mode))
      KSI_VEC_REF (r, 13) = ksi_str02sym (L"char-special");
#endif
#ifdef S_ISFIFO
    else if (S_ISFIFO (st.st_mode))
      KSI_VEC_REF (r, 13) = ksi_str02sym (L"fifo");
#endif
#ifdef S_ISSOCK
    else if (S_ISSOCK (st.st_mode))
      KSI_VEC_REF (r, 13) = ksi_str02sym (L"socket");
#endif
    else
      KSI_VEC_REF (r, 13) = ksi_str02sym (L"unknown");

    KSI_VEC_REF (r, 14) = ksi_ulong2num ((~S_IFMT) & st.st_mode);

    return r;
  }

  ksi_exn_error(0, x, "stat: %m");
  return ksi_false;
}


static struct Ksi_ETag tc_dir =
{
  L"directory",
  ksi_default_tag_equal,
  ksi_default_tag_print
};

struct Ksi_Dir
{
  struct Ksi_EObj ko;
#ifdef _MSC_VER
  intptr_t hd;
  int eof;
  struct _finddata_t file;
#else
  DIR *dir;
#endif
};

ksi_obj
ksi_opendir (ksi_obj name)
{
  const char *dirname;
#ifdef _MSC_VER
  char *fname;
#else
  DIR *dir;
#endif
  struct Ksi_Dir *obj;

  dirname = ksi_mk_filename (name, "opendir");
  dirname = ksi_tilde_expand (dirname);

#ifdef _MSC_VER

  fname = (char*) ksi_malloc_data (strlen (dirname) + 6);
  strcpy (fname, dirname);
  strcat (fname, "\\*.*");

  obj = (struct Ksi_Dir*) ksi_malloc (sizeof *obj);
  obj->ko.o.itag = KSI_TAG_EXTENDED;
  obj->ko.etag = &tc_dir;
  obj->hd      = _findfirst (fname, &obj->file);
  obj->eof     = 0;
  if (obj->hd != -1)
    return (ksi_obj) obj;

  ksi_exn_error(0, name, "opendir: cannot open dir %s", fname);

#else

  dir = opendir (dirname);
  if (dir) {
    obj = (struct Ksi_Dir*) ksi_malloc (sizeof *obj);
    obj->ko.o.itag = KSI_TAG_EXTENDED;
    obj->ko.etag = &tc_dir;
    obj->dir     = dir;
    return (ksi_obj) obj;
  }

#endif

  return ksi_false;
}

ksi_obj
ksi_readdir (ksi_obj dir)
{
#ifdef _MSC_VER
    ksi_obj str;
#else
    struct dirent *rd;
#endif

    KSI_CHECK(dir, KSI_EXT_IS(dir, &tc_dir), "readdir: invalid directory");

#ifdef _MSC_VER

    if (((struct Ksi_Dir*) dir)->hd == -1)
        ksi_exn_error(0, dir, "readdir: directory is closed");

    if (((struct Ksi_Dir*) dir)->eof)
        return ksi_eof;

    str = ksi_str02string(ksi_utf(((struct Ksi_Dir*) dir)->file.name));

    if (_findnext (((struct Ksi_Dir*) dir)->hd, &((struct Ksi_Dir*) dir)->file) != 0)
        ((struct Ksi_Dir*) dir)->eof = 1;

    return str;

#else

    if (((struct Ksi_Dir*) dir)->dir == 0)
        ksi_exn_error(0, dir, "readdir: directory is closed");

    rd = readdir(((struct Ksi_Dir*) dir)->dir);
    return (rd ? ksi_str02string(ksi_to_utf(rd->d_name, NAMLEN(rd))) : ksi_eof);

#endif
}

ksi_obj
ksi_closedir (ksi_obj dir)
{
  KSI_CHECK (dir, KSI_EXT_IS (dir, &tc_dir), "closedir: invalid dir");

#ifdef _MSC_VER
  if (((struct Ksi_Dir*) dir) -> hd == -1)
    ksi_exn_error(0, dir, "closedir: directory is closed");
  _findclose (((struct Ksi_Dir*) dir) -> hd);
  ((struct Ksi_Dir*) dir) -> hd = -1;
#else
  if (((struct Ksi_Dir*) dir) -> dir == 0)
    ksi_exn_error(0, dir, "closedir: directory is closed");
  closedir (((struct Ksi_Dir*) dir) -> dir);
  ((struct Ksi_Dir*) dir) -> dir = 0;
#endif

  return ksi_void;
}


ksi_obj
ksi_exp_fname (ksi_obj x, ksi_obj dir)
{
  const char *fname;

  if (dir && dir != ksi_false)
    x = ksi_cons(dir, x);

  fname = ksi_mk_filename(x, "path-list->file-name");
  if (dir != ksi_false)
    fname = ksi_expand_file_name(fname);
  return ksi_str02string(ksi_utf(fname));
}

ksi_obj
ksi_split_fname (ksi_obj x)
{
    wchar_t *ptr;
    int len, i;
    ksi_obj res;

    KSI_CHECK(x, KSI_STR_P(x), "split-file-name: invalid string in arg1");

    ptr = KSI_STR_PTR(x);
    len = KSI_STR_LEN(x);
    res = ksi_nil;
    for (i = 0; i < len; /**/) {
#if defined(MSDOS) || defined(WIN32) || defined(OS2)
        if (ptr[i] == L'\\' || ptr[i] == L'/')
#else
        if (ptr[i] == L'/')
#endif
        {
            res = ksi_cons(ksi_str2string(ptr, i), res);
            ptr += i+1;
            len -= i+1;
            i = 0;
        } else {
            i++;
        }
    }
    res = ksi_cons(ksi_str2string(ptr, i), res);

    return ksi_reverse_x (res);
}

ksi_obj
ksi_split_path (ksi_obj x)
{
    wchar_t *ptr;
    int len, i;
    ksi_obj res;

    KSI_CHECK(x, KSI_STR_P(x), "split-path: invalid string in arg1");

    ptr = KSI_STR_PTR(x);
    len = KSI_STR_LEN(x);
    res = ksi_nil;
    for (i = 0; i < len; /**/) {
#if defined(MSDOS) || defined(WIN32) || defined(OS2)
        if (ptr[i] == L';')
#else
        if (ptr[i] == L':')
#endif
        {
            res = ksi_cons(ksi_str2string(ptr, i), res);
            ptr += i+1;
            len -= i+1;
            i = 0;
        } else {
            i++;
        }
    }
    res = ksi_cons(ksi_str2string(ptr, i), res);

    return ksi_reverse_x (res);
}

ksi_obj
ksi_has_suffix_p (ksi_obj name, ksi_obj suff)
{
  const char *ptr = ksi_mk_filename(name, "file-name-has-suffix?");

  KSI_CHECK(suff, KSI_STR_P(suff), "file-name-has-suffix?: invalid string in arg2");

  return (ksi_has_suffix(ptr, ksi_local(KSI_STR_PTR(suff))) ? ksi_true : ksi_false);
}


 /* End of code */
