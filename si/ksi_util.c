/*
 * ksi_util.c
 *
 * Copyright (C) 2006-2010, 2014, ivan demakov.
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Fri Dec 22 22:31:28 2006
 * Last Update:   Thu Jul 31 03:29:29 2014
 *
 */


#include "ksi_util.h"
#include "ksi_gc.h"
#include "ksi_printf.h"

/* This should be figured out by autoconf.  */
#ifdef CLK_TCK
# define CLKTCK CLK_TCK
# ifdef CLOCKS_PER_SEC
#  if defined (unix) || defined (__unix)
#   define LACK_CLOCK
    /* This is because clock() might be POSIX rather than ANSI.
       This occurs on HP-UX machines */
#  endif
# endif
#else
# ifdef CLOCKS_PER_SEC
#  define CLKTCK CLOCKS_PER_SEC
# else
#  define LACK_CLOCK
#  define CLKTCK 60
# endif
#endif

#ifdef HAVE_PWD_H
#  include <pwd.h>
#endif
#ifdef HAVE_SYS_STAT_H
#  include <sys/stat.h>
#endif
#if defined(WIN32)
#  include <direct.h>
#endif


#if defined(WIN32)

static int proc_times_supported = 0;

static void init_win_times()
{
    static int inited = 0;
    if (!inited) {
        FILETIME x;
        proc_times_supported = GetProcessTimes (GetCurrentProcess (), &x, &x, &x, &x);
        inited = 1;
    }
}

static inline double
ft2d (FILETIME *ft)
{
    /* convert FILETIME to seconds */
    return (ft->dwHighDateTime * 4294967296.0 + ft->dwLowDateTime) / 10000000.0;
}
#endif


double
ksi_cpu_time ()
{
#if defined(WIN32)
    FILETIME dummy, user;
    init_win_times();
    if (proc_times_supported) {
        GetProcessTimes (GetCurrentProcess (), &dummy, &dummy, &dummy, &user);
        return ft2d (&user);
    }

    return GetTickCount () / 1000.0;
#elif defined(HAVE_TIMES)

    struct tms time_buffer;
    times (&time_buffer);
    return ((double) time_buffer.tms_utime) / CLKTCK;

#elif !defined(LACK_CLOCK)
    return ((double) clock ()) / CLKTCK;

#else
    static time_t base = time (0);
    return time (0) - base;

#endif
}


double
ksi_eval_time ()
{
#if defined(WIN32)
    FILETIME dummy, user, sys;
    DWORD    tmp;

    init_win_times();
    if (proc_times_supported) {
        GetProcessTimes (GetCurrentProcess (), &dummy, &dummy, &sys, &user);
        user.dwHighDateTime += sys.dwHighDateTime;
        tmp = user.dwLowDateTime + sys.dwLowDateTime;
        if (tmp < user.dwLowDateTime) user.dwHighDateTime += 1;
        user.dwLowDateTime = tmp;

        return ft2d (&user);
    }

    return GetTickCount () / 1000.0;

#elif defined(HAVE_TIMES)
    struct tms time_buffer;
    times (&time_buffer);
    return ((double) time_buffer.tms_utime + time_buffer.tms_stime) / CLKTCK;

#elif !defined(LACK_CLOCK)
    return ((double) clock ()) / CLKTCK;

#else
    static time_t base = time (0);
    return time (0) - base;

#endif
}


double
ksi_real_time ()
{
#ifdef HAVE_GETTIMEOFDAY
    struct timeval tbuf;
    gettimeofday (&tbuf, 0);
    return (tbuf.tv_usec / 1000000.0) + tbuf.tv_sec;

#elif defined(HAVE_FTIME)
    struct timeb buf;
    ftime (&buf);
    return buf.time + (buf.millitm / 1000.0);

#else
    return time (0);

#endif
}

struct tm *
ksi_gmtime (time_t it, struct tm *bt)
{
#ifdef HAVE_GMTIME_R
    struct tm tm;
    struct tm *pm = gmtime_r(&it, &tm);
#else
    struct tm *pm = gmtime (&it);
#endif
    if (pm) {
        if (bt)
            *bt = *pm;
        return bt;
    }

    return 0;
}

struct tm *
ksi_localtime (time_t it, struct tm *bt)
{
#ifdef HAVE_LOCALTIME_R
    struct tm tm;
    struct tm *pm = localtime_r(&it, &tm);
#else
    struct tm *pm = localtime (&it);
#endif
    if (pm) {
        if (bt)
            *bt = *pm;
        return bt;
    }

    return 0;
}


int
ksi_has_suffix (const char* fname, const char* suffix)
{
    size_t l1 = strlen (fname);
    size_t l2 = strlen (suffix);
    if (l1 < l2)
        return 0;

    while (l2) {
#if defined(WIN32) || defined(OS2) || defined(MSDOS) || defined(__CYGWIN32__) || defined(__CYGWIN__)
        if (tolower (fname[l1]) != tolower (suffix[l2]))
            return 0;
        --l1; --l2;
#else
        if (fname[--l1] != suffix[--l2])
            return 0;
#endif
    }

    return 1;
}


const char *
ksi_tilde_expand (const char *name)
{
#ifdef unix
    int len;
#endif
    const char *p;
    char *res;

#ifdef unix
    struct passwd *pwPtr;
#endif

    if (name[0] != '~')
        return name;

    if ((name[1] == '/') || (name[1] == '\0')) {
        p = getenv ("HOME");
        if (p == NULL)
            p = ".";

        res = (char *)ksi_malloc_data(strlen(p) + strlen(name));
        strcat(strcpy(res, p), name+1);
        return res;
    }

#ifdef unix

    for (p = &name[1]; (*p != 0) && (*p != '/'); p++); /* Null body;  just find end of name. */

    len = p - (name+1);
    res = (char *) ksi_malloc_data (len + 1);
    memcpy (res, name+1, len);
    res[len] = '\0';

    pwPtr = getpwnam(res);
    if (pwPtr == NULL) {
        endpwent ();
        name = ".";
    } else {
        name = pwPtr->pw_dir;
    }
    res = (char *)ksi_malloc_data(strlen(name) + strlen(p) + 1);
    strcat(strcpy(res, name), p);
    endpwent ();

    return res;

#else

    return name;

#endif
}

char*
ksi_get_cwd ()
{
    int len = KSI_MAX_PATH_LENGTH;
    char *dir = ksi_malloc_data(len);

    while (1) {
        if (getcwd(dir, len))
            break;
        len *= 2;
        dir = ksi_realloc(dir, len);
    }

#if defined(WIN32) || defined(OS2) || defined(MSDOS)
    {
        int i;
        for (i = 0; dir [i]; i++)
            if (dir [i] == '\\')
                dir [i] = '/';
    }
#endif

    return dir;
}

static char*
ksi_absolute_path (char* fname)
{
    char *cwd, *dir, *dst, *src;

#if defined(WIN32) || defined(OS2) || defined(MSDOS)
    if (fname [0] == '/' || (fname [0] != '\0' && fname [1] == ':'))
        dir = fname;
#else
    if (fname [0] == '/')
        dir = fname;
#endif
    else {
        cwd = ksi_get_cwd ();
        dir = (char*) ksi_malloc_data (strlen (fname) + strlen (cwd) + 2);
        strcpy (dir, cwd);
        strcat (dir, "/");
        strcat (dir, fname);
    }

    dst = dir;
    src = dir;
    while (*src) {
        if (*src == '/') {
            src++;
            if (*src == '.') {
                if (*(src+1) == '/' || *(src+1) == '\0') /* skip "/./" */
                    src++;
                else if (*(src+1) == '.' && (*(src+2) == '/' || *(src+2) == '\0')) {  /* have "/../" */
                    src += 2;
                    while (dst > dir) {   /* back up to the previous '/'  */
                        --dst;
#if defined(WIN32) || defined(OS2) || defined(MSDOS)
                        if (*dst == '/' || *dst == ':')
#else
                            if (*dst == '/')
#endif
                                break;
                    }
                }
                else
                    *dst++ = '/';
            }
            else if (*src != '/') /* skip "//" */
                *dst++ = '/';
        }
        else
            *dst++ = *src++;
    }
    *dst = '\0';                /* zero terminator */

    return dir;
}


char*
ksi_expand_file_name (const char* fname)
{
    size_t len = strlen (fname);
    char *ptr = ksi_malloc_data (len+1);
    strcpy (ptr, fname);

#if defined(WIN32) || defined(OS2) || defined(MSDOS)
    {
        size_t i;
        for (i = 0; i < len; ++i)
            if (ptr[i] == '\\')
                ptr[i] = '/';
    }
#endif

    ptr = (char*) ksi_tilde_expand (ptr);
    ptr = ksi_absolute_path (ptr);

#if defined(WIN32) || defined(OS2) || defined(MSDOS)
    {
        size_t i;
        len = strlen (ptr);
        for (i = 0; i < len; ++i)
        {
            if (ptr[i] == '/')
                ptr[i] = '\\';
            else
                ptr[i] = tolower (ptr[i]);
        }
    }
#elif defined(__CYGWIN32__) || defined(__CYGWIN__)
    {
        size_t i;
        len = strlen (ptr);
        for (i = 0; i < len; ++i)
            ptr[i] = tolower (ptr[i]);
    }
#endif

    return ptr;
}

void
ksi_random_bits(char *buf, int size)
{
    if (!buf || size <= 0)
        return;

#ifdef RANDOM_FILE
    {
        static int rand_fd = -1;
        if (rand_fd < 0) {
            rand_fd = open(RANDOM_FILE, O_RDONLY);
        }
        if (rand_fd >= 0) {
            if (read(rand_fd, buf, size) == size)
                return;
        }
    }
#endif
    {
        static int randbits = 0;

        if (!randbits) {
            int max = RAND_MAX;
            do { ++randbits; } while ((max=max>>1));
#ifdef HAVE_GETTIMEOFDAY
            {
                struct timeval buf;
                gettimeofday(&buf, 0);
                srand(buf.tv_usec);
            }
#elif defined(HAVE_FTIME)
            {
                struct timeb buf;
                ftime(&buf);
                srand(buf.millitm);
            }
#else
            srand(time(0));
#endif
            rand(); // Skip first
        }
        for (;;) {
            int randnum = rand();
            int randmax = RAND_MAX;
            for (;;) {
                if (size <= 0)
                    return;
                --size;
                *buf++ = (randnum & 0xff);
                randnum >>= 8;
                randmax >>= 8;
                if (randmax == 0)
                    break;
            }
        }
    }
}

char *
ksi_base64 (const char *buf, int size)
{
    const char alphabet[] = "ABCDEFGH" "IJKLMNOP" "QRSTUVWX" "YZabcdef" "ghijklmn" "opqrstuv" "wxyz0123" "456789+/";
    const char padchar = '=';
    int padlen = 0;
    char *tmp = ksi_malloc_data(((size * 4) / 3 + 4) * sizeof(*tmp));

    int i = 0, j, k, l, m;
    char *out = tmp;
    while (i < size) {
        int chunk = 0;
        chunk |= ((int) buf[i++]) << 16;
        if (i == size) {
            padlen = 2;
        } else {
            chunk |= ((int) buf[i++]) << 8;
            if (i == size) padlen = 1;
            else chunk |= ((int) buf[i++]);
        }

        j = (chunk & 0x00fc0000) >> 18;
        k = (chunk & 0x0003f000) >> 12;
        l = (chunk & 0x00000fc0) >> 6;
        m = (chunk & 0x0000003f);
        *out++ = alphabet[j];
        *out++ = alphabet[k];
        if (padlen > 1) *out++ = padchar;
        else *out++ = alphabet[l];
        if (padlen > 0) *out++ = padchar;
        else *out++ = alphabet[m];
    }
    *out = '\0';

    return tmp;
}


unsigned
ksi_hash_str (const wchar_t* str, int len, unsigned n)
{
#define HASH_FUN 3

#if HASH_FUN == 0

  unsigned h = 0;
  while (len)
    h = ((h << 8) + ((unsigned) (str[--len]))) % n;
  return h;

#elif HASH_FUN == 1

  unsigned h = 0;
  while (len)
    h = (h*16777619) ^ ((unsigned) str[--len]);
  return h % n;

#elif HASH_FUN == 2

  unsigned h = 0;
  while (len)
    h = 0x63c63cd9 * h + 0x9c39c33d + ((unsigned) str[--len]);
  return h % n;

#elif HASH_FUN == 3

#define HASHC h = *str++ + 65599 * h
  unsigned h;
  unsigned loop;

  if (len == 0)
    return 0;
  h = 0;
  loop = (len + 8 - 1) >> 3;
  switch (len & (8 - 1))
    {
    case 0:
      do {    HASHC;
      case 7: HASHC;
      case 6: HASHC;
      case 5: HASHC;
      case 4: HASHC;
      case 3: HASHC;
      case 2: HASHC;
      case 1: HASHC;
      } while (--loop);
    }
  return h % n;

#else

  register unsigned h = 0;
  while (len)
    h += (h << 3) + ((unsigned) str[--len]);
  return h % n;

#endif
}


wchar_t *
ksi_to_utf (const char *str, size_t len)
{
    mbstate_t state;
    size_t nbytes;
    wchar_t *res = ksi_malloc_data((len + 1) * sizeof(*res));
    wchar_t *ptr = res;

    memset(&state, '\0', sizeof(state));
    while (len > 0 && (nbytes = mbrtowc(ptr, str, len, &state)) > 0) {
        if (nbytes >= (size_t) -2)
            break;
        ptr++;
        len -= nbytes;
        str += nbytes;
    }
    *ptr = L'\0';

    return res;
}

wchar_t *
ksi_utf (const char *str)
{
    return ksi_to_utf(str, strlen(str));
}

char *
ksi_to_local (const wchar_t *str, size_t len)
{
    mbstate_t state;
    size_t num, size = len + 1;
    char *res = ksi_malloc_data(size);

    memset(&state, 0, sizeof(state));
    for (num = 0; len > 0; str++, --len) {
        char tmp[MB_LEN_MAX];
        size_t n = wcrtomb(tmp, *str, &state);
        if (n >= (size_t) -1)
            break;
        if (num + n >= size) {
            size = num + n + len * 2 + 1;
            res = ksi_realloc(res, size);
        }
        memcpy(res+num, tmp, n);
        num += n;
    }
    if (num >= size) {
        size = num + 1;
        res = ksi_realloc(res, size);
    }
    res[num] = '\0';

    return res;
}

char *
ksi_local (const wchar_t *str)
{
    return ksi_to_local(str, wcslen(str));
}


/* End of code */
