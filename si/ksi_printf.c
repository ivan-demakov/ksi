/*
 * ksi_printf.c
 *
 * Copyright (C) 2006-2010, 2014, ivan demakov.
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Sun Dec 17 22:30:33 2006
 * Last Update:   Thu Jul 31 03:28:27 2014
 *
 */

#include "ksi_printf.h"
#include "ksi_util.h"
#include "ksi_gc.h"

#include <math.h>
#include <float.h>

#define MAX_INT_DIGS ((sizeof(int) * 8) + 1)

#ifndef HAVE_LONG_LONG
# define LONG_LONG long
#endif

struct printf_spec_t
{
  int		conv;
  int		type;
  int		width;
  int		prec;
  int		base;
  unsigned	width_param : 1;
  unsigned	prec_param : 1;
  unsigned	minus : 1;
  unsigned	plus : 1;
  unsigned	space : 1;
  unsigned	sharp : 1;
  unsigned	zero : 1;
  unsigned	quote : 1;
  unsigned	upcase : 1;
  unsigned	efmt : 1;
  unsigned	g_flag : 1;
};

static
const char*
parse_spec (const char *fmt, struct printf_spec_t *spec)
{
    memset (spec, 0, sizeof *spec);
    while (*fmt) {
        switch (*fmt) {
        case '-':  spec->minus = 1; fmt++; break;
        case '+':  spec->plus  = 1; fmt++; break;
        case ' ':  spec->space = 1; fmt++; break;
        case '#':  spec->sharp = 1; fmt++; break;
        case '0':  spec->zero  = 1; fmt++; break;
        case '\'': spec->quote = 1; fmt++; break;
        default:   goto parse_width;
        }
    }

parse_width:
    if (*fmt == '*') {
        spec->width_param = 1;
        fmt++;
    }
    else {
        while (iswdigit(*fmt)) {
            spec->width *= 10;
            spec->width += *fmt - '0';
            fmt++;
        }
    }

    if (*fmt != '.')
        spec->prec = -1;
    else {
        fmt++;
        if (*fmt == '*') {
            spec->prec_param = 1;
            fmt++;
        }
        else {
            while (iswdigit(*fmt)) {
                spec->prec *= 10;
                spec->prec += *fmt - '0';
                fmt++;
            }
        }
    }

    if (*fmt == 'l') {
        fmt++;
        if (*fmt == 'l') {
            fmt++;
            spec->type = 'L';
        } else {
            spec->type = 'l';
        }
    } else if (*fmt == 'h' || *fmt ==  'L') {
        spec->type = *fmt++;
    }

    if (*fmt)
        spec->conv = *fmt++;
    return fmt;
}

static
wchar_t *
uint2str (unsigned LONG_LONG num, unsigned radix, int sign, wchar_t *buff, int buflen)
{
    wchar_t *str = buff + buflen;
    *--str = L'\0';

    while (1) {
        int r = (int) (num % radix);
        *--str = (r < 10 ? r + L'0' : (r-10) + L'a');
        num /= radix;
        if (num == 0)
            break;
    }

    if (sign < 0)
        *--str = L'-';
    else if (sign > 0)
        *--str = L'+';
    return str;
}

static
wchar_t *
int2str (LONG_LONG num, unsigned radix, wchar_t *buff, int buflen)
{
    if (num < 0)
        return uint2str(-num, radix, -1, buff, buflen);
    return uint2str(num, radix, 0, buff, buflen);
}

static
int
printf_int (ksi_buffer_t output, const wchar_t *str, struct printf_spec_t *spec)
{
    int len, size;
    wchar_t sign = 0;

    if (*str == L'-' || *str == L'+')
        sign = *str++;
    else if (spec->plus)
        sign = L'+';
    else if (spec->space)
        sign = L' ';

    if (spec->prec >= 0)
        while (*str == L'0')
            str++;

    size = len = (int)wcslen(str);
    if (sign)
        size += 1;
    if (spec->sharp) {
        if (spec->base == 2 || spec->base == 16)
            size += 2;
        else if (spec->base == 8)
            size += 1;
    }

    spec->width -= size;
    if (spec->prec >= 0) {
        if ((spec->prec -= size) < 0)
            spec->prec = 0;
        spec->width -= spec->prec;

        if (!spec->minus)
            while (--spec->width >= 0)
                ksi_buffer_put(output, L' ');
    }
    else if (spec->minus)
        spec->prec = 0;
    else if (spec->zero) {
        spec->prec  = spec->width;
        spec->width = 0;
    }
    else {
        while (--spec->width >= 0)
            ksi_buffer_put(output, L' ');
    }

    if (sign)
        ksi_buffer_put(output, sign);

    if (spec->sharp) {
        if (spec->base == 2)
            ksi_buffer_append(output, (spec->upcase ? L"0B" : L"0b"), 2);
        else if (spec->base == 16)
            ksi_buffer_append(output, (spec->upcase ? L"0X" : L"0x"), 2);
        else if (spec->base == 8)
            ksi_buffer_put(output, L'0');
    }

    while (--spec->prec >= 0)
        ksi_buffer_put(output, L'0');

    while (*str) {
        wchar_t c = *str++;
        ksi_buffer_put(output, (spec->upcase ? towupper(c) : towlower(c)));
    }

    while (--spec->width >= 0)
        ksi_buffer_put(output, L' ');
    return 0;
}

static int
printf_char (ksi_buffer_t output, wint_t c, struct printf_spec_t *spec)
{
    if (!spec->minus) {
        while (--spec->width > 0)
            ksi_buffer_put(output, L' ');
    }
    ksi_buffer_put(output, c);

    while (--spec->width > 0)
        ksi_buffer_put(output, L' ');
    return 0;
}

static int
printf_str (ksi_buffer_t output, const wchar_t *str, struct printf_spec_t *spec)
{
    int len, width;
    if (str == 0)
        str = L"(null)";

    len = (int)wcslen(str);
    if (0 < spec->prec && spec->prec < len)
        len = spec->prec;
    width = spec->width - len;

    if (!spec->minus) {
        while (--width >= 0)
            ksi_buffer_put(output, L' ');
    }
    ksi_buffer_append(output, str, len);
    while (--width >= 0)
        ksi_buffer_put(output, L' ');
    return 0;
}

static int
printf_double (ksi_buffer_t output, double num, struct printf_spec_t *spec)
{
    int i, len, wid, exp, int_digs, fra_digs, exp_digs, has_dig;
    wchar_t *num_buf, *exp_buf, sign, exp_sign;

    if (num != num)
        return printf_str(output, spec->upcase ? L"NAN" : L"nan", spec);
    if (num == num/2.0) {
        if (num < 0.0)
            return printf_str(output, spec->upcase ? L"-INF" : L"-inf", spec);
        if (num > 0.0)
            return printf_str(output, spec->upcase ? L"+INF" : L"+inf", spec);
    }

    if (spec->prec < 0)
        spec->prec = FLT_DIG;

    sign = 0;
    if (ksi_copysign(1.0, num) < 0.0) {
        sign = L'-';
        num  = -num;
    } else if (spec->plus) {
        sign = L'+';
    } else if (spec->space) {
        sign = L' ';
    }

    if (spec->g_flag) {
        if (num < 1.0e-4 || num >= pow(10.0, (double) spec->prec))
            spec->efmt = 1;
        if (spec->prec == 0)
            spec->prec = 1;
    }

    exp = 0;
    exp_sign = L'+';
    if (spec->efmt)  {
        if (num == 0.0) {
            if (spec->g_flag)
                spec->efmt = 0;
        } else {
            exp = (int) floor(log10(num));
            num /= pow(10.0, (double) exp);
            if (exp < 0) {
                exp_sign = L'-';
                exp = -exp;
            }
        }
    }

    int_digs = (num > 1.0 ? 1 + (int) floor(log10(num)) : 1);
    fra_digs = (spec->g_flag ? spec->prec - int_digs : spec->prec);
    num = floor(num * pow(10.0, (double) fra_digs) + 0.5);
    if (num > 0.0 && (int) log10(num) + 1 > int_digs + fra_digs)
        int_digs++;

    len = int_digs + fra_digs + 4;
    num_buf = alloca(len * sizeof(*num_buf));
    num_buf = num_buf + len;

    *--num_buf = L'\0';
    has_dig = 0;
    for (i = 0; i < fra_digs; i++) {
        int c = ((int) fmod(num, 10.0)) + '0';
        *--num_buf = btowc(c);
        num = floor (num / 10.0);
        if (!has_dig && c == '0' && spec->g_flag)
            num_buf++;
        else
            has_dig = 1;
    }
    if (has_dig || spec->sharp)
        *--num_buf = L'.';
    for (i = 0; i < int_digs; i++) {
        int c = ((int) fmod(num, 10.0)) + '0';
        *--num_buf = btowc(c);
        num = floor(num / 10.0);
        if (num < DBL_EPSILON)
            break;
    }

    exp_digs = 0;
    exp_buf  = 0;
    if (spec->efmt) {
        exp_buf = alloca(MAX_INT_DIGS * sizeof(*exp_buf));
        exp_buf += MAX_INT_DIGS;
        while (exp || exp_digs < 2) {
            int c = exp % 10 + '0';
            *--exp_buf = btowc(c);
            exp /= 10;
            exp_digs++;
        }
    }

    wid = len = (int)wcslen(num_buf);
    if (sign)
        wid += 1;
    if (exp_digs > 0)
        wid += exp_digs + 2;

    spec->width -= wid;

    if (spec->minus) {
        if (sign)
            ksi_buffer_put(output, sign);
    }
    else if (spec->zero) {
        if (sign)
            ksi_buffer_put(output, sign);
        while (--spec->width >= 0)
            ksi_buffer_put(output, L'0');
    }
    else {
        while (--spec->width >= 0)
            ksi_buffer_put(output, L' ');
        if (sign)
            ksi_buffer_put(output, sign);
    }

    ksi_buffer_append(output, num_buf, len);
    if (exp_digs > 0) {
        ksi_buffer_put(output, (spec->upcase ? L'E' : L'e'));
        ksi_buffer_put(output, exp_sign);
        ksi_buffer_append(output, exp_buf, exp_digs);
    }

    while (--spec->width >= 0)
        ksi_buffer_put(output, L' ');
    return 0;
}

void
ksi_bufvprintf(ksi_buffer_t output, const char *fmt, va_list args) {
#ifdef HAVE_LONG_LONG
    wchar_t int_buf[SIZEOF_LONG_LONG * 8 + 2]; /* max digits + sign + '\0' */
#else
    wchar_t int_buf[SIZEOF_LONG * 8 + 2];
#endif

    if (fmt) {
        while (*fmt) {
            if (*fmt != '%') {
                ksi_buffer_put(output, btowc(*fmt));
                fmt++;
            } else {
                struct printf_spec_t spec;
                const char *ptr;
                const wchar_t *str;
                double d;
                int i;

                ptr = fmt;
                fmt = parse_spec(fmt+1, &spec);
                if (spec.width_param) {
                    spec.width = va_arg(args, int);
                    if (spec.width < 0) {
                        spec.minus = 1;
                        spec.width = -spec.width;
                    }
                }
                if (spec.prec_param)
                    spec.prec = va_arg(args, int);

                switch (spec.conv) {
                case '%':
                    ksi_buffer_put(output, L'%');
                    break;
                case 'd': case 'i':
                    spec.base = 10;
                    if (spec.type == 'L') {
                        LONG_LONG n = va_arg(args, LONG_LONG);
                        str = int2str(n, spec.base, int_buf, sizeof(int_buf)/sizeof(int_buf[0]));
                    } else if (spec.type == 'l') {
                        long n = va_arg(args, long);
                        str = int2str(n, spec.base, int_buf, sizeof(int_buf)/sizeof(int_buf[0]));
                    } else if (spec.type == 'h') {
                        short n = (short) va_arg(args, int);
                        str = int2str(n, spec.base, int_buf, sizeof(int_buf)/sizeof(int_buf[0]));
                    } else {
                        int n = va_arg(args, int);
                        str = int2str(n, spec.base, int_buf, sizeof(int_buf)/sizeof(int_buf[0]));
                    }
                    printf_int(output, str, &spec);
                    break;
                case 'u':
                    spec.base = 10;
                print_uint:
                    if (spec.type == 'L') {
                        unsigned LONG_LONG n = va_arg(args, unsigned LONG_LONG);
                        str = uint2str(n, spec.base, 0, int_buf, sizeof(int_buf)/sizeof(int_buf[0]));
                    } else if (spec.type == 'l') {
                        unsigned long n = va_arg(args, unsigned long);
                        str = uint2str(n, spec.base, 0, int_buf, sizeof(int_buf)/sizeof(int_buf[0]));
                    } else if (spec.type == 'h') {
                        unsigned short n = (unsigned short) va_arg(args, unsigned int);
                        str = uint2str(n, spec.base, 0, int_buf, sizeof(int_buf)/sizeof(int_buf[0]));
                    } else {
                        unsigned int n = va_arg(args, unsigned int);
                        str = uint2str(n, spec.base, 0, int_buf, sizeof(int_buf)/sizeof(int_buf[0]));
                    }
                    printf_int(output, str, &spec);
                    break;
                case 'B':
                    spec.upcase = 1;
                case 'b':
                    spec.base = 2;
                    goto print_uint;
                case 'o':
                    spec.base = 8;
                    goto print_uint;
                case 'X':
                    spec.upcase = 1;
                case 'x':
                    spec.base = 16;
                    goto print_uint;
                case 'p':
                    spec.base  = 16;
                    spec.sharp = 1;
                    spec.plus  = 0;
                    spec.space = 0;
                    ptr = va_arg(args, void *);
                    str = uint2str((KSI_WORD) ptr, spec.base, 0, int_buf, sizeof(int_buf)/sizeof(int_buf[0]));
                    printf_int(output, str, &spec);
                    break;
                case 'c':
                    if (spec.type == 'l' || spec.type == 'L') {
                        wint_t c = (wint_t) va_arg(args, int);
                        printf_char(output, c, &spec);
                    } else {
                        i = va_arg(args, int);
                        printf_char(output, btowc(i), &spec);
                    }
                    break;
                case 'C': {
                    wint_t c = (wint_t) va_arg(args, int);
                    printf_char(output, c, &spec);
                    break;
                }
                case 's':
                    if (spec.type == 'l' || spec.type == 'L') {
                        str = va_arg(args, wchar_t *);
                        printf_str(output, str, &spec);
                    } else {
                        char *p = va_arg(args, char *);
                        if (!p) {
                            printf_str(output, 0, &spec);
                        } else {
                            printf_str(output, ksi_utf(p), &spec);
                        }
                    }
                    break;
                case 'S':
                    str = va_arg(args, wchar_t *);
                    printf_str(output, str, &spec);
                    break;
                case 'm': {
#if HAVE_STRERROR_R
                    char buf[256];
#ifdef STRERROR_R_CHAR_P
                    printf_str(output, ksi_utf(strerror_r(errno, buf, sizeof buf)), &spec);
#else
                    if (strerror_r(errno, buf, sizeof buf) == 0)
                        printf_str(output, ksi_utf(buf), &spec);
                    else
                        printf_str(output, L"unknown error", &spec);
#endif
#else
                    char *ptr = strerror(errno);
                    printf_str(output, ksi_utf(ptr), &spec);
#endif
                    break;
                }
                case 'E':
                    spec.upcase = 1;
                case 'e':
                    spec.efmt   = 1;
                    goto print_float;
                case 'G':
                    spec.upcase = 1;
                case 'g':
                    spec.g_flag = 1;
                    goto print_float;
                case 'f':
                print_float:
                    if (spec.type == 'l' || spec.type == 'L')
                        d = (double) va_arg(args, long double);
                    else
                        d = va_arg(args, double);
                    printf_double(output, d, &spec);
                    break;
                default:
                    while (ptr < fmt) {
                        wint_t c = btowc(*ptr);
                        ksi_buffer_put(output, c);
                        ptr++;
                    }
                }
            } /* if (*fmt != '%') else */
        } /* while (*fmt) */
    } /* if (fmt) */
}

void
ksi_bufprintf (ksi_buffer_t buf, const char *fmt, ...)
{
  va_list args;

  va_start(args, fmt);
  ksi_bufvprintf(buf, fmt, args);
  va_end(args);
}

wchar_t *
ksi_avprintf(const char *fmt, va_list args) {
    ksi_buffer_t output = ksi_new_buffer(0, 0);
    ksi_bufvprintf(output, fmt, args);
    ksi_buffer_put(output, L'\0');
    return ksi_buffer_data(output);
}

wchar_t *
ksi_aprintf (const char* fmt, ...)
{
  wchar_t *ptr;
  va_list args;

  va_start(args, fmt);
  ptr = ksi_avprintf(fmt, args);
  va_end(args);

  return ptr;
}


 /* End of code */
