/*
 * read.c
 *
 * Copyright (C) 2009-2010, 2015, ivan demakov.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Wed Sep 16 23:01:59 2009
 *
 */

#include "ksi.h"

#include <stdio.h>


int
main(int argc, char *argv[])
{
    struct Ksi_Context ctx;
    ksi_obj x;

    ksi_init_std_ports(0, 1, 2);

    ksi_init_context(&ctx, 0);
    ksi_set_current_context(&ctx);
    ctx.errlog_priority = KSI_ERRLOG_ALL;

    ksi_setlocale(ksi_str02sym(L"LC_ALL"), ksi_str02string(L""));

    x = ksi_read(ksi_current_input_port());
    ksi_write(x, 0);
    ksi_newline(0);

    ksi_term();
    return 0;
}

 /* End of code */
