/*
 * ksi_klos.c
 * ksi object system
 *
 * Copyright (C) 1997-2010, 2014, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Thu Jun 12 19:50:51 1997
 * Last Update:   Sat Aug  2 04:11:47 2014
 *
 */

#include "ksi_type.h"
#include "ksi_klos.h"
#include "ksi_env.h"
#include "ksi_proc.h"
#include "ksi_port.h"
#include "ksi_hash.h"
#include "ksi_printf.h"
#include "ksi_gc.h"
#include "ksi_jump.h"


#define sym_cname ksi_data->sym_cname
#define sym_dsupers ksi_data->sym_dsupers
#define sym_dslots ksi_data->sym_dslots
#define sym_defargs ksi_data->sym_defargs
#define sym_cpl ksi_data->sym_cpl
#define sym_slots ksi_data->sym_slots
#define sym_nfields ksi_data->sym_nfields
#define sym_gns ksi_data->sym_gns

#define sym_gname ksi_data->sym_gname
#define sym_methods ksi_data->sym_methods
#define sym_arity ksi_data->sym_arity

#define sym_gf ksi_data->sym_gf
#define sym_specs ksi_data->sym_specs
#define sym_proc ksi_data->sym_proc
#define sym_combination ksi_data->sym_combination
#define sym_after ksi_data->sym_after
#define sym_before ksi_data->sym_before
#define sym_around ksi_data->sym_around
#define sym_primary ksi_data->sym_primary

#define key_initform ksi_data->key_initform
#define key_initarg ksi_data->key_initarg
#define key_defargs ksi_data->key_defargs
#define key_type ksi_data->key_type
#define key_name ksi_data->key_name
#define key_dsupers ksi_data->key_dsupers
#define key_dslots ksi_data->key_dslots
#define key_specs ksi_data->key_specs
#define key_proc ksi_data->key_proc
#define key_gf ksi_data->key_gf
#define key_arity ksi_data->key_arity
#define key_combination ksi_data->key_combination

#define Top ksi_data->Top
#define Object ksi_data->Object
#define Class ksi_data->Class
#define Generic ksi_data->Generic
#define Method ksi_data->Method
#define Proc ksi_data->Proc
#define Entity ksi_data->Entity
#define Boolean ksi_data->Boolean
#define Char ksi_data->Char
#define String ksi_data->String
#define Symbol ksi_data->Symbol
#define Number ksi_data->Number
#define Complex ksi_data->Complex
#define Real ksi_data->Real
#define Rational ksi_data->Rational
#define Integer ksi_data->Integer
#define Vector ksi_data->Vector
#define Pair ksi_data->Pair
#define List ksi_data->List
#define Null ksi_data->Null
#define Procedure ksi_data->Procedure
#define Keyword ksi_data->Keyword
#define Unknown ksi_data->Unknown
#define Record ksi_data->Record
#define Rtd ksi_data->Rtd



/*
 * instance
 * --------
 * Экземпляр (instance) в klos представлен структурой Ksi_Instance.
 * В этой структуре содержатся поля
 *  `klass' - указатель на класс экземпляра;
 *  `flags' - тип экземпляра;
 *  `slots' - массив слотов.
 *
 * Класс экземпляра -- это тоже некоторый экземпляр некоторого класса.
 * Класс класса называется метаклассом (что, однако не мешает метаклассу
 * быть, в свою очередь, классом и экземпляром -- смотря с какой стороны
 * смотреть).  Все метаклассы должны быть подклассами класса `<class>'.
 *
 * Поле `flags' используется для эффективности -- это массив битовых
 * флагов:
 *	I_CLASS		- экземпляр является классом
 *	I_GENERIC	- экземпляр является generic-функцией
 *	I_METHOD	- экземпляр является методом
 *	I_PURE_CLASS	- данный экземпляр является классом и позволяет
 *			  обращаться к своим слотам по смещению,
 *			  задаваемому предопределенной константой
 *	I_PURE_GENERIC	- класс данного экземпляра -- это класс <generic>.
 *			  В этом случае используется протокол вызова
 *			  генерик-функций определенный в коде на С.
 *			  (Идея взята из Stk).
 *
 * I_PURE_CLASS и I_PURE_GENERIC не может быть установлен, если
 * не установлен I_CLASS и I_GENERIC соответственно
 * (см. функцию `ksi_alloc_instance').
 *
 * Массив слотов содержит значения локальных слотов экземпляра (т.е.
 * слотов, уникальных для каждого экземпляра).
 * Описание слотов экземпляра находится в его классе (т.е. сам по себе
 * экземпляр, без ссылки на его класс, не может существовать).
 *
 * класс
 * -----
 * Класс в klos представлен экземпляром класса или подкласса `<class>'.
 * (`<class>', в свою очередь, тоже является классом -- его классом является
 * он сам).
 *
 * Экзмпляр класса `<class>' (и его подклассов) в klos имеет следующие слоты:
 *	name	- имя класса;
 *	dsupers	- список непосредственных суперклассов;
 *	dslots	- список непосредственных слотов (с опциями);
 *	defargs	- список умолчательных аргументов инициализации;
 *	cpl	- список приоритетов классов (class precedence list);
 *	nfields	- количество локальных слотов экземпляра данного класса;
 *	slots	- список всех слотов данного класса, каждый слот представлен
 *		  списком, в котором первый элемент -- имя слота,
 *		  а остальные -- опции этого слота (опции могут отсутствовать);
 *	gns	- список, каждый элемент которого описывает слот.
 *		  Элемент этого списка должен быть вектором, длина которого
 *		  должна быть больше или равна 6.
 *		  1 элемент вектора -- имя слота,
 *                2                 -- allocation
 *		  3                 -- getter
 *		  4                 -- setter
 *		  5                 -- инициализатор слота,
 *		  6                 -- тип слота,
 *
 * Имя слота -- это символ (хотя ничто не мешает, задавать в качестве
 * имени произвольный объект схемы, доступ к таким слотам будет затруднен,
 * и что важнее, макросы, определенные в "klos.scm", для создания
 * экземпляров класса (`define-class' и т.д.) не будут работать).
 *
 * allocation -- это способ выделения памяти под слот
 *
 * Getter и setter -- это либо целое положительное число (задающее
 * смещение в массиве слотов экземпляра), либо процедура чтения и записи,
 * соответственно.
 *
 * Инициализатор -- это процедура, не имеющая аргументов, которая
 * вызывается для получения начального значения слота при инициализации
 * экземпляра, в том случае, если начальное значение не задается
 * явно при создании экземпляра.
 *
 * Тип слота определяет какие значения могут хранится
 * в слоте.  При записи и инициализации слота, для проверки
 * допустимости значения, вызывается функция `type?' с записываемым
 * значением и типом слота. (См. ниже описание предиката `type?').
 *
 * Значения слотов класса `name', `dsupers' и `dslots' задаются при
 * создании класса пользователем.  Значения остальных слотов вычисляются
 * при инициализации класса.  Поскольку класс в klos является обычным
 * экземпляром, пользователь имеет возможность присвоить слотам класса
 * (при инициализации или потом) произвольные значения, однако если
 * эти значения не соответствуют соглашениям, описанным выше, последствия
 * этого не определены.
 *
 *
 * Проверка типа.
 * --------------
 * Предикат ``type? VAL TYPE'' проверяет является ли значение VAL типом TYPE.
 *
 *        TYPE           (`type?' VAL TYPE)
 *    -----------       ----------------
 *	#t		#t
 *	#f		#f
 *	class		(subclass? (class-of VAL) TYPE)
 *	procedure	(TYPE VAL)
 *	list	        (memv VAL TYPE)
 *
 * То есть,
 *
 *  - если TYPE булево значение, то результат проверки всегда равен этому
 *    значению;
 *
 *  - если TYPE -- это класс, то проверка успешна в том случае, если
 *    значение VAL -- это экземпляр этого класса или его подкласса;
 *
 *  - если TYPE -- это процедура, то результат проверки равен
 *    результату вызова этой процедуры с параметром VAL;
 *
 *  - если TYPE -- это список, то результат проверки успешен в том случае,
 *    если VAL равен (в смысле eqv?) одному из элементов этого
 *    списка.
 */


#define CONCAT(a,b) a##b

#define CLASS_SLOT_REF(x,i,s) (KSI_INST_IS ((x), I_PURE_CLASS) ?        \
                               KSI_SLOT_REF ((x), i) :                  \
                               ksi_slot_ref ((x), s))

#define GF_SLOT_REF(x,i,s) (KSI_INST_IS ((x), I_PURE_GENERIC) ? \
                            KSI_SLOT_REF ((x), i) :             \
                            ksi_slot_ref ((x), s))

#define METHOD_SLOT_REF(x,i,s) (KSI_INST_IS ((x), I_PURE_METHOD) ?      \
                                KSI_SLOT_REF ((x), i) :                 \
                                ksi_slot_ref ((x), s))

#define CNAME_OF(x)     CLASS_SLOT_REF ((x), S_CNAME, sym_cname)
#define DSUPERS_OF(x)   CLASS_SLOT_REF ((x), S_DSUPERS, sym_dsupers)
#define DSLOTS_OF(x)    CLASS_SLOT_REF ((x), S_DSLOTS, sym_dslots)
#define DEFARGS_OF(x)   CLASS_SLOT_REF ((x), S_DEFARGS, sym_defargs)
#define CPL_OF(x)       CLASS_SLOT_REF ((x), S_CPL, sym_cpl)
#define SLOTS_OF(x)     CLASS_SLOT_REF ((x), S_SLOTS, sym_slots)
#define NFIELDS_OF(x)   CLASS_SLOT_REF ((x), S_NFIELDS, sym_nfields)
#define GNS_OF(x)       CLASS_SLOT_REF ((x), S_GNS, sym_gns)

#define GNAME_OF(x)     GF_SLOT_REF ((x), S_GNAME, sym_gname)
#define METHODS_OF(x)   GF_SLOT_REF ((x), S_METHODS, sym_methods)
#define ARITY_OF(x)     GF_SLOT_REF ((x), S_ARITY, sym_arity)

#define GF_OF(x)          METHOD_SLOT_REF ((x), S_GF, sym_gf)
#define SPECS_OF(x)       METHOD_SLOT_REF ((x), S_SPECS, sym_specs)
#define PROC_OF(x)        METHOD_SLOT_REF ((x), S_PROC, sym_proc)
#define COMBINATION_OF(x) METHOD_SLOT_REF ((x), S_COMBINATION, sym_combination)
#define SUBCLASS_P(x,c)   (ksi_memq (c, CPL_OF (x)) != ksi_false)


/* ksi_get_arg
 *
 * Search KEY in arg-lists ARGS.
 *
 * Returns value for key, or ksi_void if not found.
 */

ksi_obj
ksi_get_arg (ksi_obj key, ksi_obj args, ksi_obj def)
{
    if (key == ksi_void || args == ksi_nil || args == ksi_false)
        return def ? def : ksi_void;

    while (KSI_PAIR_P (key)) {
        if (KSI_CDR (key) == ksi_nil) {
            key = KSI_CAR (key);
        } else {
            ksi_obj x = ksi_get_arg (KSI_CAR (key), args, 0);
            if (x != ksi_void)
                return x;
            key = KSI_CDR (key);
        }
    }

    while (args != ksi_nil) {
        KSI_CHECK (args, KSI_PAIR_P (args), "@get-arg: improper list in arg2");
        KSI_CHECK (args, KSI_PAIR_P (KSI_CDR (args)), "@get-arg: no value for key");

        if (key == KSI_CAR (args))
            return KSI_CAR (KSI_CDR (args));

        args = KSI_CDR (KSI_CDR (args));
    }

    return def ? def : ksi_void;
}


/*
 * some klos predicates
 */

ksi_obj
ksi_instance_p (ksi_obj x)
{
    return KSI_INST_P (x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_class_p (ksi_obj x)
{
    return KSI_CLASS_P (x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_generic_p (ksi_obj x)
{
    return KSI_GENERIC_P (x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_method_p (ksi_obj x)
{
    return KSI_METHOD_P (x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_new_next (ksi_obj gf, ksi_obj args, ksi_obj procs)
{
    ksi_next_method next;
    next = ksi_malloc(sizeof(*next));
    next -> o.itag = KSI_TAG_NEXT_METHOD;
    next -> m_gf = gf;
    next -> m_args = args;
    next -> m_procs = procs;
    return (ksi_obj) next;
}

/* ksi_new_instance
 *	allocate object ``instance''
 */

static ksi_instance
ksi_new_instance (ksi_instance cls, int n, int flags)
{
    ksi_instance inst;
    int i;

    inst = (ksi_instance) ksi_malloc (sizeof *inst + (n * sizeof (ksi_obj)));
    inst -> o.itag = KSI_TAG_INSTANCE;
    inst -> m_flags = flags;
    inst -> m_class = cls;
    inst -> m_slots = (ksi_obj*) (inst + 1);
    for (i = 0; i < n; i++)
        inst -> m_slots [i] = ksi_void;

    return inst;
}


/* ksi_obj build_gns (ksi_obj slot_list)
 *	build `getters-n-seters' for given SLOT_LIST.
 *	This function doesn't handle slot options.
 *	It serves only for booting top classes.
 *	Real procedure will be defined in `klos.scm'.
 */

static ksi_obj
build_gns (ksi_obj slot_list)
{
    int i;
    ksi_obj res = ksi_nil;

    for (i = 0; KSI_PAIR_P(slot_list); i++) {
        ksi_obj slot = KSI_CAR(slot_list);
        ksi_obj name = slot;
        ksi_obj init = ksi_void;
        ksi_obj type = ksi_void;

        if (KSI_PAIR_P(slot)) {
            name = KSI_CAR(slot);
            init = ksi_get_arg(key_initform, KSI_CDR(slot), 0);
            type = ksi_get_arg(key_type, KSI_CDR(slot), 0);
        }
        if (type == ksi_void)
            type = ksi_true;

        slot = (ksi_obj) ksi_alloc_vector(GNS_SIZE, KSI_TAG_VECTOR);
        KSI_VEC_REF(slot, GNS_NAME)   = name;
        KSI_VEC_REF(slot, GNS_ALLOC)  = ksi_str02sym(L"instance");
        KSI_VEC_REF(slot, GNS_GETTER) = ksi_long2num(i);
        KSI_VEC_REF(slot, GNS_SETTER) = ksi_long2num(i);
        KSI_VEC_REF(slot, GNS_INIT)   = init;
        KSI_VEC_REF(slot, GNS_TYPE)   = type;

        res = ksi_cons(slot, res);
        slot_list = KSI_CDR(slot_list);
    }

    return res;
}


/* ksi_instance build_top (char* name, ksi_obj dsuper, ksi_instance klass)
 *	Allocate and initialize top class.
 *	This function doesn't handle multiple inheritance,
 *	and classes that have slots.
 *	It serves only for booting top classes.
 *
 * Arguments:
 *	NAME	- name of class
 *	DSUPER	- superclass of created class
 *	klass	- class of created class (metaclass).
 */

static ksi_obj
build_top (wchar_t *name, ksi_obj dsuper, ksi_obj kls)
{
    ksi_instance res;
    ksi_obj tmp, cpl = ksi_nil, *loc = &cpl;

    for (tmp = dsuper; tmp != ksi_nil; /**/) {
        *loc = ksi_cons(tmp, ksi_nil);
        loc = & (KSI_CDR(*loc));

        tmp = KSI_SLOT_REF (tmp, S_DSUPERS);
        if (tmp != ksi_nil)
            tmp = KSI_CAR (tmp);
    }

    res = ksi_new_instance ((ksi_instance) kls, NUMBER_OF_CLASS_SLOTS, I_CLASS | I_PURE_CLASS);
    KSI_SLOT_REF (res, S_CNAME)    = ksi_str02sym (name);
    KSI_SLOT_REF (res, S_DSUPERS)  = (dsuper == ksi_nil ? ksi_nil : KSI_LIST1 (dsuper));
    KSI_SLOT_REF (res, S_DSLOTS)   = ksi_nil;
    KSI_SLOT_REF (res, S_DEFARGS)  = ksi_nil;
    KSI_SLOT_REF (res, S_CPL)      = ksi_cons((ksi_obj) res, cpl);
    KSI_SLOT_REF (res, S_SLOTS)    = ksi_nil;
    KSI_SLOT_REF (res, S_NFIELDS)  = ksi_long2num(0);
    KSI_SLOT_REF (res, S_GNS)      = ksi_nil;

    return (ksi_obj) res;
}


/* void ksi_init_top_classes
 *	Initialize top classes.
 */

void
ksi_init_top_classes ()
{
    ksi_obj slots;

    /* alloc `<class>' class */
    Class = (ksi_obj) ksi_new_instance (0,
                                        NUMBER_OF_CLASS_SLOTS,
                                        I_CLASS | I_PURE_CLASS);
    KSI_CLASS_OF (Class) = (ksi_instance) Class;

    /* build top classes */
    Top       = build_top (L"<top>",		ksi_nil, Class);
    Object    = build_top (L"<object>",		Top,     Class);

    Boolean   = build_top (L"<boolean>",		Top,	Class);
    Char      = build_top (L"<char>",		Top,	Class);
    String    = build_top (L"<string>",		Top,	Class);
    Symbol    = build_top (L"<symbol>",		Top,	Class);
    Keyword   = build_top (L"<keyword>",		Top,	Class);

    List      = build_top (L"<list>",		Top,	Class);
    Pair      = build_top (L"<pair>",		List,	Class);
    Null      = build_top (L"<null>",		List,	Class);

    Vector    = build_top (L"<vector>",		Top,	Class);

    Number    = build_top (L"<number>",		Top, Class);
    Complex   = build_top (L"<complex>",		Number, Class);
    Real      = build_top (L"<real>",		Complex, Class);
    Rational  = build_top (L"<rational>",		Real, Class);
    Integer   = build_top (L"<integer>",		Rational, Class);

    Unknown   = build_top (L"<unknown>",		Top, Class);

    /* we can initialize `<class>' now. */
    slots = KSI_LIST8 (KSI_LIST5 (sym_cname,
                                  key_initarg, key_name,
                                  key_type, Symbol),
                       KSI_LIST5 (sym_dsupers,
                                  key_initarg, key_dsupers,
                                  key_type, List),
                       KSI_LIST5 (sym_dslots,
                                  key_initarg, key_dslots,
                                  key_type, List),
                       KSI_LIST7 (sym_defargs,
                                  key_initarg, key_defargs,
                                  key_initform, ksi_nil,
                                  key_type, List),
                       KSI_LIST3 (sym_cpl,
                                  key_type, List),
                       KSI_LIST3 (sym_slots,
                                  key_type, List),
                       KSI_LIST5 (sym_nfields,
                                  key_initform, ksi_long2num(0),
                                  key_type, Integer),
                       KSI_LIST3 (sym_gns,
                                  key_type, List));

    KSI_SLOT_REF (Class, S_CNAME) = ksi_str02sym (L"<class>");
    KSI_SLOT_REF (Class, S_DSUPERS) = KSI_LIST1 (Object);
    KSI_SLOT_REF (Class, S_DSLOTS) = slots;
    KSI_SLOT_REF (Class, S_DEFARGS) = ksi_nil;
    KSI_SLOT_REF (Class, S_CPL) = KSI_LIST3 (Class, Object, Top);
    KSI_SLOT_REF (Class, S_SLOTS) = slots;
    KSI_SLOT_REF (Class, S_NFIELDS) = ksi_long2num (NUMBER_OF_CLASS_SLOTS);
    KSI_SLOT_REF (Class, S_GNS) = build_gns (slots);


    /* build record classes */
    Rtd = build_top (L"<rtd>", Class, Class);
    KSI_SLOT_REF (Rtd, S_SLOTS) = slots;
    KSI_SLOT_REF (Rtd, S_NFIELDS) = ksi_long2num (NUMBER_OF_CLASS_SLOTS);
    KSI_SLOT_REF (Rtd, S_GNS) = build_gns (slots);

    Record = build_top (L"<record>", Top, Class);

    /* build procedure classes */
    Proc = build_top (L"<procedure-class>", Class, Class);
    KSI_SLOT_REF (Proc, S_SLOTS) = slots;
    KSI_SLOT_REF (Proc, S_NFIELDS) = ksi_long2num (NUMBER_OF_CLASS_SLOTS);
    KSI_SLOT_REF (Proc, S_GNS) = build_gns (slots);

    Entity = build_top (L"<entity-class>", Proc, Class);
    KSI_SLOT_REF (Entity, S_SLOTS) = slots;
    KSI_SLOT_REF (Entity, S_NFIELDS) = ksi_long2num (NUMBER_OF_CLASS_SLOTS);
    KSI_SLOT_REF (Entity, S_GNS) = build_gns (slots);

    Procedure = build_top (L"<procedure>", Top, Proc);

    /* build <method> */
    slots = KSI_LIST4 (KSI_LIST3 (sym_gf,
                                  key_initarg, key_gf),
                       KSI_LIST3 (sym_specs,
                                  key_initarg, key_specs),
                       KSI_LIST5 (sym_combination,
                                  key_initarg, key_combination,
                                  key_initform, sym_primary),
                       KSI_LIST3 (sym_proc,
                                  key_initarg, key_proc));

    Method = build_top (L"<method>", Object, Class);
    KSI_SLOT_REF (Method, S_DSLOTS) = slots;
    KSI_SLOT_REF (Method, S_SLOTS) = slots;
    KSI_SLOT_REF (Method, S_NFIELDS) = ksi_long2num (NUMBER_OF_METHOD_SLOTS);
    KSI_SLOT_REF (Method, S_GNS) = build_gns (slots);

    /* build <generic> */
    slots = KSI_LIST3 (KSI_LIST3 (sym_gname,
                                  key_initarg, key_name),
                       KSI_LIST3 (sym_methods,
                                  key_initform, ksi_nil),
                       KSI_LIST5 (sym_arity,
                                  key_initarg, key_arity,
                                  key_initform, ksi_true));

    Generic = build_top (L"<generic>", Object, Entity);
    KSI_SLOT_REF (Generic, S_DSLOTS) = slots;
    KSI_SLOT_REF (Generic, S_SLOTS) = slots;
    KSI_SLOT_REF (Generic, S_NFIELDS) = ksi_long2num (NUMBER_OF_GENERIC_SLOTS);
    KSI_SLOT_REF (Generic, S_GNS) = build_gns (slots);
}

ksi_obj
ksi_klos_val (ksi_obj sym, ksi_obj def)
{
    ksi_env env = ksi_lib_env(KSI_LIST2(ksi_str02sym(L"ksi"), ksi_str02sym(L"klos")), 0);
    if (env) {
        ksi_envrec rec = ksi_lookup_env(env, sym);
        if (rec)
            return rec->val;
    }
    return def;
}

static ksi_obj
find_slot (ksi_obj gns, ksi_obj name)
{
    ksi_obj slot;

    for (/**/; gns != ksi_nil; gns = KSI_CDR(gns)) {
        KSI_CHECK(gns, KSI_PAIR_P(gns), "find_slot: invalid gns");

        slot = KSI_CAR(gns);
        KSI_CHECK(gns, KSI_VEC_P(slot) && KSI_VEC_LEN(slot) >= GNS_SIZE, "find_slot: invalid gns");

        if (KSI_VEC_REF(slot, GNS_NAME) == name)
            return slot;
    }

    return 0;
}

static ksi_obj
get_slot_value (ksi_obj obj, ksi_obj name, ksi_obj (*apply) (ksi_obj, ksi_obj), const char *func)
{
    ksi_obj cls = (ksi_obj) KSI_CLASS_OF (obj);
    if (KSI_EINT_P (name)) {
        int i = ksi_num2long (name, func);
        if (0 <= i && i < ksi_num2long (NFIELDS_OF (cls), func))
            return KSI_SLOT_REF (obj, i);
        return ksi_inst_slot_missing (cls, obj, name, 0);
    } else {
        ksi_obj slot = find_slot (GNS_OF (cls), name);
        if (!slot) {
            return ksi_inst_slot_missing (cls, obj, name, 0);
        } else {
            ksi_obj getter = KSI_VEC_REF (slot, GNS_GETTER);
            if (KSI_EINT_P (getter))
                return KSI_SLOT_REF (obj, ksi_num2long (getter, func));
            return apply (getter, obj);
        }
    }
}


/* ksi_slot_ref
 */

ksi_obj
ksi_slot_ref (ksi_obj obj, ksi_obj name)
{
    ksi_obj val;
    KSI_CHECK(obj, KSI_INST_P(obj), "slot-ref: invalid instance in arg1");

    val = get_slot_value(obj, name, ksi_apply_1, "slot-ref");
    if (val == ksi_void)
        return ksi_inst_slot_unbound((ksi_obj) KSI_CLASS_OF(obj), obj, name);

    return val;
}


ksi_obj
ksi_slot_set (ksi_obj obj, ksi_obj name, ksi_obj val)
{
    ksi_obj cls;
    KSI_CHECK(obj, KSI_INST_P(obj), "slot-set!: invalid instance in arg1");

    cls = (ksi_obj) KSI_CLASS_OF(obj);
    if (KSI_EINT_P(name)) {
        int i = ksi_num2long(name, "slot-set!");
        if (0 <= i && i < ksi_num2long(NFIELDS_OF(cls), "slot-set!"))
            KSI_SLOT_REF(obj, i) = val;
        else
            ksi_inst_slot_missing(cls, obj, name, val);
    } else {
        ksi_obj slot = find_slot(GNS_OF(cls), name);
        if (!slot) {
            ksi_inst_slot_missing(cls, obj, name, val);
        } else {
            ksi_obj type = KSI_VEC_REF(slot, GNS_TYPE);
            ksi_obj setter = KSI_VEC_REF(slot, GNS_SETTER);

            if (type != ksi_void && ksi_type_p(val, type) == ksi_false) {
                ksi_exn_error(0, val, "slot-set!: invalid value for slot `%ls' in %ls", ksi_obj2str(name), ksi_obj2str(cls));
            }

            if (KSI_EINT_P(setter))
                KSI_SLOT_REF(obj, ksi_num2long(setter, "slot-set!")) = val;
            else
                ksi_apply_2(setter, obj, val);
        }
    }

    return ksi_void;
}

static ksi_obj
ksi_apply_1_without_error (ksi_obj proc, ksi_obj arg1)
{
    volatile ksi_wind wind = ksi_add_catch(ksi_true, 0, 0);

    KSI_FLUSH_REGISTER_WINDOWS;
    if (setjmp (wind->the_catch->jmp.j_buf) == 0) {
        ksi_obj result = ksi_apply_1(proc, arg1);
        ksi_del_catch(wind);
        return result;
    }

    return ksi_void;
}

ksi_obj
ksi_slot_bound_p (ksi_obj obj, ksi_obj name)
{
    ksi_obj val;
    KSI_CHECK(obj, KSI_INST_P(obj), "slot-bound?: invalid instance in arg1");

    val = get_slot_value(obj, name, ksi_apply_1_without_error, "slot-bound?");
    if (val == ksi_void)
        return ksi_false;
    return ksi_true;
}


ksi_obj
ksi_slot_exist_p (ksi_obj obj, ksi_obj name)
{
  KSI_CHECK (obj, KSI_INST_P (obj), "slot-exist?: invalid instance in arg1");
  return ksi_slot_exist_in_class_p ((ksi_obj) KSI_CLASS_OF (obj), name);
}


ksi_obj
ksi_slot_exist_in_class_p (ksi_obj cls, ksi_obj name)
{
  KSI_CHECK (cls, KSI_CLASS_P (cls), "slot-exist-in-class?: invalid class in arg1");

  if (KSI_EINT_P (name)) {
    int i = ksi_num2long (name, "slot-exist-in-class?");
    return (0 <= i && i < ksi_num2long (NFIELDS_OF (cls), "slot-exist-in-class?") ? ksi_true : ksi_false);
  }

  return (find_slot (GNS_OF (cls), name) ? ksi_true : ksi_false);
}


/* slot_num
 *	return offset of slot NAME in instance slot array, if slot is
 *	local slot, and `-1' otherwise.
 */

static int
slot_num (ksi_obj gns, ksi_obj name)
{
  ksi_obj slot = find_slot (gns, name);
  if (slot) {
    ksi_obj getter = KSI_VEC_REF (slot, GNS_GETTER);
    if (KSI_EINT_P (getter))
      return ksi_num2long (getter, "<internal slot_num>");
  }

  return -1;
}


/*
 * ksi_alloc_instance (ksi_obj cls)
 *	allocate instance of class CLS.
 *
 * Scheme procedure
 *	@allocate-instance cls
 *
 * Note:
 *	this procedure is used as standard procedure for allocating
 *	instance in klos.
 */

static ksi_obj
ksi_alloc_instance (ksi_obj cls)
{
  int type, n;
  ksi_obj cpl;
  KSI_CHECK (cls, KSI_CLASS_P (cls), "@allocate-instance: invalid class in arg1");

  /* first, compute type of instance */

  if (cls == Generic) {
    type = I_PURE_GENERIC | I_GENERIC | I_METHODS_SORTED;
  } else {
    type = 0;

    /* look type in cpl of class */
    for (cpl = CPL_OF (cls); cpl != ksi_nil; cpl = KSI_CDR (cpl)) {
      ksi_obj x = KSI_CAR (cpl);

      if (x == Generic)
        type |= I_GENERIC;
      else if (x == Method)
        type |= I_METHOD;
      else if (x == Class)
        type |= I_CLASS;
    }

    if (type & I_CLASS) {
      /* if we can access all class slots via constant offsets
       * in the instance slot array, set I_PURE_CLASS flag.
       */
      ksi_obj gets = GNS_OF (cls);
      if (slot_num (gets, sym_cname) == S_CNAME
          && slot_num (gets, sym_dsupers) == S_DSUPERS
          && slot_num (gets, sym_dslots) == S_DSLOTS
          && slot_num (gets, sym_defargs) == S_DEFARGS
          && slot_num (gets, sym_cpl) == S_CPL
          && slot_num (gets, sym_slots) == S_SLOTS
          && slot_num (gets, sym_nfields) == S_NFIELDS
          && slot_num (gets, sym_gns) == S_GNS)
      {
        type |= I_PURE_CLASS;
      }
    }

    if (type & I_METHOD) {
      /* if we can access all method slots via constant offsets
       * in the instance slot array, set I_PURE_METHOD flag.
       */
      ksi_obj gets = GNS_OF (cls);
      if (slot_num (gets, sym_gf) == S_GF
          && slot_num (gets, sym_specs) == S_SPECS
          && slot_num (gets, sym_combination) == S_COMBINATION
          && slot_num (gets, sym_proc) == S_PROC)
      {
        type |= I_PURE_METHOD;
      }
    }
  }

  /* for last, allocate memory for instance */
  n = ksi_num2long (NFIELDS_OF (cls), "@allocate-instance");
  return (ksi_obj) ksi_new_instance ((ksi_instance) cls, n, type);
}


/*
 * ksi_modify_instance (ksi_obj old, ksi_obj new)
 *	Set class and slots of OLD the same as in NEW
 *
 * Scheme procedure
 *	@modify-instance! old new
 *
 * Note:
 *	this procedure is used as helper of change-class
 */

static ksi_obj
ksi_modify_instance (ksi_obj o, ksi_obj n)
{
  KSI_CHECK (o, KSI_INST_P (o), "@modify-instance: invalid instance in arg1");
  KSI_CHECK (n, KSI_INST_P (n), "@modify-instance: invalid instance in arg2");

  KSI_CLASS_OF (o) = KSI_CLASS_OF (n);
  KSI_SLOTS_PTR (o) = KSI_SLOTS_PTR (n);
  return o;
}


/*
 * Implementation of Pure Generic Functions
 */

/* specs_conform ARITY SPECS
 *
 * test if method specializers are conforms the generic function arity.
 */

static ksi_obj
specs_conform (ksi_obj arity, ksi_obj specs)
{
  ksi_obj a, s;

  while (KSI_PAIR_P (arity)) {
    if (!KSI_PAIR_P (specs))
      return ksi_false;

    a = KSI_CAR (arity);
    s = KSI_CAR (specs);

    KSI_CHECK (s, (s == ksi_true || KSI_CLASS_P (s) || KSI_PROC_P (s) || KSI_LIST_P (s)), "append-method: invalid specializer");

    if (a != ksi_true) {
      if (KSI_CLASS_P (a)) {
        if (!KSI_CLASS_P (s) || !SUBCLASS_P (s, a))
          return ksi_false;
      } else if (KSI_PAIR_P (a)) {
        if (!KSI_PAIR_P (s))
          return ksi_false;

        while (KSI_PAIR_P (s)) {
          if (ksi_memv (KSI_CAR (s), a) == ksi_false)
            return ksi_false;
          s = KSI_CDR (s);
        }
      } else if (a != s) {
        return ksi_false;
      }
    }

    arity = KSI_CDR (arity);
    specs = KSI_CDR (specs);
  }

  if (arity == ksi_nil && specs != ksi_nil)
    return ksi_false;
  return ksi_true;
}

static int
lists_congr (ksi_obj l1, ksi_obj l2)
{
  ksi_obj common = ksi_nil;
  ksi_obj uniq = ksi_nil;

  while (KSI_PAIR_P (l1)) {
    if (ksi_memv (KSI_CAR (l1), l2) == ksi_false)
      uniq = ksi_cons(KSI_CAR(l1), uniq);
    else
      common = ksi_cons(KSI_CAR(l1), common);
    l1 = KSI_CDR (l1);
  }

  if (common == ksi_nil)
    return 0;

  if (uniq == ksi_nil) {
    while (l2 != ksi_nil) {
      if (ksi_memv (KSI_CAR (l2), common) == ksi_false)
        return -1;
      l2 = KSI_CDR (l2);
    }

    return 1;
  }

  return -1;
}


/* more_specific_p M1 M2 ARGS
 *
 * Test if method M1 is more specific than method M2.
 * Both M1 and M2 should be different and applicable to argmuments ARGS,
 * otherwise results would be unpredictable.
 *
 * If ARGS is NULL, the procedure tries to check that M1 is more specific
 * than M2 for any possible arguments.  This can be done if a type of
 * an argument is not taken into account (corresponding specializer is not
 * a class), or if the corresponding specializer of one method
 * is a superclass of the specializer of another method.
 *
 * Returns
 *	1  if M1 is more specific than M2;
 *	0  if not;
 *     -1  if can't determine (this happen only if ARGS == NULL);
 */

static int
more_specific_p (ksi_obj m1, ksi_obj m2, ksi_obj args)
{
  /*
   * Note:
   * Specializer lists of M1 and M2 can have different length
   * (i.e. one can be longer than the other when we have a dotted
   * parameter list).  For instance, with the call:
   *
   *   (M 1)
   *
   * with the methods:
   *
   *   1) (define-method M (a . l) ....)
   *   2) (define-method M (a b . l) ...)
   *   3) (define-method M (a b) ....)
   *
   * we consider that the 2'nd method is more specific than 1'st,
   * and 3'rd is more specific than second.
   *
   */

  ksi_obj s1 = SPECS_OF (m1), s2 = SPECS_OF (m2);
  for (/**/; /**/; s1 = KSI_CDR (s1), s2 = KSI_CDR (s2)) {
    ksi_obj cs1, cs2, l;

    if (s1 == ksi_nil) return 1;
    if (s2 == ksi_nil) return 0;
    if (!KSI_PAIR_P (s1)) return 0;
    if (!KSI_PAIR_P (s2)) return 1;

    cs1 = KSI_CAR (s1), cs2 = KSI_CAR (s2);
    if (cs1 != cs2) {
      /* Only one method should have specializer that is not class.
       * It is error if both method have non-class specializers.
       * Method with list specializer is more specific and
       * method with procedure specializer is less specific.
       */

      if (KSI_PAIR_P (cs1)) return 1;
      if (KSI_PAIR_P (cs2)) return 0;
      if (!KSI_CLASS_P (cs1) && !KSI_CLASS_P (cs2)) goto next;
      if (!KSI_CLASS_P (cs1)) return 0;
      if (!KSI_CLASS_P (cs2)) return 1;

      /* Now both specializers are a classes.
       * More specific method is a method with the specializer
       * that occur first in the class precedence list of the argument.
       */

      if (args) {
        l = CPL_OF (ksi_class_of (KSI_CAR (args)));
        while (l != ksi_nil) {
          if (cs1 == KSI_CAR (l)) return 1;
          if (cs2 == KSI_CAR (l)) return 0;
          l = KSI_CDR (l);
        }
      } else {
        if (SUBCLASS_P (cs1, cs2)) return 1;
        if (SUBCLASS_P (cs2, cs1)) return 0;
        return -1;
      }

      /* for applicable methods next line will never execute,
       * because generic function cannot have two methods that
       * doesn't have any difference in specializers.
       */
      break;
    }
  next:
    if (args)
      args = KSI_CDR (args);
  }

  ksi_exn_error (0, 0, "@method-more-specific?: not applicable methods");
  return 0; /* should not occur! */
}

static ksi_obj
method_more_specific_p (ksi_obj m1, ksi_obj m2, ksi_obj al)
{
  KSI_CHECK (m1, KSI_METHOD_P (m1), "@method-more-specific?: invalid method");
  KSI_CHECK (m2, KSI_METHOD_P (m2), "@method-more-specific?: invalid method");
  KSI_CHECK (al, KSI_LIST_P (al), "@method-more-specific?: invalid list");

  return more_specific_p (m1, m2, al) ? ksi_true : ksi_false;
}


/* applicable_p METHOD ARGS
 *
 * Test if the METHOD is applicable to ARGS.
 */

static int
applicable_p (ksi_obj method, ksi_obj args)
{
  ksi_obj specs = SPECS_OF (method);
  for (/**/; args != ksi_nil; specs = KSI_CDR (specs), args = KSI_CDR (args)) {
    KSI_CHECK (args, KSI_PAIR_P (args), "@method-applicable?: invalid list in arg2");

    /* spec list ends but args not, the method isn't applicable */
    if (specs == ksi_nil)
      return 0;

    /* in case of dotted pair in spec list,
     * the method is applicable to any rest args.
     */
    if (!KSI_PAIR_P (specs))
      return 1;

    if (ksi_type_p (KSI_CAR (args), KSI_CAR (specs)) == ksi_false)
      return 0;
  }

  /* The args ends; and now if spec list ends too or has dotted pair at
   * this point, the method is applicable, otherwise not.
   */
  return (KSI_PAIR_P (specs) ? 0 : 1);
}


/* ksi_applicable_p METHOD ARGS
 *
 * Scheme primitive to call C-function `applicable_p'.
 */

static ksi_obj
ksi_applicable_p (ksi_obj method, ksi_obj args)
{
  KSI_CHECK (method, KSI_METHOD_P (method), "@method-applicable?: invalid method in arg1");

  return applicable_p (method, args) ? ksi_true : ksi_false;
}


/* append_method GF METHOD
 *
 *  Append the METHOD to list of methods of generic function GF,
 *  if the list has not method with the same specializers,
 *  and replace existance nethod otherwise.
 *
 * Return the METHOD.
 *
 * Used from `klos.scm'.
 */

static ksi_obj
append_method (ksi_obj gf, ksi_obj method)
{
  int tmp;
  ksi_obj arity, specs, comb, mthds, l, l1, l2, s1, s2, *loc;

  KSI_CHECK (gf, KSI_GENERIC_P (gf), "append-method: invalid generic function in arg1");
  KSI_CHECK (method, KSI_METHOD_P (method), "append-method: invalid method in arg2");

  arity = ARITY_OF (gf);
  specs = SPECS_OF (method);
  if (specs_conform (arity, specs) == ksi_false)
    ksi_exn_error (0, method, "append-method: method is not conform %ls", ksi_obj2str(gf));

  comb = COMBINATION_OF (method);
  mthds = METHODS_OF (gf);
  for (l = mthds; l != ksi_nil; l = KSI_CDR (l)) {
    if (comb != COMBINATION_OF (KSI_CAR (l)))
      continue;

    l1 = specs;
    l2 = SPECS_OF (KSI_CAR (l));
    for (/**/; /**/; l1 = KSI_CDR (l1), l2 = KSI_CDR (l2)) {
      if (!KSI_PAIR_P (l1) && !KSI_PAIR_P (l2) && l1 == l2) {
        /* If both specializers lists ends or has dotted cdr at end,
         * lists are congruent (i.e methods can't be distinguished).
         * Old method is replaced by new.
         */
        KSI_CAR (l) = method;
        return method;
      }

      /* if any of spec list ends, spec lists are noncongruent. */
      if (!KSI_PAIR_P (l1) || !KSI_PAIR_P (l2))
        break;

      s1 = KSI_CAR (l1);
      s2 = KSI_CAR (l2);
      if ((s1 == Top || s1 == ksi_true) && (s2 == Top || s2 == ksi_true))
        continue;

      if (s1 != s2) {
        /* If any (or both) specializer is a class, spec lists are
         * noncongruent (the methods can be distinguised simply).
         * If one or both specializers are procedures, spec lists are
         * considered noncongruent (it is error to provide such
         * procedure specializers that introduce conflicts).
         * If both spesializers are lists and if all elements
         * of the lists are equal, the methods are congruent.
         * If all elements are not equal, the methods
         * are noncongruent. Otherwise (some elements equal and
         * some not equal), the methods are conflicting,
         * and error signaled.
         */

        if (!KSI_PAIR_P (s1) || !KSI_PAIR_P (s2))
          break;

        tmp = lists_congr (s1, s2);
        if (tmp < 0)
          ksi_exn_error (0, method, "append-method: method conflicts with %ls", ksi_obj2str(KSI_CAR(l)));
        if (tmp == 0)
          break;
      }
    }
  }

  /* If we are here, we have not encountered a method
   * with congruent specializers. (i.e. new method can (or, at least, should)
   * be distinguished from any other method of the generic function).
   */

  loc = &mthds;
  if ((((ksi_instance) gf) -> m_flags & I_METHODS_SORTED) != 0) {
    for (l = mthds; l != ksi_nil; l = KSI_CDR (l)) {
      if (comb != COMBINATION_OF (KSI_CAR (l)))
        continue;

      tmp = more_specific_p (method, KSI_CAR (l), 0);
      if (tmp > 0) {
        loc = & KSI_CDR (l);
      } else if (tmp < 0) {
        ((ksi_instance) gf) -> m_flags &= ~I_METHODS_SORTED;
        break;
      }
    }
  }

  *loc = ksi_cons(method, *loc);
  ksi_slot_set (gf, sym_methods, mthds);
  return method;
}


/* ksi_compute_applicable_methods GF ARGS COMBINATION
 *
 * Builds a list of applicable methods of GF for the argument-list ARGS.
 * In the list more specific methods preceed less specific methods.
 *
 * Return builden list, or ksi_nil if there are no one applicable method in
 * generic function.
 */

ksi_obj
ksi_compute_applicable_methods (ksi_obj gf, ksi_obj args, ksi_obj combination)
{
  ksi_obj l, m, x, *loc, res;

  l = METHODS_OF (gf);

  /* find any applicable method */
  for (res = 0; l != ksi_nil; l = KSI_CDR (l)) {
    m = KSI_CAR (l);
    if (COMBINATION_OF (m) == combination && applicable_p (m, args)) {
      res = m;
      break;
    }
  }

  if (!res)
    return ksi_nil;

  res = ksi_cons(res, ksi_nil);
  while ((l = KSI_CDR (l)) != ksi_nil) {
    m = KSI_CAR (l);
    if (COMBINATION_OF (m) == combination && applicable_p (m, args)) {
      if ((((ksi_instance) gf) -> m_flags & I_METHODS_SORTED) != 0) {
        KSI_ASSERT (more_specific_p (m, KSI_CAR (res), args));
        res = ksi_cons(m, res);
      } else {
        loc = &res;
        for (x = res; x != ksi_nil; x = KSI_CDR (x))
        {
          if (more_specific_p (m, KSI_CAR (x), args))
            break;
          loc = & KSI_CDR (x);
        }
        *loc = ksi_cons(m, *loc);
      }
    }
  }

  return res;
}


/* compute_methods
 *
 * Scheme primitive that call ksi_compute_applicable_methods
 */

static ksi_obj
compute_methods (ksi_obj gf, ksi_obj args, ksi_obj combination)
{
  KSI_CHECK (gf, KSI_GENERIC_P (gf), "@compute-applicable-methods: invalid generic in arg1");
  KSI_CHECK (args, KSI_LIST_P (args), "@compute-applicable-methods: invalid list in arg2");

  return ksi_compute_applicable_methods (gf, args, combination);
}

static ksi_obj
get_procs (ksi_obj methods, int reverse)
{
  ksi_obj res = ksi_nil, *loc = &res;
  while (methods != ksi_nil) {
    ksi_obj proc = PROC_OF (KSI_CAR (methods));
    if (reverse) {
      res = ksi_cons(proc, res);
    } else {
      *loc = ksi_cons(proc, ksi_nil);
      loc = & KSI_CDR(*loc);
    }
    methods = KSI_CDR(methods);
  }
  return res;
}

static ksi_obj
call_first (ksi_obj data, int argc, ksi_obj* argv)
{
  ksi_obj proc, res;

  proc = KSI_CAR (data);
  while (proc != ksi_nil) {
    ksi_apply_proc (KSI_CAR (proc), argc-2, argv+2);
    proc = KSI_CDR (proc);
  }

  proc = KSI_CAR (KSI_CDR (data));
  res  = ksi_apply_proc (proc, argc, argv);

  proc = KSI_CAR (KSI_CDR (KSI_CDR (data)));
  while (proc != ksi_nil) {
    ksi_apply_proc (KSI_CAR (proc), argc-2, argv+2);
    proc = KSI_CDR (proc);
  }

  return res;
}

ksi_obj
ksi_compute_effective_method (ksi_obj gf, ksi_obj args)
{
  ksi_obj vals [3];
  ksi_obj primary, around, before, after;

  primary = ksi_compute_applicable_methods (gf, args, sym_primary);
  around  = ksi_compute_applicable_methods (gf, args, sym_around);
  if (primary == ksi_nil) {
    if (around == ksi_nil)
      return ksi_nil;

    return ksi_new_next (gf, args, get_procs (around, 0));
  }

  before = ksi_compute_applicable_methods (gf, args, sym_before);
  after  = ksi_compute_applicable_methods (gf, args, sym_after);
  if (before == ksi_nil && after == ksi_nil) {
    if (around == ksi_nil)
      return ksi_new_next (gf, args, get_procs (primary, 0));

    vals [0] = get_procs (around, 0);
    vals [1] = get_procs (primary, 0);
    return ksi_new_next (gf, args, ksi_append_x (2, vals));
  }

  primary = get_procs (primary, 0);
  around  = get_procs (around, 0);
  before  = get_procs (before, 0);
  after   = get_procs (after, 1);

  vals[0] = around;
  vals[2] = KSI_CDR (primary);

  primary = KSI_LIST3 (before, KSI_CAR (primary), after);
  primary = ksi_close_proc ((ksi_obj) ksi_new_prim(L"#<call-first-method>", call_first, KSI_CALL_REST1, 3), 1, &primary);
  vals[1] = KSI_LIST1 (primary);

  return ksi_new_next (gf, args, ksi_append_x (3, vals));
}

ksi_obj
ksi_inst_slot_unbound (ksi_obj cls, ksi_obj obj, ksi_obj slot)
{
  ksi_obj sym = ksi_str02sym(L"slot-unbound");
  ksi_obj proc = ksi_klos_val (sym, ksi_false);

  if (KSI_PROC_P (proc))
    return ksi_apply_3 (proc, cls, obj, slot);

  ksi_exn_error (0, 0, "slot-ref: unbound slot: `%ls\' in %ls", ksi_obj2str(slot), ksi_obj2str(obj));
  return ksi_false;
}

ksi_obj
ksi_inst_slot_missing (ksi_obj cls, ksi_obj obj, ksi_obj slot, ksi_obj val)
{
  ksi_obj vals[4];
  ksi_obj sym = ksi_str02sym (L"slot-missing");
  ksi_obj proc = ksi_klos_val (sym, ksi_false);

  if (KSI_PROC_P (proc)) {
    vals[0] = cls;
    vals[1] = obj;
    vals[2] = slot;
    vals[3] = val;
    return ksi_apply_proc (proc, val ? 4 : 3, vals);
  }

  ksi_exn_error (0, 0, "%s: no slot with name `%ls' in %ls",
                 (val ? "slot-set!" : "slot-ref"),
                 ksi_obj2str(slot), ksi_obj2str(cls));

  return ksi_false;
}

ksi_obj
ksi_inst_eqv_p (ksi_obj o1, ksi_obj o2)
{
  ksi_obj sym = ksi_str02sym(L"instance-eqv?");
  ksi_obj proc = ksi_klos_val (sym, ksi_false);

  if (KSI_PROC_P (proc))
    return ksi_apply_2 (proc, o1, o2);

  return ksi_false;
}

ksi_obj
ksi_inst_equal_p (ksi_obj o1, ksi_obj o2)
{
    ksi_obj sym = ksi_str02sym(L"instance-equal?");
    ksi_obj proc = ksi_klos_val(sym, ksi_false);

    if (KSI_PROC_P(proc))
        return ksi_apply_2(proc, o1, o2);

    return ksi_false;
}

unsigned int
ksi_hash_inst (ksi_obj x, unsigned n, unsigned d)
{
    ksi_obj cls = (ksi_obj) KSI_CLASS_OF(x);
    int i, len = ksi_num2long(NFIELDS_OF(cls), "hash-instance");
    unsigned int h;

    if (len > 5) {
        i = d/2; h = 1;
        while (--i >= 0)
            h = ((h<<8) + ksi_hasher(KSI_SLOT_REF(x, h % len), n, 2)) % n;
        return h;
    } else {
        i = len; h = n-1;
        while (--i >= 0)
            h = ((h<<8) + (ksi_hasher(KSI_SLOT_REF(x, i), n, d/len))) % n;
        return h;
    }
}

void
ksi_write_inst (ksi_obj x, ksi_char_port port, int slashify)
{
  ksi_obj sym, proc;

  sym  = ksi_str02sym(slashify ? L"write-instance" : L"display-instance");
  proc = ksi_klos_val(sym, ksi_false);

  if (KSI_PROC_P(proc)) {
    ksi_apply_2(proc, x, (ksi_obj) port);
  } else {
    const wchar_t *str = ksi_aprintf("#<instance %p>", x);
    ksi_port_write(port, str, (int)wcslen(str));
  }
}

const wchar_t *
ksi_inst2str (ksi_instance x)
{
    ksi_obj str, port = ksi_open_string_output_port();
    ksi_write_inst((ksi_obj) x, (ksi_char_port) port, 1);
    str = ksi_extract_string_port_data(port);
    return KSI_STR_PTR(str);
}


/*
 * ksi_obj ksi_class_of (ksi_obj x)
 *	Return class of X.
 *	If X isn't instance, return one of predefined top classes.
 */

ksi_obj
ksi_class_of (ksi_obj x)
{
    if (KSI_INST_P (x))
        return (ksi_obj) KSI_CLASS_OF (x);

    if (x == ksi_false || x == ksi_true)
        return Boolean;
    if (x == ksi_nil)
        return Null;
    if (KSI_PAIR_P (x))
        return Pair;
    if (KSI_CHAR_P (x))
        return Char;
    if (KSI_SYM_P (x))
        return Symbol;
    if (KSI_KEY_P (x))
        return Keyword;
    if (KSI_STR_P (x))
        return String;
    if (KSI_VEC_P (x))
        return Vector;
    if (KSI_BIGNUM_P (x))
        return KSI_EINT_P(x) ? Integer : Rational;
    if (KSI_FLONUM_P (x))
        return ksi_imag_part(x) != 0.0 ? Complex : (KSI_RATIONAL_P(x) ? Rational : Real);
    if (KSI_PROC_P (x))
        return Procedure;
    return Unknown;
}


/* ksi_obj ksi_type_of (ksi_obj x)
 *
 * Returns a symbol that names the type of X.
 */

ksi_obj
ksi_type_of (ksi_obj x)
{
    if (x == ksi_nil)
        return ksi_str02sym(L"null");
    if (x == ksi_eof)
        return ksi_str02sym(L"eof-object");
    if (x == ksi_false || x == ksi_true)
        return ksi_str02sym(L"boolean");
    if (KSI_CHAR_P(x))
        return ksi_str02sym(L"char");
    if (KSI_NUM_P(x))
        return ksi_str02sym(L"number");
    if (KSI_STR_P(x))
        return ksi_str02sym(L"string");
    if (KSI_SYM_P(x))
        return ksi_str02sym(L"symbol");
    if (KSI_KEY_P(x))
        return ksi_str02sym(L"keyword");
    if (KSI_PORT_P(x))
        return ksi_str02sym(L"port");
    if (KSI_PAIR_P(x)) {
        if (KSI_LIST_P(x))
            return ksi_str02sym(L"list");
        return ksi_str02sym(L"pair");
    }
    if (KSI_VEC_P(x))
        return ksi_str02sym(L"vector");
    if (KSI_PROC_P(x))
        return ksi_str02sym(L"procedure");
    if (KSI_INST_P(x)) {
        if (KSI_CLASS_P(x))
            return ksi_str02sym(L"class");
        if (SUBCLASS_P(ksi_class_of(x), Record))
            return ksi_str02sym(L"record");
        return ksi_str02sym(L"instance");
    }

    return ksi_str02sym(L"unknown");
}


/* ksi_type_p X TYPE
 *
 * Test if X has the type TYPE.
 *
 * Used to test a value in ksi_slot_set,
 * and to determine applicabability of methods.
 */

ksi_obj
ksi_type_p (ksi_obj x, ksi_obj type)
{
  if (type == ksi_false || type == ksi_true)
    return type;

  if (KSI_CLASS_P (type)) {
    if (KSI_INST_P (x))
      return (SUBCLASS_P ((ksi_obj) KSI_CLASS_OF (x), type) ? ksi_true : ksi_false);
    return SUBCLASS_P (ksi_class_of (x), type) ? ksi_true : ksi_false;
  }

  if (KSI_PROC_P (type))
    return ksi_apply_1 (type, x);

  while (KSI_PAIR_P (type)) {
    if (KSI_EQV_P (x, KSI_CAR (type)))
      return ksi_true;
    type = KSI_CDR (type);
  }

  return ksi_false;
}


/*
 * Some useful getters for <class> slots.
 *
 * Note, that this getters are primitives, not generic functions.
 * And so, they can be used while generic functions are not initialized yet.
 */

#define DEF_GETTER(c,n,s,i)                                     \
static ksi_obj                                                  \
CONCAT(class_,c) (ksi_obj obj)                                  \
{                                                               \
    if (!KSI_CLASS_P (obj))                                     \
        ksi_exn_error (0, obj, "class-%ls: invalid class:", n); \
    if (KSI_INST_IS (obj, I_PURE_CLASS))                        \
        return KSI_SLOT_REF (obj, i);                           \
    return ksi_slot_ref (obj, s);                               \
}

DEF_GETTER (name, S_cname, sym_cname, S_CNAME)
DEF_GETTER (dsupers, S_dsupers, sym_dsupers, S_DSUPERS)
DEF_GETTER (dslots, S_dslots, sym_dslots, S_DSLOTS)
DEF_GETTER (defargs, S_defargs, sym_defargs, S_DEFARGS)
DEF_GETTER (cpl, S_cpl, sym_cpl, S_CPL)
DEF_GETTER (slots, S_slots, sym_slots, S_SLOTS)

static ksi_obj
class_get_n_set (ksi_obj cls, ksi_obj slot)
{
  ksi_obj gns;
  KSI_CHECK (cls, KSI_CLASS_P (cls), "class-get-n-set: invalid class in arg1");

  gns = GNS_OF (cls);
  if (slot)
    gns = find_slot (gns, slot);

  return gns ? gns : ksi_false;
}


/* ksi_merge_args
 *
 * Merge argument lists.
 * Argument list is devided in parts of two elements.
 * In each part the first element is a key and the second is a value.
 * Key ussually is a symbol or a keyword.  Value may be any type.
 */

static ksi_obj
ksi_merge_args (int argc, ksi_obj* argv)
{
  ksi_obj l, x, res = ksi_nil;
  while (--argc >= 0) {
    for (l = *argv++; l != ksi_nil; l = KSI_CDR (KSI_CDR (l))) {
      KSI_CHECK(l, KSI_PAIR_P (l), "@merge-args: improper list");
      KSI_CHECK(l, KSI_PAIR_P (KSI_CDR (l)), "@merge-args: no value for key");

      for (x = res; x != ksi_nil; x = KSI_CDR (KSI_CDR (x)))
        if (KSI_CAR (x) == KSI_CAR (l))
          goto next;

      res = ksi_cons(KSI_CAR(l), ksi_cons(KSI_CAR(KSI_CDR(l)), res));
    next:;
    }
  }

  return res;
}


static struct Ksi_Prim_Def defs [] =
{
  { L"instance?",		ksi_instance_p,			KSI_CALL_ARG1, 1 },
  { L"class?",			ksi_class_p,			KSI_CALL_ARG1, 1 },
  { L"generic?",		ksi_generic_p,			KSI_CALL_ARG1, 1 },
  { L"method?",			ksi_method_p,			KSI_CALL_ARG1, 1 },

  { L"class-of",		ksi_class_of,			KSI_CALL_ARG1, 1 },
  { L"type-of",			ksi_type_of,			KSI_CALL_ARG1, 1 },
  { L"type?",			ksi_type_p,			KSI_CALL_ARG2, 2 },

  { L"slot-ref",		ksi_slot_ref,			KSI_CALL_ARG2, 2 },
  { L"slot-set!",		ksi_slot_set,			KSI_CALL_ARG3, 3 },
  { L"slot-bound?",		ksi_slot_bound_p,		KSI_CALL_ARG2, 2 },
  { L"slot-exist?",		ksi_slot_exist_p,		KSI_CALL_ARG2, 2 },
  { L"slot-exist-in-class?",	ksi_slot_exist_in_class_p,	KSI_CALL_ARG2, 2 },

  { L"class-name",		class_name,			KSI_CALL_ARG1, 1 },
  { L"class-direct-supers",	class_dsupers,			KSI_CALL_ARG1, 1 },
  { L"class-direct-slots",	class_dslots,			KSI_CALL_ARG1, 1 },
  { L"class-precedence-list",	class_cpl,			KSI_CALL_ARG1, 1 },
  { L"class-slots",		class_slots,			KSI_CALL_ARG1, 1 },
  { L"class-default-initargs",	class_defargs,			KSI_CALL_ARG1, 1 },
  { L"class-get-n-set",		class_get_n_set,		KSI_CALL_ARG2, 1 },

  { L"append-method",		append_method,			KSI_CALL_ARG2, 2 },

  { L"@allocate-instance",	ksi_alloc_instance,		KSI_CALL_ARG1, 1 },
  { L"@modify-instance!",	ksi_modify_instance,		KSI_CALL_ARG2, 2 },
  { L"@method-applicable?",	ksi_applicable_p,		KSI_CALL_ARG2, 2 },
  { L"@method-more-specific?",	method_more_specific_p,		KSI_CALL_ARG3, 3 },
  { L"@compute-applicable-methods", compute_methods,		KSI_CALL_ARG3, 3 },

  { L"@merge-args",		ksi_merge_args,			KSI_CALL_REST0, 0 },
  { L"@get-arg",		ksi_get_arg,			KSI_CALL_ARG3,  2 },

  { 0 }
};

void
ksi_init_klos (void)
{
    ksi_env env = ksi_get_lib_env(L"ksi", L"core", L"klos", 0);

    ksi_reg_unit (defs, env);
    ksi_defsym(L"<top>", Top, env);
    ksi_defsym(L"<object>", Object, env);
    ksi_defsym(L"<boolean>", Boolean, env);
    ksi_defsym(L"<char>", Char, env);
    ksi_defsym(L"<string>", String, env);
    ksi_defsym(L"<symbol>", Symbol, env);
    ksi_defsym(L"<keyword>", Keyword, env);
    ksi_defsym(L"<list>", List, env);
    ksi_defsym(L"<pair>", Pair, env);
    ksi_defsym(L"<null>", Null, env);
    ksi_defsym(L"<vector>", Vector, env);
    ksi_defsym(L"<number>", Number, env);
    ksi_defsym(L"<complex>", Complex, env);
    ksi_defsym(L"<real>", Real, env);
    ksi_defsym(L"<rational>", Rational, env);
    ksi_defsym(L"<integer>", Integer, env);
    ksi_defsym(L"<unknown>", Unknown, env);

    ksi_defsym(L"<rtd>", Rtd, env);
    ksi_defsym(L"<record>", Record, env);

    ksi_defsym(L"<class>", Class, env);
    ksi_defsym(L"<method>", Method, env);
    ksi_defsym(L"<generic>", Generic, env);

    ksi_defsym(L"<proc>", Proc, env);
    ksi_defsym(L"<entity>", Entity, env);
    ksi_defsym(L"<procedure>", Procedure, env);

    ksi_defsym(L"gns-name",       ksi_long2num (GNS_NAME),   env);
    ksi_defsym(L"gns-allocation", ksi_long2num (GNS_ALLOC),  env);
    ksi_defsym(L"gns-getter",     ksi_long2num (GNS_GETTER), env);
    ksi_defsym(L"gns-setter",     ksi_long2num (GNS_SETTER), env);
    ksi_defsym(L"gns-init",       ksi_long2num (GNS_INIT),   env);
    ksi_defsym(L"gns-type",       ksi_long2num (GNS_TYPE),   env);
    ksi_defsym(L"gns-size",       ksi_long2num (GNS_SIZE),   env);
}


 /* End of code */
