#
# makefile for Visual C
#

define=^#define
KSI_CPU=ix86
KSI_OS=windows
KSI_LIB_DIR=c:/ksi

!include $(TOPDIR)\VERSION

CUR_TARGET=ksi-$(KSI_RELEASE)
CUR_LIB=$(BUILDDIR)\$(CUR_TARGET).lib
CUR_DLL=$(BUILDDIR)\$(CUR_TARGET).dll
CUR_MAP=$(BUILDDIR)\$(CUR_TARGET).map

BUILD_TARGETS=ksi_ver.h $(CUR_LIB)

cur_hdrs=$(ksi_HEADERS)

!include $(TOPDIR)\visual.mif

OBJS=$(ksi_OBJS)
TARGET_CFLAGS=$(LIB_CFLAGS)

!include $(SRCDIR)\makedefs.mak
!include $(TOPDIR)\visual1.mif

all: do_curdir

clean: do_curdir_clean
	del ksi_ver.h

dist:
	@for %i in ($(cur_hdrs)) do copy $(SRCDIR)\%i $(TOPDIR)\$(DISTDIR)\include\ksi
	copy $(CUR_LIB) $(TOPDIR)\$(DISTDIR)\lib
	copy $(CUR_DLL) $(TOPDIR)\$(DISTDIR)\bin
