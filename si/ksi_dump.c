/*
 * ksi_dump.c
 *
 * Copyright (C) 1998-2000,2001,2009, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Wed Jul  1 20:59:28 1998
 *
 */

/* Comments:
 *
 * The code is not very portable.
 * Only 32-bit Intel processor is fully supported.
 * It might not be too hard to support processors with:
 *   1) number of bits in char == 8
 *   2) sizeof(char) < sizeof(int) <= sizeof(ksi_obj).
 *
 */

#include "ksi_int.h"
#include "ksi_comp.h"
#include "ksi_numb.h"
#include <math.h>


#if !(KSI_INT_BIT == 32 || KSI_INT_BIT == 64)
#  error unsupported int size
#endif

#if KSI_INT_BIT == 64
#  define LONG int
#  define LMAX INT_MAX
#elif SIZEOF_LONG >= SIZEOF_INT * 2
#  define LONG long
#  define LMAX LONG_MAX
#elif defined(LONG_LONG) && (SIZEOF_LONG_LONG >= SIZEOF_INT * 2)
#  define LONG LONG_LONG
#  define LMAX LONG_MAX
#endif


#define D_END		0x00
#define D_FALSE		0x01
#define D_TRUE		0x02
#define D_NIL		0x03
#define D_EOF		0x04 /* ^D */
#define D_VOID		0x05

#define D_BIG1		0x10
#define D_BIG2		0x11
#define D_BIG4		0x12
#define D_BIG8		0x13

//#define D_RAT		0x14
#define D_FLO		0x15

#define D_SYM1		0x20
#define D_SYM2		0x21
#define D_SYM4		0x22
#define D_SYM8		0x23

#define D_KEY1		0x24
#define D_KEY2		0x25
#define D_KEY4		0x26
#define D_KEY8		0x27

#define D_CHAR1		0x30
#define D_CHAR2		0x31
#define D_CHAR4		0x32
#define D_CHAR8		0x33

#define D_STR1		0x34
#define D_STR2		0x35
#define D_STR4		0x36
#define D_STR8		0x37

#define D_CSTR1		0x38
#define D_CSTR2		0x39
#define D_CSTR4		0x3a
#define D_CSTR8		0x3b

#define D_VEC1		0x40
#define D_VEC2		0x41
#define D_VEC4		0x42
#define D_VEC8		0x43

#define D_CVEC1		0x44
#define D_CVEC2		0x45
#define D_CVEC4		0x46
#define D_CVEC8		0x47

#define D_PAIR		0x50
#define D_CPAIR		0x51

#define D_CODE1		0x60
#define D_CODE2		0x61
#define D_CODE4		0x62
#define D_CODE8		0x63
#define D_FREEVAR	0x64
#define D_VAR1		0x65
#define D_VAR2		0x66
#define D_VAR4		0x67
#define D_VAR8		0x68
#define D_IMMOP		0x69


static int
dump_int4 (int x, ksi_port port)
{
  int n = 0;
  n += ksi_port_putc (port, (int) (x & 0xff));
  n += ksi_port_putc (port, (int) ((x >> 8)  & 0xff));
  n += ksi_port_putc (port, (int) ((x >> 16) & 0xff));
  n += ksi_port_putc (port, (int) ((x >> 24) & 0xff));
  return n;
}

static int
dump_tag (int tag, ksi_port port)
{
#if KSI_TAG_LAST_OP <= 0x7f
  return ksi_port_putc (port, tag);
#elif KSI_TAG_LAST_OP <= 0x7fff
  int n = 0;
  n += ksi_port_putc (port, tag & 0xff);
  n += ksi_port_putc (port, (tag >> 8) & 0xff);
  return n;
#else
  int n = 0;
  n += ksi_port_putc (port, tag & 0xff);
  n += ksi_port_putc (port, (tag >> 8) & 0xff);
  n += ksi_port_putc (port, (tag >> 16) & 0xff);
  n += ksi_port_putc (port, (tag >> 24) & 0xff);
  return n;
#endif
}

static int
dump_len (int tag, int len, ksi_port port)
{
  int n = 0;

  if (ABS (len) <= 0x7f) {
    n += ksi_port_putc (port, tag);
    n += ksi_port_putc (port, len);
  } else if (ABS (len) <= 0x7fff) {
    n += ksi_port_putc (port, tag+1);
    n += ksi_port_putc (port, len & 0xff);
    n += ksi_port_putc (port, (len >> 8) & 0xff);
  } else
#if KSI_INT_BIT == 64
    if (ABS (len) <= 0x7fffffff)
#endif
    {
      n += ksi_port_putc (port, tag+2);
      n += ksi_port_putc (port, len & 0xff);
      n += ksi_port_putc (port, (len >> 8) & 0xff);
      n += ksi_port_putc (port, (len >> 16) & 0xff);
      n += ksi_port_putc (port, (len >> 24) & 0xff);
    }
#if KSI_INT_BIT == 64
    else
    {
      n += ksi_port_putc (port, tag+3);
      n += ksi_port_putc (port, len & 0xff);
      n += ksi_port_putc (port, (len >> 8) & 0xff);
      n += ksi_port_putc (port, (len >> 16) & 0xff);
      n += ksi_port_putc (port, (len >> 24) & 0xff);
      n += ksi_port_putc (port, (len >> 32) & 0xff);
      n += ksi_port_putc (port, (len >> 40) & 0xff);
      n += ksi_port_putc (port, (len >> 48) & 0xff);
      n += ksi_port_putc (port, (len >> 56) & 0xff);
    }
#endif

  return n;
}

static int
dump_double (double x, ksi_port port)
{
  int exp, val1, val2, n;

  x = frexp (x, &exp);
  val1 = (int) (x * 0x40000000);
  val2 = (int) ((x - val1) * 0x40000000);

  n  = dump_int4 (val1, port);
  n += dump_int4 (val2, port);
  n += dump_int4 (exp, port);

  return n;
}

static int
dump (ksi_obj x, ksi_port port)
{
  int num = 0;

again:

  if (x == ksi_void)  return num + ksi_port_putc (port, D_VOID);
  if (x == ksi_false) return num + ksi_port_putc (port, D_FALSE);
  if (x == ksi_true)  return num + ksi_port_putc (port, D_TRUE);
  if (x == ksi_eof)   return num + ksi_port_putc (port, D_EOF);
  if (x == ksi_nil)   return num + ksi_port_putc (port, D_NIL);

  if (KSI_BIGNUM_P (x)) {
    int sign = ((ksi_bignum) x) -> sign;
    int len  = ABS (sign);
    int i;

#if KSI_INT_BIT == 32
    num += dump_len (D_BIG1, sign, port);
    for (i = 0; i < len; i++)
      num += dump_int4 (((ksi_bignum) x) -> digs [i], port);
#elif KSI_INT_BIT == 64
    num += dump_len (D_BIG1, 2*sign, port);
    for (i = 0; i < len; i++) {
      unsigned val = ((ksi_bignum) x) -> digs [i];
      num += dump_int4 (val, port);
      num += dump_int4 (val >> 32, port);
    }
#endif /* KSI_INT_BIT == 64 */

    return num;
  }

  if (KSI_FLONUM_P (x)) {
    num += ksi_port_putc (port, D_FLO);
    num += dump_double (KSI_REPART (x), port);
    num += dump_double (KSI_IMPART (x), port);
    return num;
  }

  if (KSI_CHAR_P (x)) {
    return num + dump_len (D_CHAR1, KSI_CHAR_CODE (x), port);
  }

  if (KSI_SYM_P (x)) {
    num += dump_len (D_SYM1, KSI_SYM_LEN (x), port);
    return num + ksi_port_write ((ksi_obj) port, KSI_SYM_PTR (x), KSI_SYM_LEN (x));
  }

  if (KSI_KEY_P (x)) {
    num += dump_len (D_KEY1, KSI_KEY_LEN (x), port);
    return num + ksi_port_write ((ksi_obj) port, KSI_KEY_PTR (x), KSI_KEY_LEN (x));
  }

  if (KSI_C_STR_P (x)) {
    num += dump_len (D_CSTR1, KSI_STR_LEN (x), port);
    return num + ksi_port_write ((ksi_obj) port, KSI_STR_PTR (x), KSI_STR_LEN (x));
  }

  if (KSI_STR_P (x)) {
    num += dump_len (D_STR1, KSI_STR_LEN (x), port);
    return num + ksi_port_write ((ksi_obj) port, KSI_STR_PTR (x), KSI_STR_LEN (x));
  }

  if (KSI_C_VEC_P (x)) {
    int i;
    num += dump_len (D_CVEC1, KSI_VEC_LEN (x), port);
    for (i = 0; i < KSI_VEC_LEN (x); i++)
      num += dump (KSI_VEC_REF (x, i), port);
    return num;
  }

  if (KSI_VEC_P (x)) {
    int i;
    num += dump_len (D_VEC1, KSI_VEC_LEN (x), port);
    for (i = 0; i < KSI_VEC_LEN (x); i++)
      num += dump (KSI_VEC_REF (x, i), port);
    return num;
  }

  if (KSI_C_PAIR_P (x)) {
    num += ksi_port_putc (port, D_CPAIR);
    num += dump (KSI_CAR (x), port);
    x = KSI_CDR (x);
    goto again;
  }

  if (KSI_PAIR_P (x)) {
    num += ksi_port_putc (port, D_PAIR);
    num += dump (KSI_CAR (x), port);
    x = KSI_CDR (x);
    goto again;
  }

  if (KSI_CODE_P (x)) {
    int i, len = KSI_CODE_NUM (x) + 1;
    num += dump_len (D_CODE1, len, port);
    num += dump_tag (x->o.itag, port);
    for (i = 0; i < len; i++)
      num += dump (KSI_CODE_VAL (x, i), port);
    return num;
  }

  if (KSI_FREEVAR_P (x)) {
    num += ksi_port_putc (port, D_FREEVAR);
    return num + dump (KSI_VAR_SYM (x), port);
  }

  if (KSI_LOCVAR_P (x)) {
    num += dump_len (D_VAR1, KSI_VAR_LEV (x), port);
    num += dump_int4 (KSI_VAR_NUM (x), port);
    return num;
  }

  if (KSI_IMMOP_P (x)) {
    num += ksi_port_putc (port, D_IMMOP);
    num += dump_tag (x->o.itag, port);
    return num;
  }

  ksi_exn_error ("misc", x, "object->dump: cannot dump object");
  return num;
}


static int
undump_int1 (ksi_port port, const char *proc)
{
  int c = ksi_port_getc (port);
  if (c < 0)
    ksi_error ("%s: unexpected end of data", proc);
  return ((c & 0x80) ? (int) ((signed char) c) : c);
}

static int
undump_int2 (ksi_port port, const char *proc)
{
  int c1 = ksi_port_getc (port);
  int c2 = ksi_port_getc (port);
  if (c1 < 0 || c2 < 0)
    ksi_error ("%s: unexpected end of data", proc);
  c1 = (c2 << 8) | c1;
  return ((c1 & 0x8000) ? (int) ((signed short) c1) : c1);
}

static int
undump_int4 (ksi_port port, const char *proc)
{
  int c1 = ksi_port_getc (port);
  int c2 = ksi_port_getc (port);
  int c3 = ksi_port_getc (port);
  int c4 = ksi_port_getc (port);
  if (c1 < 0 || c2 < 0 || c3 < 0 || c4 < 0)
    ksi_error ("%s: unexpected end of data", proc);
  c1 = (c4 << 24) | (c3 << 16) | (c2 << 8) | c1;
#if KSI_INT_BIT == 32
  return c1;
#elif KSI_INT_BIT == 64
  return ((c1 & 0x80000000) ? -((c1 ^ 0xffffffff) + 1) : c1);
#endif
}

#if defined(LONG)
static LONG
undump_int8 (ksi_port port, const char *proc)
{
  int c1 = ksi_port_getc (port);
  int c2 = ksi_port_getc (port);
  int c3 = ksi_port_getc (port);
  int c4 = ksi_port_getc (port);
  int c5 = ksi_port_getc (port);
  int c6 = ksi_port_getc (port);
  int c7 = ksi_port_getc (port);
  int c8 = ksi_port_getc (port);
  if (c1 < 0 || c2 < 0 || c3 < 0 || c4 < 0 || c5 < 0 || c6 < 0 || c7 < 0 || c8 < 0)
    ksi_error ("%s: unexpected end of data", proc);

  c1 = (c4 << 24) | (c3 << 16) | (c2 << 8) | c1;
  c2 = (c8 << 24) | (c7 << 16) | (c6 << 8) | c5;
  return (((LONG) c2) << 32) | c1;
}
#endif

#if KSI_TAG_LAST_OP <= 0x7f
#  define undump_tag undump_int1
#elif KSI_TAG_LAST_OP <= 0x7fff
#  define undump_tag undump_int2
#else
#  define undump_tag undump_int4
#endif

static double
undump_double (ksi_port port, const char *proc)
{
  int val1 = undump_int4 (port, proc);
  int val2 = undump_int4 (port, proc);
  int exp  = undump_int4 (port, proc);
  return ldexp ((((double) val2 / 0x40000000) + val1) / 0x40000000, exp);
}

static void
undump_str (ksi_port port, char *ptr, int len, const char *proc)
{
  while (len > 0) {
    int n = ksi_port_read ((ksi_obj) port, ptr, len);
    if (n <= 0)
      ksi_error ("%s: unexpected end of data", proc);
    ptr += n;
    len -= n;
  }
}

static ksi_obj
undump (ksi_port port, const char *proc)
{
  int len;
  int cod = ksi_port_getc (port);
  switch (cod)
    {
    case -1:		return ksi_eof;
    case D_FALSE:	return ksi_false;
    case D_TRUE:	return ksi_true;
    case D_NIL:		return ksi_nil;
    case D_EOF:		return ksi_eof;
    case D_VOID:	return ksi_void;

    case D_BIG1:
      len = undump_int1 (port, proc);
    d_big:
      {
	int i, sign;
	unsigned *digs;

	sign = len;
	len  = ABS (len);
#if KSI_INT_BIT == 32
	digs = (unsigned*) ksi_malloc_data (len * sizeof *digs);
	for (i = 0; i < len; i++)
	  digs[i] = (unsigned) undump_int4 (port, proc);
	return ksi_make_bignum (sign, digs);
#elif KSI_INT_BIT == 64
	digs = (unsigned*) ksi_malloc_data ((len+1)/2 * sizeof *digs);
	for (i = 0; i < len; i++)
	  {
	    unsigned tmp = (unsigned) undump_int4 (port, proc);
	    if (i & 1)
	      digs[i/2] |= tmp << 32;
	    else
	      digs[i/2] = tmp;
	  }
	return ksi_make_bignum (sign, digs);
#endif
      }
    case D_BIG2:
      len = undump_int2 (port, proc);
      goto d_big;
    case D_BIG4:
      len = undump_int4 (port, proc);
      goto d_big;
#if KSI_INT_BIT == 64
    case D_BIG8:
      len = undump_int8 (port, proc);
      goto d_big;
#endif

    case D_FLO:
      {
        double re = undump_double (port, proc);
        double im = undump_double (port, proc);
        return ksi_rectangular (re, im);
      }

    case D_CHAR1: return ksi_int2char (undump_int1 (port, proc));
    case D_CHAR2: return ksi_int2char (undump_int2 (port, proc));
    case D_CHAR4: return ksi_int2char (undump_int4 (port, proc));
    case D_CHAR8: return ksi_int2char (undump_int8 (port, proc));

    case D_SYM1:
      len = undump_int1 (port, proc);
    d_sym:
      {
        char *ptr = (char*) alloca (len);
        undump_str (port, ptr, len, proc);
        return ksi_str2sym (ptr, len);
      }
    case D_SYM2:
      len = undump_int2 (port, proc);
      goto d_sym;
    case D_SYM4:
      len = undump_int4 (port, proc);
      goto d_sym;
#if KSI_INT_BIT == 64
    case D_SYM8:
      len = undump_int8 (port, proc);
      goto d_sym;
#endif

    case D_KEY1:
      len = undump_int1 (port, proc);
    d_key:
      {
        char *ptr = (char*) alloca (len);
        undump_str (port, ptr, len, proc);
        return ksi_str2key (ptr, len);
      }
    case D_KEY2:
      len = undump_int2 (port, proc);
      goto d_key;
    case D_KEY4:
      len = undump_int4 (port, proc);
      goto d_key;
#if KSI_INT_BIT == 64
    case D_KEY8:
      len = undump_int8 (port, proc);
      goto d_key;
#endif

    case D_STR1:
      len = undump_int1 (port, proc);
    d_str:
      {
        char *ptr = (char*) alloca (len);
        undump_str (port, ptr, len, proc);
        return ksi_str2string (ptr, len);
      }
    case D_STR2:
      len = undump_int2 (port, proc);
      goto d_str;
    case D_STR4:
      len = undump_int4 (port, proc);
      goto d_str;
#if KSI_INT_BIT == 64
    case D_STR8:
      len = undump_int8 (port, proc);
      goto d_str;
#endif

    case D_CSTR1:
      len = undump_int1 (port, proc);
    d_cstr:
      {
        ksi_obj x;
        char *ptr = (char*) alloca (len);
        undump_str (port, ptr, len, proc);
        x = ksi_str2string (ptr, len);
        x->o.itag = KSI_TAG_CONST_STRING;
        return x;
      }
    case D_CSTR2:
      len = undump_int2 (port, proc);
      goto d_cstr;
    case D_CSTR4:
      len = undump_int4 (port, proc);
      goto d_cstr;
#if KSI_INT_BIT == 64
    case D_CSTR8:
      len = undump_int8 (port, proc);
      goto d_cstr;
#endif

    case D_VEC1:
      len = undump_int1 (port, proc);
    d_vec:
      {
        int i;
        ksi_vector x = ksi_alloc_vector (len, KSI_TAG_VECTOR);
        for (i = 0; i < len; i++)
          KSI_VEC_REF (x, i) = undump (port, proc);
        return (ksi_obj) x;
      }
    case D_VEC2:
      len = undump_int2 (port, proc);
      goto d_vec;
    case D_VEC4:
      len = undump_int4 (port, proc);
      goto d_vec;
#if KSI_INT_BIT == 64
    case D_VEC8:
      len = undump_int8 (port, proc);
      goto d_vec;
#endif

    case D_CVEC1:
      len = undump_int1 (port, proc);
    d_cvec:
      {
        int i;
        ksi_vector x = ksi_alloc_vector (len, KSI_TAG_CONST_VECTOR);
        for (i = 0; i < len; i++)
          KSI_VEC_REF (x, i) = undump (port, proc);
        return (ksi_obj) x;
      }
    case D_CVEC2:
      len = undump_int2 (port, proc);
      goto d_cvec;
    case D_CVEC4:
      len = undump_int4 (port, proc);
      goto d_cvec;
#if KSI_INT_BIT == 64
    case D_CVEC8:
      len = undump_int8 (port, proc);
      goto d_cvec;
#endif

    case D_PAIR:
      {
        ksi_obj x1 = undump (port, proc);
        ksi_obj x2 = undump (port, proc);
        return ksi_cons (x1, x2);
      }
    case D_CPAIR:
      {
        ksi_obj x1 = undump (port, proc);
        ksi_obj x2 = undump (port, proc);
        x1 = ksi_cons (x1, x2);
        x1->o.itag = KSI_TAG_CONST_PAIR;
        return x1;
      }

    case D_CODE1:
      len = undump_int1 (port, proc);
    d_code:
      {
        int i, tag = undump_tag (port, proc);
        ksi_obj x = (ksi_obj) ksi_new_code (len, tag);
        for (i = 0; i < len; i++)
          KSI_CODE_VAL (x, i) = undump (port, proc);
        return x;
      }
    case D_CODE2:
      len = undump_int2 (port, proc);
      goto d_code;
    case D_CODE4:
      len = undump_int4 (port, proc);
      goto d_code;
#if KSI_INT_BIT == 64
    case D_CODE8:
      len = undump_int8 (port, proc);
      goto d_code;
#endif

    case D_FREEVAR:
      {
        ksi_obj sym = undump (port, proc);
        return ksi_new_freevar (sym);
      }

    case D_VAR1:
      len = undump_int1 (port, proc);
    d_var:
      return ksi_new_varbox (len, undump_int4 (port, proc));
    case D_VAR2:
      len = undump_int2 (port, proc);
      goto d_var;
    case D_VAR4:
      len = undump_int4 (port, proc);
      goto d_var;
#if KSI_INT_BIT == 64
    case D_VAR8:
      len = undump_int8 (port, proc);
      goto d_var;
#endif

    case D_IMMOP:
      return ksi_new_imm (undump_tag (port, proc));

    default:
      ksi_error ("%s: unknown data type %x", proc, cod);
    }

  return 0;
}


void*
ksi_dump (ksi_obj x, int* size)
{
  ksi_port p = ksi_new_str_port (0);
  p->output = 1;
  dump (x, p);

  x = ksi_port_string ((ksi_obj) p);
  if (size)
    *size = KSI_STR_LEN (x);
  return KSI_STR_PTR (x);
}

ksi_obj
ksi_undump (void* data, int size)
{
  ksi_string x = (ksi_string) ksi_str2string (data, size);
  ksi_port p = ksi_new_str_port (x);
  p->input = 1;

  return undump (p, "ksi_undump");
}

ksi_obj
ksi_write_dump (ksi_obj x, ksi_obj p)
{
  KSI_TYPE_CHECK (p, KSI_OUTPUT_PORT_P (p), "write-dump: invalid output port in arg2");

  dump (x, (ksi_port) p);
  return ksi_void;
}

ksi_obj
ksi_read_dump (ksi_obj p)
{
  KSI_TYPE_CHECK (p, KSI_INPUT_PORT_P (p), "read-dump: invalid input port in arg1");
  return undump ((ksi_port) p, "read-dump");
}

ksi_obj
ksi_obj2dump (ksi_obj x)
{
  ksi_port p = ksi_new_str_port (0);
  p->output = 1;
  dump (x, p);
  return ksi_port_string ((ksi_obj) p);
}

ksi_obj
ksi_dump2obj (ksi_obj x)
{
  ksi_port p;

  KSI_TYPE_CHECK (x, KSI_STR_P (x), "dump->object: invalid string in arg1");
  p = ksi_new_str_port ((ksi_string) x);
  p->input = 1;
  return undump (p, "dump->object");
}


 /* End of code */
