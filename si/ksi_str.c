/*
 * ksi_str.c
 * strings
 *
 * Copyright (C) 1997-2010, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Sat Jan 25 00:25:23 1997
 * Last Update:   Sun Aug 15 18:30:48 2010
 *
 */

#include "ksi_type.h"
#include "ksi_proc.h"
#include "ksi_gc.h"

#if defined(_MSC_VER)
#pragma warning(disable : 4333)
#endif


static wchar_t
tohex (int c)
{
    return btowc(c < 10 ? c + '0' : (c-10) + 'a');
}

const wchar_t*
ksi_string2str (ksi_obj x)
{
    int len, i, k, num;
    const wchar_t *str;
    wchar_t *ptr;

    KSI_CHECK(x, KSI_STR_P(x), "string2str: invalid string in arg1");

    len = KSI_STR_LEN(x);
    str = KSI_STR_PTR(x);

    if (len <= 0)
        return L"\"\"";

    for (i = 0, num = 0; i < len; ++i) {
        wint_t c = str[i];
        switch (c) {
        case L'\0':
            num += 6;
            break;
        case L'\n': case L'\r': case L'\t': case L'\f': case L'\b': case L'\a': case L'\v':
            ++num;
            break;
        case L'"': case L'\\':
            ++num;
            break;
        default:
            if (!iswprint(c)) {
                if (c > 0xffff)
                    num += 2;
                num += 6;
            }
        }
    }

    ptr = ksi_malloc_data((len + num + 4) * sizeof(*ptr));
    ptr[k=0] = L'"';
    for (i = 0; i < len; ++i) {
        wint_t c = str[i];
        switch (c) {
        case L'\0':
            ptr[++k] = L'\\';
            ptr[++k] = L'x';
            ptr[++k] = L'0';
            ptr[++k] = L'0';
            ptr[++k] = L'0';
            ptr[++k] = L'0';
            c = L';';
            break;
        case L'\n':
            ptr[++k] = L'\\';
            c = L'n';
            break;
        case L'\r':
            ptr[++k] = L'\\';
            c = L'r';
            break;
        case L'\t':
            ptr[++k] = L'\\';
            c = L't';
            break;
        case L'\f':
            ptr[++k] = L'\\';
            c = L'f';
            break;
        case L'\b':
            ptr[++k] = L'\\';
            c = L'b';
            break;
        case L'\a':
            ptr[++k] = L'\\';
            c = L'a';
            break;
        case L'\v':
            ptr[++k] = L'\\';
            c = 'v';
            break;
        case L'"': case L'\\':
            ptr[++k] = L'\\';
            break;
        default:
            if (!iswprint(c)) {
                ptr[++k] = L'\\';
                ptr[++k] = L'x';
                if (c > 0xffff) {
                    ptr[++k] = tohex((c >> 20) & 0xf);
                    ptr[++k] = tohex((c >> 16) & 0xf);
                }
                ptr[++k] = tohex((c >> 12) & 0xf);
                ptr[++k] = tohex((c >> 8) & 0xf);
                ptr[++k] = tohex((c >> 4) & 0xf);
                ptr[++k] = tohex(c & 0xf);
                c = L';';
            }
        }
        ptr[++k] = c;
    }

    ptr[++k] = '"';
    ptr[++k] = '\0';

    return ptr;
}

ksi_obj
ksi_str2string (const wchar_t *s, int l)
{
    ksi_string str = (ksi_string) ksi_malloc(sizeof(*str));
    str->o.itag = KSI_TAG_STRING;
    str->o.ilen = l;
    str->ptr = ksi_malloc_data((l + 1) * sizeof(str->ptr[0]));
    if (s) {
        wmemcpy(str->ptr, s, l);
        str->ptr[l] = L'\0';
    } else {
        str->ptr[0] = L'\0';
    }

    return (ksi_obj) str;
}

ksi_obj
ksi_str02string (const wchar_t *s)
{
    return ksi_str2string(s, s ? (int)wcslen(s) : 0);
}

ksi_obj
ksi_make_string (int len, wchar_t c)
{
    ksi_string str = (ksi_string) ksi_malloc(sizeof(*str));
    str->o.itag = KSI_TAG_STRING;
    str->ptr = ksi_malloc_data((len + 1) * sizeof(str->ptr[0]));
    if (len)
        wmemset(str->ptr, c, len);
    str->ptr[len] = L'\0';
    str->o.ilen = len;

    return (ksi_obj) str;
}

ksi_obj
ksi_string_p (ksi_obj x)
{
    return KSI_STR_P (x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_string_length (ksi_obj str)
{
    KSI_CHECK(str, KSI_STR_P(str), "string-length: invalid string");

    return ksi_long2num(KSI_STR_LEN(str));
}

ksi_obj
ksi_string_ref (ksi_obj str, ksi_obj k)
{
    unsigned ind;

    KSI_CHECK(str, KSI_STR_P(str), "string-ref: invalid string in arg1");
    KSI_CHECK(k, KSI_UINT_P(k), "string-ref: invalid integer in arg2");

    ind = ksi_num2ulong(k, "string-ref");
    KSI_CHECK(k, ind < KSI_STR_LEN(str), "string-ref: invalid index in arg2");

    return ksi_int2char(KSI_STR_PTR(str)[ind]);
}

ksi_obj
ksi_string_set_x (ksi_obj str, ksi_obj k, ksi_obj ch)
{
    unsigned ind;

    KSI_CHECK(str, KSI_STR_P(str), "string-set!: invalid string in arg1");
    KSI_CHECK(str, !KSI_C_STR_P(str), "string-set!: constant string in arg1");
    KSI_CHECK(k, KSI_UINT_P(k), "string-set!: invalid integer in arg2");
    KSI_CHECK(ch, KSI_CHAR_P(ch), "string-set!: invalid char in arg3");

    ind = ksi_num2ulong(k, "string-set!");
    KSI_CHECK(k, ind < KSI_STR_LEN(str), "string-set!: index out of range in arg2");

    KSI_STR_PTR(str)[ind] = KSI_CHAR_CODE(ch);
    return ksi_void;
}

ksi_obj
ksi_substring (ksi_obj str, ksi_obj start, ksi_obj end)
{
    unsigned int s, e;

    KSI_CHECK (str, KSI_STR_P (str), "substring: invalid string in arg1");
    KSI_CHECK (start, KSI_UINT_P (start), "substring: invalid index in arg2");
    KSI_CHECK (end, !end || KSI_UINT_P (end), "substring: invalid index in arg3");

    s = ksi_num2uint (start, "substring");
    e = (end ? ksi_num2uint (end, "substring") : KSI_STR_LEN (str));

    KSI_CHECK (start, s <= e, "substring: invalid index in arg2");
    KSI_CHECK (end, e <= KSI_STR_LEN (str), "substring: invalid index in arg3");

    return (ksi_obj) ksi_str2string(KSI_STR_PTR(str) + s, e - s);
}

ksi_obj
ksi_string_append (int argc, ksi_obj* args)
{
    ksi_obj str;
    int i, len = 0;
    wchar_t *ptr;

    for (i = 0; i < argc; i++) {
        if (KSI_CHAR_P (args[i])) {
            len += 1;
        } else {
            KSI_CHECK (args[i], KSI_STR_P (args[i]), "string-append: invalid string");
            len += KSI_STR_LEN (args[i]);
        }
    }

    str = (ksi_obj) ksi_make_string(len, '\0');
    ptr = KSI_STR_PTR(str);
    for (i = 0; i < argc; i++) {
        if (KSI_CHAR_P (args[i])) {
            *ptr++ = KSI_CHAR_CODE (args[i]);
        } else if (KSI_STR_LEN (args[i]) > 0) {
            wmemcpy(ptr, KSI_STR_PTR(args[i]), KSI_STR_LEN(args[i]));
            ptr += KSI_STR_LEN(args[i]);
        }
    }

    return str;
}

ksi_obj
ksi_string2list (ksi_obj str)
{
    int len;
    wchar_t *ptr;
    ksi_obj list;

    KSI_CHECK(str, KSI_STR_P(str), "string->list: invalid string");

    len = KSI_STR_LEN(str);
    ptr = KSI_STR_PTR(str);
    list = ksi_nil;
    while (len--)
        list = ksi_cons(ksi_int2char(ptr[len]), list);

    return list;
}

ksi_obj
ksi_list2string (ksi_obj list)
{
    int i, len = ksi_list_len(list);
    ksi_obj str;
    wchar_t *ptr;

    KSI_CHECK(list, len >= 0, "list->string: invalid list in arg1");

    str = (ksi_obj) ksi_make_string(len, '\0');
    ptr = KSI_STR_PTR (str);
    for (i = 0; i < len; ++i, list = KSI_CDR (list)) {
        ksi_obj x = KSI_CAR(list);
        KSI_CHECK(x, KSI_CHAR_P(x), "list->string: invalid char");
        ptr[i] = KSI_CHAR_CODE(x);
    }

    return str;
}

ksi_obj
ksi_new_string (int argc, ksi_obj* argv)
{
    int i;
    ksi_obj str = ksi_make_string(argc, '\0');
    wchar_t *ptr = KSI_STR_PTR(str);

    for (i = 0; i < argc; ++i) {
        KSI_CHECK(argv[i], KSI_CHAR_P(argv[i]), "string: invalid char");
        ptr[i] = KSI_CHAR_CODE(argv[i]);
    }

    return str;
}

ksi_obj
ksi_string_copy (ksi_obj str)
{
    KSI_CHECK(str, KSI_STR_P(str), "string-copy: invalid string");
    return (ksi_obj) ksi_str2string(KSI_STR_PTR(str), KSI_STR_LEN(str));
}

ksi_obj
ksi_string_fill_x (ksi_obj str, ksi_obj c)
{
    int i;

    KSI_CHECK(str, KSI_STR_P (str), "string-fill!: invalid string in arg1");
    KSI_CHECK(str, !KSI_C_STR_P (str), "string-fill!: constant string in arg1");
    KSI_CHECK(c, KSI_CHAR_P (c), "string-fill!: invalid char in arg2");

    for (i = 0; i < KSI_STR_LEN(str); ++i)
        KSI_STR_PTR(str)[i] = KSI_CHAR_CODE(c);

    return ksi_void;
}

ksi_obj
ksi_string_eqv_p (ksi_obj s1, ksi_obj s2)
{
    int l1, l2;
    wchar_t *p1, *p2;

    KSI_CHECK (s1, KSI_STR_P (s1), "string=?: invalid string in arg1");
    KSI_CHECK (s2, KSI_STR_P (s2), "string=?: invalid string in arg2");

    l1 = KSI_STR_LEN(s1);
    l2 = KSI_STR_LEN(s2);
    p1 = KSI_STR_PTR(s1);
    p2 = KSI_STR_PTR(s2);

    if (l1 == l2 && p1 == p2)
        return ksi_true;

    return ksi_false;
}

ksi_obj
ksi_string_equal_p (ksi_obj s1, ksi_obj s2)
{
    int l1, l2;
    wchar_t *p1, *p2;

    KSI_CHECK (s1, KSI_STR_P (s1), "string=?: invalid string");
    KSI_CHECK (s2, KSI_STR_P (s2), "string=?: invalid string");

    l1 = KSI_STR_LEN(s1);
    l2 = KSI_STR_LEN(s2);
    p1 = KSI_STR_PTR(s1);
    p2 = KSI_STR_PTR(s2);

    if (l1 == l2) {
        if (p1 != p2)
            while (l1--)
                if (*p1++ != *p2++)
                    return ksi_false;

        return ksi_true;
    }

    return ksi_false;
}

ksi_obj
ksi_string_ci_equal_p (ksi_obj s1, ksi_obj s2)
{
    int l1, l2;
    wchar_t *p1, *p2;

    KSI_CHECK (s1, KSI_STR_P (s1), "string-ci=?: invalid string");
    KSI_CHECK (s2, KSI_STR_P (s2), "string-ci=?: invalid string");

    l1 = KSI_STR_LEN(s1);
    l2 = KSI_STR_LEN(s2);
    p1 = KSI_STR_PTR(s1);
    p2 = KSI_STR_PTR(s2);

    if (l1 == l2) {
        if (p1 != p2)
            while (l1--)
                if (towlower(*p1++) != towlower(*p2++))
                    return ksi_false;

        return ksi_true;
    }

    return ksi_false;
}

ksi_obj
ksi_string_eq_p (int argc, ksi_obj* args)
{
    int i;
    for (i = 1; i < argc; i++) {
        if (ksi_string_equal_p (args[0], args[i]) == ksi_false)
            return ksi_false;
    }

    return ksi_true;
}

ksi_obj
ksi_string_ci_eq_p (int argc, ksi_obj* args)
{
    int i;
    for (i = 1; i < argc; i++) {
        if (ksi_string_ci_equal_p(args[0], args[i]) == ksi_false)
            return ksi_false;
    }

    return ksi_true;
}


static int
string_less_p (ksi_obj s1, ksi_obj s2, char* fname)
{
    int l1, l2;
    wchar_t *p1, *p2;
    if (!KSI_STR_P(s1))
        ksi_exn_error (0, s1, "%s: invalid string", fname);
    if (!KSI_STR_P(s2))
        ksi_exn_error (0, s2, "%s: invalid string", fname);

    l1 = KSI_STR_LEN(s1);
    l2 = KSI_STR_LEN(s2);
    p1 = KSI_STR_PTR(s1);
    p2 = KSI_STR_PTR(s2);
    for (; l1 && l2; --l1, --l2) {
        int c = *p1++ - *p2++;
        if (c > 0) return 0;
        if (c < 0) return 1;
    }
    return (l1 < l2);
}

static int
string_ci_less_p (ksi_obj s1, ksi_obj s2, char* fname)
{
    int l1, l2;
    wchar_t *p1, *p2;

    if (!KSI_STR_P (s1))
        ksi_exn_error (0, s1, "%s: invalid string", fname);
    if (!KSI_STR_P (s2))
        ksi_exn_error (0, s2, "%s: invalid string", fname);

    l1 = KSI_STR_LEN(s1);
    l2 = KSI_STR_LEN(s2);
    p1 = KSI_STR_PTR(s1);
    p2 = KSI_STR_PTR(s2);
    for (; l1 && l2; --l1, --l2) {
        int c = towlower(*p1) - towlower(*p2);
        p1++; p2++;
        if (c > 0) return 0;
        if (c < 0) return 1;
    }
    return (l1 < l2);
}

#define DEF_CMP(fun, cmp)                       \
ksi_obj                                         \
fun (int ac, ksi_obj* av)                       \
{                                               \
  int i;                                        \
  for (i = 1; i < ac; i++) {                    \
    if (cmp)                                    \
      return ksi_false;                         \
  }                                             \
  return ksi_true;                              \
}

DEF_CMP(ksi_string_ls_p, !string_less_p(av[i-1], av[i], "string<?"))
DEF_CMP(ksi_string_gt_p, !string_less_p(av[i], av[i-1], "string>?"))
DEF_CMP(ksi_string_le_p, string_less_p(av[i], av[i-1], "string<=?"))
DEF_CMP(ksi_string_ge_p, string_less_p(av[i-1], av[i], "string>=?"))

DEF_CMP(ksi_string_ci_ls_p, !string_ci_less_p(av[i-1], av[i], "string<?"))
DEF_CMP(ksi_string_ci_gt_p, !string_ci_less_p(av[i], av[i-1], "string>?"))
DEF_CMP(ksi_string_ci_le_p, string_ci_less_p(av[i], av[i-1], "string<=?"))
DEF_CMP(ksi_string_ci_ge_p, string_ci_less_p(av[i-1], av[i], "string>=?"))


ksi_obj
ksi_string_index (ksi_obj str, ksi_obj chr, ksi_obj beg)
{
    const wchar_t *ptr;
    unsigned l, b;

    KSI_CHECK(str, KSI_STR_P(str), "string-index: invalid string in arg1");
    KSI_CHECK(chr, KSI_CHAR_P(chr), "string-index: invalid char in arg2");

    if (!beg) {
        b = 0;
    } else {
        KSI_CHECK(beg, KSI_UINT_P(beg), "string-index: invalid integer in arg3");
        b = ksi_num2uint(beg, "string-index");
    }

    ptr = KSI_STR_PTR(str);
    l = KSI_STR_LEN(str);
    if (b < l) {
        wchar_t *where = wmemchr(ptr+b, KSI_CHAR_CODE(chr), l-b);
        if (where)
            return ksi_longlong2num(where - ptr);
    }

    return ksi_false;
}

ksi_obj
ksi_string_rindex (ksi_obj str, ksi_obj chr, ksi_obj beg)
{
    unsigned len, pos;

    KSI_CHECK (str, KSI_STR_P (str), "string-rindex: invalid string in arg1");
    KSI_CHECK (chr, KSI_CHAR_P (chr), "string-rindex: invalid char in arg2");

    len = KSI_STR_LEN (str);
    if (!beg) {
        pos = len;
    } else {
        KSI_CHECK (beg, KSI_EINT_P (beg), "string-rindex: invalid integer in arg3");
        pos = ksi_num2ulong (beg, "string-rindex");
        if (pos > len)
            pos = len;
    }

    while (--pos >= 0)
        if (KSI_STR_PTR(str)[pos] == KSI_CHAR_CODE(chr))
            return ksi_long2num(pos);

    return ksi_false;
}

ksi_obj
ksi_string_upcase_x (ksi_obj s)
{
    int i;

    KSI_CHECK (s, KSI_STR_P (s), "string-upcase!: invalid string in arg1");
    KSI_CHECK (s, !KSI_C_STR_P (s), "string-upcase!: constant string in arg1");

    for (i = 0; i < KSI_STR_LEN(s); i++)
        KSI_STR_PTR(s)[i] = towupper(KSI_STR_PTR(s)[i]);
    return s;
}

ksi_obj
ksi_string_downcase_x (ksi_obj s)
{
    int i;

    KSI_CHECK (s, KSI_STR_P (s), "string-downcase!: invalid string in arg1");
    KSI_CHECK (s, !KSI_C_STR_P (s), "string-downcase!: constant string in arg1");

    for (i = 0; i < KSI_STR_LEN(s); i++)
        KSI_STR_PTR(s)[i] = towlower(KSI_STR_PTR(s)[i]);
    return s;
}

ksi_obj
ksi_string_capitalize_x (ksi_obj str)
{
    wchar_t *ptr;
    int i, len, fl;

    KSI_CHECK(str, KSI_STR_P(str), "string-capitalize!: invalid string in arg1");
    KSI_CHECK(str, !KSI_C_STR_P(str), "string-capitalize!: constant string in arg1");

    ptr = KSI_STR_PTR(str);
    len = KSI_STR_LEN(str);
    fl = 0;
    for (i = 0; i < len; i++) {
        if (iswalpha(ptr[i])) {
            if (fl) {
                ptr[i] = towlower(ptr[i]);
            } else {
                ptr[i] = towupper(ptr[i]);
                fl = 1;
            }
        } else {
            fl = 0;
        }
    }

    return str;
}

ksi_obj
ksi_string_for_each (ksi_obj proc, ksi_obj str, int ac, ksi_obj *av)
{
    int i, len, n;

    KSI_CHECK(proc, KSI_PROC_P(proc), "string-for-each: invalid procedure in arg1");
    KSI_CHECK(str, KSI_STR_P(str), "string-for-each: invalid string in arg2");

    len = KSI_STR_LEN(str);
    for (i = 0; i < ac; i++) {
        if (!KSI_STR_P(av[i]))
            ksi_exn_error(0, av[i], "string-for-each: invalid string in arg%d", i+3);
        if (len != KSI_STR_LEN(av[i]))
            ksi_exn_error(0, av[i], "string-for-each: invalid string length in arg%d", i+3);
    }
    if (ksi_procedure_has_arity_p(proc, ksi_long2num(ac+1), 0) == ksi_false)
        ksi_exn_error(0, proc, "string-for-each: invalid arity of the procedure in arg1");

    if (ac == 0) {
        wchar_t *ptr = KSI_STR_PTR(str);
        for (i = 0; i < len; i++) {
            ksi_obj ch = ksi_int2char(ptr[i]);
            ksi_apply_1(proc, ch);
        }
    } else {
        ksi_obj *xs = (ksi_obj*) alloca((ac + 1) * sizeof *xs);
        for (i = 0; i < len; i++) {
            wchar_t *ptr = KSI_STR_PTR(str);
            xs[0] = ksi_int2char(ptr[i]);
            for (n = 0; n < ac; n++) {
                ptr = KSI_STR_PTR(av[n]);
                xs[n+1] = ksi_int2char(ptr[i]);
            }
            ksi_apply_proc(proc, ac+1, xs);
        }
    }

    return ksi_void;
}

 /* End of code */
