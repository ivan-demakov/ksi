/*
 * ksi_jump.c
 * long jump & continuation
 *
 * Copyright (C) 1997-2010, 2014, Ivan Demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Fri Aug  8 00:01:49 1997
 * Last Update:   Fri Apr 25 22:45:26 2014
 *
 */

#include "ksi_jump.h"
#include "ksi_type.h"
#include "ksi_proc.h"
#include "ksi_gc.h"


static void copy_stack (struct Ksi_Jump* buf, KSI_WORD *start, KSI_WORD *end);
static void uncopy_stack (KSI_WORD *a, struct Ksi_Jump* buf);
static void restore_stack (KSI_WORD *a, struct Ksi_Jump* buf);
static void dowind (ksi_wind w);
static void unwind (ksi_wind wind);


/*
 * copy_stack
 *	Выделяет буфер необходимого размера для копии стека
 *	и копирует содержимое стека.
 */

static void
copy_stack (struct Ksi_Jump* buf, KSI_WORD *start, KSI_WORD *here)
{
    ptrdiff_t size;

#ifdef KSI_STACK_GROWS_UP
    size = here - start;
    buf->from = start;
#else
    size = start - here;
    buf->from = here;
#endif

    if (size > 0) {
        buf->stack = ksi_malloc (size * sizeof (*here));
        buf->size  = size;
    } else {
        buf->stack = 0;
        buf->size  = 0;
    }

    for (here = buf->stack, start = buf->from; --size >= 0; *here++ = *start++);
}


/*
 * restore_stack
 *	Совместно с функцией Ksi_Jump::uncopy_stack выделяет на стеке
 *	необходимое для восстановления сохраненного содержимого
 *	место.
 */

static void
restore_stack (KSI_WORD *a, struct Ksi_Jump *buf)
{
    KSI_WORD junk[100];

#if KSI_STACK_GROWS_UP
    junk[0]  = (KSI_WORD) KSI_STK_CMP(&junk[0], (buf->from + buf->size));
#else
    junk[0]  = (KSI_WORD) KSI_STK_CMP(&junk[0], buf->from);
#endif
    junk[1]  = (KSI_WORD) a[1];
    junk[99] = (KSI_WORD) junk;

    uncopy_stack (junk, buf);
}


/*
 * uncopy_stack
 *	Если размер стека слишком мал для востановления его содержимого,
 *	вызывает функцию Ksi_Jump::restore_stack, которая, выделив на
 *	стеке массив некоторого размера, снова вызывает uncopy_stack.
 */

static void
uncopy_stack (KSI_WORD *a, struct Ksi_Jump *buf)
{
    KSI_WORD *from;
    KSI_WORD *copy;
    ptrdiff_t size;

    if (!a[0])
        restore_stack (a, buf);

    KSI_FLUSH_REGISTER_WINDOWS;
    from = buf->from;
    copy = buf->stack;
    size = buf->size;
    while (--size >= 0)
        *from++ = *copy++;

    if (buf->wind != ksi_current_context->wind)
        dowind(buf->wind);

    longjmp(buf->j_buf, 1);
}


static void
dowind (ksi_wind w)
{
    if (w != ksi_current_context->wind) {
        dowind(w->cont);
        if (w->pre) ksi_apply_0(w->pre);
        ksi_current_context->wind = w;
    }
}


static void
unwind (ksi_wind wind)
{
    int len1 = 0, len2 = 0;
    ksi_wind w1, w2;

    for (w1 = wind; w1; w1 = w1->cont) ++len1;
    for (w2 = ksi_current_context->wind; w2; w2 = w2->cont) ++len2;

    for (w1 = wind; len1 > len2; w1 = w1->cont) --len1;
    for (w2 = ksi_current_context->wind; len2 > len1; w2 = w2->cont) ++len1;

    while (w1 != w2) {
        w1 = w1->cont;
        w2 = w2->cont;
    }

    /* w1 == w2 */
    while (ksi_current_context->wind != w1) {
        ksi_obj val = ksi_current_context->wind->post;
        ksi_current_context->wind = ksi_current_context->wind->cont;
        if (val) ksi_apply_0 (val);
    }
}

void
ksi_init_jump (ksi_jump b, KSI_WORD *start, KSI_WORD *here)
{
    /*
     * Store all needed values in b before copiing stack, since b may be placed
     * on stack and when we will restore stack this values will not corrupted.
     */

    KSI_ASSERT(ksi_current_context);

    b->wind = ksi_current_context->wind;
    if (start) {
        copy_stack(b, start, here);
    } else {
        b->stack = 0;
        b->size = 0;
    }
}

ksi_obj
ksi_set_jump (ksi_jump b, KSI_WORD *start)
{
    KSI_WORD here;

    KSI_ASSERT(ksi_current_context);

    ksi_init_jump(b, start, &here);
    KSI_FLUSH_REGISTER_WINDOWS;
    if (setjmp(b->j_buf))
        return ksi_current_context->jump_val;

    return 0;
}

void
ksi_long_jump (ksi_jump buf, ksi_obj val)
{
    KSI_WORD z[2];

    KSI_ASSERT(ksi_current_context);

    if (buf->wind != ksi_current_context->wind)
        unwind(buf->wind);

    ksi_current_context->jump_val = val;

    if (buf->stack == 0)
        longjmp(buf->j_buf, 1);

#if KSI_STACK_GROWS_UP
    z[0] = (KSI_WORD) KSI_STK_CMP(&z[0], (buf->from + buf->size));
#else
    z[0] = (KSI_WORD) KSI_STK_CMP(&z[0], buf->from);
#endif
    z[1] = (KSI_WORD) z;

    uncopy_stack (z, buf);
}


static ksi_obj
cont_prim (struct Ksi_Jump* jmp, int argc, ksi_obj* argv)
{
    ksi_long_jump (jmp, ksi_new_values (argc, argv));
    return ksi_void;
}

ksi_obj
ksi_new_continuation (KSI_WORD *here)
{
    struct Ksi_Jump *jmp;

    KSI_ASSERT(ksi_current_context);

    jmp = (struct Ksi_Jump*) ksi_malloc(sizeof *jmp);
    ksi_init_jump(jmp, ksi_current_context->stack, here);
    return ksi_close_proc((ksi_obj) ksi_new_prim (L"#<continuation>", cont_prim, KSI_CALL_REST1, 1), 1, (ksi_obj*) &jmp);
}

ksi_obj
ksi_continuation (ksi_obj *esc)
{
    struct Ksi_Jump *jmp;

    KSI_ASSERT(ksi_current_context);

    jmp = (struct Ksi_Jump*) ksi_malloc(sizeof *jmp);
    ksi_init_jump(jmp, ksi_current_context->stack, (KSI_WORD *) &jmp);

    *esc = ksi_close_proc((ksi_obj) ksi_new_prim(L"#<continuation>", cont_prim, KSI_CALL_REST1, 1), 1, (ksi_obj*) &jmp);

    KSI_FLUSH_REGISTER_WINDOWS;
    if (setjmp(jmp->j_buf))
        return ksi_current_context->jump_val;
    return 0;
}

ksi_wind
ksi_add_catch (ksi_obj tag, ksi_obj handler, int may_retry)
{
    ksi_wind wind;
    struct Ksi_Catch* the_catch;

    KSI_ASSERT(ksi_current_context);

    the_catch = (struct Ksi_Catch*) ksi_malloc(sizeof *the_catch);
    the_catch->tag = tag;
    the_catch->handler = handler;
    the_catch->retry = (may_retry ? ksi_false : 0);

    ksi_init_jump(&the_catch->jmp, 0, 0);

    wind = (ksi_wind) ksi_malloc(sizeof *wind);
    wind->cont = ksi_current_context->wind;
    wind->the_catch = the_catch;
    ksi_current_context->wind = wind;

    return wind;
}

void
ksi_del_catch (ksi_wind wind)
{
    KSI_ASSERT(ksi_current_context);
    KSI_ASSERT(wind && wind == ksi_current_context->wind);

    ksi_current_context->wind = ksi_current_context->wind->cont;
}

ksi_wind
ksi_find_catch (ksi_obj tag)
{
    struct Ksi_Catch* the_catch;
    ksi_wind wind;
    ksi_obj x;

    if (!ksi_current_context)
        return 0;

    for (wind = ksi_current_context->wind; wind; wind = wind->cont) {
        if (wind == ksi_current_context->exit_catch)
            return wind;

        the_catch = wind->the_catch;
        if (the_catch) {
            if (the_catch->tag == ksi_true || the_catch->tag == tag)
                return wind;

            for (x = the_catch->tag; KSI_PAIR_P(x); x = KSI_CDR(x))
                if (KSI_CAR(x) == tag)
                    return wind;
        }
    }

    return 0;
}

static ksi_obj
throw_prim (ksi_wind wind)
{
    ksi_obj tag = wind->the_catch->thrown;
    ksi_obj val = wind->the_catch->value;
    ksi_obj ret = wind->the_catch->retry;

    /* we are in post already */
    wind->post = 0;
    wind->the_catch->value = ksi_err;

    if (ret) {
        wind->the_catch->retry = ksi_false;
        val = ksi_apply_3 (wind->the_catch->handler, ret, tag, val);
    } else {
        val = ksi_apply_2 (wind->the_catch->handler, tag, val);
    }

    wind->the_catch->value = val;
    return ksi_void;
}

ksi_obj
ksi_throw_to_catch (ksi_wind wind, int retry, ksi_obj tag, ksi_obj val, ksi_rethrow_t rethrow)
{
    ksi_obj result;
    struct Ksi_Catch *the_catch;

    the_catch = wind->the_catch;
    KSI_ASSERT(the_catch != 0);

    the_catch->thrown  = tag;
    the_catch->value   = (val ? val : ksi_void);
    the_catch->rethrow = rethrow;

    if (the_catch->handler) {
        if (retry && the_catch->retry) {
            result = ksi_continuation(&the_catch->retry);
            if (result) {
                the_catch->retry = ksi_false;
                return result;
            }
        }

        wind->post = ksi_close_proc((ksi_obj) ksi_new_prim(L"#<catch>", throw_prim, KSI_CALL_ARG1, 1), 1, (ksi_obj*) &wind);
    }

    ksi_long_jump(&the_catch->jmp, ksi_true);

    /* never returns */
    return 0;
}

ksi_obj
ksi_rethrow (ksi_wind wind)
{
    ksi_obj tag = wind->the_catch->thrown;
    ksi_obj val = wind->the_catch->value;

    switch (wind->the_catch->rethrow) {
    case KSI_RETHROW_NORMAL: return ksi_throw (tag, val);
    case KSI_RETHROW_ERROR:  return ksi_throw_error (val);
    case KSI_RETHROW_EXIT:   return ksi_exit (val);
    }

    ksi_exn_error(0, 0, "ksi_rethrow: internal error");
    return 0;
}

ksi_obj
ksi_throw_error (ksi_obj exn)
{
    ksi_wind wind;

    if (!exn || !KSI_EXN_P(exn))
        exn = ksi_make_exn(0, exn, L"unspecified error", 0);

    wind = ksi_find_catch(ksi_err);
    if (wind)
        ksi_throw_to_catch(wind, 0, ksi_err, exn, KSI_RETHROW_ERROR);

    ksi_handle_error(ksi_current_context, ksi_err, exn);
    ksi_term();
    exit(KSI_ERROR_EXIT_CODE);

#ifndef __GNUC__
    return 0;
#endif
}

ksi_obj
ksi_exit (ksi_obj x)
{
    int code;

    if (!x)
        code = KSI_NORMAL_EXIT_CODE;
    else if (x == ksi_false)
        code = KSI_ERROR_EXIT_CODE;
    else if (x == ksi_true)
        code = KSI_NORMAL_EXIT_CODE;
    else if (KSI_EINT_P(x))
        code = ksi_num2long(x, "exit");
    else
        code = KSI_ERROR_EXIT_CODE;

    ksi_term();
    exit(code);

#ifndef __GNUC__
    return ksi_void;
#endif
}

ksi_obj
ksi_throw (ksi_obj tag, ksi_obj val)
{
    ksi_wind wind;

    if (tag == ksi_err)
        ksi_throw_error(val);

    wind = ksi_find_catch (tag);
    if (wind == 0)
        ksi_exn_error(0, tag, "throw: uncatched thrown object");

    return ksi_throw_to_catch(wind, 1, tag, val, KSI_RETHROW_NORMAL);
}

ksi_obj
ksi_dynamic_wind (ksi_obj pre, ksi_obj act, ksi_obj post)
{
    ksi_obj result;
    ksi_wind wind;

    KSI_ASSERT(ksi_current_context);

    ksi_apply_0(pre);

    wind = (ksi_wind) ksi_malloc(sizeof *wind);
    wind->cont = ksi_current_context->wind;
    wind->the_catch = 0;
    wind->pre = pre;
    wind->post = post;
    ksi_current_context->wind = wind;

    result = ksi_apply_0(act);

    ksi_current_context->wind = ksi_current_context->wind->cont;
    ksi_apply_0(post);
    return result;
}


ksi_obj
ksi_call_cc (ksi_obj proc)
{
    ksi_obj val, cont;

    KSI_CHECK(proc, KSI_PROC_P(proc), "call/cc: invalid procedure");
    val = ksi_continuation(&cont);
    return val ? val : ksi_apply_1(proc, cont);
}


ksi_obj
ksi_catch (ksi_obj tag, ksi_obj body, ksi_obj handler)
{
    volatile ksi_wind wind;
    KSI_CHECK(body, KSI_PROC_P(body), "catch: invalid procedure in arg2");
    KSI_CHECK(handler, !handler || KSI_PROC_P(handler), "catch: invalid procedure in arg3");

    wind = ksi_add_catch(tag, handler, 0);

    KSI_FLUSH_REGISTER_WINDOWS;
    if (setjmp(wind->the_catch->jmp.j_buf) == 0) {
        ksi_obj result = ksi_apply_0(body);
        ksi_del_catch(wind);
        return result;
    }

    return wind->the_catch->value;
}

ksi_obj
ksi_catch_with_retry (ksi_obj tag, ksi_obj body, ksi_obj handler)
{
    volatile ksi_wind wind;
    KSI_CHECK(body, KSI_PROC_P(body), "catch-with-retry: invalid procedure in arg2");
    KSI_CHECK(handler, !handler || KSI_PROC_P(handler), "catch-with-retry: invalid procedure in arg3");

    wind = ksi_add_catch(tag, handler, 1);

    KSI_FLUSH_REGISTER_WINDOWS;
    if (setjmp(wind->the_catch->jmp.j_buf) == 0) {
        ksi_obj result = ksi_apply_0(body);
        ksi_del_catch(wind);
        return result;
    }

    return wind->the_catch->value;
}


/* End of code */
