/*
 * ksi_vect.c
 * vectors
 *
 * Copyright (C) 1997-2010, 2015, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Fri Apr 25 03:59:34 1997
 * Last Update:   Thu Jul  9 06:38:57 2015
 *
 */

#include "ksi_type.h"
#include "ksi_env.h"
#include "ksi_proc.h"
#include "ksi_gc.h"


ksi_obj
ksi_alloc_vector (int dim, int tag)
{
  if (dim < 0) {
    return 0;
  } else {
    ksi_vector vec = ksi_malloc(sizeof(*vec) + (dim - 1) * sizeof(vec->arr[0]));
    vec -> o.itag = tag;
    vec -> o.ilen = dim;
    return (ksi_obj) vec;
  }
}

ksi_obj
ksi_new_vector (int argc, ksi_obj* argv)
{
  int i;
  ksi_obj vec = ksi_alloc_vector(argc, KSI_TAG_VECTOR);

  if (argv) {
      for (i = 0; i < argc; ++i)
          KSI_VEC_REF(vec, i) = argv[i];
  } else {
      for (i = 0; i < argc; ++i)
          KSI_VEC_REF(vec, i) = ksi_void;
  }

  return vec;
}

ksi_obj
ksi_copy_vector (ksi_obj x)
{
  ksi_obj vec;
  unsigned int i;

  KSI_CHECK(x, KSI_VEC_P(x), "copy-vector: invalid vector");

  vec = ksi_alloc_vector(KSI_VEC_LEN(x), KSI_TAG_VECTOR);
  for (i = 0; i < KSI_VEC_LEN(x); i++)
    KSI_VEC_REF (vec, i) = KSI_VEC_REF (x, i);

  return vec;
}

ksi_obj
ksi_vector_p (ksi_obj x)
{
  return KSI_VEC_P(x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_list2vector (ksi_obj x)
{
  ksi_obj vec;
  int i, len = ksi_list_len(x);
  KSI_CHECK (x, len >= 0, "list->vector: invalid list");

  vec = ksi_alloc_vector(len, KSI_TAG_VECTOR);
  for (i = 0; i < len; ++i, x = KSI_CDR(x))
    KSI_VEC_REF(vec, i) = KSI_CAR(x);

  return vec;
}

ksi_obj
ksi_vector2list (ksi_obj v)
{
  ksi_obj list = ksi_nil;
  int len;

  KSI_CHECK(v, KSI_VEC_P(v), "vector->list: invalid vector");

  len = KSI_VEC_LEN(v);
  while (len--)
    list = ksi_cons(KSI_VEC_REF(v, len), list);

  return list;
}

ksi_obj
ksi_make_vector (ksi_obj k, ksi_obj fill)
{
  ksi_obj vec;
  unsigned i, n;

  KSI_CHECK(k, KSI_UINT_P(k), "make-vector: invalid integer in arg1");
  n = ksi_num2ulong(k, "make-vector");

  if (!fill)
    fill = ksi_void;

  vec = ksi_alloc_vector (n, KSI_TAG_VECTOR);
  for (i = 0; i < n; ++i)
    KSI_VEC_REF(vec, i) = fill;

  return vec;
}

ksi_obj
ksi_vector_length (ksi_obj v)
{
  KSI_CHECK(v, KSI_VEC_P(v), "vector-length: invalid vector");
  return ksi_long2num(KSI_VEC_LEN(v));
}

ksi_obj
ksi_vector_ref (ksi_obj vec, ksi_obj k)
{
  unsigned int idx = 0;

  if (KSI_UINT_P (k))
    idx = ksi_num2uint (k, "vector-ref");
  else if (KSI_CHAR_P (k))
    idx = KSI_CHAR_CODE (k);
  else
    ksi_exn_error(0, k, "vector-ref: invalid index in arg2");

  KSI_CHECK (vec, KSI_VEC_P (vec), "vector-ref: invalid vector in arg1");
  KSI_CHECK (k, idx < KSI_VEC_LEN (vec), "vector-ref: index out of range in arg2");

  return KSI_VEC_REF (vec, idx);
}

ksi_obj
ksi_vector_set_x (ksi_obj vec, ksi_obj k, ksi_obj val)
{
    unsigned int idx = 0;

    if (KSI_UINT_P(k))
        idx = ksi_num2uint(k, "vector-set!");
    else if (KSI_CHAR_P(k))
        idx = KSI_CHAR_CODE(k);
    else
        ksi_exn_error(0, k, "vector-set!: invalid index in arg2");

    KSI_CHECK(vec, KSI_M_VEC_P(vec), "vector-set!: invalid or constant vector in arg1");
    KSI_CHECK(k, idx < KSI_VEC_LEN(vec), "vector-set!: index out of range in arg2");

    KSI_VEC_REF(vec, idx) = val;
    return ksi_void;
}

ksi_obj
ksi_vector_fill_x (ksi_obj vec, ksi_obj fill)
{
    int len;

    KSI_CHECK(vec, KSI_M_VEC_P(vec), "vector-fill!: invalid or constant vector in arg1");

    len = KSI_VEC_LEN(vec);
    while (--len >= 0)
        KSI_VEC_REF(vec, len) = fill;

    return ksi_void;
}

ksi_obj
ksi_vector_map (ksi_obj proc, ksi_obj vec, int ac, ksi_obj *av)
{
    int len, i;
    ksi_obj res;

    KSI_CHECK(proc, KSI_PROC_P(proc), "vector-map: invalid procedure in arg1");
    KSI_CHECK(vec, KSI_VEC_P(vec), "vector-map: invalid vector in arg2");

    len = KSI_VEC_LEN(vec);
    for (i = 0; i < ac; i++) {
        if (!KSI_VEC_P(av[i]))
            ksi_exn_error(0, av[i], "vector-map: invalid vector in arg%d", i+3);
        if (len != KSI_VEC_LEN(av[i]))
            ksi_exn_error(0, av[i], "vector-map: invalid vector length in arg%d", i+3);
    }
    if (ksi_procedure_has_arity_p(proc, ksi_long2num(ac+1), 0) == ksi_false)
        ksi_exn_error(0, proc, "vector-map: invalid arity of the procedure in arg1");

    if (ac == 0) {
        res = ksi_alloc_vector(len, KSI_TAG_VECTOR);
        while (--len >= 0) {
            KSI_VEC_REF(res, len) = ksi_apply_1(proc, KSI_VEC_REF(vec, len));
        }
    } else {
        ksi_obj *xs = (ksi_obj*) alloca((ac + 1) * sizeof *xs);
        res = ksi_alloc_vector(len, KSI_TAG_VECTOR);
        while (--len >= 0) {
            xs[0] = KSI_VEC_REF(vec, len);
            for (i = 0; i < ac; i++) {
                xs[i+1] = KSI_VEC_REF(av[i], len);
            }
            KSI_VEC_REF(res, len) = ksi_apply_proc(proc, ac+1, xs);
        }
    }

    return res;
}

ksi_obj
ksi_vector_for_each (ksi_obj proc, ksi_obj vec, int ac, ksi_obj *av)
{
    int i, len, n;

    KSI_CHECK(proc, KSI_PROC_P(proc), "vector-for-each: invalid procedure in arg1");
    KSI_CHECK(vec, KSI_VEC_P(vec), "vector-for-each: invalid vector in arg2");

    len = KSI_VEC_LEN(vec);
    for (i = 0; i < ac; i++) {
        if (!KSI_VEC_P(av[i]))
            ksi_exn_error(0, av[i], "vector-for-each: invalid vector in arg%d", i+3);
        if (len != KSI_VEC_LEN(av[i]))
            ksi_exn_error(0, av[i], "vector-for-each: invalid vector length in arg%d", i+3);
    }
    if (ksi_procedure_has_arity_p(proc, ksi_long2num(ac+1), 0) == ksi_false)
        ksi_exn_error(0, proc, "vector-for-each: invalid arity of the procedure in arg1");

    if (ac == 0) {
        for (i = 0; i < len; i++) {
            ksi_apply_1 (proc, KSI_VEC_REF (vec, i));
        }
    } else {
        ksi_obj *xs = (ksi_obj*) alloca((ac + 1) * sizeof *xs);
        for (i = 0; i < len; i++) {
            xs[0] = KSI_VEC_REF(vec, i);
            for (n = 0; n < ac; n++) {
                xs[n+1] = KSI_VEC_REF(av[n], len);
            }
            ksi_apply_proc(proc, ac+1, xs);
        }
    }

    return ksi_void;
}

/* End of code */
