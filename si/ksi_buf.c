/*
 * ksi_buf.c
 *
 * Copyright (C) 2006-2010, ivan demakov.
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Sun Dec 17 22:56:50 2006
 * Last Update:   Fri Jul 30 16:31:35 2010
 *
 */

#include "ksi_buf.h"
#include "ksi_gc.h"


struct Ksi_Buffer
{
  wchar_t  *data;
  size_t   size;    /* allocated chars */
  size_t   length;  /* used chars */
  size_t   step;    /* allocate step */
};


#define DEFAULT_STEP 32


static
size_t
ksi_adjust_size(size_t size, size_t step)
{
    size_t d = size % step;
    if (d != 0)
        size += step - d;
    return size;
}

ksi_buffer_t
ksi_new_buffer(size_t initial_size, size_t alloc_step)
{
    ksi_buffer_t buf = ksi_malloc(sizeof *buf);

    if (alloc_step <= 0)
        alloc_step = DEFAULT_STEP;

    if (initial_size <= 0)
        initial_size = DEFAULT_STEP;
    else
        initial_size = ksi_adjust_size(initial_size, alloc_step);

    buf->data   = ksi_malloc_data(initial_size * sizeof(buf->data[0]));
    buf->size   = initial_size;
    buf->length = 0;
    buf->step   = alloc_step;

    return buf;
}

static
ksi_buffer_t
ksi_buffer_realloc(ksi_buffer_t buf, size_t new_size)
{
    if (buf->size <= new_size) {
        new_size = ksi_adjust_size(new_size, buf->step);

        buf->data = ksi_realloc(buf->data, new_size * sizeof(buf->data[0]));
        buf->size = new_size;
    }

    return buf;
}


ksi_buffer_t
ksi_buffer_put(ksi_buffer_t buf, wchar_t c)
{
    ksi_buffer_realloc(buf, buf->length + 1);
    buf->data[buf->length++] = c;

    return buf;
}


ksi_buffer_t
ksi_buffer_append(ksi_buffer_t buf, const wchar_t *str, size_t len)
{
    ksi_buffer_realloc(buf, buf->length + len);
    wmemcpy(buf->data + buf->length, str, len);
    buf->length += len;

    return buf;
}


size_t
ksi_buffer_len(ksi_buffer_t buf)
{
    return buf->length;
}


wchar_t *
ksi_buffer_data(ksi_buffer_t buf)
{
    return buf->data;
}



 /* End of code */
