/*
 * ksi_vtab.c
 * symtab
 *
 * Copyright (C) 1997-2010, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Fri Aug  1 04:46:01 1997
 * Last Update:   Sat Aug 14 12:16:25 2010
 *
 */

#include "ksi_vtab.h"
#include "ksi_gc.h"

#if WIN32
#  define GC_THREADS 1
#  include <gc/gc.h>
#else
#  include <gc/gc.h>
#endif


static unsigned ksi_primes [] =
{
  17, 41, 113, 349, 1049, 3089, 6247, 8209, 14057, 21089, 31627,
  47431, 71143, 106721, 160073, 240101, 360163, 540217, 810343,
  810343, 1215497, 0
};

#define PRIMES_NUM      (sizeof (ksi_primes) / sizeof (ksi_primes [0]) - 1)
#define REBUILD_COUNT   3
#define MIN_SIZE        (ksi_primes [0])
#define MAX_SIZE        (ksi_primes [PRIMES_NUM-1])

unsigned *
ksi_get_primes(unsigned *size)
{
  if (size)
    *size = PRIMES_NUM;
  return ksi_primes;
}

static
void
ksi_rebuild_vtab (ksi_valtab_t tab, unsigned sz)
{
  struct Ksi_Tabrec *rec, **newtab;
  unsigned i, index;

  if (sz >= MAX_SIZE)
    return;

  for (i = 0; ksi_primes[i] <= sz; ++i)
    if (ksi_primes[i] <= 0)
      return;
  sz = ksi_primes[i];

  newtab = (struct Ksi_Tabrec**) ksi_malloc (sz * sizeof *newtab);

  for (i = 0; i < tab -> size; ++i) {
    while (tab->table[i]) {
      rec = tab->table[i];
      tab->table[i] = rec->next;

      index = tab->hash(rec->val, sz, tab->data);

      rec->next = newtab[index];
      newtab[index] = rec;
    }
  }

  ksi_free(tab->table);
  tab->table = newtab;
  tab->size = sz;
}

static
void
ksi_finalizer_valtab (void *obj, void *client_data)
{
  ksi_valtab_t tab = (ksi_valtab_t) obj;
  KSI_FINI_LOCK(tab->lock);
}

ksi_valtab_t
ksi_new_valtab (unsigned sz, ksi_hash_f h, ksi_cmp_f c, void* d)
{
  ksi_valtab_t tab = (ksi_valtab_t) ksi_malloc (sizeof *tab);
  int i;

  tab->count = 0;
  tab->inserts = 0;
  tab->hash = h;
  tab->cmp = c;
  tab->data = d;

  KSI_INIT_LOCK(tab->lock);
  GC_register_finalizer_no_order(tab, ksi_finalizer_valtab, 0, 0, 0);

  i = 0;
  while (i < PRIMES_NUM && ksi_primes[i] < sz) {
    ++i;
  }
  tab->size = ksi_primes[i];
  tab->table = ksi_malloc (tab->size * sizeof *tab->table);

  return tab;
}

void
ksi_clear_vtab (ksi_valtab_t tab)
{
  KSI_LOCK_W(tab->lock);

  ksi_free(tab->table);

  tab->count = 0;
  tab->size  = ksi_primes[0];
  tab->table = ksi_malloc(tab->size * sizeof *tab->table);

  KSI_UNLOCK_W(tab->lock);
}

void *
ksi_lookup_vtab (ksi_valtab_t tab, void *val, int append)
{
  register struct Ksi_Tabrec* rec;
  unsigned index, size, inserts;

  KSI_LOCK_R(tab->lock);
  size = tab->size;
  inserts = tab->inserts;
  index = tab->hash(val, tab->size, tab->data);

  for (rec = tab->table[index]; rec; rec = rec->next) {
    if (tab->cmp(rec->val, val, tab->data) == 0) {
      KSI_UNLOCK_R(tab->lock);
      return rec->val;
    }
  }
  KSI_UNLOCK_R(tab->lock);

  if (append) {
    KSI_LOCK_W(tab->lock);

    /* if another thread added a new item, try to lookup the item again */
    if (size != tab->size) {
      index = tab->hash(val, tab->size, tab->data);
    }
    if (inserts != tab->inserts) {
      for (rec = tab->table[index]; rec; rec = rec->next) {
        if (tab->cmp(rec->val, val, tab->data) == 0) {
          KSI_UNLOCK_W(tab->lock);
          return rec->val;
        }
      }
    }

    rec = (struct Ksi_Tabrec*) ksi_malloc (sizeof *rec);
    rec->val = val;

    rec->next = tab->table[index];
    tab->table[index] = rec;
    tab->count += 1;
    tab->inserts += 1;

    if (tab->count > tab->size * REBUILD_COUNT)
      ksi_rebuild_vtab(tab, tab->size);

    KSI_UNLOCK_W(tab->lock);
    return rec->val;
  }

  return 0;
}

void *
ksi_remove_vtab (ksi_valtab_t tab, void *val)
{
  register struct Ksi_Tabrec *rec, **ptr;
  unsigned index;

  KSI_LOCK_W(tab->lock);
  index = (tab->size <= 1 ? 0 : tab->hash(val, tab->size, tab->data));

  ptr = &tab->table[index];
  for (rec = tab->table[index]; rec; rec = rec->next) {
    if (tab->cmp(rec->val, val, tab->data) == 0) {
      val = rec->val;
      tab->count -= 1;

      *ptr = rec->next;

      KSI_UNLOCK_W(tab->lock);
      return val;
    }
    ptr = &rec->next;
  }

  KSI_UNLOCK_W(tab->lock);
  return 0;
}

void*
ksi_iterate_vtab (ksi_valtab_t tab, ksi_iter_f fun, void* iter_data)
{
  register int i;
  register struct Ksi_Tabrec* rec;

  KSI_LOCK_R(tab->lock);
  for (i = 0; i < tab->size; i++)
    for (rec = tab->table[i]; rec; rec = rec->next)
      if (fun(rec->val, iter_data)) {
        KSI_UNLOCK_R(tab->lock);
        return rec->val;
      }

  KSI_UNLOCK_R(tab->lock);
  return 0;
}


/* End of code */
