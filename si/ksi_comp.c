/*
 * ksi_comp.c
 * compilation
 *
 * Copyright (C) 1997-2011, 2014, 2015, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Tue Oct 28 16:06:31 1997
 * Last Update:   Fri Apr 25 22:40:42 2014
 *
 */

#include "ksi_comp.h"
#include "ksi_env.h"
#include "ksi_proc.h"
#include "ksi_port.h"
#include "ksi_jump.h"
#include "ksi_klos.h"
#include "ksi_printf.h"
#include "ksi_util.h"
#include "ksi_gc.h"


#ifdef _MSC_VER
#  include <io.h>
#  define F_OK 0
#  define W_OK 2
#  define R_OK 4
#  define X_OK R_OK
#endif

#if defined(unix) || defined(CYGWIN)
# define ABSOLUTE_PATH(p)			\
  (p[0] == '/' ||				\
   (p[0] == '.' && p[1] == '/') ||		\
   (p[0] == '.' && p[1] == '.' && p[2] == '/'))

# define DIR_SEP		"/"
# define PATH_SEP		":"

#elif defined(MSDOS) || defined(WIN32) || defined(OS2)

# define ABSOLUTE_PATH(p)                                               \
  ((p[0] == '/' || p[0] == '\\') ||                                     \
   (p[0] == '.' && (p[1] == '/' || p[1] == '\\')) ||                    \
   (p[0] == '.' && p[1] == '.' && (p[2] == '/' || p[2] == '\\')) ||     \
   (p[0] != '\0' && p[1] == ':' && (p[2] == '/' || p[2] == '\\')))

# define DIR_SEP		"\\"
# define PATH_SEP		";"

#else

# define ABSOLUTE_PATH(p) (0)
# define IS_DIR_SUFFIX(c) (0)

#endif


typedef struct Ksi_RefInfo *ksi_refinfo;
typedef struct Ksi_DepInfo *ksi_depinfo;
typedef struct Ksi_VarInfo *ksi_varinfo;
typedef struct Ksi_FrmInfo *ksi_frminfo;


struct Ksi_RefInfo
{
    ksi_refinfo next;
    ksi_obj var;
    ksi_obj exp;
};

struct Ksi_DepInfo
{
    ksi_depinfo next;
    ksi_varinfo var;
    int lev;
};

struct Ksi_VarInfo
{
    ksi_varinfo next;
    ksi_obj name;
    ksi_obj defs;
    ksi_refinfo refs;
    ksi_depinfo deps;
    int idx;
    int nloc;    /* var is refered from nested evironment */
    int larg;    /* var is lambda argument */
    int mark;    /* used for variable allocation */
    int stop;    /* used for variable allocation */
};

struct Ksi_EnvInfo
{
    struct Ksi_EObj ko;

    ksi_envinfo next;
    ksi_frminfo frm;
    ksi_varinfo vars;
    ksi_varinfo curvar;
    int top;
};

struct Ksi_FrmInfo
{
    ksi_frminfo next;
    ksi_varinfo vars;
    ksi_env env;
    int free;
    int mark;
};


static ksi_obj ksi_comp_sexp (ksi_obj form, ksi_envinfo env, const wchar_t *annotation);
static ksi_obj ksi_comp_apply (ksi_obj form, ksi_envinfo env);
static ksi_obj ksi_eval_port(const char *fname, ksi_obj port, ksi_env env);
static ksi_char_port ksi_open_src_port (ksi_byte_port file, const char *file_name, int file_line);
static ksi_char_port ksi_open_src_file (const char *fname);


ksi_frame
ksi_new_frame (int n, ksi_frame parent_frm)
{
    ksi_frame frm = ksi_malloc(sizeof(*frm) + (n - 1) * sizeof(frm->vals[0]));
    frm->num = n;
    frm->next = parent_frm;
    frm->env = (parent_frm ? parent_frm->env : 0);

    return frm;
}

ksi_code
ksi_new_code (int n, int tag)
{
    ksi_code code = ksi_malloc(sizeof(*code) + (n - 1) * sizeof(code->val[0]));
    code->o.itag = tag;
    code->o.ilen = n - 1;
    return code;
}

ksi_obj
ksi_new_quote (ksi_obj x)
{
    ksi_code q = ksi_new_code(1, KSI_TAG_QUOTE);
    q->val[0] = x;
    return (ksi_obj) q;
}

ksi_obj
ksi_new_freevar (ksi_obj sym, ksi_env env)
{
    ksi_freevar var = ksi_malloc(sizeof *var);
    var->o.itag = KSI_TAG_FREEVAR;
    var->sym = sym;
    var->env = env;
    var->val = 0;
    return (ksi_obj) var;
}

ksi_obj
ksi_new_id (ksi_obj sym, ksi_env env, const wchar_t *annotation)
{
    ksi_obj id = (ksi_obj) ksi_new_code(1, KSI_TAG_SYNTAX);
    KSI_CODE_VAL(id, 0) = ksi_new_freevar(sym, env);
    id->annotation = annotation;
    return id;
}

ksi_obj
ksi_new_varbox (int lev, int num)
{
    ksi_varbox var = ksi_malloc (sizeof *var);
    KSI_VARBOX_LEV(var) = lev;
    KSI_VARBOX_NUM(var) = num;
    var->o.itag = (lev == 0 ? KSI_TAG_VAR0 :
                   lev == 1 ? KSI_TAG_VAR1 :
                   lev == 2 ? KSI_TAG_VAR2 : KSI_TAG_VARN);

    return (ksi_obj) var;
}

ksi_closure
ksi_new_closure (int nums, int nary, int opts, ksi_frame frm, ksi_obj body)
{
    ksi_closure clos = ksi_malloc(sizeof (*clos));
    clos->o.itag = KSI_TAG_CLOSURE;
    KSI_CLOS_NUMS(clos) = nums;
    KSI_CLOS_NARY(clos) = nary;
    KSI_CLOS_OPTS(clos) = opts;
    clos->frm = (frm ? frm : ksi_new_frame(0, 0));
    clos->body = body;
    clos->doc = ksi_false;

    return clos;
}

static const wchar_t *
ksi_envinfo_tag_print (struct Ksi_EObj *x, int slashify)
{
    return ksi_aprintf ("#<%ls %p %ls>", x->etag->type_name, x, ksi_obj2str((ksi_obj) ((ksi_envinfo) x)->frm->env));
}

static struct Ksi_ETag tc_envinfo =
{
    L"syntax-env",
    ksi_default_tag_equal,
    ksi_envinfo_tag_print
};

static ksi_envinfo
ksi_new_envinfo (ksi_envinfo parent_env, ksi_frminfo frm)
{
    ksi_envinfo env = ksi_malloc(sizeof *env);
    env->ko.o.itag = KSI_TAG_EXTENDED;
    env->ko.etag = &tc_envinfo;
    env->next = parent_env;
    env->frm = frm;

    return env;
}

static ksi_envrec
ksi_get_freevar_rec(ksi_obj var)
{
    ksi_envrec rec = 0;

    if (KSI_FREEVAR_P(var)) {
        rec = KSI_FREEVAR_VAL(var);
        if (!rec)
            rec = ksi_lookup_env(KSI_FREEVAR_ENV(var), KSI_FREEVAR_SYM(var));
        if (rec) {
            var->itag = (rec->imported ? KSI_TAG_IMPORTED : KSI_TAG_LOCAL);
            KSI_FREEVAR_VAL(var) = rec;
        }
    }
    return rec;
}

static int
ksi_syntax_is (ksi_obj sym, int tag, ksi_envinfo env)
{
    ksi_varinfo cur;
    ksi_envrec rec = 0;

    KSI_CHECK(((ksi_obj) env), KSI_EXT_IS(((ksi_obj) env), &tc_envinfo), "ksi_syntax_is(): invalid env");

    if (KSI_SYM_P(sym)) {
        ksi_env glob = env->frm->env;
        for (/**/; env; env = env->next) {
            for (cur = env->vars; cur; cur = cur->next) {
                if (cur->name == sym && cur->idx >= 0) {
                    return 0;
                }
            }
        }

        rec = ksi_lookup_env(glob, sym);
    } else if (KSI_OBJ_IS(sym, KSI_TAG_SYNTAX)) {
        rec = ksi_get_freevar_rec(KSI_CODE_VAL(sym, 0));
    }

    if (rec && rec->syntax && KSI_OBJ_IS(rec->val, KSI_TAG_CORE)) {
        if (((ksi_core) rec->val)->o.ilen == tag)
            return 1;
    }
    return 0;
}

static int
ksi_aux_is (ksi_obj sym, ksi_obj val, ksi_envinfo env)
{
    ksi_varinfo cur;
    ksi_envrec rec = 0;

    KSI_CHECK(((ksi_obj) env), KSI_EXT_IS(((ksi_obj) env), &tc_envinfo), "ksi_aux_is(): invalid env");

    if (KSI_SYM_P(sym)) {
        ksi_env glob = env->frm->env;
        for (/**/; env; env = env->next) {
            for (cur = env->vars; cur; cur = cur->next) {
                if (cur->name == sym && cur->idx >= 0) {
                    return 0;
                }
            }
        }

        rec = ksi_lookup_env(glob, sym);
    } else if (KSI_OBJ_IS(sym, KSI_TAG_SYNTAX)) {
        rec = ksi_get_freevar_rec(KSI_CODE_VAL(sym, 0));
    }

    if (rec && rec->syntax) {
        if (rec->val == val)
            return 1;
    }
    return 0;
}

ksi_obj
ksi_bound_identifier_p (ksi_obj id, ksi_envinfo env)
{
    ksi_varinfo cur;
    ksi_envrec rec;

    KSI_CHECK(((ksi_obj) env), KSI_EXT_IS(((ksi_obj)env), &tc_envinfo), "bound-identifier?: invalid env in arg2");

    if (KSI_SYM_P(id)) {
        ksi_env glob = env->frm->env;
        for (/**/; env; env = env->next) {
            for (cur = env->vars; cur; cur = cur->next) {
                if (cur->name == id && cur->idx >= 0) {
                    return ksi_true;
                }
            }
        }
        rec = ksi_lookup_env(glob, id);
        if (rec) {
            return ksi_true;
        }
    }

    return ksi_false;
}

static ksi_obj
mk_const (ksi_obj x)
{
    unsigned int i;
    ksi_obj tmp;

    switch (x->itag) {
    case KSI_TAG_STRING:
        tmp = ksi_str2string(KSI_STR_PTR(x), KSI_STR_LEN(x));
        tmp->itag = KSI_TAG_CONST_STRING;
        tmp->annotation = x->annotation;
        return tmp;

    case KSI_TAG_VECTOR:
        tmp = (ksi_obj) ksi_alloc_vector(KSI_VEC_LEN(x), KSI_TAG_CONST_VECTOR);
        for (i = 0; i < KSI_VEC_LEN(x); i++)
            KSI_VEC_REF(tmp, i) = mk_const(KSI_VEC_REF(x, i));
        tmp->annotation = x->annotation;
        return tmp;

    case KSI_TAG_PAIR:
        tmp = ksi_cons(mk_const(KSI_CAR(x)), mk_const(KSI_CDR(x)));
        tmp->annotation = x->annotation;
        return tmp;
    }

    return x;
}

static ksi_obj
ksi_comp_quote (ksi_obj x, ksi_envinfo env, const wchar_t *annotation)
{
    x = mk_const(x);

    if (KSI_TAG_FIRST_CONST <= x->itag && x->itag < KSI_TAG_LAST_CONST)
        return x;

    x = ksi_new_quote(x);
    x->annotation = annotation;
    return x;
}

static ksi_obj
ksi_const_val (ksi_obj x)
{
    if (x->itag == KSI_TAG_QUOTE)
        return x;
    if (KSI_TAG_FIRST_CONST <= x->itag && x->itag < KSI_TAG_LAST_CONST)
        return x;
    return 0;
}

static void
append_dep (ksi_varinfo var, int lev, ksi_depinfo *list)
{
    ksi_depinfo dep;

    if (var->larg)
        return;

    for (dep = *list; dep; dep = dep->next)
        if (dep->var == var)
            return;

    dep = (ksi_depinfo) ksi_malloc (sizeof *dep);
    dep->next = *list;
    dep->var  = var;
    dep->lev  = lev;
    *list = dep;
}

static void
remove_dep (ksi_varinfo var, ksi_depinfo *list)
{
    for (/**/; *list; list = &(*list)->next)
        if ((*list)->var == var) {
            *list = (*list)->next;
            return;
        }
}

static int
weak_dep (ksi_varinfo def, ksi_varinfo ref)
{
    ksi_depinfo dep;

    if (def == ref)
        return 1;

    for (dep = ref->deps; dep; dep = dep->next) {
        if (def == dep->var && dep->lev > 0) {
            remove_dep(def, &ref->deps);
            return 1;
        }
    }

    return 0;
}

static ksi_obj
ksi_comp_sym (ksi_obj x, ksi_envinfo env, ksi_obj exp, const wchar_t *annotation)
{
    ksi_frminfo frm;
    ksi_refinfo ref;
    ksi_varinfo cur;
    ksi_obj var;
    int lev;

    frm = env->frm;
    for (/**/; env; env = env->next) {
        for (cur = env->vars; cur; cur = cur->next) {
            if (cur->name == x && cur->idx >= 0) {
                for (lev = 0; frm != env->frm; frm = frm->next, lev++) {
                    if (frm == 0)
                        ksi_exn_error(0, 0, "ksi_comp_sym: internal error -- invalid frame");
                }

                var = ksi_new_varbox(lev, cur->idx);
                var->annotation = annotation;

                ref = (ksi_refinfo) ksi_malloc(sizeof *ref);
                ref->next = cur->refs;
                ref->var = var;
                ref->exp = exp;
                cur->refs = ref;
                if (lev > 0)
                    cur->nloc = 1;

                /* for letrec bindings */
                if (env->curvar) {
                    if (lev == 0 || !weak_dep(env->curvar, cur)) {
                        append_dep(cur, lev, &env->curvar->deps);
                    }
                }

                return var;
            }
        }
    }

    var = ksi_new_freevar(x, frm->env);
    var->annotation = annotation;

    return var;
}

static void
del_ref (ksi_obj x, ksi_envinfo env)
{
    ksi_refinfo *lst;
    ksi_varinfo var;

    for (/**/; env; env = env->next)
        for (var = env->vars; var; var = var->next)
            for (lst = &var->refs; *lst; lst = &(*lst)->next)
                if ((*lst)->var == x) {
                    *lst = (*lst)->next;
                    return;
                }
}


static ksi_obj
ksi_comp_seq (ksi_obj seq, ksi_envinfo env, const wchar_t *annotation)
{
    unsigned int i, len;
    ksi_obj x, res;

    for (res = ksi_nil, len = 0; seq != ksi_nil; seq = KSI_CDR(seq)) {
        x = ksi_comp_sexp(KSI_CAR(seq), env, seq->annotation);
        if (x->itag == KSI_TAG_BEGIN) {
            for (i = 0; i <= KSI_CODE_NUM(x); i++) {
                if (KSI_CDR(seq) != ksi_nil && KSI_VARBOX_P(KSI_CODE_VAL(x, i))) {
                    del_ref(KSI_CODE_VAL(x, i), env);
                } else if (KSI_CDR(seq) == ksi_nil || !(ksi_const_val(KSI_CODE_VAL(x, i)) || KSI_FREEVAR_P(KSI_CODE_VAL(x, i)))) {
                    res = ksi_cons(KSI_CODE_VAL(x, i), res);
                    len += 1;
                }
            }
        } else if (KSI_CDR(seq) != ksi_nil && KSI_VARBOX_P(x)) {
            del_ref(x, env);
        } else if (KSI_CDR(seq) == ksi_nil || !(ksi_const_val(x) || KSI_FREEVAR_P(x))) {
            res = ksi_cons(x, res);
            len += 1;
        }
    }

    if (len == 0)
        return ksi_comp_quote(ksi_void, env, annotation);

    if (len == 1)
        return KSI_CAR(res);

    x = (ksi_obj) ksi_new_code(len, KSI_TAG_BEGIN);
    while (--len >= 0) {
        KSI_CODE_VAL(x, len) = KSI_CAR(res);
        res = KSI_CDR(res);
    }

    x->annotation = annotation;
    return x;
}


/* ksi_comp_define -- compile define expression
 *
 * <form>	::= (define <sym> <expr>)
 *		  | (define (<sym> <formals>) <body>)
 *
 * Note: definitions on top level(s) allowed only.
 */

static ksi_obj
ksi_comp_define (ksi_obj form, ksi_envinfo env, int syntax)
{
    ksi_obj x, sym;

    if (syntax) {
        if (!env->top)
            ksi_exn_error(L"syntax", form, "%ls: invalid context for definition", ksi_obj2str(KSI_CAR(form)));
    }

    x = KSI_CDR(form);
    if (!KSI_PAIR_P(x))
        ksi_exn_error(L"syntax", form, "%ls: missing identifier", ksi_obj2str(KSI_CAR(form)));

    sym = KSI_CAR(x);
    x = KSI_CDR(x);
    if (x == ksi_nil) {
        ksi_obj quote_id = ksi_new_id(ksi_data->sym_quote, ksi_data->syntax_env, form->annotation);
        x = KSI_LIST2(quote_id, ksi_void);
        x->annotation = form->annotation;
        x = KSI_LIST1(x);
        x->annotation = form->annotation;
    }

    while (KSI_PAIR_P(sym)) {
        ksi_obj lambda_id = ksi_new_id(ksi_data->sym_lambda, ksi_data->syntax_env, sym->annotation);

        x = KSI_LIST1(ksi_cons(lambda_id, ksi_cons(KSI_CDR(sym), x)));
        x->annotation = sym->annotation;
        sym = KSI_CAR(sym);
    }
    if (!KSI_SYM_P(sym))
        ksi_exn_error(L"syntax", form, "%ls: invalid identifier", ksi_obj2str(KSI_CAR(form)));

    if (env->top) {
        ksi_code r = ksi_new_code(3, KSI_TAG_DEFINE);
        KSI_CODE_VAL(r, 0) = sym;
        KSI_CODE_VAL(r, 1) = ksi_comp_sexp(KSI_CAR(x), env, x->annotation);
        KSI_CODE_VAL(r, 2) = syntax ? ksi_true : ksi_false;
        r->o.annotation = form->annotation;
        return (ksi_obj) r;
    } else {
        int idx = env->frm->free++;
        ksi_varinfo var = ksi_malloc(sizeof *var);
        ksi_varinfo prev_curvar = env->curvar;
        var->name = sym;
        var->idx  = idx;
        var->next = env->vars;
        env->vars = var;

        env->curvar = var;
        var->defs = ksi_comp_sexp(KSI_CAR(x), env, x->annotation);
        env->curvar = prev_curvar;
    }

    return ksi_comp_quote(ksi_void, env, form->annotation);
}


/* ksi_comp_set -- compile set! expression
 *
 * form		::= (set! <sym> <expr>)
 */

static ksi_obj
ksi_comp_set (ksi_obj form, ksi_envinfo env)
{
    ksi_obj val, x, sym;

    x = KSI_CDR(form);
    KSI_SYNTAX(form, KSI_PAIR_P(x), "set!: missing identifier");

    sym = KSI_CAR(x);
    KSI_SYNTAX(form, KSI_SYM_P(sym), "set!: invalid identifier");

    x = KSI_CDR(x);
    KSI_SYNTAX(form, KSI_PAIR_P(x), "set!: missing value");
    KSI_SYNTAX(form, KSI_CDR(x) == ksi_nil, "set!: invalid syntax");

    val = (ksi_obj) ksi_new_code(2, KSI_TAG_SET);
    KSI_CODE_VAL(val, 0) = ksi_comp_sym(sym, env, val, form->annotation);
    KSI_CODE_VAL(val, 1) = ksi_comp_sexp(KSI_CAR(x), env, x->annotation);

    val->annotation = form->annotation;
    return val;
}

static int
realloc_vars (ksi_frminfo frm, int free)
{
    ksi_varinfo var;
    ksi_refinfo ref;
    int *used, pos, mark, size;

    mark = frm->mark;
    for (var = frm->vars; var; var = var->next) {
        /* nonlocal variables is not reused */
        if (!var->larg && var->nloc) {
            for (ref = var->refs; ref; ref = ref->next)
                KSI_VARBOX_NUM(ref->var) = free;
            free++;
        }
        if (mark < var->mark)
            mark = var->mark;
    }

    /* local variables can be reused */
    size = free;
    used = alloca(sizeof(*used) * (mark - frm->mark + 2));
    used[0] = free;
    used[1] = free;
    pos = 1;
    mark = frm->mark;
    for (var = frm->vars; var; var = var->next) {
        if (mark < var->mark) {
            used[pos+1] = used[pos];
            pos++;
            mark = var->mark;
        } else if (mark > var->mark) {
            pos--;
            mark = var->mark;
            if (var->stop)
                used[pos] = used[pos-1];
        } else if (var->stop) {
            used[pos] = used[pos-1];
        }

        if (!var->larg && !var->nloc && var->refs) {
            for (ref = var->refs; ref; ref = ref->next)
                KSI_VARBOX_NUM(ref->var) = used[pos];
            used[pos]++;
        }
        if (size < used[pos])
            size = used[pos];
    }

    return size;
}

static void
copy_env_vars(ksi_frminfo frm, ksi_envinfo env)
{
    while (env->vars) {
        ksi_varinfo var = env->vars;
        env->vars = var->next;
        var->next = frm->vars;
        frm->vars = var;
        var->mark = frm->mark;
        var->stop = 0;
    }
    if (frm->vars)
        frm->vars->stop = 1;
}

static int
var_asigned (ksi_refinfo refs)
{
  for (/**/; refs; refs = refs->next)
    if (refs->exp)
      return 1;

  return 0;
}

static ksi_varinfo
find_var (ksi_obj var, ksi_envinfo env)
{
    ksi_frminfo frm;
    ksi_varinfo cur;
    unsigned int i;

    for (i = 0, frm = env->frm; i < KSI_VARBOX_LEV(var); i++, frm = frm->next) {
        if (frm == 0)
            ksi_exn_error(0, 0, "find_var: internal error -- null frame");
    }

    for (/**/; env; env = env->next)
        if (env->frm == frm)
            break;

    for (/**/; env; env = env->next)
        for (cur = env->vars; cur; cur = cur->next)
            if (cur->idx == KSI_VARBOX_NUM(var))
                return cur;

    ksi_exn_error(0, 0, "find_var: internal error -- cannot find variable");
    return 0;
}

static void
inline_var (ksi_varinfo var, ksi_envinfo env)
{
    if (var->defs && !var_asigned(var->refs)) {
        if (var->defs->itag == KSI_TAG_QUOTE && (KSI_QUOTE_VAL(var->defs)->itag == KSI_TAG_IMM)) {
            while (var->refs) {
                var->refs->var->itag = KSI_TAG_QUOTE;
                KSI_CODE_NUM(var->refs->var) = 0;
                KSI_CODE_VAL(var->refs->var, 0) = KSI_QUOTE_VAL(var->defs);
                var->refs = var->refs->next;
            }
        } else if (KSI_VARBOX_P(var->defs)) {
            ksi_refinfo tmp;
            ksi_varinfo def = find_var(var->defs, env);
            int lev;

            if (!var_asigned(def->refs)) {
                while (var->refs) {
                    tmp = var->refs;
                    var->refs = tmp->next;

                    lev = KSI_VARBOX_LEV(tmp->var) + KSI_VARBOX_LEV(var->defs);
                    tmp->var->itag = (lev == 0 ? KSI_TAG_VAR0 :
                                      lev == 1 ? KSI_TAG_VAR1 :
                                      lev == 2 ? KSI_TAG_VAR2 : KSI_TAG_VARN);
                    KSI_VARBOX_NUM(tmp->var) = def->idx;
                    KSI_VARBOX_LEV(tmp->var) = lev;

                    tmp->next = def->refs;
                    def->refs = tmp;
                    if (lev > 0)
                        def->nloc = 1;
                }
            }
        }
    }
}

static ksi_obj
gen_body (ksi_obj form, ksi_obj body, ksi_envinfo env, const wchar_t *annotation)
{
    unsigned int i, len, nextstep;
    ksi_obj v, x, res;
    ksi_refinfo ref;
    ksi_varinfo cur, p;

    for (cur = env->vars; cur; cur = cur->next)
        inline_var(cur, env);

    len = 0; res = ksi_nil;
again:
    nextstep = 0;
    for (cur = env->vars; cur; cur = cur->next) {
        if (cur->larg || !cur->defs)
            continue;

        if (cur->deps)
            continue;

        if (cur->refs == 0) {
            x = cur->defs;
            if (x->itag == KSI_TAG_BEGIN) {
                for (i = 0; i < KSI_CODE_NUM(x); i++) {
                    res = ksi_cons(KSI_CODE_VAL(x, i), res);
                    len += 1;
                }
                x = KSI_CODE_VAL(x, i);
            }

            if (ksi_const_val(x) || KSI_VARBOX_P(x) || KSI_FREEVAR_P(x) || x->itag == KSI_TAG_LAMBDA)
                goto next;
        } else {
            v = ksi_new_varbox(0, cur->idx);
            x = (ksi_obj) ksi_new_code(2, KSI_TAG_SET);
            KSI_CODE_VAL(x, 0) = v;
            KSI_CODE_VAL(x, 1) = cur->defs;

            ref = (ksi_refinfo) ksi_malloc(sizeof *ref);
            ref->next = cur->refs;
            ref->var = v;
            ref->exp = 0;
            cur->refs = ref;
        }

        res = ksi_cons(x, res);
        len += 1;

    next:
        nextstep++;
        cur->defs = 0;
        for (p = env->vars; p; p = p->next) {
            if (!p->larg) {
                remove_dep(cur, &p->deps);
            }
        }
    }
    for (cur = env->vars; cur; cur = cur->next) {
        if (!cur->larg) {
            if (cur->defs) {
                /*!! in letrec only possible  */
                if (nextstep)
                    goto again;
                ksi_exn_error(L"syntax", form, "letrec: invalid bindings");
            }
        }
    }

    if (body->itag != KSI_TAG_BEGIN) {
        res = ksi_cons(body, res);
        len += 1;
    } else {
        for (i = 0; i <= KSI_CODE_NUM(body); i++) {
            res = ksi_cons(KSI_CODE_VAL(body, i), res);
            len += 1;
        }
    }

    if (len == 1)
        return KSI_CAR(res);

    x = (ksi_obj) ksi_new_code(len, KSI_TAG_BEGIN);
    while (--len >= 0) {
        KSI_CODE_VAL(x, len) = KSI_CAR(res);
        res = KSI_CDR(res);
    }

    x->annotation = annotation;
    return x;
}


/* ksi_comp_lambda -- compile lambda expression
 *
 * <form>	::= (lambda <formals> <body>)
 */

static ksi_obj
ksi_comp_lambda (ksi_obj form, ksi_envinfo old_env)
{
    ksi_envinfo new_env;
    ksi_frminfo new_frm;
    ksi_obj formals, body, x;
    int len, nums, i, opts;

    len = ksi_list_len(form);
    KSI_SYNTAX(form, len >= 3, "lambda: invalid syntax");

    formals = KSI_CAR(KSI_CDR(form));
    body = KSI_CDR(KSI_CDR(form));

    opts = nums = 0;
    for (x = formals; x != ksi_nil; x = KSI_CDR(x), nums++) {
        if (KSI_SYM_P(x)) {
            nums += 1;
            opts = 1;
            break;
        }
        KSI_SYNTAX(form, KSI_PAIR_P(x) && KSI_SYM_P(KSI_CAR(x)), "lambda: invalid formals");
    }

    new_frm = ksi_malloc(sizeof *new_frm);
    new_frm->next = old_env->frm;
    new_frm->vars = 0;
    new_frm->env = old_env->frm->env;
    new_frm->free = nums;
    new_frm->mark = old_env->frm->mark + 1;

    new_env = ksi_new_envinfo(old_env, new_frm);

    for (i = 0, x = formals; i < nums; i++) {
        ksi_varinfo p = ksi_malloc(sizeof *p);
        p->next = new_env->vars;
        new_env->vars = p;

        p->idx = i;
        p->nloc = 1;
        p->larg = 1;

        if (KSI_SYM_P(x)) {
            p->name = x;
            break;
        } else {
            p->name = KSI_CAR(x);
            x = KSI_CDR(x);
        }
    }

    body = ksi_comp_seq(body, new_env, body->annotation);
    body = gen_body(form, body, new_env, body->annotation);

    copy_env_vars(new_frm, new_env);
    len = realloc_vars(new_frm, nums);

    x = (ksi_obj) ksi_new_code(4, KSI_TAG_LAMBDA);
    KSI_CODE_VAL(x, 0) = ksi_long2num(len);
    KSI_CODE_VAL(x, 1) = ksi_long2num(nums);
    KSI_CODE_VAL(x, 2) = ksi_long2num(opts);
    KSI_CODE_VAL(x, 3) = body;

    x->annotation = form->annotation;
    return x;
}


static int
split_bindings (char *name, ksi_obj bind, ksi_obj *vars, ksi_obj *init)
{
    int num;
    ksi_obj x;

    *vars = ksi_nil;
    *init = ksi_nil;
    for (num = 0; bind != ksi_nil; num++, bind = KSI_CDR(bind)) {
        if (!KSI_PAIR_P(bind))
            ksi_exn_error(L"syntax", bind, "%s: invalid binding", name);

        x = KSI_CAR(bind);
        if (ksi_list_len(x) != 2 || !KSI_SYM_P(KSI_CAR(x)))
            ksi_exn_error(L"syntax", x, "%s: invalid binding", name);

        *vars = ksi_cons(KSI_CAR(x), ksi_nil);
        *init = ksi_cons(KSI_CAR(KSI_CDR(x)), ksi_nil);
        (*init)->annotation = KSI_CDR(x)->annotation;

        vars = & KSI_CDR(*vars);
        init = & KSI_CDR(*init);
    }

    return num;
}


/* ksi_comp_letrec
 *
 * <form>	::= (letrec (<binding> ...) <expr> <expr> ...)
 * <binding>	::= (<var> <init>)
 */

static ksi_obj
ksi_comp_letrec (ksi_obj form, ksi_envinfo old_env)
{
    ksi_envinfo new_env;
    ksi_obj bind, body, vars, init, defs;
    int i, len, nums, free;
    ksi_varinfo cur;

    len = ksi_list_len(form) - 2;
    KSI_SYNTAX(form, len >= 1, "letrec: invalid syntax");

    bind = KSI_CAR(KSI_CDR(form));
    body = KSI_CDR(KSI_CDR(form));
    nums = split_bindings("letrec", bind, &vars, &init);

    new_env = ksi_new_envinfo(old_env, old_env->frm);

    free = new_env->frm->free;
    new_env->frm->free = free + nums;
    new_env->frm->mark += 1;

    defs = ksi_nil;
    for (i = 0; i < nums; i++, vars = KSI_CDR(vars), init = KSI_CDR(init)) {
        ksi_varinfo cur =  ksi_malloc(sizeof *cur);
        cur->next = new_env->vars;
        new_env->vars = cur;

        cur->name = KSI_CAR(vars);
        cur->idx = i + free;

        defs = ksi_cons(KSI_CAR(init), defs);
        defs->annotation = init->annotation;
    }

    for (cur = new_env->vars; cur; cur = cur->next, defs = KSI_CDR(defs)) {
        ksi_varinfo prev = new_env->curvar;
        new_env->curvar = cur;
        cur->defs = ksi_comp_sexp(KSI_CAR(defs), new_env, defs->annotation);
        new_env->curvar = prev;
    }

    body = ksi_comp_seq(body, new_env, body->annotation);
    body = gen_body(form, body, new_env, form->annotation);
    copy_env_vars(new_env->frm, new_env);
    new_env->frm->mark -= 1;
    return body;
}


/* ksi_comp_letrec_star
 *
 * <form>	::= (letrec* (<binding> ...) <expr> <expr> ...)
 * <binding>	::= (<var> <init>)
 */

static ksi_obj
ksi_comp_letrec_star (ksi_obj form, ksi_envinfo old_env)
{
    ksi_envinfo new_env;
    ksi_obj bind, body, vars, init, defs;
    int i, len, nums, free;
    ksi_varinfo cur;

    len = ksi_list_len(form) - 2;
    KSI_SYNTAX(form, len >= 1, "letrec*: invalid syntax");

    bind = KSI_CAR(KSI_CDR(form));
    body = KSI_CDR(KSI_CDR(form));
    nums = split_bindings("letrec*", bind, &vars, &init);

    new_env = ksi_new_envinfo(old_env, old_env->frm);

    free = new_env->frm->free;
    new_env->frm->free = free + nums;
    new_env->frm->mark += 1;

    defs = ksi_nil;
    for (i = 0; i < nums; i++, vars = KSI_CDR(vars), init = KSI_CDR(init)) {
        ksi_varinfo cur = ksi_malloc(sizeof *cur);
        cur->next = new_env->vars;
        new_env->vars = cur;

        cur->name = KSI_CAR(vars);
        cur->idx = i + free;
        cur->defs = ksi_comp_quote(ksi_void, new_env, init->annotation);

        defs = ksi_cons(KSI_CAR(init), defs);
    }

    for (cur = new_env->vars; cur; cur = cur->next, defs = KSI_CDR(defs)) {
        ksi_obj set_id = ksi_new_id(ksi_data->sym_set, ksi_data->syntax_env, form->annotation);
        ksi_obj x = KSI_LIST3(set_id, cur->name, KSI_CAR(defs));
        body = ksi_cons(x, body);
    }

    body = ksi_comp_seq(body, new_env, body->annotation);
    body = gen_body(form, body, new_env, form->annotation);
    copy_env_vars(new_env->frm, new_env);
    new_env->frm->mark -= 1;
    return body;
}


/* ksi_comp_let
 *
 * <form>	::= (let (<binding> ...) <expr> <expr> ...)
 *		  | (let <symbol> (<binding> ...) <expr> <expr> ...)
 * <bindings>	::= (<var> <init>)
 */

static ksi_obj
ksi_comp_let (ksi_obj form, ksi_envinfo old_env)
{
    ksi_envinfo new_env;
    ksi_obj x, bind, body, vars, init, defs;
    int i, len, nums, free;
    ksi_varinfo cur;

    len = ksi_list_len(form) - 2;
    KSI_SYNTAX(form, len >= 1, "let: invalid syntax");

    bind = KSI_CAR(KSI_CDR(form));
    body = KSI_CDR(KSI_CDR(form));

    if (KSI_SYM_P(bind)) {
        ksi_obj proc = bind, lambda_id, letrec_id;
        KSI_SYNTAX(form, KSI_PAIR_P(body), "let: invalid syntax");
        bind = KSI_CAR(body);
        body = KSI_CDR(body);
        split_bindings("let", bind, &vars, &init);

        lambda_id = ksi_new_id(ksi_data->sym_lambda, ksi_data->syntax_env, form->annotation);
        letrec_id = ksi_new_id(ksi_data->sym_letrec, ksi_data->syntax_env, KSI_CDR(form)->annotation);

        x = ksi_cons(lambda_id, ksi_cons(vars, body));
        x->annotation = KSI_CDR(form)->annotation;
        x = KSI_LIST2(proc, x);
        x->annotation = form->annotation;
        x = KSI_LIST3(letrec_id, KSI_LIST1(x), proc);
        x->annotation = form->annotation;
        x = ksi_cons(x, init);
        x->annotation = form->annotation;

        return ksi_comp_apply(x, old_env);
    }

    nums = split_bindings("let", bind, &vars, &init);

    new_env = ksi_new_envinfo(old_env, old_env->frm);

    free = new_env->frm->free;
    new_env->frm->free = free + nums;
    new_env->frm->mark += 1;

    defs = ksi_nil;
    for (i = 0; i < nums; i++, vars = KSI_CDR(vars), init = KSI_CDR(init)) {
        cur = ksi_malloc(sizeof *cur);
        cur->next = new_env->vars;
        new_env->vars = cur;

        cur->name = KSI_CAR(vars);
        cur->idx = -1; /* ignore this var in ksi_comp_sym() */
        cur->nloc = 0;
        cur->larg = 0;

        defs = ksi_cons(KSI_CAR(init), defs);
        defs->annotation = init->annotation;
    }

    for (cur = new_env->vars; cur; cur = cur->next, defs = KSI_CDR(defs))
        cur->defs = ksi_comp_sexp(KSI_CAR(defs), new_env, defs->annotation);

    for (cur = new_env->vars; cur; cur = cur->next)
        cur->idx = free++;

    body = ksi_comp_seq(body, new_env, body->annotation);
    body = gen_body(form, body, new_env, form->annotation);
    copy_env_vars(new_env->frm, new_env);
    new_env->frm->mark -= 1;
    return body;
}


static ksi_obj
ksi_comp_apply (ksi_obj form, ksi_envinfo env)
{
    ksi_obj x;
    int i, len = ksi_list_len(form);
    KSI_SYNTAX(form, len >= 1, "compile: invalid syntax");

    if (KSI_PAIR_P(KSI_CAR(form)) && ksi_syntax_is(KSI_CAR(KSI_CAR(form)), KSI_TAG_LAMBDA, env)) {
        ksi_obj formals, body, args, bind, let_id;

        /* ((lambda (f ...) body ...) e ...)
         *   ==>
         * (let ((f e) ...) body ...)
         */

        body = KSI_CAR(form);
        KSI_SYNTAX(body, ksi_list_len(body) >= 3, "lambda: invalid syntax");

        formals = KSI_CAR(KSI_CDR(body));
        body = KSI_CDR(KSI_CDR(body));
        args = KSI_CDR(form);

        for (bind = ksi_nil; /**/; formals = KSI_CDR(formals), args = KSI_CDR(args)) {
            if (formals == ksi_nil) {
                KSI_WNA(args == ksi_nil, 0, ksi_obj2str(KSI_CAR(form)));
                break;
            }

            if (KSI_SYM_P(formals)) {
                if (args == ksi_nil)
                    bind = ksi_cons(KSI_LIST2(formals, KSI_LIST2(ksi_data->sym_quote, ksi_nil)), bind);
                else
                    bind = ksi_cons(KSI_LIST2(formals, ksi_cons(KSI_LIST2(ksi_data->sym_quote, ksi_data->list_proc), args)), bind);
                break;
            }

            KSI_SYNTAX(KSI_CAR(form), KSI_PAIR_P(formals) && KSI_SYM_P(KSI_CAR(formals)), "lambda: invalid syntax");
            KSI_WNA(KSI_PAIR_P(args), 0, ksi_obj2str(KSI_CAR(form)));

            bind = ksi_cons(KSI_LIST2(KSI_CAR(formals), KSI_CAR(args)), bind);
        }

        let_id = ksi_new_id(ksi_data->sym_let, ksi_data->syntax_env, form->annotation);

        x = ksi_cons(let_id, ksi_cons(bind, body));
        x->annotation = form->annotation;
        return ksi_comp_let(x, env);
    }

    x = (ksi_obj) ksi_new_code(len, KSI_TAG_CALL);
    for (i = 0; i < len; i++, form = KSI_CDR(form))
        KSI_CODE_VAL(x, i) = ksi_comp_sexp(KSI_CAR(form), env, form->annotation);

    x->annotation = form->annotation;
    return x;
}


/* ksi_comp_if -- compile if expression
 *
 * form		::= (if <test-expr> <then-expr>)
 *		  | (if <test-expr> <then-expr> <else-expr>)
 */

static ksi_obj
ksi_comp_if (ksi_obj form, ksi_envinfo env)
{
    int len;
    ksi_obj x, args = KSI_CDR(form);

    len = ksi_list_len(args);
    KSI_SYNTAX(form, len == 2 || len == 3, "if: invalid syntax");

    x = (ksi_obj) ksi_new_code(3, KSI_TAG_IF);
    if (len == 3) {
        KSI_CODE_VAL(x, 0) = ksi_comp_sexp(KSI_CAR(args), env, args->annotation);
        KSI_CODE_VAL(x, 1) = ksi_comp_sexp(KSI_CAR(KSI_CDR(args)), env, KSI_CDR(args)->annotation);
        KSI_CODE_VAL(x, 2) = ksi_comp_sexp(KSI_CAR(KSI_CDR(KSI_CDR(args))), env, KSI_CDR(KSI_CDR(args))->annotation);
    } else {
        KSI_CODE_VAL(x, 0) = ksi_comp_sexp(KSI_CAR(args), env, args->annotation);
        KSI_CODE_VAL(x, 1) = ksi_comp_sexp(KSI_CAR(KSI_CDR(args)), env, KSI_CDR(args)->annotation);
        KSI_CODE_VAL(x, 2) = ksi_comp_quote(ksi_void, env, form->annotation);
    }

    x->annotation = form->annotation;
    return x;
}


static ksi_obj
ksi_comp_and (ksi_obj form, ksi_envinfo env)
{
    ksi_obj x, args;
    int len, i;

    args = KSI_CDR(form);
    len = ksi_list_len(args);
    KSI_SYNTAX(form, len >= 0, "and: invalid syntax");

    if (len == 0)
        return ksi_comp_quote(ksi_true, env, form->annotation);
    if (len == 1)
        return ksi_comp_sexp(KSI_CAR(args), env, args->annotation);

    x = (ksi_obj) ksi_new_code(len, KSI_TAG_AND);
    for (i = 0; i < len; i++, args = KSI_CDR(args))
        KSI_CODE_VAL(x, i) = ksi_comp_sexp(KSI_CAR(args), env, args->annotation);

    x->annotation = form->annotation;
    return x;
}

static ksi_obj
ksi_comp_or (ksi_obj form, ksi_envinfo env)
{
    ksi_obj x, args;
    int len, i;

    args = KSI_CDR(form);
    len = ksi_list_len(args);
    KSI_SYNTAX(form, len >= 0, "or: invalid syntax");

    if (len == 0)
        return ksi_comp_quote(ksi_false, env, form->annotation);
    if (len == 1)
        return ksi_comp_sexp(KSI_CAR(args), env, args->annotation);

    x = (ksi_obj) ksi_new_code(len, KSI_TAG_OR);
    for (i = 0; i < len; i++, args = KSI_CDR(args))
        KSI_CODE_VAL(x, i) = ksi_comp_sexp(KSI_CAR(args), env, args->annotation);

    x->annotation = form->annotation;
    return x;
}


/* ksi_cond_macro -- compile cond expresion
 *
 * <form>	::= (cond <clause> <clause> ...)
 * <clause>	::= (<test> <expr> ...)
 *		  | (<test> => <recipient>)
 *		  | (else <expr> <expr> ...).
 */

ksi_obj
ksi_cond_macro (ksi_obj form, ksi_envinfo env)
{
    ksi_obj x, c, cl, cls, sym, quote_id, begin_id, let_id, if_id;
    int len;

    cls = KSI_CDR (form);
    if (cls == ksi_nil) {
        quote_id = ksi_new_id(ksi_data->sym_quote, ksi_data->syntax_env, form->annotation);

        x = KSI_LIST2(quote_id, ksi_void);
        x->annotation = form->annotation;
        return x;
    }

    KSI_SYNTAX(form, KSI_PAIR_P(cls), "cond: invalid syntax");
    cl = KSI_CAR(cls);
    cls = KSI_CDR(cls);

    len = ksi_list_len(cl);
    KSI_SYNTAX(form, len > 0, "cond: invalid syntax");

    if (ksi_aux_is(KSI_CAR(cl), ksi_data->sym_else, env)) {
        KSI_SYNTAX(form, len > 1 && cls == ksi_nil, "cond: invalid syntax");

        begin_id = ksi_new_id(ksi_data->sym_begin, ksi_data->syntax_env, form->annotation);

        x = ksi_cons(begin_id, KSI_CDR(cl));
        x->annotation = cl->annotation;
        return x;
    }

    if (len == 1) {
        sym = ksi_gensym(0, 0);

        let_id = ksi_new_id(ksi_data->sym_let, ksi_data->syntax_env, form->annotation);
        if_id = ksi_new_id(ksi_data->sym_if, ksi_data->syntax_env, cl->annotation);

        x = KSI_LIST4(if_id, sym, sym, ksi_cons(KSI_CAR(form), cls));
        x->annotation = cl->annotation;
        x = KSI_LIST3(let_id, KSI_LIST1(KSI_LIST2(sym, KSI_CAR(cl))), x);
        x->annotation = form->annotation;
        return x;
    }

    if (len == 3 && ksi_aux_is(KSI_CAR(KSI_CDR(cl)), ksi_data->sym_arrow, env)) {
        sym = ksi_gensym(0, 0);
        let_id = ksi_new_id(ksi_data->sym_let, ksi_data->syntax_env, form->annotation);
        if_id = ksi_new_id(ksi_data->sym_if, ksi_data->syntax_env, cl->annotation);

        x = ksi_cons(KSI_CAR(form), cls);
        x->annotation = cls->annotation;
        c = KSI_LIST2(KSI_CAR(KSI_CDR(KSI_CDR(cl))), sym);
        c->annotation = cl->annotation;
        x = KSI_LIST4(if_id, sym, c, x);
        x->annotation = cl->annotation;
        x = KSI_LIST3(let_id, KSI_LIST1(KSI_LIST2(sym, KSI_CAR(cl))), x);
        x->annotation = form->annotation;
        return x;
    }

    x = ksi_cons(KSI_CAR(form), cls);
    x->annotation = cls->annotation;
    c = ksi_cons(ksi_data->sym_begin, KSI_CDR(cl));
    c->annotation = KSI_CDR(cl)->annotation;
    x = KSI_LIST3(KSI_CAR(cl), c, x);
    x->annotation = cl->annotation;
    x = ksi_cons(ksi_data->sym_if, x);
    x->annotation = form->annotation;
    return x;
}


/* ksi_case_macro -- compile case expresion
 *
 * <case-expr>	::= (case <key> <clause> <clause> ...)
 * <clause>	::= ((<datum> <datum> ...) <body>) | (else <body>)
 * <body>	::= <expr> <expr> ...
 *
 */

ksi_obj
ksi_case_macro (ksi_obj form, ksi_envinfo env)
{
    ksi_obj x, key, c, cl, cls, sym, let_id, begin_id, quote_id, if_id;

    cls = KSI_CDR(form);
    KSI_SYNTAX(form, KSI_PAIR_P(cls), "case: invalid syntax");

    key = KSI_CAR(cls);
    cls = KSI_CDR(cls);

    if (KSI_PAIR_P(key)) {
        sym = ksi_gensym(0, 0);
        let_id = ksi_new_id(ksi_data->sym_let, ksi_data->syntax_env, form->annotation);

        x = ksi_cons(KSI_CAR(form), ksi_cons(sym, cls));
        x->annotation = form->annotation;
        x = KSI_LIST3(let_id, KSI_LIST1(KSI_LIST2(sym, key)), x);
        x->annotation = key->annotation;
        return x;
    }

    if (cls == ksi_nil) {
        quote_id = ksi_new_id(ksi_data->sym_quote, ksi_data->syntax_env, form->annotation);

        x = KSI_LIST2(quote_id, ksi_void);
        x->annotation = form->annotation;
        return x;
    }

    KSI_SYNTAX(form, KSI_PAIR_P(cls), "case: invalid syntax");
    cl = KSI_CAR(cls);
    cls = KSI_CDR(cls);

    KSI_SYNTAX(form, ksi_list_len(cl) > 1, "case: invalid syntax");
    c = KSI_CAR(cl);
    cl = KSI_CDR(cl);

    if (ksi_aux_is(c, ksi_data->sym_else, env)) {
        KSI_SYNTAX(form, cls == ksi_nil, "case: invalid syntax");
        begin_id = ksi_new_id(ksi_data->sym_begin, ksi_data->syntax_env, form->annotation);

        x = ksi_cons(begin_id, cl);
        x->annotation = cl->annotation;
        return x;
    }

    if (c == ksi_nil) {
        x = ksi_cons(KSI_CAR(form), ksi_cons(key, cls));
        x->annotation = cls->annotation;
        return x;
    }

    KSI_SYNTAX(form, ksi_list_len(c) >= 1, "case: invalid syntax");
    if (KSI_CDR(c) == ksi_nil) {
        begin_id = ksi_new_id(ksi_data->sym_begin, ksi_data->syntax_env, form->annotation);
        if_id = ksi_new_id(ksi_data->sym_if, ksi_data->syntax_env, form->annotation);

        x = KSI_LIST3(KSI_LIST2(ksi_data->sym_quote, ksi_data->eqv_proc), key, KSI_LIST2(ksi_data->sym_quote, KSI_CAR(c)));
        x->annotation = c->annotation;
        c = ksi_cons(begin_id, cl);
        c->annotation = cl->annotation;
        cl = ksi_cons(KSI_CAR(form), ksi_cons(key, cls));
        cl->annotation = cls->annotation;
        x = KSI_LIST4(if_id, x, c, cl);
        x->annotation = form->annotation;
        return x;
    }

    quote_id = ksi_new_id(ksi_data->sym_quote, ksi_data->syntax_env, form->annotation);
    begin_id = ksi_new_id(ksi_data->sym_begin, ksi_data->syntax_env, form->annotation);
    if_id = ksi_new_id(ksi_data->sym_if, ksi_data->syntax_env, form->annotation);

    x = KSI_LIST3(KSI_LIST2(quote_id, ksi_data->memv_proc), key, KSI_LIST2(quote_id, c));
    x->annotation = c->annotation;
    c = ksi_cons(begin_id, cl);
    c->annotation = cl->annotation;
    cl = ksi_cons(KSI_CAR(form), ksi_cons(key, cls));
    cl->annotation = cls->annotation;
    x = KSI_LIST4(if_id, x, c, cl);
    x->annotation = form->annotation;
    return x;
}


ksi_obj
ksi_letstar_macro (ksi_obj form, ksi_envinfo env)
{
    ksi_obj bind, body, bx, x, let_id;
    int len = ksi_list_len(form);
    KSI_SYNTAX(form, len >= 3, "let*: invalid syntax");

    bind = KSI_CAR(KSI_CDR(form));
    body = KSI_CDR(KSI_CDR(form));
    if (bind == ksi_nil) {
        let_id = ksi_new_id(ksi_data->sym_let, ksi_data->syntax_env, form->annotation);

        x = ksi_cons(let_id, ksi_cons(ksi_nil, body));
        x->annotation = form->annotation;
        return x;
    }

    len = ksi_list_len(bind);
    KSI_SYNTAX(form, len >= 1, "let*: invalid syntax");

    bx = KSI_CAR(bind);
    bind = KSI_CDR(bind);

    len = ksi_list_len(bx);
    KSI_SYNTAX(form, len == 2, "let*: invalid syntax");
    KSI_SYNTAX(form, KSI_SYM_P(KSI_CAR(bx)), "let*: invalid syntax");

    if (bind == ksi_nil) {
        let_id = ksi_new_id(ksi_data->sym_let, ksi_data->syntax_env, form->annotation);

        x = ksi_cons(let_id, ksi_cons(KSI_LIST1(bx), body));
        x->annotation = form->annotation;
        return x;
    }

    let_id = ksi_new_id(ksi_data->sym_let, ksi_data->syntax_env, form->annotation);

    x = ksi_cons(KSI_CAR(form), ksi_cons(bind, body));
    x->annotation = form->annotation;
    x = KSI_LIST3(let_id, KSI_LIST1(bx), x);
    x->annotation = form->annotation;
    return x;
}

static int
expr_is_nil(ksi_obj x, ksi_envinfo env)
{
    if (KSI_PAIR_P(x) && (ksi_syntax_is(KSI_CAR(x), KSI_TAG_SYNTAX, env) || ksi_syntax_is(KSI_CAR(x), KSI_TAG_QUOTE, env))) {
        ksi_obj z = KSI_CDR(x);
        if (KSI_PAIR_P(z) && KSI_CAR(z) == ksi_nil && KSI_CDR(z) == ksi_nil) {
            return 1;
        }
    }

    return (x == ksi_nil ? 1 : 0);
}

static ksi_obj
mqq (ksi_obj qq, ksi_obj q, ksi_obj uq, ksi_obj uqs, ksi_obj cons, ksi_obj nil, ksi_obj form, int depth, ksi_envinfo env)
{
    ksi_obj x0, x1, xs, rs, ss, *p, quote_id;

    if (KSI_PAIR_P(form)) {
        x0 = KSI_CAR(form);
        xs = KSI_CDR(form);

        if (x0 == qq) {
            KSI_SYNTAX(form, KSI_PAIR_P(xs), "quasiquote: invalid syntax in quiasiquote subexpression");
            x1 = KSI_CAR(xs);
            xs = KSI_CDR(xs);
            KSI_SYNTAX(form, xs == ksi_nil, "quasiquote: invalid syntax in quiasiquote subexpression");
            x0 = KSI_LIST2(q, x0);
            x0->annotation = form->annotation;
            x1 = mqq(qq, q, uq, uqs, cons, nil, x1, depth+1, env);
            x1->annotation = form->annotation;
            xs = KSI_LIST3(cons, x1, nil);
            xs->annotation = form->annotation;
            return KSI_LIST3(cons, x0, xs);
        }

        if (ksi_aux_is(x0, uq, env)) {
            int len = ksi_list_len(xs);
            if (depth == 0) {
                KSI_SYNTAX(form, len == 1, "quasiquote: invalid syntax in unquote subexpression");
                return KSI_CAR(xs);
            }
            KSI_SYNTAX(form, len >= 0, "quasiquote: invalid syntax in unquote subexpression");
            rs = nil;
            p = &rs;
            while (KSI_PAIR_P(xs)) {
                x1 = mqq(qq, q, uq, uqs, cons, nil, KSI_CAR(xs), depth-1, env);
                x1->annotation = form->annotation;
                *p = KSI_LIST3(cons, x1, nil); /* (cons x1 '()) -> (cons x1 (cons x2 '()) */
                p = & KSI_CAR(KSI_CDR(KSI_CDR(*p)));
                xs = KSI_CDR(xs);
            }
            x0 = KSI_LIST2(q, x0);
            x0->annotation = form->annotation;
            return KSI_LIST3(cons, x0, rs);
        }

        if (ksi_aux_is(x0, uqs, env)) {
            ksi_exn_error(L"syntax", form, "quasiquote: invalid context for unquote-splicing subexpression");
        }

        if (KSI_PAIR_P(x0)) {
            x1 = KSI_CAR(x0);

            if (ksi_aux_is(x1, uq, env)) {
                ss = KSI_CDR(x0);
                KSI_SYNTAX(form, ksi_list_len(ss) >= 0, "quasiquote: invalid syntax in unquote subexpression");
                if (depth == 0) {
                    xs = mqq(qq, q, uq, uqs, cons, nil, xs, depth, env);
                    xs->annotation = form->annotation;
                    rs = xs;
                    for (p = &rs; KSI_PAIR_P(ss); ss = KSI_CDR(ss)) {
                        x0 = KSI_CAR(ss);
                        *p = KSI_LIST3(cons, x0, xs);
                        (*p)->annotation = form->annotation;
                        p = & KSI_CAR(KSI_CDR(KSI_CDR(*p)));
                    }
                    return rs;
                }
            unquote:
                rs = nil;
                for (p = &rs; KSI_PAIR_P(ss); ss = KSI_CDR(ss)) {
                    x0 = mqq(qq, q, uq, uqs, cons, nil, KSI_CAR(ss), depth-1, env);
                    x0->annotation = form->annotation;
                    *p = KSI_LIST3(cons, x0, nil);
                    (*p)->annotation = form->annotation;
                    p = & KSI_CAR(KSI_CDR(KSI_CDR(*p)));
                }
                x1 = KSI_LIST2(q, x1);
                x1->annotation = form->annotation;
                x1 = KSI_LIST3(cons, x1, rs);
                x1->annotation = form->annotation;
                xs = mqq(qq, q, uq, uqs, cons, nil, xs, depth, env);
                xs->annotation = form->annotation;
                return KSI_LIST3(cons, x1, xs);
            }

            if (ksi_aux_is(x1, uqs, env)) {
                ss = KSI_CDR(x0);
                KSI_SYNTAX(form, ksi_list_len(ss) >= 0, "quasiquote: invalid syntax in unquote-splicing subexpression");
                if (depth == 0) {
                    int add_some = 0, xs_is_nil = 0;
                    xs = mqq(qq, q, uq, uqs, cons, nil, xs, depth, env);
                    if (expr_is_nil(xs, env))
                        xs_is_nil = 1;
                    else
                        xs->annotation = form->annotation;
                    rs = (xs_is_nil ? ksi_nil : KSI_LIST1(xs));
                    for (p = &rs; KSI_PAIR_P(ss); ss = KSI_CDR(ss)) {
                        x0 = KSI_CAR(ss);
                        if (expr_is_nil(x0, env))
                            continue;
                        add_some = 1;
                        *p = ksi_cons(x0, *p);
                        (*p)->annotation = form->annotation;
                        p = & KSI_CDR(*p);
                    }
                    if (!add_some)
                        return xs;
                    /* here rs is pair, because add_some != 0 */
                    if (xs_is_nil && KSI_CDR(rs) == ksi_nil)
                        return KSI_CAR(rs);

                    quote_id = ksi_new_id(ksi_data->sym_quote, ksi_data->syntax_env, form->annotation);
                    return ksi_cons(KSI_LIST2(quote_id, ksi_data->append_proc), rs);
                } /* depth == 0 */
                goto unquote;
            }
        }

        x0 = mqq(qq, q, uq, uqs, cons, nil, x0, depth, env);
        x0->annotation = form->annotation;
        xs = mqq(qq, q, uq, uqs, cons, nil, xs, depth, env);
        xs->annotation = form->annotation;
        return KSI_LIST3(cons, x0, xs);
    }

    if (KSI_VEC_P(form)) {
        quote_id = ksi_new_id(ksi_data->sym_quote, ksi_data->syntax_env, form->annotation);

        xs = ksi_vector2list(form);
        xs->annotation = form->annotation;
        xs = mqq(qq, q, uq, uqs, cons, nil, xs, depth, env);
        xs->annotation = form->annotation;
        x0 = KSI_LIST2(quote_id, ksi_data->list2vector_proc);
        x0->annotation = form->annotation;
        return KSI_LIST2(x0, xs);
    }

    return (form == ksi_nil ? nil : KSI_LIST2(q, form));
}

ksi_obj
ksi_quasiquote_macro (ksi_obj form, ksi_envinfo env)
{
    ksi_obj x, nil, cons, quote_id;
    KSI_SYNTAX(form, ksi_list_len(form) == 2, "quasiquote: invalid syntax");

    quote_id = ksi_new_id(ksi_data->sym_quote, ksi_data->syntax_env, form->annotation);

    cons = KSI_LIST2(quote_id, ksi_data->cons_proc);
    nil = KSI_LIST2(quote_id, ksi_nil);
    x = mqq(KSI_CAR(form), quote_id, ksi_data->sym_unquote, ksi_data->sym_unquote_splicing, cons, nil, KSI_CAR(KSI_CDR(form)), 0, env);
    x->annotation = form->annotation;
    return x;
}

ksi_obj
ksi_quasisyntax_macro (ksi_obj form, ksi_envinfo env)
{
    ksi_obj x, nil, cons, quote_id, syn_id;
    KSI_SYNTAX(form, ksi_list_len(form) == 2, "quasisyntax: invalid syntax");

    quote_id = ksi_new_id(ksi_data->sym_quote, ksi_data->syntax_env, form->annotation);
    syn_id = ksi_new_id(ksi_data->sym_syntax, ksi_data->syntax_env, form->annotation);

    cons = KSI_LIST2(quote_id, ksi_data->cons_proc);
    nil = KSI_LIST2(quote_id, ksi_nil);
    x = mqq(KSI_CAR(form), syn_id, ksi_data->sym_unsyntax, ksi_data->sym_unsyntax_splicing, cons, nil, KSI_CAR(KSI_CDR(form)), 0, env);
    x->annotation = form->annotation;
    return x;
}

static ksi_obj
parse_libname(ksi_obj spec, ksi_obj *ver)
{
    ksi_obj x, lib;
    int i, len = ksi_list_len(spec);
    KSI_SYNTAX(spec, len > 0, "import: invalid library name");

    lib = ksi_nil;
    *ver = ksi_nil;
    for (i = 0; i < len; i++) {
        x = KSI_CAR(spec);
        spec = KSI_CDR(spec);
        if (spec == ksi_nil && (x == ksi_nil || KSI_PAIR_P(x))) {
            *ver = x;
            break;
        }
        lib = ksi_cons(x, lib);
    }
    return ksi_reverse_x(lib);
}

static ksi_obj
import_lib (ksi_obj spec)
{
    ksi_obj ver, lib = parse_libname(spec, &ver);
    ksi_code code = ksi_new_code (3, KSI_TAG_IMPORT);
    code->val[0] = ksi_data->sym_library;
    code->val[1] = lib;
    code->val[2] = ver;
    return (ksi_obj) code;
}

static ksi_obj
import_set (ksi_obj set)
{
    ksi_obj x = 0;
    int i, len = ksi_list_len(set);
    KSI_SYNTAX(set, len > 0, "import: invalid import set");

    if (KSI_CAR(set) == ksi_data->sym_only || KSI_CAR(set) == ksi_data->sym_except) {
        KSI_SYNTAX(set, len >= 2, "import: invalid import set");
        x = (ksi_obj) ksi_new_code(len, KSI_TAG_IMPORT);
        KSI_CODE_VAL(x, 0) = KSI_CAR(set);
        set = KSI_CDR(set);
        KSI_CODE_VAL(x, 1) = import_set(KSI_CAR(set));
        set = KSI_CDR(set);
        for (i = 2; i < len; i++) {
            ksi_obj sym = KSI_CAR(set);
            KSI_SYNTAX(sym, KSI_SYM_P(sym), "import: invalid symbol in import set");
            KSI_CODE_VAL(x, i) = sym;
            set = KSI_CDR(set);
        }
        return x;
    }
    if (KSI_CAR(set) == ksi_data->sym_prefix) {
        KSI_SYNTAX(set, len == 3, "import: invalid import set");
        x = (ksi_obj) ksi_new_code(3, KSI_TAG_IMPORT);
        KSI_CODE_VAL(x, 0) = KSI_CAR(set);
        set = KSI_CDR(set);
        KSI_CODE_VAL(x, 1) = import_set(KSI_CAR(set));
        set = KSI_CAR(KSI_CDR(set));
        KSI_SYNTAX(set, KSI_SYM_P(set) || KSI_KEY_P(set) || KSI_STR_P(set), "import: invalid symbol in import set");
        KSI_CODE_VAL(x, 2) = set;
        return x;
    }
    if (KSI_CAR(set) == ksi_data->sym_rename) {
        KSI_SYNTAX(set, len >= 2, "import: invalid import set");
        x = (ksi_obj) ksi_new_code(len, KSI_TAG_IMPORT);
        KSI_CODE_VAL(x, 0) = KSI_CAR(set);
        set = KSI_CDR(set);
        KSI_CODE_VAL(x, 1) = import_set(KSI_CAR(set));
        set = KSI_CDR(set);
        for (i = 2; i < len; i++) {
            ksi_obj ren = KSI_CAR(set);
            int n = ksi_list_len(ren);
            KSI_SYNTAX(ren, n == 2, "import: invalid rename in import set");
            KSI_SYNTAX(KSI_CAR(ren), KSI_SYM_P(KSI_CAR(ren)), "import: invalid symbol in import set");
            KSI_SYNTAX(KSI_CAR(KSI_CDR(ren)), KSI_SYM_P(KSI_CAR(KSI_CDR(ren))), "import: invalid symbol in import set");
            KSI_CODE_VAL(x, i) = ksi_cons(KSI_CAR(ren), KSI_CAR(KSI_CDR(ren)));
            set = KSI_CDR(set);
        }
        return x;
    }
    if (KSI_CAR(set) == ksi_data->sym_library) {
        KSI_SYNTAX(set, len == 2, "import: invalid library spec");
        return import_lib(KSI_CAR(KSI_CDR(set)));
    }


    return import_lib(set);
}

static ksi_obj
import_spec (ksi_obj spec)
{
    int len = ksi_list_len(spec);
    KSI_SYNTAX(spec, len > 0, "import: invalid import spec");

    if (KSI_CAR(spec) == ksi_data->sym_for) {
        KSI_SYNTAX(spec, len >= 2, "import: invalid import spec");
        spec = KSI_CAR(KSI_CDR(spec));
    }
    return import_set(spec);
}

static ksi_obj
ksi_comp_import (ksi_obj spec)
{
    int len = 0;
    ksi_obj x, res;

    for (res = ksi_nil; spec != ksi_nil; spec = KSI_CDR(spec)) {
        x = import_spec(KSI_CAR(spec));
        res = ksi_cons(x, res);
        len++;
    }

    x = (ksi_obj) ksi_new_code(len, KSI_TAG_IMPORT);
    while (--len >= 0) {
        KSI_CODE_VAL(x, len) = KSI_CAR(res);
        res = KSI_CDR(res);
    }
    return x;
}

static ksi_obj
ksi_mk_syntax (ksi_obj x, ksi_envinfo env, const wchar_t *annotation)
{
    unsigned int i;

    if (KSI_PAIR_P(x)) {
        ksi_obj x1 = ksi_mk_syntax(KSI_CAR(x), env, x->annotation);
        ksi_obj x2 = ksi_mk_syntax(KSI_CDR(x), env, x->annotation);
        x1 = ksi_cons(x1, x2);
        x1->annotation = x->annotation;
        return x1;
    }
    if (KSI_VEC_P(x)) {
        ksi_obj v = (ksi_obj) ksi_alloc_vector(KSI_VEC_LEN(x), KSI_TAG_CONST_VECTOR);
        for (i = 0; i < KSI_VEC_LEN(x); i++)
            KSI_VEC_REF(v, i) = ksi_mk_syntax(KSI_VEC_REF(x, i), env, x->annotation);
        v->annotation = x->annotation;
        return v;
    }
    if (KSI_SYM_P(x)) {
        ksi_obj var = ksi_comp_sym(x, env, 0, annotation);
        if (KSI_FREEVAR_P(var)) {
            return ksi_new_id(x, env->frm->env, annotation);
        } else {
            ksi_exn_error(L"syntax", x, "syntax: variable is out of scope");
        }
    }

    return x;
}

/* compile (wrap) syntax form (a list of scheme objects)
 * ie the form such as "(syntax x)"
 */
static ksi_obj
ksi_comp_syntax_form (ksi_obj form, ksi_envinfo env)
{
    ksi_obj args;
    int len;

    args = KSI_CDR(form);
    len = ksi_list_len(args);
    KSI_SYNTAX(form, len == 1, "syntax: invalid syntax");

    return ksi_mk_syntax(KSI_CAR(args), env, form->annotation);
}

/* compile (unwraped) syntax expression (ie identifier)
 * syntax expression can be returned from macro expansion procedire
 */
static ksi_obj
ksi_comp_syntax_sexp (ksi_obj x, ksi_envinfo env)
{
    ksi_obj var = KSI_CODE_VAL(x, 0);

    if (KSI_FREEVAR_P(var))
        return var;

    ksi_exn_error(0, 0, "ksi_comp_syntax_sexp: internal error -- invalid syntax expression: %ls", ksi_obj2str(var));
    return 0;
}

static ksi_obj
ksi_comp_pair (ksi_obj form, ksi_envinfo env)
{
    ksi_envinfo p;
    ksi_varinfo cur;
    ksi_envrec rec;
    ksi_obj x = KSI_CAR(form);

    if (KSI_OBJ_IS(x, KSI_TAG_SYNTAX)) {
        rec = ksi_get_freevar_rec(KSI_CODE_VAL(x, 0));
        goto syntax;
    } else if (KSI_SYM_P(x)) {
        for (p = env; p; p = p->next) {
            for (cur = p->vars; cur; cur = cur->next) {
                if (cur->name == x && cur->idx >= 0) {
                    goto apply;
                }
            }
        }

        rec = ksi_lookup_env(env->frm->env, x);
    syntax:
        if (rec) {
            if (rec->syntax) {
                if (KSI_OBJ_IS(rec->val, KSI_TAG_CORE)) {
                    switch (((ksi_core) rec->val)->o.ilen) {
                    case KSI_TAG_QUOTE:
                        KSI_SYNTAX(form, ksi_list_len (form) == 2, "quote: invalid syntax");
                        return ksi_comp_quote(KSI_CAR(KSI_CDR(form)), env, form->annotation);
                    case KSI_TAG_BEGIN:
                        KSI_SYNTAX(form, ksi_list_len(form) > 1, "begin: invalid syntax");
                        return ksi_comp_seq(KSI_CDR(form), env, form->annotation);
                    case KSI_TAG_AND:
                        return ksi_comp_and(form, env);
                    case KSI_TAG_OR:
                        return ksi_comp_or(form, env);
                    case KSI_TAG_IF:
                        return ksi_comp_if(form, env);
                    case KSI_TAG_LAMBDA:
                        return ksi_comp_lambda(form, env);
                    case KSI_TAG_DEFINE:
                        return ksi_comp_define(form, env, 0);
                    case KSI_TAG_DEFSYNTAX:
                        return ksi_comp_define(form, env, 1);
                    case KSI_TAG_SET:
                        return ksi_comp_set(form, env);
                    case KSI_TAG_LET:
                        return ksi_comp_let(form, env);
                    case KSI_TAG_LETREC:
                        return ksi_comp_letrec(form, env);
                    case KSI_TAG_LETREC_STAR:
                        return ksi_comp_letrec_star(form, env);
                    case KSI_TAG_SYNTAX:
                        return ksi_comp_syntax_form(form, env);
                    default:
                        ksi_exn_error(L"syntax", form, "compile: invalid core syntax in syntactic form");
                    }
                }
                if (KSI_PROC_P(rec->val)) {
                    ksi_obj r = ksi_apply_2(rec->val, form, (ksi_obj) env);
                    /*ksi_debug("macro expand: form=%ls env=%ls\nresult=%ls", ksi_obj2str(form), ksi_obj2str((ksi_obj) env), ksi_obj2str(r));*/
                    return ksi_comp_sexp(r, env, form->annotation);
                }
            }
        } else {
            if (x == ksi_data->sym_import && env->frm->env->name == ksi_void) {
                /* special case: import in top level environment */
                KSI_SYNTAX(form, ksi_list_len(form) > 1, "import: invalid syntax");
                return ksi_comp_import(KSI_CDR(form));
            }
        }
    }
apply:
    return ksi_comp_apply(form, env);
}

static ksi_obj
ksi_comp_sexp (ksi_obj form, ksi_envinfo env, const wchar_t *annotation)
{
    if (form == ksi_nil)
        ksi_exn_error(L"syntax", form, "compile: no subexpresion in procedure call");

    switch (form->itag) {
    case KSI_TAG_IMM:
    case KSI_TAG_CHAR:
    case KSI_TAG_STRING:
    case KSI_TAG_CONST_STRING:
    case KSI_TAG_BIGNUM:
    case KSI_TAG_FLONUM:
    case KSI_TAG_KEYWORD:
    case KSI_TAG_BYTEVECTOR:
    case KSI_TAG_CONST_BYTEVECTOR:
        return ksi_comp_quote(form, env, annotation);

    case KSI_TAG_SYMBOL:
        return ksi_comp_sym(form, env, 0, annotation);

    case KSI_TAG_SYNTAX:
        return ksi_comp_syntax_sexp(form, env);

    case KSI_TAG_PAIR:
    case KSI_TAG_CONST_PAIR:
        return ksi_comp_pair(form, env);
    }

    ksi_exn_error(L"syntax", form, "compile: expression is not evaluatable");
    return 0;
}

ksi_obj
ksi_comp(ksi_obj form, ksi_env env)
{
    ksi_frminfo frminfo;
    ksi_envinfo envinfo;
    ksi_obj code;
    int len;

    KSI_CHECK ((ksi_obj) env, KSI_ENV_P(env), "compile: invalid environ in arg2");

    frminfo = ksi_malloc(sizeof *frminfo);
    frminfo->next = 0;
    frminfo->vars = 0;
    frminfo->env = env;
    frminfo->free = 0;
    frminfo->mark = 1;

    envinfo = ksi_new_envinfo(0, frminfo);
    envinfo->top = 1;

    code = ksi_comp_sexp(form, envinfo, 0);
    len = realloc_vars(frminfo, 0);

    ksi_code x = ksi_new_code(3, KSI_TAG_FRAME);
    KSI_CODE_VAL(x, 0) = ksi_long2num(len);
    KSI_CODE_VAL(x, 1) = code;
    KSI_CODE_VAL(x, 2) = (ksi_obj) env;
    x->o.annotation = code->annotation;
    return (ksi_obj) x;
}

static ksi_env
eval_import_lib(ksi_obj lib)
{
    static char *scm_suffix[] = { ".scm", ".ss", 0 };

    int i, fd;
    char *tmp, *scm_name;
    const char *dir, *fname;
    ksi_byte_port data = 0;
    ksi_char_port port = 0;
    ksi_env env = ksi_lib_env(lib, 0);

    if (!env) {
        fname = ksi_mk_filename(lib, "import");

#ifdef DYNAMIC_LINKING
        {
            const wchar_t *msg;
            scm_name = alloca(strlen(fname) + strlen(KSI_DL_SUFFIX) + 1);
            strcat(strcpy(scm_name, fname), KSI_DL_SUFFIX);
            msg = ksi_dynload_file(scm_name);
            if (!msg) {
                env = ksi_lib_env(lib, 0);
                if (!env) {
                    ksi_exn_error(0, lib, "import: %s does not install required library", scm_name);
                }
                return env;
            } else if (*msg) {
                ksi_exn_error(0, lib, "import: %s loading error: %ls", scm_name, msg);
            }
        }
#endif

        if (ksi_current_context && ksi_current_context->loader_proc) {
            for (i = 0; scm_suffix[i]; i++) {
                tmp = ksi_local(ksi_aprintf("ksi/%d.%d.%d/%s%s", KSI_MAJOR_VERSION, KSI_MINOR_VERSION, KSI_PATCH_LEVEL, fname, scm_suffix[i]));
                data = ksi_current_context->loader_proc(tmp, ksi_current_context->loader_data);
                if (data) {
                    port = ksi_open_src_port(data, tmp, 1);
                    if (port)
                        break;
                }
            }
        }

        if (!port) {
            if (ABSOLUTE_PATH(fname) || fname[0] == L'~') {
                scm_name = ksi_expand_file_name(fname);
            } else {
                dir = ksi_scheme_lib_dir();
                scm_name = alloca(strlen(dir) + strlen(DIR_SEP) + strlen(fname) + 1);
                strcat(strcat(strcpy(scm_name, dir), DIR_SEP), fname);
            }

            for (i = 0; scm_suffix[i]; i++) {
                tmp = alloca(strlen(scm_name) + 6);
                strcat(strcpy(tmp, scm_name), scm_suffix[i]);
                if (access(tmp, R_OK) == 0) {
                    if ((fd = open(tmp, O_RDONLY)) < 0) {
                        ksi_exn_error(0, 0, "import: can't open file \"%s\": %m", tmp);
                    }

                    data = ksi_new_fd_input_port(fd, tmp);
                    port = ksi_open_src_port(data, tmp, 1);
                    break;
                }
            }
        }

        if (port) {
            env = ksi_lib_env(lib, 1);
            ksi_eval_port(fname, (ksi_obj) port, env);
            ksi_close_port((ksi_obj) port);
            return env;
        }
    }

    return env;
}

static ksi_env
eval_import_helper (ksi_obj form, ksi_obj *export, ksi_obj *name)
{
    unsigned int i;
    ksi_obj op = KSI_CODE_VAL(form, 0);

    if (op == ksi_data->sym_library) {
        ksi_env env = eval_import_lib(KSI_CODE_VAL(form, 1));
        *name = KSI_CODE_VAL(form, 1);
        *export = ksi_nil;
        if (env) {
            ksi_obj exp = env->exported;
            while (exp != ksi_nil) {
                ksi_obj x = KSI_CAR(exp);
                if (KSI_PAIR_P(x))
                    x = KSI_CAR(x);
                *export = ksi_cons(ksi_cons(x, x), *export);
                exp = KSI_CDR(exp);
            }
        }
        return env;
    }
    if (op == ksi_data->sym_only) {
        ksi_env env = eval_import_helper(KSI_CODE_VAL(form, 1), export, name);
        if (env) {
            ksi_obj exp = *export;
            *export = ksi_nil;
            while (exp != ksi_nil) {
                ksi_obj x = KSI_CAR(exp);
                for (i = 2; i <= KSI_CODE_NUM(form); i++) {
                    if (KSI_CAR(x) == KSI_CODE_VAL(form, i)) {
                        *export = ksi_cons(x, *export);
                        break;
                    }
                }
                exp = KSI_CDR(exp);
            }
        }
        return env;
    }
    if (op == ksi_data->sym_except) {
        ksi_env env = eval_import_helper(KSI_CODE_VAL(form, 1), export, name);
        if (env) {
            ksi_obj exp = *export;
            *export = ksi_nil;
            while (exp != ksi_nil) {
                ksi_obj x = KSI_CAR(exp);
                for (i = 2; i <= KSI_CODE_NUM(form); i++) {
                    if (KSI_CAR(x) == KSI_CODE_VAL(form, i)) {
                        goto skip;
                    }
                }
                *export = ksi_cons(x, *export);
            skip:
                exp = KSI_CDR(exp);
            }
        }
        return env;
    }
    if (op == ksi_data->sym_prefix) {
        ksi_env env = eval_import_helper(KSI_CODE_VAL(form, 1), export, name);
        if (env) {
            ksi_obj exp = *export;
            *export = ksi_nil;
            while (exp != ksi_nil) {
                ksi_obj x = KSI_CAR(exp);
                ksi_obj z = KSI_CODE_VAL(form, 2);
                const wchar_t *ptr1 = (KSI_SYM_P(z) ? KSI_SYM_PTR(z) : (KSI_STR_P(z) ? KSI_STR_PTR(z) : ksi_obj2str(z)));
                wchar_t *ptr = ksi_aprintf("%ls%ls", ptr1, ksi_obj2str(KSI_CAR(x)));
                z = ksi_str02sym(ptr);
                *export = ksi_cons(ksi_cons(z, KSI_CDR(x)), *export);
                exp = KSI_CDR(exp);
            }
        }
        return env;
    }
    if (op == ksi_data->sym_rename) {
        ksi_env env = eval_import_helper(KSI_CODE_VAL(form, 1), export, name);
        if (env) {
            ksi_obj exp = *export;
            *export = ksi_nil;
            while (exp != ksi_nil) {
                ksi_obj x = KSI_CAR(exp);
                for (i = 2; i <= KSI_CODE_NUM(form); i++) {
                    if (KSI_CAR(x) == KSI_CDR(KSI_CODE_VAL(form, i))) {
                        *export = ksi_cons(ksi_cons(KSI_CAR(KSI_CODE_VAL(form, i)), KSI_CDR(x)), *export);
                        goto next;
                    }
                }
                *export = ksi_cons(x, *export);
            next:
                exp = KSI_CDR(exp);
            }
        }
        return env;
    }

    return 0;
}

static ksi_obj
ksi_eval_syntax (ksi_obj form, ksi_env env)
{
    return form; //KSI_CODE_VAL(form, 0);
}

static ksi_obj
ksi_eval_import (ksi_obj form, ksi_env env)
{
    unsigned int i;

    for (i = 0; i <= KSI_CODE_NUM(form); i++) {
        ksi_obj export;
        ksi_obj name = KSI_CODE_VAL(form, i);
        ksi_env lib = eval_import_helper(name, &export, &name);
        if (lib) {
            ksi_info("import library %ls in %ls", ksi_obj2str(name), ksi_obj2str((ksi_obj) env));
            while (export != ksi_nil) {
                ksi_obj x = KSI_CAR(export);
                ksi_import(lib, KSI_CDR(x), env, KSI_CAR(x));
                export = KSI_CDR(export);
            }
        } else {
            ksi_exn_error(L"import", name, "import: unknown library in %ls", ksi_obj2str((ksi_obj) env));
        }
    }
    return ksi_void;
}

static void
eval_lib_export(ksi_obj exp, ksi_env env)
{
    while (KSI_PAIR_P(exp)) {
        ksi_obj e1 = KSI_CAR(exp);
        if (KSI_SYM_P(e1)) {
            ksi_export(env, e1, e1);
        } else {
            if (KSI_PAIR_P(e1) && KSI_CAR(e1) == ksi_data->sym_rename) {
                ksi_obj rs = KSI_CDR(e1);
                while (KSI_PAIR_P(rs)) {
                    ksi_obj r1 = KSI_CAR(rs);
                    int l = ksi_list_len(r1);
                    if (l != 2)
                        ksi_exn_error(L"import", e1, "import: invalid library export");
                    ksi_export(env, KSI_CAR(r1), KSI_CAR(KSI_CDR(r1)));
                    rs = KSI_CDR(rs);
                }
            } else {
                ksi_exn_error(L"import", e1, "import: invalid library export");
            }
        }
        exp = KSI_CDR(exp);
    }
}

ksi_obj
ksi_environment(int argc, ksi_obj *argv)
{
    int i;
    ksi_env env = ksi_top_level_env();
    env->name = ksi_nil;

    for (i = 0; i < argc; i++) {
        ksi_obj name = import_set(argv[i]);
        ksi_obj export;
        ksi_env lib = eval_import_helper(name, &export, &name);
        if (lib) {
            ksi_info("import library %ls in new environment", ksi_obj2str(name));
            while (export != ksi_nil) {
                ksi_obj x = KSI_CAR(export);
                ksi_import(lib, KSI_CDR(x), env, KSI_CAR(x));
                export = KSI_CDR(export);
            }
        } else {
            ksi_exn_error(L"import", name, "environment: unknown library");
        }
    }
    return (ksi_obj) env;
}

static void
ksi_recomp_var (ksi_obj x, ksi_frame frm, char *name)
{
    ksi_envrec rec;
    ksi_obj sym = KSI_FREEVAR_SYM(x);
    ksi_env env = KSI_FREEVAR_ENV(x);

    if (!env) {
        ksi_exn_error(0, x, "ksi_recomp_var: internal error -- null environment");
    }

    rec = ksi_lookup_env(env, sym);
    if (rec) {
        if (rec->syntax)
            ksi_exn_error(L"syntax", x, "%s: invalid use of syntactic keyword in %ls", name, ksi_obj2str((ksi_obj) env));

        x->itag = (rec->imported ? KSI_TAG_IMPORTED : KSI_TAG_LOCAL);
        KSI_FREEVAR_VAL(x) = rec;
    } else {
        ksi_exn_error(L"syntax", x, "%s: unbound variable in %ls", name, ksi_obj2str((ksi_obj) env));
    }
}

static void
ksi_eval_set (ksi_obj form, ksi_frame frm)
{
    ksi_obj res, var;
    int i;

    var = KSI_CODE_VAL(form, 0);
    res = ksi_eval_code(KSI_CODE_VAL (form, 1), frm);

again:
    switch (var->itag) {
    case KSI_TAG_VAR0:
        frm->vals[KSI_VARBOX_NUM(var)] = res;
        break;

    case KSI_TAG_VAR1:
        frm->next->vals[KSI_VARBOX_NUM(var)] = res;
        break;

    case KSI_TAG_VAR2:
        frm->next->next->vals[KSI_VARBOX_NUM(var)] = res;
        break;

    case KSI_TAG_VARN:
        i = KSI_VARBOX_LEV(var);
        while (--i >= 0)
            frm = frm->next;
        frm->vals[KSI_VARBOX_NUM(var)] = res;
        break;

    case KSI_TAG_LOCAL:
        if (KSI_FREEVAR_VAL(var)->exported)
            ksi_exn_error(L"syntax", var, "set!: cannot assign exported variable in %ls", ksi_obj2str((ksi_obj) KSI_FREEVAR_ENV(var)));
        KSI_FREEVAR_VAL(var)->val = res;
        break;

    case KSI_TAG_IMPORTED:
        ksi_exn_error(L"syntax", var, "set!: cannot assign imported variable in %ls", ksi_obj2str((ksi_obj) KSI_FREEVAR_ENV(var)));
        break;

    case KSI_TAG_FREEVAR:
        ksi_recomp_var(var, frm, "set!");
        goto again;
    }
}

static void
ksi_eval_def (ksi_obj form, ksi_frame frm)
{
    ksi_obj val, sym = KSI_CODE_VAL(form, 0);
    ksi_envrec rec = ksi_lookup_env(frm->env, sym);
    if (rec) {
        if (rec->imported)
            ksi_exn_error(L"syntax", form, "define: cannot modify imported variable in %ls", ksi_obj2str((ksi_obj) frm->env));
        else if (rec->exported)
            ksi_exn_error(L"syntax", form, "define: cannot modify exported variable in %ls", ksi_obj2str((ksi_obj) frm->env));
        else if (rec->syntax && KSI_CODE_VAL(form, 2) == ksi_false)
            ksi_exn_error(L"syntax", form, "define: invalid use of syntactic keyword in %ls", ksi_obj2str((ksi_obj) frm->env));
        else
            ksi_warn("define: %ls variable redefined in %ls", KSI_SYM_PTR(sym), ksi_obj2str((ksi_obj) frm->env));
    }
    val = ksi_eval_code(KSI_CODE_VAL (form, 1), frm);
    if (KSI_OBJ_IS(val, KSI_TAG_CLOSURE)) {
        if (KSI_CLOS_DOC(val) == ksi_false)
            KSI_CLOS_DOC(val) = sym;
    }
    if (rec) {
        if (rec->syntax && ksi_procedure_has_arity_p(val, ksi_long2num(2), 0) == ksi_false)
            ksi_exn_error(L"syntax", form, "define: invalid arity of the macro procedure in %ls", ksi_obj2str((ksi_obj) frm->env));
        rec->val = val;
    } else {
        if (KSI_CODE_VAL(form, 2) == ksi_false) {
            ksi_define(sym, val, frm->env);
        } else {
            if (ksi_procedure_has_arity_p(val, ksi_long2num(2), 0) == ksi_false)
                ksi_exn_error(L"syntax", form, "define: invalid arity of the macro procedure in %ls", ksi_obj2str((ksi_obj) frm->env));
            ksi_defsyntax(sym, val, frm->env, 0);
        }
    }
}


static ksi_frame
clos_frm (ksi_obj proc, int argc, ksi_obj* argv)
{
    ksi_frame frm;
    ksi_obj args;
    int nary;

    frm  = ksi_new_frame(KSI_CLOS_NUMS(proc), KSI_CLOS_FRM(proc));
    nary = KSI_CLOS_NARY(proc);

    if (KSI_CLOS_OPTS(proc)) {
        nary -= 1;
        KSI_WNA(argc >= nary, proc, ksi_obj2str (proc));

        args = ksi_nil;
        while (--argc >= nary)
            args = ksi_cons(argv[argc], args);
        frm->vals[nary] = args;
    } else {
        KSI_WNA(argc == nary, proc, ksi_obj2str (proc));
    }

    while (--nary >= 0)
        frm->vals[nary] = argv[nary];

    return frm;
}

ksi_obj
ksi_eval_code (ksi_obj form, ksi_frame frm)
{
    ksi_obj *arr;
    unsigned int i, arr_sz;
    ksi_obj val;

    arr    = 0;
    arr_sz = 0;

#define ALLOCARR(n) do {                                \
      if (arr_sz < (n))                                 \
        arr_sz = (n);                                   \
        arr = (ksi_obj*) alloca (arr_sz * sizeof *arr); \
    } while (0)

again:
    switch (form->itag) {
    case KSI_TAG_FRAME:
        frm = ksi_new_frame(ksi_num2long(KSI_CODE_VAL(form, 0), "<internal frame>"), 0);
        frm->env = (ksi_env) KSI_CODE_VAL(form, 2);
        form = KSI_CODE_VAL(form, 1);
        goto again;

    case KSI_TAG_IMM:
    case KSI_TAG_BIGNUM:
    case KSI_TAG_FLONUM:
    case KSI_TAG_SYMBOL:
    case KSI_TAG_KEYWORD:
    case KSI_TAG_PAIR:
    case KSI_TAG_CONST_PAIR:
    case KSI_TAG_VECTOR:
    case KSI_TAG_CONST_VECTOR:
    case KSI_TAG_STRING:
    case KSI_TAG_CONST_STRING:
    case KSI_TAG_CHAR:
    case KSI_TAG_BYTEVECTOR:
    case KSI_TAG_CONST_BYTEVECTOR:
        return form;

    case KSI_TAG_FREEVAR:
        ksi_recomp_var(form, frm, "eval");
    case KSI_TAG_LOCAL:
    case KSI_TAG_IMPORTED:
        return KSI_FREEVAR_VAL(form)->val;

    case KSI_TAG_VAR0:
        return frm->vals[KSI_VARBOX_NUM (form)];

    case KSI_TAG_VAR1:
        return frm->next->vals[KSI_VARBOX_NUM (form)];

    case KSI_TAG_VAR2:
        return frm->next->next->vals[KSI_VARBOX_NUM(form)];

    case KSI_TAG_VARN:
        for (i = 0; i < KSI_VARBOX_LEV(form); i++)
            frm = frm->next;
        return frm->vals[KSI_VARBOX_NUM(form)];

    case KSI_TAG_QUOTE:
        return KSI_QUOTE_VAL(form);

    case KSI_TAG_SET:
        ksi_eval_set(form, frm);
        return ksi_void;

    case KSI_TAG_DEFINE:
        ksi_eval_def (form, frm);
        return ksi_void;

    case KSI_TAG_AND:
        for (i = 0; i < KSI_CODE_NUM(form); i++) {
            if (ksi_eval_code(KSI_CODE_VAL(form, i), frm) == ksi_false) {
                return ksi_false;
            }
        }
        form = KSI_CODE_VAL(form, i);
        goto again;

    case KSI_TAG_OR:
        for (i = 0; i < KSI_CODE_NUM(form); i++)
            if ((val = ksi_eval_code(KSI_CODE_VAL (form, i), frm)) != ksi_false)
                return val;
        form = KSI_CODE_VAL(form, i);
        goto again;

    case KSI_TAG_BEGIN:
        for (i = 0; i < KSI_CODE_NUM (form); i++)
            ksi_eval_code (KSI_CODE_VAL (form, i), frm);
        form = KSI_CODE_VAL (form, i);
        goto again;

    case KSI_TAG_IF:
        if (ksi_eval_code(KSI_IF_TEST(form), frm) == ksi_false)
            form = KSI_IF_ELSE(form);
        else
            form = KSI_IF_THEN(form);
        goto again;

    case KSI_TAG_LAMBDA:
        return (ksi_obj) ksi_new_closure(ksi_num2long(KSI_CODE_VAL(form, 0), "<internal lambda>"),
                                         ksi_num2long(KSI_CODE_VAL(form, 1), "<internal lambda>"),
                                         ksi_num2long(KSI_CODE_VAL(form, 2), "<internal lambda>"),
                                         frm,
                                         KSI_CODE_VAL(form, 3));

    case KSI_TAG_IMPORT:
        return ksi_eval_import(form, frm->env);

    case KSI_TAG_SYNTAX:
        return ksi_eval_syntax(form, frm->env);

    case KSI_TAG_CALL:
        i = KSI_CODE_NUM (form) + 1;
        ALLOCARR(i);
        {
            ksi_obj *tmp = & KSI_CODE_VAL (form, 0);
            while (--i >= 0) {
                *arr++ = ksi_eval_code (*tmp++, frm);
            }
            i = KSI_CODE_NUM (form) + 1;
            arr -= i;
        }

    apply_call:
        /* arr[0]		::= function
         * arr[1 .. (i-1)]	::= arguments
         * i			::= number of arguments (include function)
         */

        switch (arr[0]->itag) {
        case KSI_TAG_PRIM:
            return ksi_apply_prim ((ksi_prim) arr[0], i-1, arr+1);

        case KSI_TAG_PRIM_0:
            KSI_WNA (i == 1, arr[0], KSI_PRIM_NAME (arr[0]));
            return ((ksi_call_arg0) KSI_PRIM_PROC (arr[0])) ();

        case KSI_TAG_PRIM_1:
            KSI_WNA (i == 2, arr[0], KSI_PRIM_NAME (arr[0]));
            return ((ksi_call_arg1) KSI_PRIM_PROC (arr[0])) (arr[1]);

        case KSI_TAG_PRIM_2:
            KSI_WNA (i == 3, arr[0], KSI_PRIM_NAME (arr[0]));
            return ((ksi_call_arg2) KSI_PRIM_PROC (arr[0])) (arr[1], arr[2]);

        case KSI_TAG_PRIM_r:
            KSI_WNA (i-1 >= KSI_PRIM_REQV (arr[0]), arr[0], KSI_PRIM_NAME (arr[0]));
            return ((ksi_call_rest0) KSI_PRIM_PROC (arr[0])) (i-1, arr+1);

        case KSI_TAG_PRIM_CLOSURE:
            return ksi_apply_prim_closure ((ksi_prim_closure) arr[0], i-1, arr+1);

        case KSI_TAG_CLOSURE:
            frm  = clos_frm(arr[0], i-1, arr+1);
            form = KSI_CLOS_BODY (arr[0]);
            goto again;

        case KSI_TAG_APPLY:
            KSI_WNA (i >= 3, arr[0], L"apply");
        apply:
            {
                int n = i - 2;
                ksi_obj *tmp = arr + 1;
                val = tmp[n];
                i = ksi_list_len (val);
                KSI_CHECK (val, i >= 0, "apply: not a list in last arg");

                ALLOCARR(i+n);
                for (i = 0; --n >= 0; /**/)
                    arr[i++] = *tmp++;
                for (/**/; val != ksi_nil; val = KSI_CDR (val))
                    arr[i++] = KSI_CAR (val);

                goto apply_call;

            } /* case KSI_TAG_APPLY */

        case KSI_TAG_CALL_CC:
            KSI_WNA (i == 2, arr[0], L"call/cc");
            arr[0] = arr[1];
            val = ksi_continuation (&arr[1]);
            if (val)
                return val;
            goto apply_call;

        case KSI_TAG_CALL_WITH_VALUES:
            KSI_WNA (i == 3, arr[0], L"call-with-values");
            val = ksi_apply_0 (arr[1]);
            if (!KSI_VALUES_P (val))
            {
                arr[0] = arr[2];
                arr[1] = val;
                i = 2;
                goto apply_call;
            }
            arr[1] = arr[2];
            arr[2] = KSI_VALUES_VALS (val);
            /* i = 3; */
            goto apply;

        case KSI_TAG_NOT:
            KSI_WNA (i == 2, arr[0], L"not");
            return (arr[1] == ksi_false ? ksi_true : ksi_false);

        case KSI_TAG_EQP:
            KSI_WNA (i == 3, arr[0], L"eq?");
            return ((arr[1] == arr[2]) ? ksi_true : ksi_false);

        case KSI_TAG_EQVP:
            KSI_WNA (i == 3, arr[0], L"eqv?");
            return (KSI_EQV_P (arr[1], arr[2]) ? ksi_true : ksi_false);

        case KSI_TAG_EQUALP:
            KSI_WNA (i == 3, arr[0], L"equal?");
            return (KSI_EQUAL_P (arr[1], arr[2]) ? ksi_true : ksi_false);

        case KSI_TAG_MEMQ:
            KSI_WNA (i == 3, arr[0], L"memq");
            return ksi_memq (arr[1], arr[2]);

        case KSI_TAG_MEMV:
            KSI_WNA (i == 3, arr[0], L"memv");
            return ksi_memv (arr[1], arr[2]);

        case KSI_TAG_MEMBER:
            KSI_WNA (i == 3, arr[0], L"member");
            return ksi_member (arr[1], arr[2]);

        case KSI_TAG_CONS:
            KSI_WNA (i == 3, arr[0], L"cons");
            return (ksi_cons(arr[1], arr[2]));

        case KSI_TAG_CAR:
            KSI_WNA (i == 2, arr[0], L"car");
            KSI_CHECK (arr[1], KSI_PAIR_P (arr[1]), "car: not pair in arg1");
            return (KSI_CAR (arr[1]));

        case KSI_TAG_CDR:
            KSI_WNA (i == 2, arr[0], L"cdr");
            KSI_CHECK (arr[1], KSI_PAIR_P (arr[1]), "cdr: not pair in arg1");
            return (KSI_CDR (arr[1]));

        case KSI_TAG_LIST:
            return (ksi_new_list (i-1, arr+1));

        case KSI_TAG_MK_VECTOR:
            return (ksi_new_vector (i-1, arr+1));

        case KSI_TAG_APPEND:
            return (ksi_append(i-1, arr+1));

        case KSI_TAG_LIST2VECTOR:
            KSI_WNA (i == 2, arr[0], L"list->vector");
            return (ksi_list2vector (arr[1]));

        case KSI_TAG_NULLP:
            KSI_WNA (i == 2, arr[0], L"null?");
            return (arr[1] == ksi_nil ? ksi_true : ksi_false);

        case KSI_TAG_PAIRP:
            KSI_WNA (i == 2, arr[0], L"pair?");
            return (KSI_PAIR_P (arr[1]) ? ksi_true : ksi_false);

        case KSI_TAG_LISTP:
            KSI_WNA (i == 2, arr[0], L"list?");
            return (KSI_LIST_P (arr[1]) ? ksi_true : ksi_false);

        case KSI_TAG_VECTORP:
            KSI_WNA (i == 2, arr[0], L"vector?");
            return (KSI_VEC_P (arr[1]) ? ksi_true : ksi_false);

        case KSI_TAG_INSTANCE:
            if (KSI_PURE_GENERIC_P (arr[0])) {
                form = ksi_new_list (i-1, arr+1);
                val  = ksi_compute_effective_method (arr[0], form);

                if (val == ksi_nil) {
                    val = arr[0];
                    i = 3;
                    ALLOCARR(i);
                    arr[0] = ksi_klos_val (ksi_data->sym_no_applicable_method, 0);
                    arr[1] = val;
                    arr[2] = form;
                    if (arr[0])
                        goto apply_call;
                    ksi_exn_error (0, arr[1], "no applicable methods");
                }

                goto call_next;
            }

            if (KSI_GENERIC_P (arr[0])) {
                form = ksi_new_list (i-1, arr+1);
                val = arr[0];
                i = 3;
                ALLOCARR(i);
                arr[0] = ksi_klos_val (ksi_data->sym_apply_generic, 0);
                arr[1] = val;
                arr[2] = form;
                if (arr[0])
                    goto apply_call;
                ksi_exn_error (0, arr[1], "apply-generic not defined");
            }
            goto bad_apply;

        case KSI_TAG_NEXT_METHOD:
            val  = arr[0];
            form = (i > 1 ? ksi_new_list (i-1, arr+1) : KSI_NEXT_ARGS (val));

            if (KSI_NEXT_PROCS (val) == ksi_nil) {
                i = 3;
                ALLOCARR(i);
                arr[0] = ksi_klos_val (ksi_data->sym_no_next_method, 0);
                arr[1] = KSI_NEXT_GF (val);
                arr[2] = form;
                if (arr[0])
                    goto apply_call;
                ksi_exn_error (0, arr[1], "no next method");
            }

        call_next:
            i = 5;
            ALLOCARR(i);
            arr[1] = KSI_CAR (KSI_NEXT_PROCS (val));
            arr[2] = ksi_new_next (KSI_NEXT_GF (val), form, KSI_CDR (KSI_NEXT_PROCS (val)));
            arr[3] = (KSI_PAIR_P (KSI_CDR (KSI_NEXT_PROCS (val))) ? ksi_data->true_proc : ksi_data->false_proc);
            arr[4] = form;
            goto apply;

        case KSI_TAG_EXTENDED:
            if (((struct Ksi_EObj *) arr[0]) -> etag -> apply) {
                return ((struct Ksi_EObj *) arr[0]) -> etag -> apply ((struct Ksi_EObj *) arr[0], i-1, arr+1);
            }
            break;
        } /* switch (arr[0]->o.itag) */

    bad_apply:
        ksi_exn_error(0, arr[0], "apply: invalid object to apply");
    } /*  switch (form->o.itag) */

    ksi_exn_error(0, form, "eval: expression is not evaluatable");
    return 0;

#undef ALLOCARR
}


ksi_obj
ksi_call_with_values (ksi_obj thunk, ksi_obj proc)
{
    ksi_obj x;
    KSI_CHECK (thunk, KSI_PROC_P (thunk), "call-with-values: invalid procedure in arg1");
    KSI_CHECK (proc, KSI_PROC_P (proc), "call-with-values: invalid procedure in arg2");

    x = ksi_apply_0(thunk);
    if (KSI_VALUES_P(x))
        return ksi_apply(proc, KSI_VALUES_VALS(x));

    return ksi_apply_1(proc, x);
}

ksi_obj
ksi_closure_env (ksi_obj clos)
{
    KSI_CHECK (clos, KSI_OBJ_IS (clos, KSI_TAG_CLOSURE), "@closure-environment: invalid closure in arg1");
    return KSI_CLOS_FRM (clos) ? (ksi_obj) KSI_CLOS_FRM (clos) -> env : ksi_false;
}

ksi_obj
ksi_closure_body (ksi_obj clos)
{
    KSI_CHECK (clos, KSI_OBJ_IS (clos, KSI_TAG_CLOSURE), "@closure-body: invalid closure in arg1");
    return KSI_CLOS_BODY (clos);
}

ksi_obj
ksi_closure_p (ksi_obj clos)
{
    return KSI_OBJ_IS (clos, KSI_TAG_CLOSURE) ? ksi_true : ksi_false;
}


ksi_obj
ksi_call_in_context(ksi_context_t ctx, void *data, ksi_obj (*proc)(ksi_context_t, void *), ksi_obj (*error)(ksi_context_t, ksi_obj, ksi_obj))
{
    ksi_obj result = ksi_void;
    struct Ksi_Context *old_ctx = ksi_current_context;
    ksi_current_context = ctx;

    if (ctx && proc) {
        volatile ksi_wind wind = ksi_add_catch(ksi_true, 0, 0);

        KSI_FLUSH_REGISTER_WINDOWS;
        if (setjmp(wind->the_catch->jmp.j_buf) == 0) {
            result = proc(ctx, data);
            ksi_del_catch(wind);
        } else {
            result = wind->the_catch->value;
            if (error)
                result = error(ctx, wind->the_catch->thrown, result);
        }
    }

    ksi_current_context = old_ctx;
    return result;
}

ksi_obj
ksi_eval (ksi_obj form, ksi_env env)
{
    KSI_CHECK ((ksi_obj) env, KSI_ENV_P(env), "eval: invalid environ in arg2");
    if (form == ksi_nil)
        ksi_exn_error(L"syntax", form, "eval: no subexpresion in procedure call");

    if (KSI_OBJ_IS(form, KSI_TAG_FRAME)) {
        if (!env || env == (ksi_env) KSI_CODE_VAL(form, 2)) {
            return ksi_eval_code(form, 0);
        }
        ksi_exn_error(L"syntax", form, "eval: environment is differ from the environment that used when compiling the code");
    }

    form = ksi_comp(form, env);
    return ksi_eval_code(form, 0);
}


static ksi_obj
ksi_comp_port(ksi_obj port, ksi_env env)
{
    ksi_obj x, *p, v;

    v = ksi_new_id(ksi_data->sym_begin, ksi_data->syntax_env, 0);
    v = ksi_cons(v, ksi_nil);
    p = &KSI_CDR(v);

    for (;;) {
        x = ksi_get_datum(port);
        if (x == ksi_eof)
            break;
        *p = ksi_cons(x, ksi_nil);
        p = &KSI_CDR(*p);
    }

    return ksi_comp(v, env);
}

ksi_obj
ksi_comp_string (const wchar_t *scheme_code, ksi_env env, const char *file_name, int file_line)
{
    ksi_obj x, port;

    port = (ksi_obj) ksi_new_string_input_port(ksi_str02string(scheme_code), file_name, file_line);
    x = ksi_comp_port(port, env);
    ksi_close_port(port);

    return x;
}

ksi_obj
ksi_eval_string (const wchar_t *scheme_code, ksi_env env, const char *file_name, int file_line, ksi_obj *compiled_code)
{
    ksi_obj x;

    x = ksi_comp_string(scheme_code, env, file_name, file_line);
    if (compiled_code) {
        *compiled_code = x;
    }
    return ksi_eval_code(x, 0);
}

ksi_obj
ksi_comp_bytes (const char *scheme_code, int len, ksi_env env, const char *file_name, int file_line)
{
    ksi_obj v, x, file, port;

    v = ksi_str2bytevector(0, 0);
    KSI_BVEC_PTR(v) = (char *) scheme_code;
    KSI_BVEC_LEN(v) = len;

    file = ksi_open_bytevector_input_port(v);
    port = (ksi_obj) ksi_open_src_port((ksi_byte_port) file, file_name, file_line);
    x = ksi_comp_port(port, env);
    ksi_close_port(port);
    ksi_close_port(file);

    return x;
}

ksi_obj
ksi_eval_bytes (const char *scheme_code, int len, ksi_env env, const char *file_name, int file_line, ksi_obj *compiled_code)
{
    ksi_obj x;

    x = ksi_comp_bytes(scheme_code, len, env, file_name, file_line);
    if (compiled_code) {
        *compiled_code = x;
    }
    return ksi_eval_code(x, 0);
}


static ksi_char_port
ksi_open_src_file (const char *fname)
{
    int fd;
    ksi_byte_port file;
    ksi_char_port port;

    if ((fd = open(fname, O_RDONLY)) < 0) {
        ksi_exn_error(0, 0, "Can't load file \"%s\": %m", fname);
    }

    file = ksi_new_fd_input_port(fd, fname);
    port = ksi_open_src_port(file, fname, 1);

    return port;
}

static ksi_char_port
ksi_open_src_port (ksi_byte_port file, const char *file_name, int file_line)
{
    int c1, c2;
    ksi_char_port port;

    c1 = ksi_get_byte(file);
    c2 = ksi_get_byte(file);
    ksi_port_setpos((ksi_obj) file, ksi_long2num(0));

    if ((c1 == 0xff && c2 == 0xfe) || (c1 == 0xfe && c2 == 0xff)) {
        port = ksi_new_transcoded_port(file, ksi_data->sym_utf16, 0, ksi_data->sym_raise, file_name, file_line);
    } else {
        port = ksi_new_transcoded_port(file, ksi_data->sym_native, 0, ksi_data->sym_raise, file_name, file_line);
    }

    if (ksi_get_char(port) == L'#' && ksi_get_char(port) == L'!') {
        wint_t ch = ksi_get_char(port);
        if (ch == L' ' || ch == L'/') {
            /* skip first line of a script */
            for (;;) {
                ch = ksi_get_char(port);
                if (ch == WEOF || ch == L'\n')
                    return port;
                if (ch == L'\\') {
                    for (;;) {
                        ch = ksi_get_char(port);
                        if (ch == WEOF)
                            return port;
                        if (ch == L'\n' || !iswspace(ch))
                            break;
                    }
                }
            }
        } else {
            goto reset;
        }
    } else {
    reset:
        ksi_port_setpos((ksi_obj) port, ksi_long2num(0));
    }

    return port;
}

static ksi_obj
ksi_eval_port(const char *fname, ksi_obj port, ksi_env env)
{
    volatile ksi_wind wind;
    ksi_obj val;
    int i, len;

    wind = ksi_add_catch(ksi_true, 0, 0);
    KSI_FLUSH_REGISTER_WINDOWS;
    if (setjmp(wind->the_catch->jmp.j_buf) == 0) {
        for (;;) {
            val = ksi_get_datum(port);
            if (val == ksi_eof) {
                break;
            } else {
                len = ksi_list_len(val);
                if (len >= 2 && KSI_CAR(val) == ksi_data->sym_library) {
                    ksi_obj ver, name = parse_libname(KSI_CAR(KSI_CDR(val)), &ver);
                    ksi_obj body = KSI_CDR(KSI_CDR(val));
                    ksi_env env = ksi_lib_env(name, 1);
                    for (i = 2; i < len; i++) {
                        ksi_obj x = KSI_CAR(body);
                        if (!KSI_PAIR_P(x)) {
                            ksi_exn_error(L"import", val, "import: invalid library expression in %s", fname);
                        }
                        if (KSI_CAR(x) == ksi_data->sym_export) {
                            eval_lib_export(KSI_CDR(x), env);
                        } else if (KSI_CAR(x) == ksi_data->sym_import) {
                            KSI_SYNTAX(x, ksi_list_len(x) > 1, "import: invalid syntax");
                            x = ksi_comp_import(KSI_CDR(x));
                            ksi_eval_import(x, env);
                        } else {
                            break;
                        }
                        body = KSI_CDR(body);
                    }
                    for (; i < len; i++) {
                        ksi_eval(KSI_CAR(body), env);
                        body = KSI_CDR(body);
                    }
                } else {
                    ksi_eval(val, env);
                }
            }
        }
        ksi_del_catch(wind);
    } else {
        ksi_obj tag = wind->the_catch->thrown;
        ksi_obj val = wind->the_catch->value;
        ksi_handle_error(ksi_current_context, tag, val);
        ksi_exn_error(0, 0, "Error while loading file \"%s\".", fname);
    }

    return 0;
}

void
ksi_load_boot_file (const char *fn, ksi_env env)
{
    volatile ksi_char_port port = 0;
    const char *dir;
    char *tmp;

    ksi_info("load boot file %s", fn);

    if (ksi_current_context && ksi_current_context->loader_proc) {
        ksi_byte_port bp = ksi_current_context->loader_proc(fn, ksi_current_context->loader_data);
        if (bp) {
            port = ksi_open_src_port(bp, fn, 1);
        }
    }

    if (!port) {
        if (ABSOLUTE_PATH(fn) || fn[0] == '~') {
            fn = ksi_expand_file_name(fn);
        } else {
            dir = ksi_scheme_lib_dir();
            tmp = alloca(strlen(dir) + strlen(DIR_SEP) + strlen(fn) + 1);
            strcat(strcat(strcpy(tmp, dir), DIR_SEP), fn);
            fn = tmp;
        }

        port = ksi_open_src_file(fn);
    }

    ksi_eval_port(fn, (ksi_obj) port, env);
}

 /* End of code */
