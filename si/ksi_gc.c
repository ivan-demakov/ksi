/*
 * ksi_gc.c
 * gc function
 *
 * Copyright (C) 1997-2010, 2014, 2015, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Wed Nov 26 23:14:46 1997
 * Last Update:   Thu Jul 31 03:27:17 2014
 *
 */


#include "ksi_gc.h"
#include "ksi_type.h"

#define GC_THREADS 1
#if WIN32
#  define GC_DLL 1
#  include <gc/gc.h>
#else
#  include <gc/gc.h>
#endif

#if defined(_MSC_VER)
#  pragma comment(lib, "gc")
#  pragma comment(lib, "mpir")
#endif

#include <gmp.h>


static int gc_inited = 0;
static ksi_gc_warn_function gc_warn = 0;


static void
ksi_gc_warn (char* msg, GC_word arg)
{
    if (gc_warn) {
        gc_warn(msg, arg);
    } else if (ksi_current_context) {
        ksi_warn(msg, arg);
    } else {
        static const char *wrn = "GC warning: ";
#ifdef WIN32
        MessageBox(NULL, msg, wrn, MB_TASKMODAL | MB_ICONEXCLAMATION | MB_OK);
#else
        write(2, wrn, strlen(wrn));
        write(2, msg, strlen(msg));
        write(2, "\n", 1);
#endif
    }
}

static void*
alloc_gmp(size_t sz)
{
    return GC_malloc(sz);
}

static void*
realloc_gmp(void *ptr, size_t old_sz, size_t new_sz)
{
    return GC_realloc(ptr, new_sz);
}

static void
free_gmp(void *ptr, size_t sz)
{
    GC_free(ptr);
}

void
ksi_init_gc(void)
{
    if (gc_inited)
	return;
    gc_inited++;

    GC_allow_register_threads();

    GC_INIT();
    GC_set_all_interior_pointers(1);
    GC_set_java_finalization(1);
    GC_set_warn_proc(ksi_gc_warn);

    mp_set_memory_functions(alloc_gmp, realloc_gmp, free_gmp);
}

void*
ksi_malloc (size_t sz)
{
    if (!gc_inited)
        ksi_init_gc();

    return GC_malloc(sz);
}

void*
ksi_malloc_data (size_t sz)
{
    if (!gc_inited)
        ksi_init_gc();

    return GC_malloc_atomic_ignore_off_page(sz);
}

void*
ksi_malloc_eternal (size_t sz)
{
    if (!gc_inited)
        ksi_init_gc();

    return GC_malloc_uncollectable(sz);
}

void*
ksi_realloc (void* ptr, size_t sz)
{
    if (!gc_inited)
        ksi_init_gc();

    return GC_realloc(ptr, sz);
}

void
ksi_free (void* ptr)
{
    if (!gc_inited)
        ksi_init_gc();

    GC_free(ptr);
}

char *
ksi_strdup (const char *ptr)
{
    size_t len;
    char *p;

    if (!gc_inited)
        ksi_init_gc();

    len = strlen(ptr);
    p = GC_malloc_atomic_ignore_off_page(len+1);
    memcpy(p, ptr, len+1);
    return p;
}

void*
ksi_base_ptr(void* ptr)
{
    if (!gc_inited)
        ksi_init_gc();

    return GC_base(ptr);
}

void
ksi_register_finalizer (void* obj, ksi_finalizer_function proc, void* data)
{
    GC_finalization_proc old_proc;
    void* old_data;

    if (!gc_inited)
        ksi_init_gc();

    GC_register_finalizer_ignore_self (obj, proc, data, &old_proc, &old_data);
}

int
ksi_expand_heap (unsigned sz)
{
    if (!gc_inited)
        ksi_init_gc();

    return GC_expand_hp(sz);
}

void
ksi_set_max_heap (unsigned sz)
{
    if (!gc_inited)
        ksi_init_gc();

    GC_set_max_heap_size(sz);
}

size_t
ksi_get_heap_size (void)
{
    if (!gc_inited)
        ksi_init_gc();

    return GC_get_heap_size();
}

size_t
ksi_get_heap_free (void)
{
    if (!gc_inited)
        ksi_init_gc();

    return GC_get_free_bytes();
}

size_t
ksi_gcollections (void)
{
    if (!gc_inited)
        ksi_init_gc();

    return GC_get_gc_no();
}

int
ksi_gcollect (int full)
{
    if (!gc_inited)
        ksi_init_gc();

    if (full) {
        while (GC_collect_a_little());
        GC_gcollect();
        return 0;
    }

    return GC_collect_a_little();
}

void
ksi_enable_gc (void)
{
    if (!gc_inited)
        ksi_init_gc();

    GC_enable();
}

void
ksi_disable_gc (void)
{
    if (!gc_inited)
        ksi_init_gc();

    GC_disable();
}

void
ksi_set_gc_warn (ksi_gc_warn_function warn)
{
    gc_warn = warn;
}

void
ksi_init_gc_thread(void)
{
    struct GC_stack_base base;

    GC_get_stack_base(&base);
    GC_register_my_thread(&base);
}

void
ksi_term_gc_thread(void)
{
    GC_unregister_my_thread();
}

#if defined(WIN32)

int APIENTRY
DllMain (HINSTANCE hInstance, DWORD fdwReason, PVOID pvReserved)
{
    switch (fdwReason) {
    case DLL_PROCESS_ATTACH:
        //ksi_init_gc();
        break;

    case DLL_PROCESS_DETACH:
        //ksi_term();
        break;

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
        break;
    }

    return TRUE;
}

#endif

/* End of code */
