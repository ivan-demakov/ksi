/*
 * ksi_proc.c
 * primitives
 *
 * Copyright (C) 1997-2010, 2014, 2015, Ivan Demakov.
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Wed Dec  3 08:16:27 1997
 * Last Update:   Mon May 19 21:44:46 2014
 *
 */

#include "ksi_gc.h"
#include "ksi_jump.h"
#include "ksi_proc.h"
#include "ksi_env.h"
#include "ksi_comp.h"
#include "ksi_klos.h"


ksi_obj
ksi_apply_prim (ksi_prim prim, unsigned int argc, ksi_obj *args)
{
    unsigned int n;
    ksi_obj *av;

    KSI_WNA(argc >= prim->reqv, (ksi_obj) prim, prim->name);

    if (prim->has_rest) {
        if (argc <= (unsigned)prim->call) {
            av = (ksi_obj *) alloca(sizeof(*av) * prim->call);
            for (n = 0; n < argc; n++)
                av[n] = args[n];
            while (n < (unsigned)prim->call)
                av[n++] = 0;
            argc = 0;
            args = 0;
        } else {
            av = args;
            argc -= prim->call;
            args += prim->call;
        }

        switch (prim->call) {
        case KSI_CALL_ARG0:
            return ((ksi_call_rest0) prim->proc)(argc, args);
        case KSI_CALL_ARG1:
            return ((ksi_call_rest1) prim->proc) (av[0], argc, args);
        case KSI_CALL_ARG2:
            return ((ksi_call_rest2) prim->proc) (av[0], av[1], argc, args);
        case KSI_CALL_ARG3:
            return ((ksi_call_rest3) prim->proc) (av[0], av[1], av[2], argc, args);
        case KSI_CALL_ARG4:
            return ((ksi_call_rest4) prim->proc) (av[0], av[1], av[2], av[3], argc, args);
        case KSI_CALL_ARG5:
            return ((ksi_call_rest5) prim->proc) (av[0], av[1], av[2], av[3], av[4], argc, args);
        case KSI_CALL_ARG6:
            return ((ksi_call_rest6) prim->proc) (av[0], av[1], av[2], av[3], av[4], av[5], argc, args);
        case KSI_CALL_ARG7:
            return ((ksi_call_rest7) prim->proc) (av[0], av[1], av[2], av[3], av[4], av[5], av[6], argc, args);
        case KSI_CALL_ARG8:
            return ((ksi_call_rest8) prim->proc) (av[0], av[1], av[2], av[3], av[4], av[5], av[6], av[7], argc, args);
        case KSI_CALL_ARG9:
            return ((ksi_call_rest9) prim->proc) (av[0], av[1], av[2], av[3], av[4], av[5], av[6], av[7], av[8], argc, args);
        default:
            break;
        }
    } else {
        if (argc == prim->call) {
            av = args;
        } else {
            KSI_WNA(argc < (unsigned)prim->call, (ksi_obj) prim, prim->name);

            av = (ksi_obj *) alloca(sizeof(*av) * prim->call);
            for (n = 0; n < argc; n++)
                av[n] = args[n];
            while (n < (unsigned)prim->call)
                av[n++] = 0;
        }

        switch (prim->call) {
        case KSI_CALL_ARG0:
            return ((ksi_call_arg0) prim->proc)();
        case KSI_CALL_ARG1:
            return ((ksi_call_arg1) prim->proc) (av[0]);
        case KSI_CALL_ARG2:
            return ((ksi_call_arg2) prim->proc) (av[0], av[1]);
        case KSI_CALL_ARG3:
            return ((ksi_call_arg3) prim->proc) (av[0], av[1], av[2]);
        case KSI_CALL_ARG4:
            return ((ksi_call_arg4) prim->proc) (av[0], av[1], av[2], av[3]);
        case KSI_CALL_ARG5:
            return ((ksi_call_arg5) prim->proc) (av[0], av[1], av[2], av[3], av[4]);
        case KSI_CALL_ARG6:
            return ((ksi_call_arg6) prim->proc) (av[0], av[1], av[2], av[3], av[4], av[5]);
        case KSI_CALL_ARG7:
            return ((ksi_call_arg7) prim->proc) (av[0], av[1], av[2], av[3], av[4], av[5], av[6]);
        case KSI_CALL_ARG8:
            return ((ksi_call_arg8) prim->proc) (av[0], av[1], av[2], av[3], av[4], av[5], av[6], av[7]);
        case KSI_CALL_ARG9:
            return ((ksi_call_arg9) prim->proc) (av[0], av[1], av[2], av[3], av[4], av[5], av[6], av[7], av[8]);
        default:
            break;
        }
    }

    return (ksi_obj) prim;
}


ksi_prim
ksi_new_prim(const wchar_t *name, ksi_proc_t proc, ksi_call_t call, unsigned int reqv)
{
    ksi_prim val = ksi_malloc(sizeof(*val));
    int has_rest = 0;

    if (call >= KSI_CALL_NUM) {
        has_rest = 1;
        call -= KSI_CALL_NUM;
    }

    if (call == KSI_CALL_ARG0 && reqv == 0 && !has_rest)
        val->o.itag = KSI_TAG_PRIM_0;
    else if (call == KSI_CALL_ARG1 && reqv == 1 && !has_rest)
        val->o.itag = KSI_TAG_PRIM_1;
    else if (call == KSI_CALL_ARG2 && reqv == 2 && !has_rest)
        val->o.itag = KSI_TAG_PRIM_2;
    else if (call == KSI_CALL_ARG0 && reqv == 0 && has_rest)
        val->o.itag = KSI_TAG_PRIM_r;
    else
        val->o.itag = KSI_TAG_PRIM;

    val->proc = proc;
    val->call = call;
    val->reqv = reqv;
    val->has_rest = has_rest;
    val->name = (name ? name : L"#<primitive>");

    return val;
}

ksi_obj
ksi_defun (const wchar_t *name, ksi_proc_t proc, ksi_call_t call, unsigned int reqv, ksi_env env)
{
    if (name) {
        ksi_obj sym = ksi_str02sym(name);
        ksi_prim val = ksi_new_prim(KSI_SYM_PTR(sym), proc, call, reqv);
        ksi_define(sym, (ksi_obj) val, env);
        ksi_export(env, sym, 0);
        return (ksi_obj) val;
    } else {
        ksi_prim val = ksi_new_prim(0, proc, call, reqv);
        return (ksi_obj) val;
    }
}

void
ksi_reg_unit (struct Ksi_Prim_Def *defs, ksi_env env)
{
    while (defs->name) {
        ksi_defun (defs->name, defs->proc, defs->call, defs->reqv, env);
        defs++;
    }
}

ksi_obj
ksi_close_proc (ksi_obj proc, unsigned int argc, ksi_obj *argv)
{
    ksi_prim_closure clos;
    unsigned int i;

    KSI_CHECK(proc, KSI_PROC_P(proc), "ksi_close_proc: invalid procedure");

    if (argc <= 0)
        return proc;

    if (ksi_procedure_has_arity_p(proc, ksi_long2num(argc), ksi_true) == ksi_false) {
        ksi_exn_error(0, proc, "ksi_close_proc: invalid procedure arity");
    }

    clos = ksi_malloc(sizeof(*clos) + argc * sizeof(*argv) - sizeof(*argv));
    clos->o.itag = KSI_TAG_PRIM_CLOSURE;
    clos->proc = proc;
    clos->argc = argc;
    for (i = 0; i < argc; i++)
        clos->argv[i] = argv[i];

    return (ksi_obj) clos;
}

ksi_obj
ksi_apply_prim_closure (ksi_prim_closure clos, unsigned int argc, ksi_obj *argv)
{
    unsigned int n, i;
    ksi_obj *v;

    n = argc + clos->argc;
    v = (ksi_obj*) alloca (sizeof (ksi_obj) * n);

    for (n = 0; n < clos->argc; n++)
        v[n] = clos->argv[n];
    for (i = 0; i < argc; n++, i++)
        v[n] = argv[i];

    return ksi_apply_proc (clos->proc, n, v);
}

ksi_obj
ksi_procedure_p (ksi_obj x)
{
    switch (x->itag) {
    case KSI_TAG_PRIM:
    case KSI_TAG_PRIM_0:
    case KSI_TAG_PRIM_1:
    case KSI_TAG_PRIM_2:
    case KSI_TAG_PRIM_r:
    case KSI_TAG_CLOSURE:
    case KSI_TAG_PRIM_CLOSURE:
    case KSI_TAG_CONS:
    case KSI_TAG_CAR:
    case KSI_TAG_CDR:
    case KSI_TAG_NOT:
    case KSI_TAG_EQP:
    case KSI_TAG_EQVP:
    case KSI_TAG_EQUALP:
    case KSI_TAG_NULLP:
    case KSI_TAG_PAIRP:
    case KSI_TAG_LISTP:
    case KSI_TAG_VECTORP:
    case KSI_TAG_LIST:
    case KSI_TAG_MK_VECTOR:
    case KSI_TAG_LIST2VECTOR:
    case KSI_TAG_APPEND:
    case KSI_TAG_APPLY:
    case KSI_TAG_CALL_CC:
    case KSI_TAG_CALL_WITH_VALUES:
    case KSI_TAG_MEMQ:
    case KSI_TAG_MEMV:
    case KSI_TAG_MEMBER:
        return ksi_true;

    case KSI_TAG_EXTENDED:
        if (((struct Ksi_EObj *) x) -> etag -> apply)
            return ksi_true;
        return ksi_false;
    }

    if (KSI_GENERIC_P(x))
        return ksi_true;

    if (KSI_NEXT_METHOD_P(x))
        return ksi_true;

    return ksi_false;
}

ksi_obj
ksi_apply_0 (ksi_obj proc)
{
    return ksi_apply_proc (proc, 0, 0);
}

ksi_obj
ksi_apply_1 (ksi_obj proc, ksi_obj arg1)
{
    return ksi_apply_proc (proc, 1, &arg1);
}

ksi_obj
ksi_apply_2 (ksi_obj proc, ksi_obj arg1, ksi_obj arg2)
{
    ksi_obj args[2];
    args[0] = arg1;
    args[1] = arg2;
    return ksi_apply_proc (proc, 2, args);
}

ksi_obj
ksi_apply_3 (ksi_obj proc, ksi_obj arg1, ksi_obj arg2, ksi_obj arg3)
{
    ksi_obj args[3];
    args[0] = arg1;
    args[1] = arg2;
    args[2] = arg3;
    return ksi_apply_proc (proc, 3, args);
}

ksi_obj
ksi_apply (ksi_obj proc, ksi_obj alist)
{
    ksi_obj *av;
    int i, len = ksi_list_len(alist);
    KSI_CHECK (alist, len >= 0, "ksi_apply: invalid argument list");

    av = (ksi_obj*) alloca(len * sizeof *av);
    for (i = 0; i < len; i++) {
        av[i] = KSI_CAR(alist);
        alist = KSI_CDR(alist);
    }

    return ksi_apply_proc(proc, len, av);
}

ksi_obj
ksi_apply_proc (ksi_obj proc, unsigned int argc, ksi_obj* argv)
{
    ksi_code code;
    unsigned int i;

    switch (proc->itag) {
    case KSI_TAG_LIST:
        return ksi_new_list (argc, argv);

    case KSI_TAG_MK_VECTOR:
        return ksi_new_vector (argc, argv);

    case KSI_TAG_APPEND:
        return ksi_append (argc, argv);

    case KSI_TAG_LIST2VECTOR:
        KSI_WNA(argc == 1, proc, L"list->vector");
        return ksi_list2vector (argv[0]);

    case KSI_TAG_NOT:
        KSI_WNA(argc == 1, proc, L"not");
        return (argv[0] == ksi_false ? ksi_true : ksi_false);

    case KSI_TAG_NULLP:
        KSI_WNA(argc == 1, proc, L"null?");
        return (argv[0] == ksi_nil ? ksi_true : ksi_false);

    case KSI_TAG_PAIRP:
        KSI_WNA(argc == 1, proc, L"pair?");
        return (KSI_PAIR_P(argv[0]) ? ksi_true : ksi_false);

    case KSI_TAG_LISTP:
        KSI_WNA(argc == 1, proc, L"list?");
        return (KSI_LIST_P (argv[0]) ? ksi_true : ksi_false);

    case KSI_TAG_VECTORP:
        KSI_WNA (argc == 1, proc, L"vector?");
        return (KSI_VEC_P(argv[0]) ? ksi_true : ksi_false);

    case KSI_TAG_EQP:
        KSI_WNA(argc == 2, proc, L"eq?");
        return argv[0] == argv[1] ? ksi_true : ksi_false;

    case KSI_TAG_EQVP:
        KSI_WNA (argc == 2, proc, L"eqv?");
        return KSI_EQV_P (argv[0], argv[1]) ? ksi_true : ksi_false;

    case KSI_TAG_EQUALP:
        KSI_WNA(argc == 2, proc, L"equal?");
        return KSI_EQUAL_P(argv[0], argv[1]) ? ksi_true : ksi_false;

    case KSI_TAG_MEMQ:
        KSI_WNA(argc == 2, proc, L"memq");
        return ksi_memq(argv[0], argv[1]);

    case KSI_TAG_MEMV:
        KSI_WNA(argc == 2, proc, L"memv");
        return ksi_memv(argv[0], argv[1]);

    case KSI_TAG_MEMBER:
        KSI_WNA(argc == 2, proc, L"member");
        return ksi_member(argv[0], argv[1]);

    case KSI_TAG_CONS:
        KSI_WNA(argc == 2, proc, L"cons");
        return ksi_cons(argv[0], argv[1]);

    case KSI_TAG_CAR:
        KSI_WNA(argc == 1, proc, L"car");
        KSI_CHECK(argv[0], KSI_PAIR_P(argv[0]), "car: invalid pair");
        return KSI_CAR (argv[0]);

    case KSI_TAG_CDR:
        KSI_WNA(argc == 1, proc, L"cdr");
        KSI_CHECK(argv[0], KSI_PAIR_P(argv[0]), "cdr: invalid pair");
        return KSI_CDR(argv[0]);

    case KSI_TAG_PRIM_0:
        KSI_WNA(argc == 0, proc, KSI_PRIM_NAME (proc));
        return ((ksi_call_arg0) KSI_PRIM_PROC(proc)) ();

    case KSI_TAG_PRIM_1:
        KSI_WNA(argc == 1, proc, KSI_PRIM_NAME(proc));
        return ((ksi_call_arg1) KSI_PRIM_PROC(proc)) (argv[0]);

    case KSI_TAG_PRIM_2:
        KSI_WNA(argc == 2, proc, KSI_PRIM_NAME(proc));
        return ((ksi_call_arg2) KSI_PRIM_PROC(proc)) (argv[0], argv[1]);

    case KSI_TAG_PRIM_r:
        KSI_WNA(argc >= KSI_PRIM_REQV(proc), proc, KSI_PRIM_NAME(proc));
        return ((ksi_call_rest0) KSI_PRIM_PROC(proc)) (argc, argv);

    case KSI_TAG_PRIM:
        return ksi_apply_prim((ksi_prim) proc, argc, argv);

    case KSI_TAG_PRIM_CLOSURE:
        return ksi_apply_prim_closure((ksi_prim_closure) proc, argc, argv);

    case KSI_TAG_EXTENDED:
        if (((struct Ksi_EObj *) proc) -> etag -> apply) {
            return ((struct Ksi_EObj *) proc) -> etag -> apply ((struct Ksi_EObj *) proc, argc, argv);
        }
        break;

    default:
        break;
    } /* switch */

    code = ksi_new_code(argc + 1, KSI_TAG_CALL);
    code->val[0] = ksi_new_quote(proc);
    for (i = 1; i <= argc; i++)
        code->val[i] = ksi_new_quote(*argv++);

    return ksi_eval_code((ksi_obj) code, 0);
}

ksi_obj
ksi_procedure_arity (ksi_obj x)
{
    ksi_obj n;
    long d;

    switch (x->itag) {
    case KSI_TAG_PRIM:
    case KSI_TAG_PRIM_0:
    case KSI_TAG_PRIM_1:
    case KSI_TAG_PRIM_2:
    case KSI_TAG_PRIM_r:
        return ksi_long2num(((ksi_prim) x) -> reqv);

    case KSI_TAG_PRIM_CLOSURE:
        n = ksi_procedure_arity(((ksi_prim_closure) x) -> proc);
        d = ksi_num2long(n, "procedure-arity") - ((ksi_prim_closure) x) -> argc;
        return ksi_long2num(d <= 0 ? 0 : d);

    case KSI_TAG_CLOSURE:
        d = KSI_CLOS_NARY(x);
        if (KSI_CLOS_OPTS(x))
            d -= 1;
        return ksi_long2num(d);

    case KSI_TAG_APPEND:
    case KSI_TAG_LIST:
    case KSI_TAG_MK_VECTOR:
        return ksi_long2num(0);

    case KSI_TAG_CAR:
    case KSI_TAG_CDR:
    case KSI_TAG_NOT:
    case KSI_TAG_NULLP:
    case KSI_TAG_PAIRP:
    case KSI_TAG_LISTP:
    case KSI_TAG_VECTORP:
    case KSI_TAG_LIST2VECTOR:
    case KSI_TAG_CALL_CC:
        return ksi_long2num(1);

    case KSI_TAG_CONS:
    case KSI_TAG_EQP:
    case KSI_TAG_EQVP:
    case KSI_TAG_EQUALP:
    case KSI_TAG_MEMQ:
    case KSI_TAG_MEMV:
    case KSI_TAG_MEMBER:
    case KSI_TAG_APPLY:
    case KSI_TAG_CALL_WITH_VALUES:
        return ksi_long2num(2);

    default:
        KSI_CHECK(x, KSI_PROC_P(x), "procedure-arity: invalid procedure in arg1");
    }

    return ksi_long2num(0);
}

ksi_obj
ksi_procedure_has_arity_p (ksi_obj x, ksi_obj argnum, ksi_obj with_opts)
{
    unsigned int num, nary;

    KSI_CHECK(argnum, KSI_UINT_P(argnum), "procedure-has-arity?: invalid integer in arg2");
    num = ksi_num2uint(argnum, "procedure-has-arity?");

    if (with_opts == ksi_false)
        with_opts = 0;

    switch (x->itag) {
    case KSI_TAG_PRIM:
    case KSI_TAG_PRIM_0:
    case KSI_TAG_PRIM_1:
    case KSI_TAG_PRIM_2:
    case KSI_TAG_PRIM_r:
        if (num < ((ksi_prim) x) -> reqv)
            return (with_opts ? ksi_true : ksi_false);
        if (((ksi_prim) x) -> has_rest)
            return ksi_true;
        return (num <= ((unsigned) ((ksi_prim) x)->call) ? ksi_true : ksi_false);

    case KSI_TAG_PRIM_CLOSURE:
        return ksi_procedure_has_arity_p(((ksi_prim_closure) x) -> proc, ksi_int2num(num + ((ksi_prim_closure) x) -> argc), with_opts);

    case KSI_TAG_CLOSURE:
        nary = KSI_CLOS_NARY(x);
        if (KSI_CLOS_OPTS(x))
            nary -= 1;
        if (num < nary)
            return (with_opts ? ksi_true : ksi_false);
        if (KSI_CLOS_OPTS(x))
            return ksi_true;
        return (num == nary ? ksi_true : ksi_false);

    case KSI_TAG_APPEND:
    case KSI_TAG_LIST:
    case KSI_TAG_MK_VECTOR:
        return ksi_true;

    case KSI_TAG_CAR:
    case KSI_TAG_CDR:
    case KSI_TAG_NOT:
    case KSI_TAG_NULLP:
    case KSI_TAG_PAIRP:
    case KSI_TAG_LISTP:
    case KSI_TAG_VECTORP:
    case KSI_TAG_LIST2VECTOR:
    case KSI_TAG_CALL_CC:
        if (with_opts)
            return (num <= 1 ? ksi_true : ksi_false);
        return (num == 1 ? ksi_true : ksi_false);

    case KSI_TAG_CONS:
    case KSI_TAG_EQP:
    case KSI_TAG_EQVP:
    case KSI_TAG_EQUALP:
    case KSI_TAG_MEMQ:
    case KSI_TAG_MEMV:
    case KSI_TAG_MEMBER:
    case KSI_TAG_APPLY:
    case KSI_TAG_CALL_WITH_VALUES:
        if (with_opts)
            return (num <= 2 ? ksi_true : ksi_false);
        return (num == 2 ? ksi_true : ksi_false);

    default:
        KSI_CHECK(x, KSI_PROC_P(x), "procedure-has-arity?: invalid procedure in arg1");
    }

    return ksi_true;
}

/* End of code */
