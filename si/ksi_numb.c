/*
 * ksi_numb.c
 * numbers
 *
 * Copyright (C) 1997-2010, 2015, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Sun Apr 27 14:17:11 1997
 * Last Update:   Tue May 19 05:01:29 2015
 *
 */

#include "ksi_type.h"
#include "ksi_printf.h"
#include "ksi_util.h"
#include "ksi_gc.h"

#include <gmp.h>
#include <math.h>


struct Ksi_Bignum
{
    struct Ksi_Obj o;
    mpq_t val;
};

struct Ksi_Flonum
{
    struct Ksi_Obj o;
    double real;
    double imag;
};

#define KSI_REPART(x) (((ksi_flonum) (x)) -> real)
#define KSI_IMPART(x) (((ksi_flonum) (x)) -> imag)

static double ksi_fp_zero = 0.0;

#define FLO_IS_INT(x) (floor((x)) == (x))
#define BIG_IS_INT(x) (mpz_cmp_ui(mpq_denref(((ksi_bignum) (x))->val), 1) == 0)


static inline ksi_bignum
new_big()
{
    ksi_bignum big = ksi_malloc(sizeof *big);
    big->o.itag = KSI_TAG_BIGNUM;
    mpq_init(big->val);
    return big;
}


/*
 * ksi_str2flo
 *	Вызывается из ksi_str2num для преобразования строки в
 *	число с плавающей точкой.
 *	RADIX должет быть равен 2, 8, 10, 16.
 *
 * Return
 *	Число полученное в результате разбора строки,
 *	либо ksi_false если строку разобрать не удалось.
 *	Число в строке должно быть записано цифрами меньше RADIX.
 *	Кроме цифр в числе могут присутсвовать символ `_' (пропускается),
 *      а также в начале последовательности цифр может присутствовать знак `+' или `-'.
 *	Символ `_' не должен быть первым.
 *
 *	Функция обрабатывает следующие формы записи числа.
 *	В квадратных скобках указаны необязательные компоненты,
 *	в фигурных - последовательности цифр
 *
 *	  * [+-]{N}/{D}
 *		Символ `/' не должен отделятся пробелами
 *		N и D преобразуются в целые числа и возвращается
 *		результат деления N на D.
 *
 *	  * [+-][{N}][.{D}][e[+-]{E}]
 *			. - десятичная точка
 *			e - символ экспоненты: один из `E F L D S e f l d s'
 *
 *
 * Bugs
 *	Если RADIX равен 16, символы `D E F d e f' трактуются как
 *	шестнадцатиричные цифры, а не как символ экспоненты.
 *
 */

static ksi_obj
ksi_str2flo (const wchar_t *str, int radix)
{
    register int c, i = 0;
    double lead_sgn;
    double res = 0.0, tmp = 0.0;
    int flg = 0, point = 0, expsgn = 1, expon = 0;
    ksi_obj second;

    if (*str == L'-') lead_sgn = -1.0, ++str;
    else if (*str == L'+') lead_sgn = 1.0, ++str;
    else lead_sgn = 0.0;

    if ((str[0] == L'n' || str[0] == L'N') && (str[1] == L'a' || str[1] == L'A') && (str[2] == L'n' || str[2] == L'N') && (str[3] == L'.')) {
        /* handle [+-]nan. */
        res = ksi_fp_zero / ksi_fp_zero;
        str += 4;
        while (str[0] == L'0') str++;
        goto done;
    }

    if ((str[0] == L'i' || str[0] == L'I') && (str[1] == L'n' || str[1] == L'N') && (str[2] == L'f' || str[2] == L'F') && (str[3] == L'.')) {
        /* handle [+-]inf. */
        res = 1.0 / ksi_fp_zero;
        str += 4;
        while (str[0] == L'0') str++;
        goto done;
    }

    if (*str == L'i' || *str == L'I') {
        /* handle `+i' and `-i'   */
        if (lead_sgn==0.0)  /* must have leading sign */
            return ksi_false;
        if (str[1] != L'\0') /* `i' not last character */
            return ksi_false;
        return ksi_rectangular (0.0, lead_sgn);
    }

    /* check initial digits */
    for (/**/; (c = str[i]) != L'\0'; ++i) {
        switch (c) {
        case L'0': case L'1': case L'2': case L'3': case L'4':
        case L'5': case L'6': case L'7': case L'8': case L'9':
            c = c - L'0';
            break;
        case L'D': case L'E': case L'F':
            if (radix <= 10)
                goto out1; /* must be exponent */
        case L'A': case L'B': case L'C':
            c = c - L'A' + 10;
            break;
        case L'd': case L'e': case L'f':
            if (radix <= 10)
                goto out1; /* must be exponent */
        case L'a': case L'b': case L'c':
            c = c - L'a' + 10;
            break;
        case L'_':
            if (flg)       /* ignore '_' inside number */
                continue;
        default:
            goto out1;
        }
        if (c >= radix)
            return ksi_false; /* bad digit for radix */
        res = res * radix + c;
        flg = 1;              /* now res is valid */
    }

out1:
    if (str[i] == L'\0')
        goto done;

    /* By here, must have seen a digit,
     * or must have next char be a `.'
     */
    if (!flg && str[i] != L'.')
        return ksi_false;

    if (str[i] == L'/') {
        flg = 0;
        for (++i; (c = str[i]) != L'\0'; ++i) {
            switch (c) {
            case L'0': case L'1': case L'2': case L'3': case L'4':
            case L'5': case L'6': case L'7': case L'8': case L'9':
                c = c - L'0';
                break;
            case L'A': case L'B': case L'C': case L'D': case L'E': case L'F':
                c = c - L'A' + 10;
                break;
            case L'a': case L'b': case L'c': case L'd': case L'e': case L'f':
                c = c - L'a' + 10;
                break;
            case L'_':
                if (flg)
                    continue;
            default:
                goto out2;
            }
            if (c >= radix)
                return ksi_false;
            tmp = tmp * radix + c;
            flg = 1; /* tmp is valid */
        }
    out2:
        if (!flg)
            return ksi_false;
        res /= tmp;
        goto done;
    }

    if (str[i] == L'.') {     /* decimal point notation */
        i++;
        if (str[i] == L'\0')  /* no digits after decimal point */
            goto out3;

        flg = 0;
        for (; (c = str[i]) != L'\0'; ++i) {
            switch (c) {
            case L'0': case L'1': case L'2': case L'3': case L'4':
            case L'5': case L'6': case L'7': case L'8': case L'9':
                c = c - L'0';
                break;
            case L'D': case L'E': case L'F':
                if (radix <= 10)
                    goto out3; /* must be exponent */
            case L'A': case L'B': case L'C':
                c = c - L'A' + 10;
                break;
            case L'd': case L'e': case L'f':
                if (radix <= 10)
                    goto out3; /* must be exponent */
            case L'a': case L'b': case L'c':
                c = c - L'a' + 10;
                break;
            case L'_':
                if (flg)
                    continue;
            default:
                goto out3;
            }
            if (c >= radix)
                return ksi_false; /* bad digit for radix */
            res = res * radix + c;
            point--;
            flg = 1;
        }
    out3:
        if (!flg)
            return ksi_false; /* no digits before AND after decimal point */
        if (str[i] == L'\0')
            goto adjust;
    }

    flg = 0;
    switch (str[i]) {      /* exponent */
    case L'd': case L'D':
    case L'e': case L'E':
    case L'f': case L'F':
    case L'l': case L'L':
    case L's': case L'S':
        if (!str[++i])
            return ksi_false; /* bad exponent */
        switch (str[i]) {
        case L'-':  expsgn = -1; /* no break */
        case L'+':  if (!str[++i]) return ksi_false; /* bad exponent */
        }
        for (; (c = str[i]) != L'\0'; ++i) {
            switch (c) {
            case L'0': case L'1': case L'2': case L'3': case L'4':
            case L'5': case L'6': case L'7': case L'8': case L'9':
                c = c - L'0';
                break;
            case L'D': case L'E': case L'F': case L'A': case L'B': case L'C':
                c = c - L'A' + 10;
                break;
            case L'd': case L'e': case L'f': case L'a': case L'b': case L'c':
                c = c - L'a' + 10;
                break;
            case L'_':
                if (flg)
                    continue;
            default:
                goto out4;
            }
            if (c >= radix)
                return ksi_false; /* bad digit for radix */
            expon = expon * radix + c;
            flg = 1;
        }
    out4:
        point += expsgn * expon;
    }

adjust:
    if (point >= 0)
        while (point--) res *= radix;
    else
        while (point++) res /= radix;

done:
    /* at this point, we have a legitimate floating point result */
    if (lead_sgn==-1.0)
        res = -res;
    if (str[i] == L'\0')
        return ksi_double2num(res);

    if (str[i] == L'i' || str[i] == L'I') {
        /* pure imaginary number  */
        if (lead_sgn==0.0)    /* must have leading sign */
            return ksi_false;
        if (str[i+1] != L'\0') /* `i' not last character */
            return ksi_false;
        return ksi_rectangular (0.0, res);
    }

    switch (str[i]) {
    case L'-': case L'+':
        /* rectangular input for complex number */
        /* get a imaginary part */
        second = ksi_str02num(&str[i], radix);
        if (KSI_NUM_P(second)) {
            if (ksi_real_part(second) == 0.0)
                return ksi_rectangular(res, ksi_imag_part(second));
        }
        break;
    case L'@':
        /* polar input for complex number */
        /* get a real for angle */
        second = ksi_str02num(&str[i+1], radix);
        if (KSI_NUM_P(second)) {
            if (ksi_imag_part(second) == 0.0)
                return ksi_polar(res, ksi_real_part(second));
        }
    }

    return ksi_false;
}


/*
 * ksi_str2big
 *	Вызывается из ksi_str2num для преобразования строки в число.
 *	RADIX должет быть равен 2, 8, 10, 16.
 *
 * Return
 *	Возвращает число, если строка может быть преобразована в число.
 *      В противном случае возращает ksi_false.
 */

static ksi_obj
ksi_str2big (const wchar_t *str, int radix)
{
    int neg = 0;
    ksi_bignum big;

    if (*str == L'+')
        str++;
    else if (*str == L'-')
        neg = 1, str++;

    if (*str == L'\0')
        return ksi_false;

    if (wcspbrk(str, L"-+."))
        return ksi_false;

    {
        int i, n = 0;
        char *tmp = alloca((wcslen(str) + 1) * sizeof(*tmp));
        for (i = 0; str[i]; i++) {
            switch(str[i]) {
            case L'a': case L'b': case L'c': case L'd': case L'e': case L'f':
            case L'A': case L'B': case L'C': case L'D': case L'E': case L'F':
                if (radix < 16)
                    return ksi_false;
                tmp[n++] = (char) str[i];
                break;
            case L'8': case L'9':
                if (radix < 10)
                    return ksi_false;
                tmp[n++] = (char) str[i];
                break;
            case L'5': case L'6': case L'7':
            case L'2': case L'3': case L'4':
                if (radix < 8)
                    return ksi_false;
                tmp[n++] = (char) str[i];
                break;
            case L'0': case L'1':
                tmp[n++] = (char) str[i];
                break;
            case L'_':
                if (i <= 0)
                    return ksi_false;
                break;
            case L'/':
                tmp[n++] = (char) str[i];
                break;
            default:
                return ksi_false;
            }
        }
        tmp[n] = L'\0';

        big = new_big();
        if (mpq_set_str(big->val, tmp, radix) == 0) {
            mpq_canonicalize(big->val);
            if (neg)
                mpq_neg(big->val, big->val);
            return (ksi_obj) big;
        }
    }

    return ksi_false;
}


/*
 * ksi_str02num
 *	Преобразует строку в число.
 *	RADIX должет быть равен 2, 8, 10, 16.
 *
 * Return
 *	Если строка не может быть преобразована в число, возвращает ksi_false.
 *
 */

ksi_obj
ksi_str02num (const wchar_t *str, int radix)
{
    ksi_obj val;
    int rd = radix, ex = 0;

    if (rd && rd != 2 && rd != 8 && rd != 10 && rd != 16)
        ksi_exn_error(0, ksi_long2num (rd), "string->number: invalid radix");

    if (str == 0 || str[0] == L'\0')
        return ksi_false;

    while (str[0] == L'#') {
        switch (str[1]) {
        case L'i': case L'I': if (ex) return ksi_false; ex = 2;  break;
        case L'e': case L'E': if (ex) return ksi_false; ex = 1;  break;
        case L'b': case L'B': if (rd) return ksi_false; rd = 2;  break;
        case L'o': case L'O': if (rd) return ksi_false; rd = 8;  break;
        case L'd': case L'D': if (rd) return ksi_false; rd = 10; break;
        case L'x': case L'X': if (rd) return ksi_false; rd = 16; break;
        default: return ksi_false;
        }
        str += 2;
    }

    if ((str[0] == L'-' || str[0] == L'+' || str[0] == L'.') && str[1] == L'\0')
        return ksi_false;

    if (rd == 0)
        rd = 10;

    if (ex == 2)
        return ksi_str2flo(str, rd);

    val = ksi_str2big(str, rd);
    if (ex == 0 && val == ksi_false)
        val = ksi_str2flo(str, rd);

    return val;
}


static wchar_t *
double2str (double f)
{
    static double fx[] = {
        0.0,
        5e-1,  5e-2,  5e-3,  5e-4,  5e-5,
        5e-6,  5e-7,  5e-8,  5e-9,  5e-10,
        5e-11, 5e-12, 5e-13, 5e-14, 5e-15,
        5e-16, 5e-17, 5e-18, 5e-19, 5e-20
    };

    int exp, wp, ch, efmt, dpt, d, i;
    wchar_t *a;

    if (f != f)
        return L"nan.0";
    if (f == f/2) {
        if (ksi_copysign(1.0, f) < 0.0)
            return (f == 0.0 ? L"-0.0" : L"-inf.0");
        return (f == 0.0 ? L"0.0" : L"+inf.0");
    }

    wp = 14;
    a = ksi_malloc_data((wp * 2) * sizeof(*a));
    exp = 0; ch = 0;

    if (f < 0.0) {
        a[ch++] = L'-';
        f = -f;
    }

    if (wp >= sizeof (fx) / sizeof (double))
        wp = sizeof (fx) / sizeof (double) - 1;

    while (f < 1.0) { f *= 10.0; exp--; }
    while (f > 10.0) { f *= 0.1; exp++; }
    if (f + fx[wp] >= 10.0) { f = 1.0; exp++; }

    efmt = (exp < -3) || (exp > wp+2);
    if (!efmt) {
        if (exp < 0) {
            a[ch++] = L'0';
            a[ch++] = L'.';
            dpt = exp;
            while (++dpt)  a[ch++] = L'0';
        } else {
            dpt = exp+1;
        }
    } else {
        dpt = 1;
    }

    do {
        d = (int) f;
        f -= d;
        a[ch++] = d+L'0';
        if (f < fx[wp])
            break;
        if (f+fx[wp] >= 1.0) {
            a[ch-1]++;
            break;
        }
        f *= 10.0;
        if (!(--dpt))
            a[ch++] = L'.';
    } while (wp--);

    if (dpt > 0) {
        if ((dpt > 4) && (exp > 6)) {
            d = (a[0] == L'-' ? 2 : 1);
            for (i = ch++; i > d; i--)
                a[i] = a[i-1];
            a[d] = L'.';
            efmt = 1;
        } else {
            while (--dpt)  a[ch++] = L'0';
            a[ch++] = L'.';
        }
    }

    if (a[ch-1]==L'.')
        a[ch++]=L'0'; /* trailing zero */
    if (efmt && exp) {
        a[ch++] = L'E';
        if (exp < 0) {
            exp = -exp;
            a[ch++] = L'-';
        }
        for (i = 10; i <= exp; i *= 10);
        for (i /= 10; i; i /= 10) {
            a[ch++] = exp/i + L'0';
            exp %= i;
        }
    }

    a[ch] = L'\0';
    return a;
}


/*
 * bignum2str
 *	Преобразует целое число в строку
 *	Используется в ksi_num2str
 */

static wchar_t *
bignum2str (ksi_bignum b, unsigned radix)
{
	size_t size;
	int malloced;
    char *buf;
    wchar_t *ptr;

    if (radix <= 0 || radix > 36)
        ksi_exn_error(0, ksi_uint2num(radix), "number->string: radix `%d' is not supported", radix);

    size = mpz_sizeinbase(mpq_numref(b->val), radix);
    size += mpz_sizeinbase(mpq_denref(b->val), radix);
    if (size <= 1020) {
        buf = alloca(size + 4);
        malloced = 0;
    } else {
        buf = ksi_malloc_atomic(size + 4);
        malloced = 1;
    }
    mpq_get_str(buf, radix, b->val);

    ptr = ksi_utf(buf);
    if (malloced)
        ksi_free(buf);

    return ptr;
}


/*
 * ksi_num2str
 *	Преобразует число в строку.
 *
 * Return
 *	Возвращает указатель на строку с внешним представлением
 *	числа X по основанию RADIX.
 *	Префикс основания числа _не_ записывается.
 *	Память под строку выделяется динамически.
 *
 * Bugs
 *	Для чисел с плавающей точкой RADIX должен быть равен 10.
 *
 *	Поскольку память под строку выделяется при каждом преобразовании,
 *	функция возвращает char*, а не const char*.  Этот факт используется
 *	в некоторых функциях.
 */

wchar_t *
ksi_num2str (ksi_obj x, int radix)
{
    if (radix == 0)
        radix = 10;
    else if (radix < 0)
        ksi_exn_error(0, ksi_long2num(radix), "number->string: negative radix `%d'", radix);

    if (KSI_BIGNUM_P(x))
        return bignum2str((ksi_bignum) x, radix);

    if (KSI_FLONUM_P(x)) {
        if (radix != 10)
            ksi_exn_error(0, ksi_long2num(radix), "number->string: radix `%d' is not supported for flonum", radix);

        if (KSI_IMPART(x) == 0.0) {
            return double2str(KSI_REPART(x));
        } else {
            wchar_t* rp = double2str(KSI_REPART(x));
            wchar_t* ip = double2str(KSI_IMPART(x));
            return ksi_aprintf("%ls%s%lsi", rp, (*ip == '-' || *ip == '+') ? "" : "+", ip);
        }
    }

    return L"#<invalid number>";
}

ksi_obj
ksi_double2num(double r)
{
    ksi_flonum num = ksi_malloc(sizeof *num);
    num->o.itag = KSI_TAG_FLONUM;
    num->real = r;
    num->imag = 0.0;
    return (ksi_obj)num;
}

ksi_obj
ksi_rectangular(double r, double i)
{
    ksi_flonum num = ksi_malloc(sizeof *num);
    num->o.itag = KSI_TAG_FLONUM;
    num->real = r;
    num->imag = i;
    return (ksi_obj)num;
}

ksi_obj
ksi_polar (double x, double a)
{
    return ksi_rectangular(x * cos(a), x * sin(a));
}

ksi_obj
ksi_int2num(int v)
{
    ksi_bignum big = new_big();
    mpq_set_si(big->val, v, 1);

    return (ksi_obj) big;
}

ksi_obj
ksi_uint2num(unsigned int v)
{
    ksi_bignum big = new_big();
    mpq_set_ui(big->val, v, 1);

    return (ksi_obj) big;
}

ksi_obj
ksi_long2num (long v)
{
    ksi_bignum big = new_big();
    mpq_set_si(big->val, v, 1);

    return (ksi_obj) big;
}

ksi_obj
ksi_ulong2num (unsigned long v)
{
    ksi_bignum big = new_big();
    mpq_set_ui(big->val, v, 1);

    return (ksi_obj) big;
}

ksi_obj
ksi_longlong2num (LONG_LONG v)
{
    mpq_t tmp;
    ksi_bignum big = new_big();
    mpq_init(tmp);
    mpq_set_si(big->val, (long) (v >> 32), 1);
    mpq_set_ui(tmp, (unsigned long) (v & 0xffffffff), 1);
    mpq_mul_2exp(big->val, big->val, 32);
    mpq_add(big->val, big->val, tmp);

    return (ksi_obj) big;
}

ksi_obj
ksi_ulonglong2num (unsigned LONG_LONG v)
{
    mpq_t tmp;
    ksi_bignum big = new_big();
    mpq_init(tmp);
    mpq_set_ui(big->val, (unsigned long) (v >> 32), 1);
    mpq_set_ui(tmp, (unsigned long) (v & 0xffffffff), 1);
    mpq_mul_2exp(big->val, big->val, 32);
    mpq_add(big->val, big->val, tmp);

    return (ksi_obj) big;
}

double
ksi_real_part (ksi_obj x)
{
    double val = 0.0;
    KSI_CHECK(x, KSI_NUM_P(x), "real-part: invalid number an arg1");

    if (KSI_BIGNUM_P(x)) {
	val = mpq_get_d(((ksi_bignum)x)->val);
    }
    else if (KSI_FLONUM_P(x)) {
	val = KSI_REPART(x);
    }
    return val;
}

double
ksi_imag_part (ksi_obj x)
{
    double val = 0.0;
    KSI_CHECK (x, KSI_NUM_P (x), "imag-part: invalid number in arg1");

    if (KSI_FLONUM_P(x)) {
        val = KSI_IMPART(x);
    }
    return val;
}

double
ksi_magnitude (ksi_obj x)
{
    return ksi_real_part(ksi_abs(x));
}

double
ksi_angle (ksi_obj z)
{
    double x = 0.0, y = 0.0;
    KSI_CHECK (z, KSI_NUM_P(z), "angle: invalid number in arg1");

    if (KSI_BIGNUM_P(z)) {
        x = mpq_sgn(((ksi_bignum) z)->val);
    } else if (KSI_FLONUM_P(z)) {
        x = KSI_REPART(z);
        y = KSI_IMPART(z);
    }

    return atan2(y, x);
}

ksi_obj
ksi_make_rectangular (ksi_obj x, ksi_obj y)
{
    KSI_CHECK (x, KSI_REAL_P(x), "make-rectangular: invalid number in arg1");
    KSI_CHECK (y, KSI_REAL_P(y), "make-rectangular: invalid number in arg1");
    return ksi_rectangular(ksi_real_part(x), ksi_real_part(y));
}

ksi_obj
ksi_make_polar (ksi_obj x, ksi_obj y)
{
    KSI_CHECK (x, KSI_REAL_P(x), "make-polar: invalid number in arg1");
    KSI_CHECK (y, KSI_REAL_P(y), "make-polar: invalid number in arg1");
    return ksi_polar(ksi_real_part(x), ksi_real_part(y));
}

int
ksi_int_p(ksi_obj x)
{
    if (KSI_BIGNUM_P(x) && BIG_IS_INT(x)) {
        if (mpz_fits_sint_p(mpq_numref(((ksi_bignum) x)->val)))
            return 1;
    }
    return 0;
}

int
ksi_long_p(ksi_obj x)
{
    if (KSI_BIGNUM_P(x) && BIG_IS_INT(x)) {
        if (mpz_fits_slong_p(mpq_numref(((ksi_bignum) x)->val)))
            return 1;
    }
    return 0;
}

int
ksi_ulong_p(ksi_obj x)
{
    if (KSI_BIGNUM_P(x) && BIG_IS_INT(x)) {
        if (mpz_fits_ulong_p(mpq_numref(((ksi_bignum) x)->val)))
            return 1;
    }
    return 0;
}

int
ksi_longlong_p(ksi_obj x)
{
    mpz_t tmp;
    if (KSI_BIGNUM_P(x) && BIG_IS_INT(x)) {
        if (mpz_fits_slong_p(mpq_numref(((ksi_bignum) x)->val)))
            return 1;

        mpz_init(tmp);
        mpz_tdiv_q_2exp(tmp, mpq_numref(((ksi_bignum) x)->val), 32);
        if (mpz_fits_slong_p(tmp))
            return 1;
    }
    return 0;
}

int
ksi_ulonglong_p(ksi_obj x)
{
    mpz_t tmp;
    if (KSI_BIGNUM_P(x) && BIG_IS_INT(x) && mpq_sgn(((ksi_bignum) x)->val) >= 0) {
        if (mpz_fits_ulong_p(mpq_numref(((ksi_bignum) x)->val)))
            return 1;

        mpz_init(tmp);
        mpz_tdiv_q_2exp(tmp, mpq_numref(((ksi_bignum) x)->val), 32);
        if (mpz_fits_ulong_p(tmp))
            return 1;
    }
    return 0;
}

int
ksi_num2int (ksi_obj x, const char *name)
{
    if (KSI_BIGNUM_P(x)) {
        if (BIG_IS_INT(x)) {
            if (mpz_fits_sint_p(mpq_numref(((ksi_bignum) x)->val)))
                return (int)mpz_get_si(mpq_numref(((ksi_bignum) x)->val));
            ksi_exn_error(0, x, "%s: integer out of range", name ? name : "ksi_num2int");
        }
    }

    if (KSI_FLONUM_P(x)) {
        if (KSI_IMPART(x) == 0.0) {
            double r = KSI_REPART(x);
            if (LONG_MIN <= r && r <= LONG_MAX)
                return (long) r;
            ksi_exn_error(0, x, "%s: integer out of range", name ? name : "ksi_num2int");
        }
    }

    ksi_exn_error(0, x, "%s: invalid integer", name ? name : "ksi_num2int");
    return 0;
}

unsigned int
ksi_num2uint (ksi_obj x, const char *name)
{
    if (KSI_BIGNUM_P(x)) {
        if (BIG_IS_INT(x)) {
            if (mpq_sgn(((ksi_bignum) x)->val) >= 0 && mpz_fits_uint_p(mpq_numref(((ksi_bignum) x)->val)))
                return (unsigned int)mpz_get_ui(mpq_numref(((ksi_bignum) x)->val));
            ksi_exn_error(0, x, "%s: integer out of range", name ? name : "ksi_num2uint");
        }
    }

    if (KSI_FLONUM_P(x)) {
        if (KSI_IMPART(x) == 0.0) {
            double r = KSI_REPART(x);
            if (0.0 <= r && r <= ULONG_MAX)
                return (unsigned long) r;
            ksi_exn_error(0, x, "%s: integer out of range", name ? name : "ksi_num2uint");
        }
    }

    ksi_exn_error(0, x, "%s: invalid integer", name ? name : "ksi_num2uint");
    return 0;
}

long
ksi_num2long (ksi_obj x, const char *name)
{
    if (KSI_BIGNUM_P(x)) {
        if (BIG_IS_INT(x)) {
            if (mpz_fits_slong_p(mpq_numref(((ksi_bignum) x)->val)))
                return (long)mpz_get_si(mpq_numref(((ksi_bignum) x)->val));
            ksi_exn_error(0, x, "%s: integer out of range", name ? name : "ksi_num2long");
        }
    }

    if (KSI_FLONUM_P(x)) {
        if (KSI_IMPART(x) == 0.0) {
            double r = KSI_REPART(x);
            if (LONG_MIN <= r && r <= LONG_MAX)
                return (long) r;
            ksi_exn_error(0, x, "%s: integer out of range", name ? name : "ksi_num2long");
        }
    }

    ksi_exn_error(0, x, "%s: invalid integer", name ? name : "ksi_num2long");
    return 0;
}

unsigned long
ksi_num2ulong (ksi_obj x, const char *name)
{
    if (KSI_BIGNUM_P(x)) {
        if (BIG_IS_INT(x)) {
            if (mpq_sgn(((ksi_bignum) x)->val) >= 0 && mpz_fits_ulong_p(mpq_numref(((ksi_bignum) x)->val)))
                return (unsigned long)mpz_get_ui(mpq_numref(((ksi_bignum) x)->val));
            ksi_exn_error(0, x, "%s: integer out of range", name ? name : "ksi_num2ulong");
        }
    }

    if (KSI_FLONUM_P(x)) {
        if (KSI_IMPART(x) == 0.0) {
            double r = KSI_REPART(x);
            if (0.0 <= r && r <= ULONG_MAX)
                return (unsigned long) r;
            ksi_exn_error(0, x, "%s: integer out of range", name ? name : "ksi_num2ulong");
        }
    }

    ksi_exn_error(0, x, "%s: invalid integer", name ? name : "ksi_num2ulong");
    return 0;
}

LONG_LONG
ksi_num2longlong (ksi_obj x, const char *name)
{
    mpz_t tmp;
    if (KSI_BIGNUM_P(x) && BIG_IS_INT(x)) {
        if (mpz_fits_slong_p(mpq_numref(((ksi_bignum) x)->val)))
            return mpz_get_si(mpq_numref(((ksi_bignum) x)->val));

        mpz_init(tmp);
        mpz_tdiv_q_2exp(tmp, mpq_numref(((ksi_bignum) x)->val), 32);
        if (mpz_fits_slong_p(tmp)) {
            LONG_LONG v = mpz_get_si(tmp);
            v <<= 32;
            v |= mpz_get_ui(mpq_numref(((ksi_bignum) x)->val));
            return v;
        }

        ksi_exn_error(0, x, "%s: integer out of range", name ? name : "ksi_num2longlong");
    }

    if (KSI_FLONUM_P(x)) {
        if (KSI_IMPART(x) == 0.0) {
            double r = KSI_REPART(x);
            if (LLONG_MIN <= r && r <= LLONG_MAX)
                return (LONG_LONG) r;
            ksi_exn_error(0, x, "%s: integer out of range", name ? name : "ksi_num2longlong");
        }
    }

    ksi_exn_error(0, x, "%s: invalid integer", name ? name : "ksi_num2longlong");
    return 0;
}

unsigned LONG_LONG
ksi_num2ulonglong (ksi_obj x, const char *name)
{
    mpz_t tmp;
    if (KSI_BIGNUM_P(x) && BIG_IS_INT(x) && mpq_sgn(((ksi_bignum) x)->val) >= 0) {
        if (mpz_fits_ulong_p(mpq_numref(((ksi_bignum) x)->val)))
            return mpz_get_ui(mpq_numref(((ksi_bignum) x)->val));

        mpz_init(tmp);
        mpz_tdiv_q_2exp(tmp, mpq_numref(((ksi_bignum) x)->val), 32);
        if (mpz_fits_ulong_p(tmp)) {
            unsigned LONG_LONG v = mpz_get_ui(tmp);
            v <<= 32;
            v |= mpz_get_ui(mpq_numref(((ksi_bignum) x)->val));
            return v;
        }

        ksi_exn_error(0, x, "%s: integer out of range", name ? name : "ksi_num2ulonglong");
    }

    if (KSI_FLONUM_P(x)) {
	if (KSI_IMPART(x) == 0.0) {
	    double r = KSI_REPART(x);
            if (0.0 <= r && r <= ULLONG_MAX)
                return (unsigned LONG_LONG) r;
	    ksi_exn_error(0, x, "%s: integer out of range", name ? name : "ksi_num2ulonglong");
	}
    }

    ksi_exn_error(0, x, "%s: invalid integer", name ? name : "ksi_num2ulonglong");
    return 0;
}

double
ksi_num2double(ksi_obj x, const char *name)
{
    if (KSI_BIGNUM_P(x)) {
	return mpq_get_d(((ksi_bignum)x)->val);
    }

    if (KSI_FLONUM_P(x)) {
	if (KSI_IMPART(x) == 0.0) {
	    return KSI_REPART(x);
	}
	ksi_exn_error(0, x, "%s: number is not real", name ? name : "ksi_num2ulonglong");
    }

    ksi_exn_error(0, x, "%s: invalid number", name ? name : "ksi_num2double");
    return 0;
}

ksi_obj
ksi_double2exact (double d, const char *name)
{
    ksi_bignum big;

    if (d == 0.0)
        return ksi_long2num(0);
    if (d != d)
        ksi_exn_error(0, ksi_double2num(d), "%s: invalid real number", name ? name : "exact");
    if (d == d/2)
        ksi_exn_error(0, ksi_double2num(d), "%s: invalid real number", name ? name : "exact");

    big = new_big();
    mpq_set_d(big->val, d);

    return (ksi_obj) big;
}

ksi_obj
ksi_inexact (ksi_obj x)
{
    if (KSI_FLONUM_P(x))
        return x;

    if (KSI_BIGNUM_P(x))
        return ksi_double2num(mpq_get_d(((ksi_bignum) x)->val));

    ksi_exn_error(0, x, "inexact: invalid number in arg1");
    return 0;
}

ksi_obj
ksi_exact (ksi_obj x)
{
    if (KSI_BIGNUM_P(x))
        return x;

    if (KSI_FLONUM_P(x) && KSI_IMPART(x) == 0.0)
        return ksi_double2exact(KSI_REPART(x), "exact");

    ksi_exn_error(0, x, "exact: invalid number in arg1");
    return 0;
}

ksi_obj
ksi_number2string (ksi_obj num, ksi_obj rad)
{
    int radix;

    KSI_CHECK (num, KSI_NUM_P(num), "number->string: invalid number in arg1");
    if (!rad) {
        radix = 10;
    } else {
        KSI_CHECK (rad, KSI_EINT_P(rad), "number->string: invalid integer in arg2");
        radix = ksi_num2long(rad, "number->string");
    }

    return ksi_str02string(ksi_num2str(num, radix));
}

ksi_obj
ksi_string2number (ksi_obj str, ksi_obj rad)
{
    int radix;

    KSI_CHECK (str, KSI_STR_P(str), "string->number: invalid string in arg1");
    if (!rad) {
        radix = 10;
    } else {
        KSI_CHECK (rad, KSI_EINT_P(rad), "string->number: invalid integer in arg2");
        radix = ksi_num2long (rad, "string->number");
    }

    return ksi_str02num(KSI_STR_PTR(str), radix);
}

ksi_obj
ksi_number_p (ksi_obj x)
{
    return KSI_NUM_P(x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_complex_p (ksi_obj x)
{
    return KSI_NUM_P(x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_real_p (ksi_obj x)
{
    if (KSI_BIGNUM_P(x))
        return ksi_true;

    if (KSI_FLONUM_P(x) && KSI_IMPART(x) == 0.0)
        return ksi_true;

    return ksi_false;
}

ksi_obj
ksi_rational_p (ksi_obj x)
{
    if (KSI_BIGNUM_P(x))
        return ksi_true;

    if (KSI_FLONUM_P(x) && KSI_IMPART(x) == 0.0) {
        double d = KSI_REPART(x);
        if (d == 0.0)
            return ksi_true;
        if (d != d || d == d/2) /* nan. or inf. */
            return ksi_false;
        return ksi_true;
    }

    return ksi_false;
}

ksi_obj
ksi_integer_p (ksi_obj x)
{
    if (KSI_BIGNUM_P(x)) {
        if (BIG_IS_INT(x))
            return ksi_true;
        return ksi_false;
    }

    if (KSI_FLONUM_P(x) && KSI_IMPART(x) == 0.0) {
        double d = KSI_REPART(x);
        if (d == 0.0)
            return ksi_true;
        if (d != d || d == d/2) /* nan. or inf. */
            return ksi_false;
        if (FLO_IS_INT(d))
            return ksi_true;
    }

    return ksi_false;
}

ksi_obj
ksi_exact_p (ksi_obj x)
{
    return KSI_BIGNUM_P(x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_inexact_p (ksi_obj x)
{
    return KSI_FLONUM_P(x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_finite_p (ksi_obj x)
{
    if (KSI_FLONUM_P(x)) {
        if (KSI_IMPART(x) != 0.0) {
            ksi_exn_error(0, x, "finite?: invalid real number");
        } else {
            double d = KSI_REPART(x);
            if (d != 0.0 && d == d/2)
                return ksi_false;
        }
    } else if (!KSI_BIGNUM_P(x)) {
        ksi_exn_error(0, x, "finite?: invalid real number");
    }
    return ksi_true;
}

ksi_obj
ksi_infinite_p (ksi_obj x)
{
    if (KSI_FLONUM_P(x)) {
        if (KSI_IMPART(x) != 0.0) {
            ksi_exn_error(0, x, "infinite?: invalid real number");
        } else {
            double d = KSI_REPART(x);
            if (d != 0.0 && d == d/2)
                return ksi_true;
        }
    } else if (!KSI_BIGNUM_P(x)) {
        ksi_exn_error(0, x, "infinite?: invalid real number");
    }
    return ksi_false;
}

ksi_obj
ksi_nan_p (ksi_obj x)
{
    if (KSI_FLONUM_P(x)) {
        if (KSI_IMPART(x) != 0.0) {
            ksi_exn_error(0, x, "nan?: invalid real number");
        } else {
            double d = KSI_REPART(x);
            if (d != d)
                return ksi_true;
        }
    } else if (!KSI_BIGNUM_P(x)) {
        ksi_exn_error(0, x, "nan?: invalid real number");
    }
    return ksi_false;
}

ksi_obj
ksi_exact_integer_p (ksi_obj x)
{
    if (KSI_BIGNUM_P(x) && BIG_IS_INT(x))
        return ksi_true;
    return ksi_false;
}

SI_API
ksi_obj
ksi_unsigned_integer_p (ksi_obj x)
{
    if (KSI_BIGNUM_P(x) && BIG_IS_INT(x)) {
        int s = mpq_sgn(((ksi_bignum) x)->val);
        if (s >= 0)
            return ksi_true;
    }
    return ksi_false;
}

ksi_obj
ksi_zero_p (ksi_obj x)
{
    if (KSI_BIGNUM_P(x)) {
        int s = mpq_sgn(((ksi_bignum) x)->val);
        return s == 0 ? ksi_true : ksi_false;
    }

    if (KSI_FLONUM_P(x)) {
        if (KSI_REPART(x) == 0.0 && KSI_IMPART(x) == 0.0)
            return ksi_true;
        return ksi_false;
    }

    ksi_exn_error(0, x, "zero?: invalid number");
    return ksi_false;
}

ksi_obj
ksi_positive_p (ksi_obj x)
{
    if (KSI_BIGNUM_P(x)) {
        int s = mpq_sgn(((ksi_bignum) x)->val);
        return s > 0 ? ksi_true : ksi_false;
    }

    if (KSI_FLONUM_P(x)) {
        if (KSI_REPART(x) > 0.0 && KSI_IMPART(x) == 0.0)
            return ksi_true;
        return ksi_false;
    }

    ksi_exn_error(0, x, "positive?: invalid number");
    return ksi_false;
}

ksi_obj
ksi_negative_p (ksi_obj x)
{
    if (KSI_BIGNUM_P(x)) {
        int s = mpq_sgn(((ksi_bignum) x)->val);
        return s < 0 ? ksi_true : ksi_false;
    }

    if (KSI_FLONUM_P(x)) {
        if (KSI_REPART(x) < 0.0 && KSI_IMPART(x) == 0.0)
            return ksi_true;
        return ksi_false;
    }

    ksi_exn_error(0, x, "negative?: invalid number");
    return ksi_false;
}

ksi_obj
ksi_odd_p (ksi_obj x)
{
    if (KSI_BIGNUM_P(x)) {
        if (BIG_IS_INT(x)) {
            if (mpz_odd_p(mpq_numref(((ksi_bignum) x)->val)))
                return ksi_true;
            return ksi_false;
        }
    }

    if (KSI_FLONUM_P(x) && KSI_IMPART(x) == 0.0) {
        double d = KSI_REPART(x);
        if (FLO_IS_INT(d)) {
            if (fmod(d, 2.0) == 1.0)
                return ksi_true;
            return ksi_false;
        }
    }

    ksi_exn_error(0, x, "odd?: invalid integer");
    return ksi_false;
}

ksi_obj
ksi_even_p (ksi_obj x)
{
    if (KSI_BIGNUM_P (x)) {
        if (BIG_IS_INT(x)) {
            if (mpz_even_p(mpq_numref(((ksi_bignum) x)->val)))
                return ksi_true;
            return ksi_false;
        }
    }

    if (KSI_FLONUM_P(x) && KSI_IMPART(x) == 0.0) {
        double d = KSI_REPART(x);
        if (FLO_IS_INT(d)) {
            if (fmod(d, 2.0) == 0.0)
                return ksi_true;
            return ksi_false;
        }
    }

    ksi_exn_error(0, x, "even?: invalid integer");
    return ksi_false;
}


/*
 * ksi_num_eqv_p
 *	Сравнивает два числа.
 *
 * Return
 *	Возвращает ksi_true если числа равны, и ksi_false в противном
 *	случае.
 *
 * Side effects
 *	Если один из параметров не число, сигнализируется ошибка.
 */

ksi_obj
ksi_num_eqv_p (ksi_obj x1, ksi_obj x2)
{
    if (x1 == x2)
        return ksi_true;

    if (KSI_BIGNUM_P(x1)) {
        if (KSI_BIGNUM_P(x2)) {
            if (mpq_equal(((ksi_bignum) x1)->val, ((ksi_bignum) x2)->val))
                return ksi_true;
            return ksi_false;
        }

        if (KSI_FLONUM_P(x2)) {
            if (KSI_IMPART(x2) == 0.0 && mpq_get_d(((ksi_bignum) x1)->val) == KSI_REPART(x2))
                return ksi_true;
            return ksi_false;
        }

        ksi_exn_error(0, x2, "= : invalid number");
    }

    if (KSI_FLONUM_P(x1)) {
        if (KSI_BIGNUM_P(x2)) {
            if (KSI_IMPART(x1) == 0.0 && KSI_REPART(x1) == mpq_get_d(((ksi_bignum) x2)->val))
                return ksi_true;
            return ksi_false;
        }

        if (KSI_FLONUM_P(x2)) {
            if (KSI_REPART(x1) == KSI_REPART(x2) && KSI_IMPART(x1) == KSI_IMPART(x2))
                return ksi_true;
            return ksi_false;
        }

        ksi_exn_error(0, x2, "= : invalid number");
    }

    ksi_exn_error(0, x1, "= : invalid number");
    return ksi_false;
}


/*
 * ksi_num_eq_p
 *	Сравнивает список чисел.
 *
 * Scheme procedure
 *	= argN ...
 *
 * Return
 *	Возвращает ksi_true если числа равны, и ksi_false в противном
 *	случае.
 *
 * Side effects
 *	Если один из параметров не число, сигнализируется ошибка.
 *
 */

ksi_obj
ksi_num_eq_p (int argc, ksi_obj* argv)
{
    int i;
    for (i = 1; i < argc; i++) {
        if (ksi_num_eqv_p (argv[0], argv[i]) == ksi_false)
            return ksi_false;
    }
    return ksi_true;
}


/*
 * ksi_less_p
 *	Сравнивает два числа
 *
 * Return
 *	Возвращает 1 если X1 меньше X2, и 0 в противном случае.
 *
 * Side effects
 *	Если один из параметров не число, сигнализируется ошибка.
 *
 */

static int
ksi_less_p (ksi_obj x1, ksi_obj x2, char* fname)
{
    if (KSI_BIGNUM_P(x1)) {
        if (KSI_BIGNUM_P(x2)) {
            if (mpq_cmp(((ksi_bignum) x1)->val, ((ksi_bignum) x2)->val) < 0)
                return 1;
            return 0;
        }

        if (KSI_FLONUM_P(x2) && KSI_IMPART(x2) == 0.0)
            return (mpq_get_d(((ksi_bignum) x1)->val) < KSI_REPART(x2) ? 1 : 0);

        x1 = x2;
    } else if (KSI_FLONUM_P(x1) && KSI_IMPART(x1) == 0.0) {
        if (KSI_BIGNUM_P(x2))
            return (KSI_REPART(x1) < mpq_get_d(((ksi_bignum) x2)->val) ? 1 : 0);

        if (KSI_FLONUM_P(x2) && KSI_IMPART(x2) == 0.0)
            return (KSI_REPART(x1) < KSI_REPART(x2) ? 1 : 0);

        x1 = x2;
    }

    ksi_exn_error(0, x1, "%s: invalid real number", fname);
    return 0;
}


/*
 * ksi_num_lt_p, ksi_num_le_p, ksi_num_gt_p, ksi_num_ge_p
 *	Сравнивает список чисел.
 *
 * Scheme procedure
 *	< argN ...
 *	<= argN ...
 *	> argN ...
 *	>= argN ...
 */

#define DEF_CMP(fun, cmp)			\
ksi_obj						\
fun (int ac, ksi_obj* av)			\
{						\
  int i;					\
  for (i = 1; i < ac; i++)			\
    {						\
      if (cmp)					\
        return ksi_false;			\
    }						\
  return ksi_true;				\
}

DEF_CMP (ksi_num_lt_p, !ksi_less_p(av[i-1], av[i], "<"))
DEF_CMP (ksi_num_gt_p, !ksi_less_p(av[i], av[i-1], ">"))
DEF_CMP (ksi_num_le_p, ksi_less_p(av[i], av[i-1], "<="))
DEF_CMP (ksi_num_ge_p, ksi_less_p(av[i-1], av[i], ">="))


/*
 * ksi_min, ksi_max
 *	Находит минимум и максимум
 *
 * Scheme procedure
 *	min arg1 argN ...
 *	max arg1 argN ...
 *
 * Bugs
 */

ksi_obj
ksi_min (int argc, ksi_obj* argv)
{
    ksi_obj x;
    int inexact, i;

    x = argv[0];
    inexact = KSI_FLONUM_P(x);
    for (i = 1; i < argc; i++) {
        if (KSI_FLONUM_P(argv[i]))
            inexact = 1;

        if (ksi_less_p(argv[i], x, "min"))
            x = argv[i];
    }

    if (inexact && KSI_BIGNUM_P(x))
        return ksi_inexact(x);
    return x;
}

ksi_obj
ksi_max (int argc, ksi_obj* argv)
{
    ksi_obj x;
    int inexact, i;

    x = argv[0];
    inexact = KSI_FLONUM_P(x);
    for (i = 1; i < argc; i++) {
        if (KSI_FLONUM_P(argv[i]))
            inexact = 1;

        if (ksi_less_p (x, argv[i], "max"))
            x = argv[i];
    }

    if (inexact && KSI_BIGNUM_P (x))
        return ksi_inexact(x);
    return x;
}

ksi_obj
ksi_abs (ksi_obj x)
{
    if (KSI_BIGNUM_P(x)) {
        if (mpq_sgn(((ksi_bignum) x)->val) < 0) {
            ksi_bignum res = new_big();
            mpq_abs(res->val, ((ksi_bignum) x)->val);
            return (ksi_obj) res;
        }
        return x;
    }

    if (KSI_FLONUM_P(x)) {
        if (KSI_IMPART(x) == 0.0) {
            return (KSI_REPART(x) < 0 ? ksi_double2num(fabs(KSI_REPART(x))) : x);
        } else {
            return ksi_double2num(sqrt(KSI_REPART(x) * KSI_REPART(x) + KSI_IMPART(x) * KSI_IMPART(x)));
        }
    }

    ksi_exn_error(0, x, "abs: invalid number");
    return ksi_void;
}

ksi_obj
ksi_add (ksi_obj a, ksi_obj b)
{
    if (KSI_BIGNUM_P(a)) {
        if (KSI_BIGNUM_P(b)) {
            ksi_bignum res = new_big();
            mpq_add(res->val, ((ksi_bignum) a)->val, ((ksi_bignum) b)->val);
            return (ksi_obj) res;
        } else if (KSI_FLONUM_P(b))
            return ksi_rectangular(mpq_get_d(((ksi_bignum) a)->val) + KSI_REPART(b), KSI_IMPART(b));

        a = b;
    } else if (KSI_FLONUM_P(a)) {
        if (KSI_BIGNUM_P(b))
            return ksi_rectangular(KSI_REPART(a) + mpq_get_d(((ksi_bignum) b)->val), KSI_IMPART(a));
        else if (KSI_FLONUM_P(b))
            return ksi_rectangular(KSI_REPART(a) + KSI_REPART(b), KSI_IMPART(a) + KSI_IMPART(b));

        a = b;
    }

    ksi_exn_error(0, a, "+ : invalid number");
    return 0;
}


ksi_obj
ksi_sub (ksi_obj a, ksi_obj b)
{
    if (KSI_BIGNUM_P(a)) {
        if (KSI_BIGNUM_P(b)) {
            ksi_bignum res = new_big();
            mpq_sub(res->val, ((ksi_bignum) a)->val, ((ksi_bignum) b)->val);
            return (ksi_obj) res;
        } else if (KSI_FLONUM_P(b))
            return ksi_rectangular(mpq_get_d(((ksi_bignum) a)->val) - KSI_REPART(b), - KSI_IMPART(b));

        a = b;
    } else if (KSI_FLONUM_P(a)) {
        if (KSI_BIGNUM_P(b))
            return ksi_rectangular(KSI_REPART(a) - mpq_get_d(((ksi_bignum) b)->val), KSI_IMPART(a));
        else if (KSI_FLONUM_P(b))
            return ksi_rectangular(KSI_REPART(a) - KSI_REPART(b), KSI_IMPART(a) - KSI_IMPART(b));

        a = b;
    }

    ksi_exn_error(0, a, "- : invalid number");
    return 0;
}

ksi_obj
ksi_mul (ksi_obj a, ksi_obj b)
{
    if (KSI_BIGNUM_P(a)) {
        if (KSI_BIGNUM_P(b)) {
            ksi_bignum res = new_big();
            mpq_mul(res->val, ((ksi_bignum) a)->val, ((ksi_bignum) b)->val);
            return (ksi_obj) res;
        } else if (KSI_FLONUM_P(b)) {
            double d = mpq_get_d(((ksi_bignum) a)->val);
            return ksi_rectangular(d * KSI_REPART(b), d * KSI_IMPART(b));
        }

        a = b;
    } else if (KSI_FLONUM_P(a)) {
        if (KSI_BIGNUM_P(b)) {
            double d = mpq_get_d(((ksi_bignum) b)->val);
            return ksi_rectangular(d * KSI_REPART(a), d * KSI_IMPART(a));
        } else if (KSI_FLONUM_P(b)) {
            double d1 = KSI_REPART(a) * KSI_REPART(b);
            double d2 = KSI_REPART(a) * KSI_IMPART(b);
            double d3 = KSI_IMPART(a) * KSI_REPART(b);
            double d4 = KSI_IMPART(a) * KSI_IMPART(b);
            return ksi_rectangular(d1 - d4, d2 + d3);
        }

        a = b;
    }

    ksi_exn_error(0, a, "* : invalid number");
    return 0;
}


ksi_obj
ksi_div (ksi_obj a, ksi_obj b)
{
    double d, re, im;

    if (KSI_BIGNUM_P(a)) {
        if (KSI_BIGNUM_P(b)) {
            if (mpq_sgn(((ksi_bignum) b)->val) == 0) {
                re = mpq_sgn(((ksi_bignum) a)->val);
                return ksi_double2num(re / ksi_fp_zero);
            } else {
                ksi_bignum res = new_big();
                mpq_div(res->val, ((ksi_bignum) a)->val, ((ksi_bignum) b)->val);
                return (ksi_obj) res;
            }
        } else if (KSI_FLONUM_P(b)) {
            re = mpq_get_d(((ksi_bignum) a)->val);
            im = 0.0;
            goto complex_div;
        }

        ksi_exn_error(0, b, "/ : invalid number");
    }

    if (KSI_FLONUM_P(a)) {
        if (KSI_BIGNUM_P(b)) {
            d = mpq_get_d(((ksi_bignum) b)->val);
            return ksi_rectangular (KSI_REPART(a) / d, KSI_IMPART(a) / d);
        } else if (KSI_FLONUM_P(b)) {
            re = KSI_REPART(a);
            im = KSI_IMPART(a);

        complex_div:
            d = (KSI_REPART(b) * KSI_REPART(b) + KSI_IMPART(b) * KSI_IMPART(b));
            return ksi_rectangular ((re * KSI_REPART(b) + im * KSI_IMPART(b)) / d,
                                    (im * KSI_REPART(b) - re * KSI_IMPART(b)) / d);
        }

        ksi_exn_error(0, b, "/ : invalid number");
    }

    ksi_exn_error(0, a, "/ : invalid number");
    return 0;
}

ksi_obj
ksi_exact_div (ksi_obj a, ksi_obj b)
{
    if (KSI_BIGNUM_P(a) && BIG_IS_INT(a)) {
        if (KSI_BIGNUM_P(b) && BIG_IS_INT(b)) {
            if (mpq_sgn(((ksi_bignum) b)->val) == 0) {
                ksi_exn_error(0, b, "exact-div: divide by zero");
            } else {
                ksi_bignum res = new_big();
                mpz_divexact(mpq_numref(res->val), mpq_numref(((ksi_bignum) a)->val), mpq_numref(((ksi_bignum) b)->val));
                return (ksi_obj) res;
            }
        }
        ksi_exn_error(0, b, "exact-div: invalid integer number in arg2");
    }

    ksi_exn_error(0, a, "exact-div: invalid integer number in arg1");
    return 0;
}

static ksi_obj
ksi_idiv_helper (ksi_obj x, ksi_obj y, const wchar_t *name)
{
    double n, d;

    if (KSI_BIGNUM_P(x)) {
        if (KSI_BIGNUM_P(y)) {
            if (mpq_sgn(((ksi_bignum) y)->val) == 0) {
                ksi_exn_error(0, y, "%ls: divide by zero", name);
            } else if (mpq_sgn(((ksi_bignum) x)->val) == 0) {
                return x;
            } else {
                ksi_bignum div = new_big();
                if (mpq_sgn(((ksi_bignum) y)->val) > 0) {
                    mpq_div(div->val, ((ksi_bignum) x)->val, ((ksi_bignum) y)->val);
                    if (BIG_IS_INT(div)) {
                        return (ksi_obj) div;
                    } else {
                        ksi_bignum res = new_big();
                        mpz_fdiv_q(mpq_numref(res->val), mpq_numref(((ksi_bignum) div)->val), mpq_denref(((ksi_bignum) div)->val));
                        return (ksi_obj) res;
                    }
                } else {
                    mpq_neg(div->val, ((ksi_bignum) y)->val);
                    mpq_div(div->val, ((ksi_bignum) x)->val, div->val);
                    if (BIG_IS_INT(div)) {
                        mpq_neg(div->val, div->val);
                        return (ksi_obj) div;
                    } else {
                        ksi_bignum res = new_big();
                        mpz_fdiv_q(mpq_numref(res->val), mpq_numref(((ksi_bignum) div)->val), mpq_denref(((ksi_bignum) div)->val));
                        mpq_neg(res->val, res->val);
                        return (ksi_obj) res;
                    }
                }
            }
        }

        if (KSI_FLONUM_P(y) && KSI_IMPART(y) == 0.0) {
            n = mpq_get_d(((ksi_bignum) x)->val);
            d = KSI_REPART(y);
            goto real_div;
        }

        ksi_exn_error(0, y, "%ls: invalid real number in arg2", name);
    }

    if (KSI_FLONUM_P(x) && KSI_IMPART(x) == 0.0) {
        n = KSI_REPART(x);
        if (KSI_BIGNUM_P(y)) {
            if (mpq_sgn(((ksi_bignum) y)->val) == 0) {
                ksi_exn_error(0, y, "%ls: divide by zero", name);
            }
            d = mpq_get_d(((ksi_bignum) y)->val);
        } else if (KSI_FLONUM_P(y) && KSI_IMPART(y) == 0.0) {
            d = KSI_REPART(y);
        } else {
            ksi_exn_error(0, y, "%ls: invalid real number in arg2", name);
            d = 0.0;
        }
    real_div:
        if (d == 0.0) {
            ksi_exn_error(0, y, "%ls: divide by zero", name);
        }
        return ksi_double2num(d > 0.0 ? floor(n / d) : -floor(n / -d));
    }

    ksi_exn_error(0, x, "%ls: invalid real number in arg1", name);
    return 0;
}

ksi_obj
ksi_idiv (ksi_obj x, ksi_obj y)
{
    return ksi_idiv_helper(x, y, L"div");
}

ksi_obj
ksi_imod(ksi_obj x, ksi_obj y)
{
    ksi_obj div = ksi_idiv_helper(x, y, L"mod");
    return ksi_sub(x, ksi_mul(div, y));
}

ksi_obj
ksi_idiv_and_mod_who (ksi_obj x, ksi_obj y, ksi_obj who)
{
    ksi_obj vals[2];

    const wchar_t *name = L"div-and-mod*";
    if (KSI_SYM_P(who))
        name = KSI_SYM_PTR(who);
    else if (KSI_STR_P(who))
        name = KSI_STR_PTR(who);

    vals[0] = ksi_idiv_helper(x, y, name);
    vals[1] = ksi_sub(x, ksi_mul(vals[0], y));
    return ksi_new_values(2, vals);
}

ksi_obj
ksi_idiv_and_mod (ksi_obj x, ksi_obj y)
{
    ksi_obj vals[2];
    vals[0] = ksi_idiv_helper(x, y, L"div-and-mod");
    vals[1] = ksi_sub(x, ksi_mul(vals[0], y));
    return ksi_new_values(2, vals);
}

ksi_obj
ksi_plus (int argc, ksi_obj* argv)
{
    ksi_obj val;
    if (argc <= 0)
        return ksi_long2num(0);

    val = *argv++;
    while (--argc > 0)
        val = ksi_add(val, *argv++);
    return val;
}

ksi_obj
ksi_minus (int argc, ksi_obj* argv)
{
    ksi_obj val;

    val = *argv++;
    if (--argc == 0) {
        if (KSI_BIGNUM_P(val)) {
            ksi_bignum res = new_big();
            mpq_neg(res->val, ((ksi_bignum) val)->val);
            return (ksi_obj) res;
        } else if (KSI_FLONUM_P(val)) {
            return ksi_rectangular(- KSI_REPART(val), KSI_IMPART(val));
        }

        ksi_exn_error(0, val, "- : invalid number");
    }

    val = ksi_sub(val, *argv++);
    while (--argc > 0)
        val = ksi_sub(val, *argv++);
    return val;
}

ksi_obj
ksi_multiply (int argc, ksi_obj* argv)
{
    ksi_obj val;
    if (argc <= 0)
        return ksi_long2num(1);

    val = *argv++;
    while (--argc > 0)
        val = ksi_mul(val, *argv++);
    return val;
}

ksi_obj
ksi_divide (int argc, ksi_obj* argv)
{
    ksi_obj val;

    val = *argv++;
    if (--argc == 0)
        return ksi_div(ksi_long2num(1), val);

    val = ksi_div(val, *argv++);
    while (--argc > 0)
        val = ksi_div(val, *argv++);
    return val;
}


static ksi_obj
gcd2 (ksi_obj n1, ksi_obj n2)
{
    ksi_bignum t;

    if (!KSI_BIGNUM_P(n1) || !KSI_BIGNUM_P(n2)) {
        ksi_exn_error(0, n1, "gcd: invalid integer (internal error)");
    }

    t = new_big();
    mpz_gcd(mpq_numref(t->val), mpq_numref(((ksi_bignum) n1)->val), mpq_numref(((ksi_bignum) n2)->val));
    return (ksi_obj) t;
}

ksi_obj
ksi_gcd (int argc, ksi_obj* argv)
{
    ksi_obj tmp, res;
    int inexact=0;

    if (argc <= 0)
        return ksi_long2num(0);

    tmp = *argv++;
    if (KSI_BIGNUM_P(tmp) && BIG_IS_INT(tmp)) {
        inexact = 0;
    } else if (KSI_FLONUM_P(tmp) && KSI_IMPART(tmp) == 0.0 && FLO_IS_INT(KSI_REPART(tmp))) {
        tmp = ksi_double2exact(KSI_REPART(tmp), "gcd");
        inexact = 1;
    } else {
        ksi_exn_error(0, tmp, "gcd: invalid integer");
    }

    res = ksi_abs(tmp);
    while (--argc > 0) {
        tmp = *argv++;
        if (KSI_FLONUM_P(tmp) && KSI_IMPART(tmp) == 0.0 && FLO_IS_INT(KSI_REPART(tmp))) {
            tmp = ksi_double2exact(KSI_REPART(tmp), "gcd");
            inexact = 1;
        } else if (!KSI_BIGNUM_P(tmp) || !BIG_IS_INT(tmp)) {
            ksi_exn_error(0, tmp, "gcd: invalid integer");
        }
        res = ksi_abs(gcd2(res, tmp));
    }

    if (inexact)
        res = ksi_inexact(res);
    return res;
}

ksi_obj
ksi_lcm (int argc, ksi_obj* argv)
{
    ksi_obj tmp, res, gcd;
    int inexact=0;

    if (argc <= 0)
        return ksi_long2num(1);

    tmp = *argv++;
    if (KSI_BIGNUM_P(tmp) && BIG_IS_INT(tmp)) {
        inexact = 0;
    } else if (KSI_FLONUM_P(tmp) && KSI_IMPART(tmp) == 0.0 && FLO_IS_INT(KSI_REPART(tmp))) {
        tmp = ksi_exact(tmp);
        inexact = 1;
    } else {
        ksi_exn_error(0, tmp, "lcm: invalid integer");
    }

    res = ksi_abs(tmp);
    while (--argc > 0) {
        tmp = *argv++;
        if (KSI_FLONUM_P(tmp) && KSI_IMPART(tmp) == 0.0 && FLO_IS_INT(KSI_REPART(tmp))) {
            tmp = ksi_exact(tmp);
            inexact = 1;
        } else if (!KSI_BIGNUM_P(tmp) || !BIG_IS_INT(tmp)) {
            ksi_exn_error(0, tmp, "lcm: invalid integer");
        }

        if (ksi_zero_p(tmp) == ksi_true)
            return inexact ? ksi_double2num(0.0) : ksi_long2num(0);

        /* !!! order of assigments is important */
        gcd = gcd2(res, tmp);
        res = ksi_mul(res, tmp);
        res = ksi_div(res, gcd);
        res = ksi_abs(res);
    }

    if (inexact)
        res = ksi_inexact (res);
    return res;
}

ksi_obj
ksi_numerator(ksi_obj x)
{
    if (KSI_BIGNUM_P(x)) {
        ksi_bignum res = new_big();
        mpz_set(mpq_numref(res->val), mpq_numref(((ksi_bignum) x)->val));
        return (ksi_obj) res;
    }
    if (KSI_FLONUM_P(x) && KSI_IMPART(x) == 0.0) {
        x = ksi_double2exact(KSI_REPART(x), "numerator");
        if (KSI_BIGNUM_P(x)) {
            ksi_bignum res = new_big();
            mpz_set(mpq_numref(res->val), mpq_numref(((ksi_bignum) x)->val));
            return ksi_inexact((ksi_obj) res);
        }
    }

    ksi_exn_error(0, x, "numerator: invalid real number");
    return 0;
}

ksi_obj
ksi_denominator(ksi_obj x)
{
    if (KSI_BIGNUM_P(x)) {
        ksi_bignum res = new_big();
        mpz_set(mpq_numref(res->val), mpq_denref(((ksi_bignum) x)->val));
        return (ksi_obj) res;
    }
    if (KSI_FLONUM_P(x) && KSI_IMPART(x) == 0.0) {
        x = ksi_double2exact(KSI_REPART(x), "denominator");
        if (KSI_BIGNUM_P(x)) {
            ksi_bignum res = new_big();
            mpz_set(mpq_numref(res->val), mpq_denref(((ksi_bignum) x)->val));
            return ksi_inexact((ksi_obj) res);
        }
    }

    ksi_exn_error(0, x, "denominator: invalid real number");
    return 0;
}

ksi_obj
ksi_floor (ksi_obj x)
{
    if (KSI_FLONUM_P(x) && KSI_IMPART(x) == 0.0)
        return ksi_double2num(floor(KSI_REPART(x)));

    if (KSI_BIGNUM_P(x)) {
        if (BIG_IS_INT(x)) {
            return x;
        } else {
            ksi_bignum res = new_big();
            mpz_fdiv_q(mpq_numref(res->val), mpq_numref(((ksi_bignum) x)->val), mpq_denref(((ksi_bignum) x)->val));
            return (ksi_obj) res;
        }
    }

    ksi_exn_error(0, x, "floor: invalid real number");
    return x;
}

ksi_obj
ksi_ceiling (ksi_obj x)
{
    if (KSI_FLONUM_P(x) && KSI_IMPART(x) == 0.0)
        return ksi_double2num(ceil(KSI_REPART(x)));

    if (KSI_BIGNUM_P(x)) {
        if (BIG_IS_INT(x)) {
            return x;
        } else {
            ksi_bignum res = new_big();
            mpz_cdiv_q(mpq_numref(res->val), mpq_numref(((ksi_bignum) x)->val), mpq_denref(((ksi_bignum) x)->val));
            return (ksi_obj) res;
        }
    }

    ksi_exn_error(0, x, "ceiling: invalid real number");
    return x;
}

ksi_obj
ksi_truncate (ksi_obj x)
{
    if (KSI_FLONUM_P(x) && KSI_IMPART(x) == 0.0) {
        double d = KSI_REPART(x);
        return ksi_double2num(d < 0.0 ? ceil(d) : floor(d));
    } else if (KSI_BIGNUM_P(x)) {
        if (BIG_IS_INT(x)) {
            return x;
        } else {
            ksi_bignum res = new_big();
            mpz_tdiv_q(mpq_numref(res->val), mpq_numref(((ksi_bignum) x)->val), mpq_denref(((ksi_bignum) x)->val));
            return (ksi_obj) res;
        }
    }

    ksi_exn_error(0, x, "truncate: invalid real number");
    return x;
}

ksi_obj
ksi_round (ksi_obj x)
{
    if (KSI_FLONUM_P (x)) {
        double d = KSI_REPART(x);
        double v = d + 0.5;
        double r = floor(v);
        if (v == r && v / 2 != floor(v / 2))
            r = r - 1.0;
        return ksi_double2num(r);
    } else if (KSI_BIGNUM_P(x)) {
        if (BIG_IS_INT(x)) {
            return x;
        } else {
            ksi_obj half = ksi_div(ksi_long2num(1), ksi_long2num(2));
            ksi_obj v = ksi_add(x, half);
            if (KSI_BIGNUM_P(v)) {
                ksi_bignum res = new_big();
                mpz_t r;
                mpz_init(r);
                mpz_fdiv_qr(mpq_numref(res->val), r, mpq_numref(((ksi_bignum) v)->val), mpq_denref(((ksi_bignum) v)->val));
                if (mpz_sgn(r) == 0 && ksi_even_p((ksi_obj) res) == ksi_false)
                    return ksi_sub((ksi_obj) res, ksi_long2num(1));
                return (ksi_obj) res;
            }
            ksi_exn_error(0, x, "round: internal error");
        }
    }

    ksi_exn_error(0, x, "round: invalid real number");
    return x;
}


ksi_obj
ksi_exp (ksi_obj x)
{
    if (KSI_REAL_P(x))
        return ksi_double2num(exp(ksi_real_part(x)));
    if (KSI_FLONUM_P(x))
        return ksi_polar(exp(KSI_REPART(x)), KSI_IMPART(x));

    ksi_exn_error(0, x, "exp: invalid number");
    return 0;
}

ksi_obj
ksi_log (ksi_obj x, ksi_obj base)
{
    if (base != 0) {
        KSI_CHECK (base, KSI_NUM_P(base), "log: invalid number in arg2");
        return ksi_div(ksi_log(x, 0), ksi_log(base, 0));
    }

    if (KSI_REAL_P(x)) {
        double d = ksi_real_part(x);
        if (d >= 0.0)
            return ksi_double2num(log(d));
        return ksi_rectangular(log(fabs(d)), ksi_angle(x));
    }
    if (KSI_FLONUM_P(x))
        return ksi_rectangular(log(ksi_magnitude(x)), ksi_angle(x));

    ksi_exn_error(0, x, "log: invalid number in arg1");
    return 0;
}

ksi_obj
ksi_sin (ksi_obj x)
{
    if (KSI_REAL_P(x))
        return ksi_double2num(sin(ksi_real_part(x)));
    if (KSI_FLONUM_P(x))
        return ksi_rectangular(sin(KSI_REPART(x)) * cosh(KSI_IMPART(x)),
                               cos(KSI_REPART(x)) * sinh(KSI_IMPART(x)));

    ksi_exn_error(0, x, "sin: invalid number");
    return 0;
}

ksi_obj
ksi_cos (ksi_obj x)
{
    if (KSI_REAL_P(x))
        return ksi_double2num(cos(ksi_real_part(x)));
    if (KSI_FLONUM_P(x))
        return ksi_rectangular(cos(KSI_REPART(x)) * cosh(KSI_IMPART(x)),
                               - sin(KSI_REPART(x)) * sinh(KSI_IMPART(x)));

    ksi_exn_error(0, x, "cos: invalid number");
    return 0;
}

ksi_obj
ksi_tan (ksi_obj z)
{
    if (KSI_REAL_P(z))
        return ksi_double2num(tan(ksi_real_part(z)));
    if (KSI_FLONUM_P(z)) {
        double x = 2.0 * KSI_REPART(z);
        double y = 2.0 * KSI_IMPART(z);
        double w = cos(x) + cosh(y);
        return ksi_rectangular(sin(x) / w, sinh(y) / w);
    }

    ksi_exn_error(0, z, "tan: invalid number");
    return 0;
}

ksi_obj
ksi_sinh (ksi_obj x)
{
    if (KSI_REAL_P(x))
        return ksi_double2num(sinh(ksi_real_part(x)));
    if (KSI_FLONUM_P(x))
        return ksi_rectangular(sinh(KSI_REPART(x)) * cos(KSI_IMPART(x)),
                               cosh(KSI_REPART(x)) * sin(KSI_IMPART(x)));

    ksi_exn_error(0, x, "sinh: invalid number");
    return 0;
}

ksi_obj
ksi_cosh (ksi_obj x)
{
    if (KSI_REAL_P(x))
        return ksi_double2num(cosh(ksi_real_part(x)));
    if (KSI_FLONUM_P(x))
        return ksi_rectangular(cosh(KSI_REPART(x)) * cos(KSI_IMPART(x)),
                               sinh(KSI_REPART(x)) * sin(KSI_IMPART(x)));

    ksi_exn_error(0, x, "cosh: invalid number");
    return 0;
}

ksi_obj
ksi_tanh (ksi_obj z)
{
    if (KSI_REAL_P(z))
        return ksi_double2num(tan(ksi_real_part(z)));
    if (KSI_FLONUM_P(z)) {
        double x = 2.0 * KSI_REPART(z);
        double y = 2.0 * KSI_IMPART(z);
        double w = cosh(x) + cos(y);
        return ksi_rectangular(sinh(x) / w, sin(y) / w);
    }

    ksi_exn_error(0, z, "tan: invalid number");
    return 0;
}

ksi_obj
ksi_asinh (ksi_obj x)
{
    if (KSI_REAL_P(x)) {
        double d = ksi_real_part(x);
        return ksi_double2num(log(d + sqrt(d * d + 1)));
    }
    if (KSI_FLONUM_P(x))
        return ksi_log(ksi_add(x, ksi_sqrt(ksi_add(ksi_mul(x, x), ksi_long2num(1)))), 0);

    ksi_exn_error(0, x, "asinh: invalid number");
    return 0;
}

ksi_obj
ksi_acosh (ksi_obj x)
{
    if (KSI_REAL_P(x)) {
        double d = ksi_real_part(x);
        if (d >= 1.0)
            return ksi_double2num(log(d + sqrt(d * d - 1)));
        goto compl;
    }
    if (KSI_FLONUM_P(x)) {
    compl:
        return ksi_log(ksi_add(x, ksi_sqrt(ksi_sub(ksi_mul(x, x), ksi_long2num(1)))), 0);
    }

    ksi_exn_error(0, x, "asinh: invalid number");
    return 0;
}

ksi_obj
ksi_atanh (ksi_obj x)
{
    if (KSI_REAL_P(x)) {
        double d = ksi_real_part(x);
        if (d > -1.0 && d < 1.0)
            return ksi_double2num(0.5 * log((1 + d) / (1 - d)));
        goto compl;
    }
    if (KSI_FLONUM_P(x)) {
    compl:
        return ksi_div(ksi_log(ksi_div(ksi_add(ksi_long2num(1), x),ksi_sub(ksi_long2num(1), x)), 0), ksi_long2num(2));
    }

    ksi_exn_error(0, x, "atanh: invalid number");
    return 0;
}

ksi_obj
ksi_asin (ksi_obj x)
{
    ksi_obj i, z;

    if (KSI_REAL_P(x)) {
        double d = ksi_real_part(x);
        if (d >= -1.0 && d <= 1.0)
            return ksi_double2num(asin(d));
        goto compl;
    }
    if (KSI_FLONUM_P(x)) {
    compl:
        i = ksi_rectangular(0.0, 1.0);
        z = ksi_asinh(ksi_mul(i, x));
        KSI_IMPART(i) = -1.0;
        return ksi_mul(i, z);
    }

    ksi_exn_error(0, x, "asin: invalid number");
    return 0;
}

ksi_obj
ksi_acos (ksi_obj x)
{
    ksi_obj i, z;

    if (KSI_REAL_P(x)) {
        double d = ksi_real_part(x);
        if (d >= -1.0 && d <= 1.0)
            return ksi_double2num(acos(d));
        goto compl;
    }
    if (KSI_FLONUM_P(x)) {
    compl:
        i = ksi_rectangular(0.0, 1.0);
        z = ksi_mul(i, ksi_asinh(ksi_mul(i, x)));
        KSI_IMPART(i) = -1.0;
        return ksi_add(ksi_div(ksi_double2num(ksi_angle(i)), ksi_long2num(2)), z);
    }

    ksi_exn_error(0, x, "asin: invalid number");
    return 0;
}

ksi_obj
ksi_atan (ksi_obj z, ksi_obj y)
{
    if (y) {
        KSI_CHECK (z, KSI_REAL_P(z), "atan: invalid real number in arg1");
        KSI_CHECK (y, KSI_REAL_P(y), "atan: invalid real number in arg2");
        return ksi_double2num(atan2(ksi_real_part(z), ksi_real_part(y)));
    }

    if (KSI_REAL_P(z))
        return ksi_double2num(atan(ksi_real_part(z)));

    if (KSI_FLONUM_P(z)) {
        ksi_obj i = ksi_rectangular(0.0, 1.0);
        ksi_obj r = ksi_log(ksi_div(ksi_sub(i, z), ksi_add(i, z)), 0);
        KSI_IMPART(i) = 2.0;
        return ksi_div(r, i);
    }

    ksi_exn_error(0, z, "atan: invalid number");
    return 0;
}

ksi_obj
ksi_sqrt (ksi_obj x)
{
    double d;

    if (KSI_BIGNUM_P(x)) {
        if (mpq_sgn(((ksi_bignum) x)->val) < 0 || !BIG_IS_INT(x)) {
            d = mpq_get_d(((ksi_bignum) x)->val);
            goto real_sqrt;
        } else {
            mpz_t root, rem;
            mpz_init(root);
            mpz_init(rem);
            mpz_sqrtrem(root, rem, mpq_numref(((ksi_bignum) x)->val));
            if (mpz_sgn(rem) == 0) {
                ksi_bignum res = new_big();
                mpz_set(mpq_numref(res->val), root);
                return (ksi_obj) res;
            }
            d = mpz_get_d(mpq_numref(((ksi_bignum) x)->val));
            return ksi_double2num(sqrt(d));
        }
    } else if (KSI_FLONUM_P(x)) {
        if (KSI_IMPART(x) == 0.0) {
            d = KSI_REPART(x);
        real_sqrt:
            if (d < 0.0)
                return ksi_rectangular(0.0, sqrt(-d));
            return ksi_double2num(sqrt(d));
        }
        return ksi_polar(sqrt(ksi_magnitude(x)), ksi_angle(x) / 2.0);
    }

    ksi_exn_error(0, x, "sqrt: invalid number in arg1");
    return 0;
}


ksi_obj
ksi_exact_sqrt (ksi_obj x)
{
    ksi_bignum vals[2];

    if (KSI_BIGNUM_P(x)) {
        if (mpq_sgn(((ksi_bignum) x)->val) >= 0 && BIG_IS_INT(x)) {
            mpz_t root, rem;
            mpz_init(root);
            mpz_init(rem);
            mpz_sqrtrem(root, rem, mpq_numref(((ksi_bignum) x)->val));

            vals[0] = new_big();
            mpz_set(mpq_numref(vals[0]->val), root);
            vals[1] = new_big();
            mpz_set(mpq_numref(vals[1]->val), rem);
            return ksi_new_values(2, (ksi_obj *) vals);
        }
    }

    ksi_exn_error(0, x, "exact-integer-sqrt: invalid non-negative exact integer in arg1");
    return 0;
}


static void
exact_expt (mpq_t res, mpq_t z1, mpz_t z2)
{
    mpq_t q;
    mpz_t x;

//    gmp_printf("%Qd ^ %Zd\n", z1, z2);

    if (mpz_sgn(z2) == 0) {
        mpq_set_ui(res, 1, 1);
        return;
    }
    if (mpz_cmp_ui(z2, 1) == 0) {
        mpq_set(res, z1);
        return;
    }
    if (mpz_sgn(z2) < 0) {
        mpq_init(q);
        mpz_init(x);
        mpz_neg(x, z2);
        exact_expt(q, z1, x);
        mpq_inv(res, q);
        return;
    }

    if (mpz_even_p(z2)) {
        /* res = (z1^(z2/2))^2 */
        mpq_init(q);
        mpz_init(x);
        mpz_divexact_ui(x, z2, 2);
        exact_expt(q, z1, x);
        mpq_mul(res, q, q);
    } else {
        /* (z1^(z2-1)) * z1 */
        mpq_init(q);
        mpz_init(x);
        mpz_sub_ui(x, z2, 1);
        exact_expt(q, z1, x);
        mpq_mul(res, q, z1);
    }
}

ksi_obj
ksi_expt (ksi_obj z1, ksi_obj z2)
{
    if (KSI_BIGNUM_P(z1)) {
        if (mpq_sgn(((ksi_bignum) z1)->val) == 0) {
            KSI_CHECK (z2, KSI_NUM_P(z2), "expt: invalid number in arg2");
            if (KSI_TRUE_P(ksi_zero_p(z2)))
                return ksi_long2num(1);
            if (ksi_real_part(z2) > 0.0)
                return ksi_long2num(0);
            goto inexact_expt;
        }

        if (KSI_BIGNUM_P(z2)) {
            if (BIG_IS_INT(z2)) {
                ksi_bignum res = new_big();
                exact_expt(res->val, ((ksi_bignum) z1)->val, mpq_numref(((ksi_bignum) z2)->val));
                return (ksi_obj) res;
            }
            if (mpq_sgn(((ksi_bignum) z2)->val) > 0 && mpz_fits_ulong_p(mpq_denref(((ksi_bignum) z2)->val))) {
                /* z2 > 0 && z2 == M/N, where M > 0 and N is ulong, and M AND N integrs */
                if (BIG_IS_INT(z1)) {
                    mpq_t pow;
                    mpz_t root, rem;
                    mpq_init(pow);
                    mpz_init(root);
                    mpz_init(rem);

                    exact_expt(pow, ((ksi_bignum) z1)->val, mpq_numref(((ksi_bignum) z2)->val));
                    mpz_rootrem(root, rem, mpq_numref(pow), mpz_get_ui(mpq_denref(((ksi_bignum) z2)->val)));
                    if (mpz_sgn(rem) == 0) {
                        ksi_bignum res = new_big();
                        mpz_set(mpq_numref(res->val), root);
                        return (ksi_obj) res;
                    }
                }
            }
            goto inexact_expt;
        }

        if (KSI_FLONUM_P(z2))
            goto inexact_expt;

        ksi_exn_error(0, z2, "expt: invalid number in arg2");
    }

    if (KSI_FLONUM_P(z1)) {
        if (KSI_REPART(z1) == 0.0 && KSI_IMPART(z1) == 0.0) {
            if (KSI_BIGNUM_P(z2)) {
                int s = mpq_sgn(((ksi_bignum) z2)->val);
                if (s == 0)
                    return ksi_double2num(1.0);
                if (s > 0)
                    return ksi_double2num(0.0);
                goto inexact_expt;
            }
            if (KSI_FLONUM_P(z2)) {
                if (KSI_REPART(z2) == 0.0 && KSI_IMPART(z2) == 0.0)
                    return ksi_double2num(1.0);
                if (KSI_REPART(z2) > 0.0)
                    return ksi_double2num(0.0);
            }

            ksi_exn_error(0, z2, "expt: invalid number in arg2");
        }

        KSI_CHECK (z2, KSI_NUM_P(z2), "expt: invalid number in arg2");
    inexact_expt:
        return ksi_exp(ksi_mul(ksi_log(z1, 0), z2));
    }

    ksi_exn_error(0, z1, "expt: invalid number in arg1");
    return 0;
}


ksi_obj
ksi_lognot (ksi_obj x)
{
  ksi_bignum r;
  KSI_CHECK (x, KSI_BIGNUM_P(x) && BIG_IS_INT(x), "lognot: invalid integer in arg1");
  r = new_big();
  mpz_com(mpq_numref(r->val), mpq_numref(((ksi_bignum) x)->val));
  return (ksi_obj) r;
}

ksi_obj
ksi_logior (ksi_obj x1, ksi_obj x2)
{
  ksi_bignum r;

  KSI_CHECK (x1, KSI_BIGNUM_P(x1) && BIG_IS_INT(x1), "logior: invalid integer in arg1");
  KSI_CHECK (x2, KSI_BIGNUM_P(x2) && BIG_IS_INT(x2), "logior: invalid integer in arg2");

  r = new_big();
  mpz_ior(mpq_numref(r->val), mpq_numref(((ksi_bignum) x1)->val), mpq_numref(((ksi_bignum) x2)->val));
  return (ksi_obj) r;
}

ksi_obj
ksi_logand (ksi_obj x1, ksi_obj x2)
{
  ksi_bignum r;

  KSI_CHECK (x1, KSI_BIGNUM_P(x1) && BIG_IS_INT(x1), "logand: invalid integer in arg1");
  KSI_CHECK (x2, KSI_BIGNUM_P(x2) && BIG_IS_INT(x2), "logand: invalid integer in arg2");

  r = new_big();
  mpz_and(mpq_numref(r->val), mpq_numref(((ksi_bignum) x1)->val), mpq_numref(((ksi_bignum) x2)->val));
  return (ksi_obj) r;
}

ksi_obj
ksi_logxor (ksi_obj x1, ksi_obj x2)
{
  ksi_bignum r;

  KSI_CHECK (x1, KSI_BIGNUM_P(x1) && BIG_IS_INT(x1), "logxor: invalid integer in arg1");
  KSI_CHECK (x2, KSI_BIGNUM_P(x2) && BIG_IS_INT(x2), "logxor: invalid integer in arg2");

  r = new_big();
  mpz_xor(mpq_numref(r->val), mpq_numref(((ksi_bignum) x1)->val), mpq_numref(((ksi_bignum) x2)->val));
  return (ksi_obj) r;
}


ksi_obj
ksi_logtest (ksi_obj x1, ksi_obj x2)
{
  mpz_t tmp;

  KSI_CHECK (x1, KSI_BIGNUM_P(x1) && BIG_IS_INT(x1), "logtest: invalid integer in arg1");
  KSI_CHECK (x2, KSI_BIGNUM_P(x2) && BIG_IS_INT(x2), "logtest: invalid integer in arg2");

  mpz_init(tmp);
  mpz_and(tmp, mpq_numref(((ksi_bignum) x1)->val), mpq_numref(((ksi_bignum) x2)->val));
  return (mpz_sgn(tmp) == 0 ? ksi_false : ksi_true);
}

ksi_obj
ksi_logbit_p (ksi_obj ind, ksi_obj x)
{
  unsigned int i;
  mpz_t t1, t2;

  KSI_CHECK (ind, KSI_UINT_P (ind), "logbit?: invalid bit index in arg1");
  KSI_CHECK (x, KSI_BIGNUM_P(x) && BIG_IS_INT(x), "logtest: invalid integer in arg2");

  i = ksi_num2uint(ind, "logbit?");
  mpz_init(t1);
  mpz_set_ui(t1, 1);
  mpz_init(t2);
  mpz_mul_2exp(t2, t1, i);
  mpz_and(t1, t2, mpq_numref(((ksi_bignum) x)->val));
  return (mpz_sgn(t1) == 0 ? ksi_false : ksi_true);
}

ksi_obj
ksi_ash (ksi_obj x, ksi_obj cnt)
{
  ksi_bignum r;
  int i;

  KSI_CHECK (x, KSI_EINT_P(x), "ash: invalid integer in agr1");
  KSI_CHECK (cnt, ksi_int_p(cnt), "ash: invalid count in arg2");

  r = new_big();
  i = ksi_num2int(cnt, "ash");
  if (i < 0) {
    mpz_div_2exp(mpq_numref(r->val), mpq_numref(((ksi_bignum) x)->val), -i);
  } else {
    mpz_mul_2exp(mpq_numref(r->val), mpq_numref(((ksi_bignum) x)->val), i);
  }

  return (ksi_obj) r;
}


 /* End of code */
