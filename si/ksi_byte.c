/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    ksi_byte.c
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Jul 30 13:59:20 2010
 *
 * @brief   bytevectors
 *
 */

#include "ksi_type.h"
#include "ksi_gc.h"


ksi_obj
ksi_native_endianness (void)
{
    union { long d; char b[sizeof(long)]; } u;
    u.d = 1;
    return (u.b[0] == 1 ? ksi_data->sym_little : ksi_data->sym_big);
}

ksi_obj
ksi_str2bytevector (const char *bytes, int len)
{
    ksi_bytevector vec = ksi_malloc(sizeof(*vec));
    vec->o.itag = KSI_TAG_BYTEVECTOR;
    vec->o.ilen = len;
    vec->ptr = ksi_malloc_data(len);
    if (bytes)
        memcpy(vec->ptr, bytes, len);
    return (ksi_obj) vec;
}

ksi_obj
ksi_bytevector_p (ksi_obj x)
{
    return KSI_BVEC_P (x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_make_bytevector (ksi_obj k, ksi_obj fill)
{
    ksi_obj vec;
    unsigned n;
    int val;

    KSI_CHECK(k, KSI_UINT_P(k), "make-bytevector: invalid integer in arg1");
    n = ksi_num2ulong(k, "make-bytevector");

    if (fill) {
        KSI_CHECK(fill, KSI_EINT_P(fill), "make-bytevector: invalid integer in arg2");
        val = ksi_num2long(fill, "make-bytevector");
        KSI_CHECK(fill, -128 <= val && val <= 255, "make-bytevector: byte value out of range in arg2");
    } else {
        val = 0;
    }

    vec = ksi_str2bytevector(0, n);
    memset(KSI_BVEC_PTR(vec), val, n);
    return vec;
}

ksi_obj
ksi_bytevector_length (ksi_obj v)
{
    KSI_CHECK(v, KSI_BVEC_P(v), "bytevector-length: invalid bytevector in arg1");
    return ksi_long2num(KSI_BVEC_LEN(v));
}

ksi_obj
ksi_bytevector_eqv_p (ksi_obj x1, ksi_obj x2)
{
    int l1, l2;
    char *p1, *p2;

    KSI_CHECK(x1, KSI_BVEC_P(x1), "bytevector=?: invalid bytevector in arg1");
    KSI_CHECK(x2, KSI_BVEC_P(x2), "bytevector=?: invalid bytevector in arg2");

    l1 = KSI_BVEC_LEN(x1);
    l2 = KSI_BVEC_LEN(x2);
    p1 = KSI_BVEC_PTR(x1);
    p2 = KSI_BVEC_PTR(x2);

    if (l1 == l2) {
        if (p1 != p2)
            if (memcmp(p1, p2, l1) != 0)
                return ksi_false;

        return ksi_true;
    }

    return ksi_false;
}

ksi_obj
ksi_bytevector_fill_x (ksi_obj v, ksi_obj fill)
{
    int val;

    KSI_CHECK(v, KSI_M_BVEC_P(v), "bytevector-fill!: invalid or constant bytevector in arg1");
    KSI_CHECK(fill, KSI_EINT_P(fill), "bytevector-fill!: invalid integer in arg2");
    val = ksi_num2long(fill, "bytevector-fill!");
    KSI_CHECK(fill, -128 <= val && val <= 255, "bytevector-fill!: byte value out of range in arg2");

    memset(KSI_BVEC_PTR(v), val, KSI_BVEC_LEN(v));
    return ksi_void;
}

ksi_obj
ksi_bytevector_copy_x (ksi_obj src, ksi_obj src_start, ksi_obj dst, ksi_obj dst_start, ksi_obj num)
{
    unsigned int s_src, s_dst, n;

    KSI_CHECK(src, KSI_BVEC_P(src), "bytevector-copy!: invalid bytevector in arg1");
    KSI_CHECK(src_start, KSI_UINT_P(src_start), "bytevector-copy: invalid integer in arg2");
    s_src = ksi_num2uint(src_start, "bytevector-copy");

    KSI_CHECK(dst, KSI_M_BVEC_P(dst), "bytevector-copy!: invalid bytevector in arg3");
    KSI_CHECK(dst_start, KSI_UINT_P(dst_start), "bytevector-copy: invalid integer in arg4");
    s_dst = ksi_num2uint(dst_start, "bytevector-copy!");

    KSI_CHECK(num, KSI_UINT_P(num), "bytevector-copy!: invalid integer in arg5");
    n = ksi_num2uint(num, "bytevector-copy!");

    KSI_CHECK(src_start, s_src+n <= KSI_VEC_LEN(src), "bytevector-copy!: index out of range in arg2");
    KSI_CHECK(dst_start, s_dst+n <= KSI_VEC_LEN(dst), "bytevector-copy!: index out of range in arg4");

    memmove(KSI_BVEC_PTR(dst) + s_dst, KSI_BVEC_PTR(src) + s_src, n);
    return ksi_void;
}

ksi_obj
ksi_bytevector_copy (ksi_obj v)
{
    KSI_CHECK(v, KSI_BVEC_P(v), "bytevector-copy: invalid bytevector in arg1");
    return ksi_str2bytevector(KSI_BVEC_PTR(v), KSI_BVEC_LEN(v));
}

ksi_obj
ksi_bytevector_u8_ref (ksi_obj v, ksi_obj k)
{
    unsigned int idx = 0;

    if (KSI_UINT_P(k))
        idx = ksi_num2uint(k, "bytevector-u8-ref");
    else
        ksi_exn_error(0, k, "bytevector-u8-ref: invalid index in arg2");

    KSI_CHECK(v, KSI_BVEC_P(v), "bytevector-u8-ref: invalid bytevector in arg1");
    KSI_CHECK(k, idx < KSI_BVEC_LEN(v), "bytevector-u8-ref: index out of range in arg2");

    return ksi_uint2num(((unsigned char *)KSI_BVEC_PTR(v))[idx]);
}

ksi_obj
ksi_bytevector_s8_ref (ksi_obj v, ksi_obj k)
{
    unsigned int idx = 0;

    if (KSI_UINT_P(k))
        idx = ksi_num2uint(k, "bytevector-s8-ref");
    else
        ksi_exn_error(0, k, "bytevector-s8-ref: invalid index in arg2");

    KSI_CHECK(v, KSI_BVEC_P(v), "bytevector-s8-ref: invalid bytevector in arg1");
    KSI_CHECK(k, idx < KSI_BVEC_LEN(v), "bytevector-s8-ref: index out of range in arg2");

    return ksi_int2num(((signed char *) KSI_BVEC_PTR(v))[idx]);
}

ksi_obj
ksi_bytevector_u8_set_x (ksi_obj vec, ksi_obj k, ksi_obj val)
{
    unsigned int idx = 0, n;

    if (KSI_UINT_P(k))
        idx = ksi_num2uint(k, "bytevector-u8-set!");
    else
        ksi_exn_error(0, k, "bytevector-u8-set!: invalid index in arg2");

    KSI_CHECK(vec, KSI_M_BVEC_P(vec), "bytevector-u8-set!: invalid or constant bytevector in arg1");
    KSI_CHECK(k, idx < KSI_VEC_LEN(vec), "bytevector-u8-set!: index out of range in arg2");
    KSI_CHECK(val, KSI_EINT_P(val), "bytevector-u8-set!: invalid exact integer in arg3");

    n = ksi_num2uint(val, "bytevector-u8-set!");
    KSI_CHECK(val, n <= 255, "bytevector-u8-set!: value out of range in arg3");

    KSI_BVEC_PTR(vec)[idx] = (unsigned char) n;
    return ksi_void;
}

ksi_obj
ksi_bytevector_s8_set_x (ksi_obj vec, ksi_obj k, ksi_obj val)
{
    unsigned int idx = 0;
    int n;

    if (KSI_UINT_P(k))
        idx = ksi_num2uint(k, "bytevector-s8-set!");
    else
        ksi_exn_error(0, k, "bytevector-s8-set!: invalid index in arg2");

    KSI_CHECK(vec, KSI_M_BVEC_P(vec), "bytevector-s8-set!: invalid or constant bytevector in arg1");
    KSI_CHECK(k, idx < KSI_VEC_LEN(vec), "bytevector-s8-set!: index out of range in arg2");
    KSI_CHECK(val, KSI_EINT_P(val), "bytevector-s8-set!: invalid exact integer in arg3");

    n = ksi_num2int(val, "bytevector-s8-set!");
    KSI_CHECK(val, -128 <= n && n <= 127, "bytevector-s8-set!: value out of range in arg3");

    KSI_BVEC_PTR(vec)[idx] = (signed char) n;
    return ksi_void;
}

ksi_obj
ksi_bytevector_float_ref (ksi_obj v, ksi_obj k, ksi_obj endian)
{
    unsigned int idx = 0;
    int i;
    union { float d; char b[sizeof(float)]; } u;

    if (KSI_UINT_P(k))
        idx = ksi_num2uint(k, "bytevector-float-ref");
    else
        ksi_exn_error(0, k, "bytevector-float-ref: invalid index in arg2");

    KSI_CHECK(v, KSI_BVEC_P(v), "bytevector-float-ref: invalid bytevector in arg1");
    KSI_CHECK(k, idx <= KSI_BVEC_LEN(v) - sizeof(float), "bytevector-float-ref: index out of range in arg2");

    if (!endian || endian == ksi_data->sym_native)
        endian = ksi_native_endianness();

    if (endian == ksi_data->sym_little) {
        for (i = 0; i < sizeof(float); i++)
            u.b[i] = KSI_BVEC_PTR(v)[idx++];
    } else if (endian == ksi_data->sym_big) {
        for (i = 0; i < sizeof(float); i++)
            u.b[sizeof(float)-1-i] = KSI_BVEC_PTR(v)[idx++];
    } else {
        ksi_exn_error(0, endian, "bytevector-float-ref: invalid endianness in arg3");
    }

    return ksi_double2num(u.d);
}

ksi_obj
ksi_bytevector_float_set_x (ksi_obj v, ksi_obj k, ksi_obj x, ksi_obj endian)
{
    unsigned int idx = 0;
    int i;
    union { float d; char b[sizeof(float)]; } u;

    if (KSI_UINT_P(k))
        idx = ksi_num2uint(k, "bytevector-float-set!");
    else
        ksi_exn_error(0, k, "bytevector-float-set!: invalid index in arg2");

    KSI_CHECK(v, KSI_BVEC_P(v), "bytevector-float-set!: invalid bytevector in arg1");
    KSI_CHECK(k, 0 <= idx && idx <= KSI_BVEC_LEN(v) - sizeof(float), "bytevector-float-set!: index out of range in arg2");

    KSI_CHECK(x, KSI_REAL_P(x), "bytevector-float-set!: invalid real number in arg3");
    u.d = (float) ksi_real_part(x);

    if (!endian || endian == ksi_data->sym_native)
        endian = ksi_native_endianness();

    if (endian == ksi_data->sym_little) {
        for (i = 0; i < sizeof(float); i++)
            KSI_BVEC_PTR(v)[idx++] = u.b[i];
    } else if (endian == ksi_data->sym_big) {
        for (i = 0; i < sizeof(float); i++)
            KSI_BVEC_PTR(v)[idx++] = u.b[sizeof(float)-1-i];
    } else {
        ksi_exn_error(0, endian, "bytevector-float-set!: invalid endianness in arg4");
    }

    return ksi_void;
}

ksi_obj
ksi_bytevector_double_ref (ksi_obj v, ksi_obj k, ksi_obj endian)
{
    unsigned int idx = 0;
    int i;
    union { double d; char b[sizeof(double)]; } u;

    if (KSI_UINT_P(k))
        idx = ksi_num2uint(k, "bytevector-double-ref");
    else
        ksi_exn_error(0, k, "bytevector-double-ref: invalid index in arg2");

    KSI_CHECK(v, KSI_BVEC_P(v), "bytevector-double-ref: invalid bytevector in arg1");
    KSI_CHECK(k, idx <= KSI_BVEC_LEN(v) - sizeof(double), "bytevector-double-ref: index out of range in arg2");

    if (!endian || endian == ksi_data->sym_native)
        endian = ksi_native_endianness();

    if (endian == ksi_data->sym_little) {
        for (i = 0; i < sizeof(double); i++)
            u.b[i] = KSI_BVEC_PTR(v)[idx++];
    } else if (endian == ksi_data->sym_big) {
        for (i = 0; i < sizeof(double); i++)
            u.b[sizeof(double)-1-i] = KSI_BVEC_PTR(v)[idx++];
    } else {
        ksi_exn_error(0, endian, "bytevector-double-ref: invalid endianness in arg3");
    }

    return ksi_double2num(u.d);
}

ksi_obj
ksi_bytevector_double_set_x (ksi_obj v, ksi_obj k, ksi_obj x, ksi_obj endian)
{
    unsigned int idx = 0;
    int i;
    union { double d; char b[sizeof(double)]; } u;

    if (KSI_UINT_P(k))
        idx = ksi_num2uint(k, "bytevector-double-set!");
    else
        ksi_exn_error(0, k, "bytevector-double-set!: invalid index in arg2");

    KSI_CHECK(v, KSI_BVEC_P(v), "bytevector-double-set!: invalid bytevector in arg1");
    KSI_CHECK(k, idx <= KSI_BVEC_LEN(v) - sizeof(double), "bytevector-double-set!: index out of range in arg2");

    KSI_CHECK(x, KSI_REAL_P(x), "bytevector-double-set!: invalid real number in arg3");
    u.d = ksi_real_part(x);

    if (!endian || endian == ksi_data->sym_native)
        endian = ksi_native_endianness();

    if (endian == ksi_data->sym_little) {
        for (i = 0; i < sizeof(double); i++)
            KSI_BVEC_PTR(v)[idx++] = u.b[i];
    } else if (endian == ksi_data->sym_big) {
        for (i = 0; i < sizeof(double); i++)
            KSI_BVEC_PTR(v)[idx++] = u.b[sizeof(double)-1-i];
    } else {
        ksi_exn_error(0, endian, "bytevector-double-set!: invalid endianness in arg4");
    }

    return ksi_void;
}


 /* End of code */
