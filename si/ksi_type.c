/*
 * ksi_type.c
 *
 * Copyright (C) 2009-2017, ivan demakov.
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Fri Feb 13 23:37:53 2009
 * Last Update:   Mon May 19 22:02:41 2014
 *
 */

#include "ksi_type.h"
#include "ksi_env.h"
#include "ksi_comp.h"
#include "ksi_proc.h"
#include "ksi_port.h"
#include "ksi_hash.h"
#include "ksi_jump.h"
#include "ksi_gc.h"
#include "ksi_util.h"
#include "ksi_printf.h"
#include "ksi_klos.h"

#include <time.h>

#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif

#ifdef HAVE_FCNTL_H
#  include <fcntl.h>
#endif

#include <locale.h>

#ifdef _MSC_VER
#  include <io.h>
#endif

#if defined(_MSC_VER)
#pragma warning(disable : 4333)
#endif


ksi_context_t ksi_current_context = 0;


int
ksi_default_tag_equal (struct Ksi_EObj *x1, struct Ksi_EObj *x2, int deep)
{
    return 0;
}

const wchar_t *
ksi_default_tag_print (struct Ksi_EObj *x, int slashify)
{
    if (x->o.itag == KSI_TAG_BROKEN)
        return ksi_aprintf ("#<broken %ls %p>", x->etag->type_name, x);
    else
        return ksi_aprintf ("#<%ls %p>", x->etag->type_name, x);
}


ksi_obj
ksi_new_imm (enum ksi_tag_t tag)
{
    ksi_obj x = ksi_malloc(sizeof *x);
    x->itag = tag;
    return x;
}

ksi_obj
ksi_new_core (enum ksi_tag_t tag)
{
    ksi_core x = ksi_malloc (sizeof *x);
    x->o.itag = KSI_TAG_CORE;
    x->o.ilen = tag;
    return (ksi_obj) x;
}

ksi_obj
ksi_new_values (int num, ksi_obj* val)
{
    if (num == 1) {
        return val[0];
    } else {
        ksi_values x = ksi_malloc (sizeof(*x));
        x->o.itag = KSI_TAG_VALUES;
        x->vals = ksi_new_list (num, val);

        return (ksi_obj) x;
    }
}

ksi_obj
ksi_void_p (ksi_obj x)
{
    return x == ksi_void ? ksi_true : ksi_false;
}

ksi_obj
ksi_bool_p (ksi_obj x)
{
    return (x == ksi_true || x == ksi_false) ? ksi_true : ksi_false;
}

ksi_obj
ksi_bool_eq_p (int ac, ksi_obj *av)
{
    int i;
    ksi_obj c1, c2;

    if (ac >= 1) {
        c1 = av[0];
        KSI_CHECK(c1, c1 == ksi_true || c1 == ksi_false, "boolean=?: invalid boolean");
        for (i = 1; i < ac; i++) {
            c2 = av[i];
            KSI_CHECK(c2, c2 == ksi_true || c2 == ksi_false, "boolean=?: invalid boolean");
            if (c1 != c2)
                return ksi_false;
            c1 = c2;
        }
    }
    return ksi_true;
}

ksi_obj
ksi_int2bool (int x)
{
    return x ? ksi_true : ksi_false;
}

int
ksi_bool2int (ksi_obj x)
{
    return x == ksi_false ? 0 : 1;
}

ksi_obj
ksi_not (ksi_obj x)
{
    return x == ksi_false ? ksi_true : ksi_false;
}


static inline int
eff_tag (ksi_obj x)
{
  int tag = x->itag;
  if (tag == KSI_TAG_CONST_STRING)
    return KSI_TAG_STRING;
  if (tag == KSI_TAG_CONST_VECTOR)
    return KSI_TAG_VECTOR;
  if (tag == KSI_TAG_CONST_PAIR)
    return KSI_TAG_PAIR;
  if (tag == KSI_TAG_LOCAL || tag == KSI_TAG_IMPORTED)
    return KSI_TAG_FREEVAR;

  return tag;
}

ksi_obj
ksi_eqv_p (ksi_obj x1, ksi_obj x2)
{
    if (x1 == x2)
        return ksi_true;

    if (eff_tag (x1) != eff_tag (x2))
        return ksi_false;

    /* now x1->itag == x2->itag */

    if (x1->itag == KSI_TAG_EXTENDED) {
        ksi_etag etag = KSI_EXT_TAG (x1);
        return (etag->equal((struct Ksi_EObj *) x1, (struct Ksi_EObj *) x2, 0) ? ksi_true : ksi_false);
    }

    /*ksi_debug("eqv? %s %s", ksi_obj2str(x1), ksi_obj2str(x2));*/
    switch (x1->itag) {
    case KSI_TAG_CHAR:
        return (KSI_CHAR_CODE (x1) == KSI_CHAR_CODE (x2) ? ksi_true : ksi_false);

    case KSI_TAG_STRING:
    case KSI_TAG_CONST_STRING:
        return ksi_string_eqv_p (x1, x2);

    case KSI_TAG_BYTEVECTOR:
    case KSI_TAG_CONST_BYTEVECTOR:
        return ksi_bytevector_eqv_p (x1, x2);

    case KSI_TAG_BIGNUM:
    case KSI_TAG_FLONUM:
        return ksi_num_eqv_p (x1, x2);

    case KSI_TAG_INSTANCE:
        return ksi_inst_eqv_p (x1, x2);
    }

    return ksi_false;
}

static int
frame_equal (ksi_frame f1, ksi_frame f2)
{
  int i;

again:
  if (f1 == f2)
    return 1;

  if (!f1 || !f2)
    return 0;
  if (f1->num != f2->num)
    return 0;
  if (f1->env != f2->env)
    return 0;

  for (i = 0; i < f1->num; i++)
    if (ksi_equal_p (f1->vals[i], f2->vals[i]) == ksi_false)
      return 0;

  f1 = f1->next;
  f2 = f2->next;
  goto again;
}

ksi_obj
ksi_equal_p (ksi_obj x1, ksi_obj x2)
{
    int i;
    ksi_obj z1, z2;

again:
    if (x1 == x2)
        return ksi_true;

    if (eff_tag(x1) != eff_tag(x2))
        return ksi_false;

    /* x1->o.itag == x2->o.itag */

    if (x1->itag == KSI_TAG_EXTENDED) {
        ksi_etag etag = KSI_EXT_TAG(x1);
        return (etag->equal((struct Ksi_EObj *) x1, (struct Ksi_EObj *) x2, 1) ? ksi_true : ksi_false);
    }

    switch (x1->itag) {
    case KSI_TAG_CHAR:
        return (KSI_CHAR_CODE(x1) == KSI_CHAR_CODE(x2) ? ksi_true : ksi_false);

    case KSI_TAG_STRING:
    case KSI_TAG_CONST_STRING:
        return ksi_string_equal_p(x1, x2);

    case KSI_TAG_BYTEVECTOR:
    case KSI_TAG_CONST_BYTEVECTOR:
        return ksi_bytevector_eqv_p (x1, x2);

    case KSI_TAG_BIGNUM:
    case KSI_TAG_FLONUM:
        return ksi_num_equal_p(x1, x2);

    case KSI_TAG_PAIR:
    case KSI_TAG_CONST_PAIR:
        for (i = 0, z1 = x1, z2 = x2; ; ) {
            if (ksi_equal_p(KSI_CAR(x1), KSI_CAR(x2)) == ksi_false)
                return ksi_false;

            x1 = KSI_CDR(x1);
            x2 = KSI_CDR(x2);

            /* if cdr's are equal then lists equal, if both lists end,
             * x1 and x2 are ksi_nil (and equal too).
             */
            if (x1 == x2)
                return ksi_true;

            if (!KSI_PAIR_P(x1) || !KSI_PAIR_P(x2))
                goto again;

            if (ksi_equal_p(KSI_CAR(x1), KSI_CAR(x2)) == ksi_false)
                return ksi_false;

            x1 = KSI_CDR(x1);
            x2 = KSI_CDR(x2);

            if (x1 == x2)
                return ksi_true;

            if (!KSI_PAIR_P(x1) || !KSI_PAIR_P(x2))
                goto again;

            z1 = KSI_CDR(z1);
            z2 = KSI_CDR(z2);

            /* if both lists are circular they are equal */
            if (z1 == x1) i |= 1;
            if (z2 == x2) i |= 2;
            if (i == 3)
                return ksi_true;
        }

    case KSI_TAG_VECTOR:
    case KSI_TAG_CONST_VECTOR:
        if (KSI_VEC_LEN(x1) == KSI_VEC_LEN(x2)) {
            for (i = 0; i < KSI_VEC_LEN(x1) - 1; i++) {
                if (ksi_equal_p(KSI_VEC_REF(x1, i), KSI_VEC_REF(x2, i)) == ksi_false)
                    return ksi_false;
            }
            x1 = KSI_VEC_REF(x1, i);
            x2 = KSI_VEC_REF(x2, i);
            goto again;
        }
        return ksi_false;

    case KSI_TAG_INSTANCE:
        return ksi_inst_equal_p(x1, x2);

    case KSI_TAG_CLOSURE:
        if (KSI_CLOS_NUMS(x1) == KSI_CLOS_NUMS(x2)
            && KSI_CLOS_NARY(x1) == KSI_CLOS_NARY(x2)
            && KSI_CLOS_OPTS(x1) == KSI_CLOS_OPTS(x2)
            && frame_equal(KSI_CLOS_FRM(x1), KSI_CLOS_FRM(x2))) {
            x1 = KSI_CLOS_BODY(x1);
            x2 = KSI_CLOS_BODY(x2);
            goto again;
        }
        return ksi_false;

    case KSI_TAG_VAR0:
    case KSI_TAG_VAR1:
    case KSI_TAG_VAR2:
    case KSI_TAG_VARN:
        if (KSI_VARBOX_NUM(x1) == KSI_VARBOX_NUM(x2) && KSI_VARBOX_LEV(x1) == KSI_VARBOX_LEV(x2))
            return ksi_true;
        return ksi_false;

    case KSI_TAG_FREEVAR:
    case KSI_TAG_LOCAL:
    case KSI_TAG_IMPORTED:
        if (KSI_FREEVAR_SYM(x1) == KSI_FREEVAR_SYM(x2))
            return ksi_true;
        return ksi_false;

    default:
        if (KSI_CODE_P(x1) && KSI_CODE_NUM(x1) == KSI_CODE_NUM(x2)) {
            for (i = 0; i < KSI_CODE_NUM(x1); ++i) {
                if (ksi_equal_p(KSI_CODE_VAL(x1, i), KSI_CODE_VAL(x2, i)) == ksi_false)
                    return ksi_false;
            }
            x1 = KSI_CODE_VAL(x1, i);
            x2 = KSI_CODE_VAL(x2, i);
            goto again;
        }
    }

    return ksi_false;
}


static unsigned
hash_sym (void *obj, unsigned num, void *unused)
{
    return ksi_hash_str(KSI_SYM_PTR(obj), KSI_SYM_LEN(obj), num);
}

static int
cmp_sym (void *x1, void *x2, void *unused)
{
    int len1 = KSI_SYM_LEN(x1);
    int len2 = KSI_SYM_LEN(x2);
    const wchar_t* str1 = KSI_SYM_PTR(x1);
    const wchar_t* str2 = KSI_SYM_PTR(x2);

    int cmp = wmemcmp(str1, str2, (len1 < len2 ? len1 : len2));
    return cmp ? cmp : len1 - len2;
}

ksi_symbol
ksi_lookup_sym (const wchar_t *key, size_t len, int append)
{
    ksi_symbol sym;

    /* no need to add 1 for the nul char at the end of a name,
     * because it already counted in the Ksi_Symbol structure.
     */
    if (!append) {
        ksi_symbol sym = (ksi_symbol) alloca(sizeof(*sym) + len * sizeof(sym->ptr[0]));
        sym->o.ilen = len;
        wmemcpy(sym->ptr, key, len);
        sym->ptr[len] = 0;
        return (ksi_symbol) ksi_lookup_vtab(ksi_data->symtab, sym, 0);
    }

    sym = (ksi_symbol) ksi_malloc_data(sizeof(*sym) + len * sizeof(sym->ptr[0]));
    sym->o.itag = KSI_TAG_SYMBOL;
    sym->o.ilen = len;
    wmemcpy(sym->ptr, key, len);
    sym->ptr[len] = 0;

    return (ksi_symbol) ksi_lookup_vtab(ksi_data->symtab, sym, 1);
}

ksi_obj
ksi_str02sym (const wchar_t* name)
{
    return (ksi_obj) ksi_lookup_sym((name), wcslen(name), 1);
}

static wchar_t
tohex (int c)
{
    return btowc(c < 10 ? c + '0' : (c-10) + 'a');
}

static int
name2str (const wchar_t *name, int len, wchar_t *buf)
{
    int i, pos = 0;

    for (i = 0; i < len; ++i) {
        wchar_t c = name[i];
        switch (c) {
        case L'\0':
        case L'#':
        case L' ': case L'(': case L')': case L'[': case L']': case L'{': case L'}':
        case L';': case L'`': case L',': case L'\'': case L'"':
        case L'|': case L'\\':
            goto hex;

        case L'1': case L'2': case L'3': case L'4': case L'5':
        case L'6': case L'7': case L'8': case L'9': case L'0':
            if (i == 0)
                goto hex;
            goto put;

        default:
            if (!iswgraph(c)) {
            hex:
                if (buf) {
                    buf[pos++] = L'\\';
                    buf[pos++] = L'x';
                    if (name[i] > 0xffff) {
                        buf[pos++] = tohex((c >> 20) & 0xf);
                        buf[pos++] = tohex((c >> 16) & 0xf);
                    }
                    buf[pos++] = tohex((c >> 12) & 0xf);
                    buf[pos++] = tohex((c >> 8) & 0xf);
                    buf[pos++] = tohex((c >> 4) & 0xf);
                    buf[pos++] = tohex(c & 0xf);
                    buf[pos++] = L';';
                } else {
                    if (name[i] > 0xffff)
                        pos += 2;
                    pos += 7;
                }
            } else {
            put:
                if (buf)
                    buf[pos] = c;
                pos++;
            }
        }
    }

    return pos;
}

const wchar_t*
ksi_symbol2str (ksi_symbol sym)
{
    int len;
    wchar_t* ptr;

    if (KSI_SYM_LEN(sym) <= 0)
        return L"||";

    len = name2str(KSI_SYM_PTR(sym), KSI_SYM_LEN(sym), 0);
    if (len <= KSI_SYM_LEN(sym))
        return KSI_SYM_PTR(sym);

    ptr = ksi_malloc_data((len + 1) * sizeof(*ptr));
    name2str(KSI_SYM_PTR(sym), KSI_SYM_LEN(sym), ptr);
    ptr[len] = L'\0';

    return ptr;
}

ksi_obj
ksi_symbol_p (ksi_obj x)
{
    return KSI_SYM_P(x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_symbol_eq_p (int ac, ksi_obj *av)
{
    int i;
    ksi_obj c1, c2;

    if (ac >= 1) {
        c1 = av[0];
        KSI_CHECK(c1, KSI_SYM_P(c1), "symbol=?: invalid symbol");
        for (i = 1; i < ac; i++) {
            c2 = av[i];
            KSI_CHECK(c2, KSI_SYM_P(c2), "symbol=?: invalid symbol");
            if (c1 != c2)
                return ksi_false;
            c1 = c2;
        }
    }
    return ksi_true;
}

ksi_obj
ksi_symbol2string (ksi_obj sym)
{
    KSI_CHECK (sym, KSI_SYM_P(sym), "symbol->string: invalid symbol");
    return ksi_str02string(KSI_SYM_PTR(sym));
}

ksi_obj
ksi_string2symbol (ksi_obj str)
{
    KSI_CHECK(str, KSI_STR_P(str), "string->symbol: invalid string");
    return ksi_str2sym(KSI_STR_PTR(str), KSI_STR_LEN(str));
}

ksi_obj
ksi_gensym (ksi_obj name, ksi_obj unused)
{
    const wchar_t *str = 0;
    wchar_t *buf = 0;
    int len = 0, sz = 0, ses_len = wcslen(ksi_data->session_id);
    ksi_obj num;
    wchar_t *ptr;
    int l;

    if (name) {
        if (KSI_STR_P(name)) {
            str = KSI_STR_PTR(name);
            len = KSI_STR_LEN(name);
        } else if (KSI_SYM_P(name)) {
            str = KSI_SYM_PTR(name);
            len = KSI_SYM_LEN(name);
        } else {
            ksi_exn_error(L"assertion", name, "gensym: invalid symbol in arg1");
        }
    } else {
        str = L"g";
        len = 1;
    }

    for (;;) {
        KSI_LOCK_W(ksi_data->lock);
        ksi_data->gensym_num = ksi_add(ksi_data->gensym_num, ksi_long2num(1));
        num = ksi_data->gensym_num;
        KSI_UNLOCK_W(ksi_data->lock);

        ptr = ksi_num2str(num, 36);
        l = 1 + ses_len + 1 + wcslen(ptr);
        if (sz <= len + l) {
            sz = len + l;
            buf = ksi_malloc_data((sz + 1) * sizeof(*buf));
        }
        if (len)
            wmemcpy(buf, str, len);
        buf[len] = L'@';
        wmemcpy(buf+len+1, ksi_data->session_id, ses_len);
        buf[len+1+ses_len] = L'@';
        wmemcpy(buf+len+1+ses_len+1, ptr, l);

        if (!ksi_lookup_sym(buf, len+l, 0))
            return (ksi_obj) ksi_str02sym(buf);
    }
}


ksi_keyword
ksi_lookup_key (const wchar_t *key, size_t len, int append)
{
    ksi_keyword sym;
    if (!append) {
        ksi_keyword sym = (ksi_keyword) alloca(sizeof(*sym) + len * sizeof(sym->ptr[0]));
        sym->o.ilen = len;
        wmemcpy(sym->ptr, key, len);
        sym->ptr[len] = L'\0';
        return (ksi_keyword) ksi_lookup_vtab (ksi_data->keytab, sym, 0);
    }

    sym = (ksi_keyword) ksi_malloc_data(sizeof(*sym) + len * sizeof(sym->ptr[0]));
    sym->o.itag = KSI_TAG_KEYWORD;
    sym->o.ilen = len;
    wmemcpy(sym->ptr, key, len);
    sym->ptr[len] = L'\0';
    return (ksi_keyword) ksi_lookup_vtab (ksi_data->keytab, sym, 1);
}

ksi_obj
ksi_str02key (const wchar_t *name)
{
    return (ksi_obj) ksi_lookup_key((name), wcslen(name), 1);
}

const wchar_t *
ksi_key2str (ksi_keyword key)
{
    int len;
    wchar_t* ptr;

    if (KSI_KEY_LEN(key) <= 0)
        return L"||:";

    len = name2str(KSI_KEY_PTR(key), KSI_KEY_LEN(key), 0);

    ptr = ksi_malloc_data((len + 2) * sizeof(*ptr));
    name2str(KSI_KEY_PTR(key), KSI_KEY_LEN(key), ptr);
    ptr[len++] = L':';
    ptr[len] = L'\0';

    return ptr;
}

ksi_obj
ksi_key_p (ksi_obj x)
{
    return KSI_KEY_P(x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_string2keyword (ksi_obj str)
{
    KSI_CHECK(str, KSI_STR_P(str), "string->keyword: invalid string");
    return ksi_str2key(KSI_STR_PTR(str), KSI_STR_LEN(str));
}

ksi_obj
ksi_keyword2string (ksi_obj key)
{
    KSI_CHECK(key, KSI_KEY_P(key), "keyword->string: invalid keyword");
    return ksi_str2string(KSI_KEY_PTR(key), KSI_KEY_LEN(key));
}

ksi_obj
ksi_symbol2keyword (ksi_obj sym)
{
    KSI_CHECK(sym, KSI_SYM_P(sym), "symbol->keyword: invalid symbol");
    return ksi_str2key(KSI_SYM_PTR(sym), KSI_SYM_LEN(sym));
}

ksi_obj
ksi_keyword2symbol (ksi_obj key)
{
    KSI_CHECK(key, KSI_KEY_P(key), "keyword->symbol: invalid keyword");
    return ksi_str2sym(KSI_KEY_PTR(key), KSI_KEY_LEN(key));
}

ksi_obj
ksi_make_keyword (ksi_obj x)
{
    if (KSI_SYM_P(x))
        return ksi_symbol2keyword (x);
    if (KSI_STR_P(x))
        return ksi_string2keyword (x);
    if (KSI_KEY_P(x))
        return x;

    ksi_exn_error(0, x, "make-keyword: invalid object in arg1");
    return 0;
}


struct abbrev_data_t
{
  int len;
  const wchar_t *str;
  ksi_obj res;
};

static int
abbrev_proc (void *val, void *abbrev_data)
{
    struct abbrev_data_t *data = (struct abbrev_data_t *) abbrev_data;
    if (data->len <= KSI_SYM_LEN(val)) {
        if (wmemcmp(data->str, KSI_SYM_PTR(val), data->len) == 0) {
            data->res = ksi_cons(val, data->res);
        }
    }
    return 0;
}

ksi_obj
ksi_abbrev (wchar_t *str, int len)
{
    struct abbrev_data_t data;
    data.len = len;
    data.str = str;
    data.res = ksi_nil;
    ksi_iterate_vtab (ksi_data->symtab, abbrev_proc, &data);
    return data.res;
}


static inline ksi_obj
mk_exn (ksi_obj type, ksi_obj errobj, ksi_obj msg, ksi_obj src)
{
    ksi_obj exn = ksi_alloc_vector (4, KSI_TAG_EXN);
    KSI_EXN_TYPE (exn) = type;
    KSI_EXN_MSG  (exn) = msg;
    KSI_EXN_VAL  (exn) = errobj;
    KSI_EXN_SRC  (exn) = src;
    return exn;
}

ksi_obj
ksi_make_exn (const wchar_t *type, ksi_obj errobj, const wchar_t *msg, const wchar_t *src)
{
    return mk_exn (ksi_str02sym(type ? type : L"misc"),
                   errobj ? errobj : ksi_void,
                   ksi_str02string (msg ? msg : L"unspecified error"),
                   src ? ksi_str02string(src) : ksi_void);
}

static ksi_obj
ksi_scm_make_exn (ksi_obj type, ksi_obj errobj, ksi_obj msg, ksi_obj src)
{
    KSI_CHECK (type, KSI_SYM_P (type), "make-exn: invalid symbol in arg1");
    KSI_CHECK (msg,  KSI_STR_P (msg),  "make-exn: invalid string in arg3");
    KSI_CHECK (src,  KSI_STR_P (msg),  "make-exn: invalid string in arg4");

    return ksi_make_exn(KSI_SYM_PTR(type), errobj, KSI_STR_PTR(msg), KSI_STR_PTR(src));
}

ksi_obj
ksi_exn_p (ksi_obj x)
{
    return KSI_EXN_P (x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_exn_type (ksi_obj x)
{
    KSI_CHECK (x, KSI_EXN_P (x), "exn-type: invalid exception in arg1");
    return KSI_EXN_TYPE (x);
}

ksi_obj
ksi_exn_message (ksi_obj x)
{
    KSI_CHECK (x, KSI_EXN_P (x), "exn-message: invalid exception in arg1");
    return KSI_EXN_MSG (x);
}

ksi_obj
ksi_exn_value (ksi_obj x)
{
    KSI_CHECK (x, KSI_EXN_P (x), "exn-value: invalid exception in arg1");
    return KSI_EXN_VAL (x);
}

ksi_obj
ksi_exn_source (ksi_obj x)
{
    KSI_CHECK (x, KSI_EXN_P (x), "exn-source: invalid exception in arg1");
    return KSI_EXN_SRC (x);
}

ksi_obj
ksi_exn_error (const wchar_t *type, ksi_obj errobj, const char *fmt, ...)
{
    va_list args;
    wchar_t *msg;

    va_start(args, fmt);
    msg = ksi_avprintf(fmt, args);
    va_end(args);

    ksi_throw_error(mk_exn(ksi_str02sym(type ? type : L"assertion"),
                           errobj ? errobj : ksi_void,
                           ksi_str02string(msg),
                           errobj && errobj->annotation ? ksi_str02string(errobj->annotation) : ksi_void));

    return 0;
}

ksi_obj
ksi_src_error (const wchar_t *src, const char *fmt, ...)
{
    va_list args;
    wchar_t *msg;

    va_start(args, fmt);
    msg = ksi_avprintf(fmt, args);
    va_end(args);

    ksi_throw_error(mk_exn(ksi_str02sym(L"lexical"),
                           ksi_void,
                           ksi_str02string(msg),
                           ksi_str02string(src)));

    return 0;
}

ksi_obj
ksi_handle_error (ksi_context_t ctx, ksi_obj tag, ksi_obj exn)
{
    const wchar_t *msg;

    if (tag != ksi_err)
        exn = ksi_make_exn(0, tag, L"uncatched thrown object", 0);
    else if (!KSI_EXN_P(exn))
        exn = ksi_make_exn(0, exn, L"unspecified error", 0);


    if (KSI_EXN_VAL(exn) != ksi_void) {
        if (KSI_EXN_SRC(exn) != ksi_void) {
            msg = ksi_aprintf("%ls\nerror-value: %ls\nerror-source: %ls",
                              KSI_STR_PTR(KSI_EXN_MSG(exn)),
                              ksi_obj2str(KSI_EXN_VAL(exn)),
                              ksi_obj2name(KSI_EXN_SRC(exn)));
        } else {
            msg = ksi_aprintf("%ls\nerror-value: %ls",
                              KSI_STR_PTR(KSI_EXN_MSG(exn)),
                              ksi_obj2str(KSI_EXN_VAL(exn)));
        }
    } else {
        if (KSI_EXN_SRC(exn) != ksi_void) {
            msg = ksi_aprintf("%ls\nerror-source: %ls",
                              KSI_STR_PTR(KSI_EXN_MSG(exn)),
                              ksi_obj2name(KSI_EXN_SRC(exn)));
        } else {
            msg = KSI_STR_PTR(KSI_EXN_MSG(exn));
        }
    }
    ksi_errlog_msg(ctx, KSI_ERRLOG_ERROR, msg);

    return ksi_void;
}


ksi_obj
ksi_open_errlog (ksi_obj dest)
{
    ksi_char_port port = 0;

    KSI_ASSERT(ksi_current_context);

    if (KSI_TEXT_OUTPUT_PORT_P(dest)) {
        port = (ksi_char_port) dest;
    } else if (KSI_STR_P(dest)) {
        char *fn = ksi_local(KSI_STR_PTR(dest));
        int fd = open(fn, O_WRONLY | O_CREAT | O_APPEND, 0666);
        if (fd < 0)
            ksi_exn_error(0, 0, "open-errlog: cannot open file \"%s\": %m", fn);
        port = ksi_new_transcoded_port(ksi_new_fd_append_port(fd, fn), 0, 0, 0, fn, 1);
    } else {
        ksi_exn_error (0, dest, "open-errlog: invalid destination in arg1");
    }

    if (ksi_current_context->errlog_port) {
        ksi_close_port((ksi_obj) ksi_current_context->errlog_port);
        ksi_current_context->errlog_port = 0;
    }

    ksi_current_context->errlog_port = port;
    return ksi_void;
}

ksi_obj
ksi_errlog_priority (ksi_obj priority)
{
    int pri;

    KSI_ASSERT(ksi_current_context);
    KSI_CHECK(priority, KSI_EINT_P(priority), "errlog-priority: invalid integer in arg1");
    pri = ksi_num2long(priority, "errlog-priority");
    KSI_CHECK(priority, KSI_ERRLOG_ERROR <= pri && pri <= KSI_ERRLOG_ALL, "errlog-priority: priority out of range");

    ksi_current_context->errlog_priority = pri;
    return ksi_void;
}

static ksi_obj
scm_error_proc (int argc, ksi_obj* argv)
{
    ksi_obj errobj = 0;
    const wchar_t *who = 0, *msg = 0;
    int i;

    if (argc > 0 && KSI_SYM_P(argv[0])) {
        who = KSI_SYM_PTR(argv[0]);
        --argc; ++argv;
    }

    if (argc > 0 && KSI_STR_P(argv[0])) {
        ksi_obj str, port = ksi_open_string_output_port();
        i = ksi_internal_format((ksi_char_port) port, KSI_STR_PTR(argv[0]), argc-1, argv+1, "error");
        argc -= i + 1;
        argv += i + 1;

        str = ksi_extract_string_port_data(port);
        if (who)
            msg = ksi_aprintf("%ls: %ls", who, KSI_STR_PTR(str));
        else
            msg = KSI_STR_PTR(str);
    }

    for (i = 0; i < argc; i++) {
        if (!errobj)
            errobj = argv[i];
    }

    if (!msg)
        msg = L"unspecified error";

    ksi_exn_error(0, errobj, "%ls", msg);
    return ksi_void;
}

static ksi_obj
scm_errlog_proc (ksi_obj priority, ksi_obj fmt, int argc, ksi_obj* argv)
{
    int pri;
    ksi_obj str;

    KSI_ASSERT(ksi_current_context);
    KSI_CHECK(priority, KSI_EINT_P(priority), "errlog: invalid priority in arg2");
    KSI_CHECK(fmt, KSI_STR_P(fmt), "errlog: invalid string in arg3");

    pri = ksi_num2long(priority, "errlog");
    KSI_CHECK(priority, KSI_ERRLOG_ERROR <= pri && pri <= KSI_ERRLOG_ALL, "errlog: priority out of range");

    if (pri > ksi_current_context->errlog_priority)
        return ksi_void;

    str = ksi_format(ksi_false, KSI_STR_PTR(fmt), argc, argv);
    ksi_errlog_msg(ksi_current_context, pri, KSI_STR_PTR(str));
    return ksi_void;
}

void
ksi_init_std_ports (int in, int out, int err)
{
    ksi_env env = ksi_get_lib_env(L"ksi", L"core", L"io", 0);

    if (in >= 0) {
        ksi_byte_port port = ksi_new_fd_input_port(in, 0);
        ksi_data->input_port = ksi_new_transcoded_port(port, 0, 0, 0, 0, 0);
        ksi_defsym(L"stdin", (ksi_obj) ksi_data->input_port, env);
    }

    if (out >= 0) {
        ksi_byte_port port = ksi_new_fd_output_port(out, 0);
        ksi_data->output_port = ksi_new_transcoded_port(port, 0, 0, 0, 0, 0);
        ksi_defsym(L"stdout", (ksi_obj) ksi_data->output_port, env);
    }

    if (err >= 0) {
        ksi_byte_port port = ksi_new_fd_output_port(err, 0);
        ksi_data->error_port = ksi_new_transcoded_port(port, 0, 0, 0, 0, 0);
        ksi_defsym(L"stderr", (ksi_obj) ksi_data->error_port, env);
    }
}

ksi_obj
ksi_current_input_port ()
{
    return ksi_current_context ? (ksi_obj) ksi_current_context->input_port : (ksi_obj) ksi_data->input_port;
}

ksi_obj
ksi_current_output_port ()
{
    return ksi_current_context ? (ksi_obj) ksi_current_context->output_port : (ksi_obj) ksi_data->output_port;
}

ksi_obj
ksi_current_error_port ()
{
    return ksi_current_context ? (ksi_obj) ksi_current_context->error_port : (ksi_obj) ksi_data->error_port;
}

#define CONCAT(a,b) a##b

#define DEF_SET_PORT(port, name, test)                          \
ksi_obj                                                         \
CONCAT(ksi_set_current_, port) (ksi_obj new_port)               \
{                                                               \
    if (ksi_current_context) {                                  \
        ksi_obj old = (ksi_obj) ksi_current_context->port;      \
        if (!test(new_port))                                    \
            ksi_exn_error(0, new_port, name ": invalid port");  \
        ksi_current_context->port = (ksi_char_port) new_port;   \
        return old;                                             \
    } else {                                                    \
        return (ksi_obj) ksi_data->port;                        \
    }                                                           \
}

DEF_SET_PORT(input_port, "set-current-input-port", KSI_TEXT_INPUT_PORT_P)
DEF_SET_PORT(output_port, "set-current-output-port", KSI_TEXT_OUTPUT_PORT_P)
DEF_SET_PORT(error_port, "set-current-error-port", KSI_TEXT_OUTPUT_PORT_P)


ksi_obj
ksi_setlocale (ksi_obj category, ksi_obj locale)
{
    const wchar_t *p = 0;
    char *tmp;
    int c = 0;

    if (KSI_SYM_P(category))
        p = KSI_SYM_PTR(category);
    else if (KSI_KEY_P(category))
        p = KSI_KEY_PTR(category);
    else
        ksi_exn_error(0, category, "setlocale: invalid category in arg1");

    if (wcscmp(p, L"LC_ALL") == 0)
        c = LC_ALL;
    else if (wcscmp(p, L"LC_COLLATE") == 0)
        c = LC_COLLATE;
    else if (wcscmp(p, L"LC_CTYPE") == 0)
        c = LC_CTYPE;
    else if (wcscmp(p, L"LC_MONETARY") == 0)
        c = LC_MONETARY;
    else if (wcscmp(p, L"LC_NUMERIC") == 0)
        c = LC_NUMERIC;
    else if (wcscmp(p, L"LC_TIME") == 0)
        c = LC_TIME;
    else
        ksi_exn_error(0, category, "setlocale: invalid category in arg1");

    if (!locale)
        p = 0;
    else if (KSI_STR_P(locale))
        p = KSI_STR_PTR(locale);
    else
        ksi_exn_error(0, locale, "setlocale: invalid locale in arg2");

    tmp = setlocale(c, ksi_local(p));

    return (tmp ? ksi_str02string(ksi_utf(tmp)) : ksi_false);
}

ksi_obj
ksi_str2obj (const wchar_t *ptr, int len)
{
    ksi_obj str, port;

    if (ptr == 0 || len == 0)
        return ksi_void;

    str = ksi_str2string(ptr, len);
    port = (ksi_obj) ksi_new_string_input_port(str, 0, 0);

    return ksi_get_datum(port);
}

ksi_obj
ksi_str02obj (const wchar_t *ptr)
{
    if (ptr == 0)
        return ksi_void;

    return ksi_str2obj(ptr, wcslen(ptr));
}

ksi_obj
ksi_object2string (ksi_obj x)
{
    ksi_obj port = ksi_open_string_output_port();
    ksi_put_datum(port, x);
    return ksi_extract_string_port_data(port);
}

const wchar_t *
ksi_obj2str (ksi_obj x)
{
    ksi_obj str = ksi_object2string(x);
    return KSI_STR_PTR(str);
}

const wchar_t *
ksi_obj2name (ksi_obj x)
{
    wchar_t *ptr;
    ksi_obj port, str;

    if (!x)
        return L"<null>";

    switch (x->itag) {
    case KSI_TAG_SYMBOL:
        return KSI_SYM_PTR(x);

    case KSI_TAG_KEYWORD:
        return KSI_KEY_PTR(x);

    case KSI_TAG_CHAR:
        ptr = ksi_malloc_data(2 * sizeof(*ptr));
        ptr[0] = KSI_CHAR_CODE(x);
        ptr[1] = L'\0';
        return ptr;

    case KSI_TAG_CONST_STRING:
    case KSI_TAG_STRING:
        return KSI_STR_PTR(x);

    case KSI_TAG_PAIR:
    case KSI_TAG_CONST_PAIR:
    case KSI_TAG_VECTOR:
    case KSI_TAG_CONST_VECTOR:
        port = ksi_open_string_output_port();
        ksi_display(x, port);
        str = ksi_extract_string_port_data(port);
        return KSI_STR_PTR(str);

    default:
        break;
    }

    return ksi_obj2str(x);
}

static ksi_obj
scm_get_u8 (ksi_obj port)
{
    int u8;

    KSI_CHECK(port, KSI_BIN_INPUT_PORT_P(port), "get-u8: invalid binary input port in arg1");
    u8 = ksi_get_byte((ksi_byte_port) port);
    if (u8 < 0)
        return ksi_eof;
    return ksi_long2num(u8);
}

static ksi_obj
scm_lookahead_u8 (ksi_obj port)
{
    int u8;

    KSI_CHECK(port, KSI_BIN_INPUT_PORT_P(port), "lookahead-u8: invalid binary input port in arg1");
    u8 = ksi_lookahead_byte((ksi_byte_port) port);
    if (u8 < 0)
        return ksi_eof;

    return ksi_long2num(u8);
}

static ksi_obj
scm_put_u8 (ksi_obj port, ksi_obj octet)
{
    int u8;

    KSI_CHECK(port, KSI_BIN_OUTPUT_PORT_P(port), "put-u8: invalid binary output port in arg1");
    KSI_CHECK(octet, KSI_EINT_P(octet), "put-u8: invalid exact integer in arg2");
    u8 = ksi_num2long(octet, "put-u8");

    ksi_put_byte((ksi_byte_port) port, u8);
    return ksi_void;
}

static ksi_obj
scm_get_char (ksi_obj port)
{
    wint_t ch;

    KSI_CHECK(port, KSI_TEXT_INPUT_PORT_P(port), "get-char: invalid textual input port in arg1");
    ch = ksi_get_char((ksi_char_port) port);
    if (ch == WEOF)
        return ksi_eof;
    return ksi_int2char(ch);
}

static ksi_obj
scm_lookahead_char (ksi_obj port)
{
    wint_t ch;

    KSI_CHECK(port, KSI_TEXT_INPUT_PORT_P(port), "lookahead-char: invalid textual input port in arg1");
    ch = ksi_lookahead_char((ksi_char_port) port);
    if (ch == WEOF)
        return ksi_eof;

    return ksi_int2char(ch);
}

static ksi_obj
scm_put_char (ksi_obj port, ksi_obj ch)
{
    KSI_CHECK(port, KSI_TEXT_OUTPUT_PORT_P(port), "put-char: invalid textual output port in arg1");
    KSI_CHECK(ch, KSI_CHAR_P(ch), "put-cahr: invalid character in arg2");

    ksi_put_char((ksi_char_port) port, KSI_CHAR_CODE(ch));
    return ksi_void;
}

const char*
ksi_mk_filename (ksi_obj x, char* name)
{
again:
    if (x == ksi_nil)
        return "";
    if (KSI_STR_P(x))
        return ksi_local(KSI_STR_PTR(x));
    if (KSI_SYM_P(x))
        return ksi_local(KSI_SYM_PTR(x));
    if (KSI_EINT_P(x))
        return ksi_local(ksi_num2str(x, 10));

    if (KSI_PAIR_P(x)) {
        if (KSI_CDR(x) == ksi_nil) {
            x = KSI_CAR(x);
            goto again;
        } else {
            return ksi_local(ksi_aprintf("%s%s%s", ksi_mk_filename(KSI_CAR(x), name), "/", ksi_mk_filename(KSI_CDR(x), name)));
        }
    }

    ksi_exn_error(0, x, "%s: invalid file-name", name);
    return 0;
}


static ksi_obj
ksi_copyright ()
{
    static wchar_t str [] = L"Copyright (C) 1997-2010, Ivan Demakov <ksion@users.sourceforge.net>";
    return ksi_str02string(str);
}

static ksi_obj
ksi_banner ()
{
    static wchar_t str [] = L"Welcome to the KSi Scheme.";
    return ksi_str02string(str);
}

static ksi_obj
ksi_scm_version ()
{
    return ksi_str02string(ksi_utf(ksi_version()));
}

static ksi_obj
ksi_scm_major_version ()
{
    return ksi_long2num(ksi_major_version());
}

static ksi_obj
ksi_scm_minor_version ()
{
    return ksi_long2num(ksi_minor_version());
}

static ksi_obj
ksi_scm_patch_level ()
{
    return ksi_long2num(ksi_patch_level());
}

static ksi_obj
ksi_scm_cpu ()
{
    return ksi_str02sym(ksi_utf(ksi_cpu()));
}

static ksi_obj
ksi_scm_os ()
{
    return ksi_str02sym(ksi_utf(ksi_os()));
}

static ksi_obj
ksi_scm_host ()
{
    return ksi_str02sym(ksi_utf(ksi_host()));
}

static ksi_obj
inst_include_dir ()
{
    return ksi_str02string(ksi_utf(ksi_instal_include_dir()));
}

static ksi_obj
inst_lib_dir ()
{
    return ksi_str02string(ksi_utf(ksi_instal_lib_dir()));
}

static ksi_obj
inst_bin_dir ()
{
    return ksi_str02string(ksi_utf(ksi_instal_bin_dir()));
}

static ksi_obj
inst_info_dir ()
{
    return ksi_str02string(ksi_utf(ksi_instal_info_dir()));
}

static ksi_obj
inst_man_dir ()
{
    return ksi_str02string(ksi_utf(ksi_instal_man_dir()));
}

static ksi_obj
build_cflags ()
{
    return ksi_str02string(ksi_utf(ksi_build_cflags()));
}

static ksi_obj
build_libs ()
{
    return ksi_str02string(ksi_utf(ksi_build_libs()));
}

static ksi_obj
lib_path ()
{
    return ksi_str02string(ksi_utf(ksi_scheme_lib_dir()));
}

static ksi_obj
gc_expand_heap (ksi_obj x)
{
    KSI_CHECK (x, KSI_EINT_P (x), "gc-expand: invalid integer");
    return ksi_expand_heap (ksi_num2ulong (x, "ksi:gc-expand")) ? ksi_true : ksi_false;
}

static ksi_obj
gc_set_heap_size (ksi_obj x)
{
    KSI_CHECK (x, KSI_EINT_P (x), "gc-set-size!: invalid integer");
    ksi_set_max_heap (ksi_num2ulong (x, "gc-set-size!"));
    return ksi_void;
}

static ksi_obj
gc_heap_size ()
{
    return ksi_ulong2num (ksi_get_heap_size ());
}

static ksi_obj
gc_free_size (void)
{
    return ksi_ulong2num (ksi_get_heap_free ());
}

static ksi_obj
gc_collections (void)
{
    return ksi_ulong2num (ksi_gcollections ());
}

static ksi_obj
gc_collect ()
{
    ksi_gcollect (1);
    return ksi_void;
}

static ksi_obj
scm_format_proc (ksi_obj port, ksi_obj str, int argc, ksi_obj* argv)
{
    KSI_CHECK(str, KSI_STR_P(str), "format: invalid string in arg2");
    return ksi_format(port, KSI_STR_PTR(str), argc, argv);
}

static ksi_obj
scm_abbrev (ksi_obj str)
{
    KSI_CHECK(str, KSI_STR_P(str), "abbrev: invalid string in arg1");
    return ksi_abbrev(KSI_STR_PTR(str), KSI_STR_LEN(str));
}

static ksi_obj
ksi_scm_current_time (void)
{
    time_t t = time(0);
    return ksi_longlong2num(t);
}

static ksi_obj
ksi_scm_cpu_time (void)
{
    double d = ksi_cpu_time();
    return ksi_double2num(d);
}

static ksi_obj
ksi_scm_eval_time (void)
{
    double d = ksi_eval_time();
    return ksi_double2num(d);
}

static ksi_obj
ksi_scm_real_time (void)
{
    double d = ksi_real_time();
    return ksi_double2num(d);
}

static ksi_obj
tm2obj (struct tm *bt)
{
  ksi_obj v = (ksi_obj) ksi_alloc_vector (9, KSI_TAG_VECTOR);
  KSI_VEC_REF (v, 0) = ksi_long2num (bt->tm_sec);
  KSI_VEC_REF (v, 1) = ksi_long2num (bt->tm_min);
  KSI_VEC_REF (v, 2) = ksi_long2num (bt->tm_hour);
  KSI_VEC_REF (v, 3) = ksi_long2num (bt->tm_mday);
  KSI_VEC_REF (v, 4) = ksi_long2num (bt->tm_mon);
  KSI_VEC_REF (v, 5) = ksi_long2num (bt->tm_year);
  KSI_VEC_REF (v, 6) = ksi_long2num (bt->tm_wday);
  KSI_VEC_REF (v, 7) = ksi_long2num (bt->tm_yday);
  KSI_VEC_REF (v, 8) = ksi_long2num (bt->tm_isdst);

  return v;
}

static int
obj2tm (ksi_obj v, struct tm *bt, const char *name)
{
    if (KSI_VEC_P (v) && KSI_VEC_LEN (v) == 9
        && KSI_EINT_P (KSI_VEC_REF (v, 1))
        && KSI_EINT_P (KSI_VEC_REF (v, 2))
        && KSI_EINT_P (KSI_VEC_REF (v, 3))
        && KSI_EINT_P (KSI_VEC_REF (v, 4))
        && KSI_EINT_P (KSI_VEC_REF (v, 5))
        && KSI_EINT_P (KSI_VEC_REF (v, 6))
        && KSI_EINT_P (KSI_VEC_REF (v, 7))
        && KSI_EINT_P (KSI_VEC_REF (v, 8)))
    {
        memset(bt, 0, sizeof *bt);

        bt->tm_sec   = ksi_num2long (KSI_VEC_REF (v, 0), name);
        bt->tm_min   = ksi_num2long (KSI_VEC_REF (v, 1), name);
        bt->tm_hour  = ksi_num2long (KSI_VEC_REF (v, 2), name);
        bt->tm_mday  = ksi_num2long (KSI_VEC_REF (v, 3), name);
        bt->tm_mon   = ksi_num2long (KSI_VEC_REF (v, 4), name);
        bt->tm_year  = ksi_num2long (KSI_VEC_REF (v, 5), name);
        bt->tm_wday  = ksi_num2long (KSI_VEC_REF (v, 6), name);
        bt->tm_yday  = ksi_num2long (KSI_VEC_REF (v, 7), name);
        bt->tm_isdst = ksi_num2long (KSI_VEC_REF (v, 8), name);

        return 1;
    }

    return 0;
}

static ksi_obj
scm_gmtime (ksi_obj arg_time)
{
    time_t it;
    struct tm tm;

    if (arg_time) {
        KSI_CHECK(arg_time, KSI_RATIONAL_P(arg_time), "gmtime: invalid real number as time in arg1");
        it = (time_t) ksi_num2long(arg_time, "gmtime");
    } else {
        it = time(0);
    }

    if (ksi_gmtime(it, &tm))
        return tm2obj(&tm);

    return ksi_false;
}

static ksi_obj
scm_localtime (ksi_obj arg_time)
{
    time_t it;
    struct tm tm;

    if (arg_time) {
        KSI_CHECK(arg_time, KSI_REAL_P(arg_time), "localtime: invalid real number as time in arg1");
        it = (time_t) ksi_num2long(arg_time, "localtime");
    } else {
        it = time(0);
    }

    if (ksi_localtime(it, &tm))
        return tm2obj(&tm);

    return ksi_false;
}

static ksi_obj
scm_strftime (ksi_obj format_obj, ksi_obj time)
{
    struct tm bt;
    char *buf, *format;
    int size, ok = obj2tm(time, &bt, "strtime");

    KSI_CHECK(format_obj, KSI_STR_P(format_obj), "strftime: invalid string in arg1");
    KSI_CHECK(time, ok, "strftime: invalid broken-time in arg2");

    format = ksi_local(KSI_STR_PTR(format_obj));
    size = 128;
    buf = ksi_malloc_data (size);
    while (strftime(buf, size, format, &bt) >= size) {
        ksi_free(buf);
        size *= 2;
        buf = ksi_malloc_data(size);
    }

    return ksi_str02string(ksi_utf(buf));
}

static ksi_obj
scm_mktime (ksi_obj time)
{
    time_t    it;
    struct tm bt;
    int ok = obj2tm(time, &bt, "mktime");

    KSI_CHECK(time, ok, "mktime: invalid broken-time");
    it = mktime(&bt);
    return (it == ((time_t) -1) ? ksi_false : ksi_longlong2num(it));
}

static ksi_obj
scm_annotation_p (ksi_obj x)
{
    if (x->annotation)
        return ksi_true;
    return ksi_false;
}

static ksi_obj
scm_annotation_source (ksi_obj x)
{
    if (x->annotation)
        return ksi_str02string (x->annotation);
    return ksi_false;
}

static ksi_obj
scm_annotation_expression (ksi_obj x)
{
     return x;
}

static ksi_obj
scm_make_string (ksi_obj k, ksi_obj c)
{
    if (!c)
        c = ksi_int2char(L'\0');

    KSI_CHECK(k, KSI_EINT_P(k), "make-string: invalid integer in arg1");
    KSI_CHECK(c, KSI_CHAR_P(c), "make-string: invalid char in arg2");

    return (ksi_obj) ksi_make_string(ksi_num2long(k, "make-string"), KSI_CHAR_CODE(c));
}

static ksi_obj
scm_real_part (ksi_obj x)
{
    if (KSI_BIGNUM_P(x))
        return x;
    if (KSI_FLONUM_P(x))
        return ksi_double2num(ksi_real_part(x));

    ksi_exn_error(0, x, "real-part: invalid number");
    return 0;
}

static ksi_obj
scm_imag_part (ksi_obj x)
{
  if (KSI_BIGNUM_P(x))
      return ksi_long2num(0);
  if (KSI_FLONUM_P (x))
      return ksi_double2num(ksi_imag_part(x));

  ksi_exn_error(0, x, "imag-part: invalid number");
  return 0;
}

static ksi_obj
scm_angle (ksi_obj z)
{
    return ksi_double2num(ksi_angle(z));
}


static ksi_obj
ksi_void_proc (int ac, ksi_obj *av)
{
    return ksi_void;
}

static ksi_obj
ksi_true_proc (int ac, ksi_obj *av)
{
    return ksi_true;
}

static ksi_obj
ksi_false_proc (int ac, ksi_obj *av)
{
    return ksi_false;
}

static ksi_obj
eval_cxr (wchar_t *data, ksi_obj val)
{
    wchar_t *ptr;

    ptr = data + wcslen(data) - 1;
    for (--ptr; *ptr != L'c'; --ptr) {
        if (!KSI_PAIR_P (val))
            ksi_exn_error(0, val, "%ls: invalid pair", data);

        val = (*ptr == L'd') ? KSI_CDR(val) : KSI_CAR(val);
    }

    return val;
}

struct Ksi_Data *
ksi_internal_data(void)
{
  static struct Ksi_Data *data = 0;
  ksi_env env;

  if (!data) {
    data = ksi_malloc_eternal(sizeof (*data));
    memset(data, 0, sizeof *data);

    KSI_INIT_LOCK(data->lock);

    data->obj_nil = ksi_new_imm(KSI_TAG_IMM);
    data->obj_false = ksi_new_imm(KSI_TAG_IMM);
    data->obj_true = ksi_new_imm(KSI_TAG_IMM);
    data->obj_void = ksi_new_imm(KSI_TAG_IMM);
    data->obj_eof = ksi_new_imm(KSI_TAG_IMM);
    data->obj_err = ksi_new_imm(KSI_TAG_IMM);

    data->symtab = ksi_new_valtab(1000, hash_sym, cmp_sym, 0);
    data->keytab = ksi_new_valtab(1000, hash_sym, cmp_sym, 0);
    data->envtab = 0; /* initialized in ksi_env.c */

    data->sym_quote = ksi_str02sym(L"quote");
    data->sym_begin = ksi_str02sym(L"begin");
    data->sym_if = ksi_str02sym(L"if");
    data->sym_and = ksi_str02sym(L"and");
    data->sym_or = ksi_str02sym(L"or");
    data->sym_lambda = ksi_str02sym(L"lambda");
    data->sym_define = ksi_str02sym(L"define");
    data->sym_defmacro = ksi_str02sym(L"define-macro!");
    data->sym_set = ksi_str02sym(L"set!");
    data->sym_case = ksi_str02sym(L"case");
    data->sym_cond = ksi_str02sym(L"cond");
    data->sym_else = ksi_str02sym(L"else");
    data->sym_let = ksi_str02sym(L"let");
    data->sym_letrec = ksi_str02sym(L"letrec");
    data->sym_letrec_star = ksi_str02sym(L"letrec*");
    data->sym_letstar = ksi_str02sym(L"let*");
    data->sym_quasiquote = ksi_str02sym(L"quasiquote");
    data->sym_unquote = ksi_str02sym(L"unquote");
    data->sym_unquote_splicing = ksi_str02sym(L"unquote-splicing");
    data->sym_syntax = ksi_str02sym(L"syntax");
    data->sym_quasisyntax = ksi_str02sym(L"quasisyntax");
    data->sym_unsyntax = ksi_str02sym(L"unsyntax");
    data->sym_unsyntax_splicing = ksi_str02sym(L"unsyntax-splicing");
    data->sym_import = ksi_str02sym(L"import");
    data->sym_export = ksi_str02sym(L"export");
    data->sym_library = ksi_str02sym(L"library");
    data->sym_rename = ksi_str02sym(L"rename");
    data->sym_prefix = ksi_str02sym(L"prefix");
    data->sym_except = ksi_str02sym(L"except");
    data->sym_only = ksi_str02sym(L"only");
    data->sym_for = ksi_str02sym(L"for");

    data->sym_plus = ksi_str02sym(L"+");
    data->sym_minus = ksi_str02sym(L"-");
    data->sym_arrow = ksi_str02sym(L"=>");
    data->sym_dots = ksi_str02sym(L"...");

    data->sym_inactive = ksi_str02sym(L"inactive");
    data->sym_wait = ksi_str02sym(L"wait");
    data->sym_ready = ksi_str02sym(L"ready");
    data->sym_timeout = ksi_str02sym(L"timeout");

    data->sym_apply_generic = ksi_str02sym(L"apply-generic");
    data->sym_no_applicable_method = ksi_str02sym(L"no-applicable-method");
    data->sym_no_next_method = ksi_str02sym(L"no-next-method");

    data->sym_cname = ksi_str02sym(S_cname);
    data->sym_gname = ksi_str02sym(S_gname);
    data->sym_dsupers = ksi_str02sym(S_dsupers);
    data->sym_dslots = ksi_str02sym(S_dslots);
    data->sym_defargs = ksi_str02sym(S_defargs);
    data->sym_cpl = ksi_str02sym(S_cpl);
    data->sym_slots = ksi_str02sym(S_slots);
    data->sym_nfields = ksi_str02sym(S_nfields);
    data->sym_gns = ksi_str02sym(S_gns);
    data->sym_gf = ksi_str02sym(S_gf);
    data->sym_specs = ksi_str02sym(S_specs);
    data->sym_proc = ksi_str02sym(S_proc);
    data->sym_methods = ksi_str02sym(S_methods);
    data->sym_arity = ksi_str02sym(S_arity);
    data->sym_combination = ksi_str02sym(S_combination);
    data->sym_after = ksi_str02sym(S_after);
    data->sym_before = ksi_str02sym(S_before);
    data->sym_around = ksi_str02sym(S_around);
    data->sym_primary = ksi_str02sym(S_primary);

    data->sym_native = ksi_str02sym(L"native");
    data->sym_latin1 = ksi_str02sym(L"latin-1");
    data->sym_utf8 = ksi_str02sym(L"utf-8");
    data->sym_utf16 = ksi_str02sym(L"utf-16");
    data->sym_raise = ksi_str02sym(L"raise");
    data->sym_ignore = ksi_str02sym(L"ignore");
    data->sym_replace = ksi_str02sym(L"replace");
    data->sym_none = ksi_str02sym(L"none");
    data->sym_lf = ksi_str02sym(L"lf");
    data->sym_cr = ksi_str02sym(L"cr");
    data->sym_crlf = ksi_str02sym(L"crlf");
    data->sym_nel = ksi_str02sym(L"nel");
    data->sym_crnel = ksi_str02sym(L"crnel");
    data->sym_ls = ksi_str02sym(L"ls");

    data->sym_no_create = ksi_str02sym(L"no-create");
    data->sym_no_fail = ksi_str02sym(L"no-fail");
    data->sym_no_truncate = ksi_str02sym(L"no-truncate");
    data->sym_line = ksi_str02sym(L"line");
    data->sym_block = ksi_str02sym(L"block");

    data->sym_little = ksi_str02sym(L"little");
    data->sym_big = ksi_str02sym(L"big");

    data->key_initform = ksi_str02key(K_initform);
    data->key_initarg = ksi_str02key(K_initarg);
    data->key_defargs = ksi_str02key(K_defargs);
    data->key_type = ksi_str02key(K_type);
    data->key_name = ksi_str02key(K_name);
    data->key_dsupers = ksi_str02key(K_dsupers);
    data->key_dslots = ksi_str02key(K_dslots);
    data->key_specs = ksi_str02key(K_specs);
    data->key_proc = ksi_str02key(K_proc);
    data->key_gf = ksi_str02key(K_gf);
    data->key_arity = ksi_str02key(K_arity);
    data->key_combination = ksi_str02key(K_combination);

    data->not_proc = ksi_new_imm(KSI_TAG_NOT);
    data->eq_proc = ksi_new_imm(KSI_TAG_EQP);
    data->eqv_proc = ksi_new_imm(KSI_TAG_EQVP);
    data->equal_proc = ksi_new_imm(KSI_TAG_EQUALP);
    data->list_proc = ksi_new_imm(KSI_TAG_LIST);
    data->vector_proc = ksi_new_imm(KSI_TAG_MK_VECTOR);
    data->list2vector_proc = ksi_new_imm(KSI_TAG_LIST2VECTOR);
    data->append_proc = ksi_new_imm(KSI_TAG_APPEND);
    data->nullp_proc = ksi_new_imm(KSI_TAG_NULLP);
    data->pairp_proc = ksi_new_imm(KSI_TAG_PAIRP);
    data->listp_proc = ksi_new_imm(KSI_TAG_LISTP);
    data->cons_proc = ksi_new_imm(KSI_TAG_CONS);
    data->car_proc = ksi_new_imm(KSI_TAG_CAR);
    data->cdr_proc = ksi_new_imm(KSI_TAG_CDR);
    data->memq_proc = ksi_new_imm(KSI_TAG_MEMQ);
    data->memv_proc = ksi_new_imm(KSI_TAG_MEMV);
    data->member_proc = ksi_new_imm(KSI_TAG_MEMBER);
    data->vectorp_proc = ksi_new_imm(KSI_TAG_VECTORP);
    data->apply_proc = ksi_new_imm(KSI_TAG_APPLY);
    data->call_cc_proc = ksi_new_imm(KSI_TAG_CALL_CC);
    data->call_vs_proc = ksi_new_imm(KSI_TAG_CALL_WITH_VALUES);

    data->void_proc = (ksi_obj) ksi_new_prim(L"void", ksi_void_proc, KSI_CALL_REST0, 0);
    data->true_proc = (ksi_obj) ksi_new_prim(L"true", ksi_true_proc, KSI_CALL_REST0, 0);
    data->false_proc = (ksi_obj) ksi_new_prim(L"false", ksi_false_proc, KSI_CALL_REST0, 0);

    {
      int i;
      char bits[12], *ptr;
      ksi_random_bits(bits, sizeof(bits));
      ptr = ksi_base64(bits, sizeof(bits));
      for (i = 0; ptr[i]; i++) {
	if (ptr[i] == '/')
	  ptr[i] = '*';
	else if (ptr[i] == '+')
	  ptr[i] = '%';
      }
      data->session_id = ksi_utf(ptr);
    }
    data->gensym_num = ksi_long2num(0);

    ksi_init_top_classes();

    data->null_port = ksi_new_null_port();
    data->input_port = data->null_port;
    data->output_port = data->null_port;
    data->error_port = data->null_port;

    data->syntax_env = env = ksi_get_lib_env(L"ksi", L"core", L"syntax", 0);
    ksi_defsyntax(ksi_data->sym_begin, ksi_new_core(KSI_TAG_BEGIN), env, 1);
    ksi_defsyntax(ksi_data->sym_if, ksi_new_core(KSI_TAG_IF), env, 1);
    ksi_defsyntax(ksi_data->sym_lambda, ksi_new_core(KSI_TAG_LAMBDA), env, 1);
    ksi_defsyntax(ksi_data->sym_define, ksi_new_core(KSI_TAG_DEFINE), env, 1);
    ksi_defsyntax(ksi_data->sym_set, ksi_new_core(KSI_TAG_SET), env, 1);
    ksi_defsyntax(ksi_data->sym_and, ksi_new_core(KSI_TAG_AND), env, 1);
    ksi_defsyntax(ksi_data->sym_or, ksi_new_core(KSI_TAG_OR), env, 1);
    ksi_defsyntax(ksi_data->sym_let, ksi_new_core(KSI_TAG_LET), env, 1);
    ksi_defsyntax(ksi_data->sym_letrec, ksi_new_core(KSI_TAG_LETREC), env, 1);
    ksi_defsyntax(ksi_data->sym_letrec_star, ksi_new_core(KSI_TAG_LETREC_STAR), env, 1);

    ksi_defsyntax(ksi_data->sym_letstar, (ksi_obj) ksi_new_prim(L"let*", ksi_letstar_macro, KSI_CALL_ARG2, 2), env, 1);
    ksi_defsyntax(ksi_data->sym_case, (ksi_obj) ksi_new_prim(L"case", ksi_case_macro, KSI_CALL_ARG2, 2), env, 1);
    ksi_defsyntax(ksi_data->sym_cond, (ksi_obj) ksi_new_prim(L"cond", ksi_cond_macro, KSI_CALL_ARG2, 2), env, 1);
    ksi_defsyntax(ksi_data->sym_else, ksi_data->sym_else, env, 1);
    ksi_defsyntax(ksi_data->sym_arrow, ksi_data->sym_arrow, env, 1);

    ksi_defsyntax(ksi_data->sym_quote, ksi_new_core(KSI_TAG_QUOTE), env, 1);
    ksi_defsyntax(ksi_data->sym_quasiquote, (ksi_obj) ksi_new_prim(L"quasiquote", ksi_quasiquote_macro, KSI_CALL_ARG2, 2), env, 1);
    ksi_defsyntax(ksi_data->sym_unquote, ksi_data->sym_unquote, env, 1);
    ksi_defsyntax(ksi_data->sym_unquote_splicing, ksi_data->sym_unquote_splicing, env, 1);

    ksi_defsyntax(ksi_data->sym_syntax, ksi_new_core(KSI_TAG_SYNTAX), env, 1);
    ksi_defsyntax(ksi_data->sym_quasisyntax, (ksi_obj) ksi_new_prim(L"quasisyntax", ksi_quasisyntax_macro, KSI_CALL_ARG2, 2), env, 1);
    ksi_defsyntax(ksi_data->sym_unsyntax, ksi_data->sym_unsyntax, env, 1);
    ksi_defsyntax(ksi_data->sym_unsyntax_splicing, ksi_data->sym_unsyntax_splicing, env, 1);

    ksi_defsyntax(ksi_data->sym_dots, ksi_data->sym_dots, env, 1);

    env = ksi_get_lib_env(L"ksi", L"core", L"base", 0);
    ksi_defun(L"void?", ksi_void_p, KSI_CALL_ARG1, 1, env);
    ksi_defsym(L"void", ksi_data->void_proc, env);

    ksi_defun(L"boolean?", ksi_bool_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"boolean=?", ksi_bool_eq_p, KSI_CALL_REST0, 0, env);
    ksi_defsym(L"true", ksi_data->true_proc, env);
    ksi_defsym(L"false", ksi_data->false_proc, env);

    ksi_defsym(L"not", ksi_data->not_proc, env);
    ksi_defsym(L"eq?", ksi_data->eq_proc, env);
    ksi_defsym(L"eqv?", ksi_data->eqv_proc, env);
    ksi_defsym(L"equal?", ksi_data->equal_proc, env);

    ksi_defun(L"symbol?", ksi_symbol_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"symbol=?", ksi_symbol_eq_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"gensym", ksi_gensym, KSI_CALL_ARG2, 0, env);
    ksi_defun(L"abbrev", scm_abbrev, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"keyword?", ksi_key_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"make-keyword", ksi_make_keyword, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"keyword->symbol", ksi_keyword2symbol, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"symbol->keyword", ksi_symbol2keyword, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"values", ksi_new_values, KSI_CALL_REST0, 0, env);
    ksi_defun(L"dynamic-wind", ksi_dynamic_wind, KSI_CALL_ARG3, 3, env);

    ksi_defun(L"procedure?", ksi_procedure_p, KSI_CALL_ARG1, 1, env);
    ksi_defsym(L"apply", ksi_data->apply_proc, env);
    ksi_defsym(L"call-with-values", ksi_data->call_vs_proc, env);
    ksi_defsym(L"call-with-current-continuation", ksi_data->call_cc_proc, env);
    ksi_defsym(L"call/cc", ksi_data->call_cc_proc, env);


    env = ksi_get_lib_env(L"ksi", L"core", L"list", 0);
    ksi_defsym(L"pair?", ksi_data->pairp_proc, env);
    ksi_defsym(L"null?", ksi_data->nullp_proc, env);
    ksi_defsym(L"list?", ksi_data->listp_proc, env);

    ksi_defsym(L"list", ksi_data->list_proc, env);
    ksi_defsym(L"cons", ksi_data->cons_proc, env);
    ksi_defun(L"cons*", ksi_cons_a, KSI_CALL_REST0, 1, env);
    ksi_defun(L"xcons", ksi_xcons, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"acons", ksi_acons, KSI_CALL_ARG3, 3, env);
    ksi_defun(L"make-list", ksi_make_list, KSI_CALL_ARG2, 1, env);

    ksi_defsym(L"car", ksi_data->car_proc, env);
    ksi_defsym(L"cdr", ksi_data->cdr_proc, env);
    {
      int l, x;
      /* c(a|d)+r defined separately */
      for (l = 2; l <= 4; ++l) {
	for (x = 0; x < (1 << l); ++x) {
	  ksi_obj args[1];
	  wchar_t *name = ksi_malloc_data((l+3) * sizeof(*name));
	  int i = 0;

	  for (name[0] = L'c'; i++ < l; )
	    name[i] = x & (1 << (l-i)) ? L'd' : L'a';
	  name[i++] = L'r';
	  name[i] = 0;

	  args[0] = (ksi_obj) name;
	  ksi_defsym(name, ksi_close_proc((ksi_obj) ksi_new_prim(name, eval_cxr, KSI_CALL_ARG2, 2), 1, args), env);
	}
      }
    }

    ksi_defun(L"set-car!", ksi_set_car_x, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"set-cdr!", ksi_set_cdr_x, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"list-set!", ksi_list_set_x, KSI_CALL_ARG3, 3, env);

    ksi_defun(L"list-head", ksi_list_head, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"list-tail", ksi_list_tail, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"last-pair", ksi_last_pair, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"list-ref", ksi_list_ref, KSI_CALL_ARG2, 2, env);

    ksi_defun(L"length", ksi_length, KSI_CALL_ARG1, 1, env);
    ksi_defsym(L"append", ksi_data->append_proc, env);
    ksi_defun(L"append!", ksi_append_x, KSI_CALL_REST0, 0, env);
    ksi_defun(L"reverse", ksi_reverse, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"reverse!", ksi_reverse_x, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"copy-list", ksi_copy_list, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"copy-tree", ksi_copy_tree, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"map", ksi_map, KSI_CALL_REST1, 2, env);
    ksi_defun(L"for-each", ksi_for_each, KSI_CALL_REST1, 2, env);
    ksi_defun(L"fold-left", ksi_fold_left, KSI_CALL_REST2, 3, env);
    ksi_defun(L"fold-right", ksi_fold_right, KSI_CALL_REST2, 3, env);

    ksi_defun(L"remp", ksi_remp, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"remq", ksi_remq, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"remv", ksi_remv, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"remove", ksi_remove, KSI_CALL_ARG2, 2, env);

    ksi_defun(L"memp", ksi_memp, KSI_CALL_ARG2, 2, env);
    ksi_defsym(L"memq", ksi_data->memq_proc, env);
    ksi_defsym(L"memv", ksi_data->memv_proc, env);
    ksi_defsym(L"member", ksi_data->member_proc, env);

    ksi_defun(L"assp", ksi_assp, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"assq", ksi_assq, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"assv", ksi_assv, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"assoc", ksi_assoc, KSI_CALL_ARG2, 2, env);

    ksi_defun(L"assp-ref", ksi_assp_ref, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"assq-ref", ksi_assq_ref, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"assv-ref", ksi_assv_ref, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"assoc-ref", ksi_assoc_ref, KSI_CALL_ARG2, 2, env);

    ksi_defun(L"assp-set!", ksi_assp_set_x, KSI_CALL_ARG3, 3, env);
    ksi_defun(L"assq-set!", ksi_assq_set_x, KSI_CALL_ARG3, 3, env);
    ksi_defun(L"assv-set!", ksi_assv_set_x, KSI_CALL_ARG3, 3, env);
    ksi_defun(L"assoc-set!", ksi_assoc_set_x, KSI_CALL_ARG3, 3, env);

    ksi_defun(L"assp-rem!", ksi_assp_rem_x, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"assq-rem!", ksi_assq_rem_x, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"assv-rem!", ksi_assv_rem_x, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"assoc-rem!", ksi_assoc_rem_x, KSI_CALL_ARG2, 2, env);


    env = ksi_get_lib_env(L"ksi", L"core", L"number", 0);
    ksi_defun(L"number?", ksi_number_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"complex?", ksi_complex_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"real?", ksi_real_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"rational?", ksi_rational_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"integer?", ksi_integer_p, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"exact?", ksi_exact_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"inexact?", ksi_inexact_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"inexact", ksi_inexact, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"exact", ksi_exact, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"=", ksi_num_eq_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"<", ksi_num_lt_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L">", ksi_num_gt_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"<=", ksi_num_le_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L">=", ksi_num_ge_p, KSI_CALL_REST0, 0, env);

    ksi_defun(L"zero?", ksi_zero_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"positive?", ksi_positive_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"negative?", ksi_negative_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"odd?", ksi_odd_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"even?", ksi_even_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"finite?", ksi_finite_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"infinite?", ksi_infinite_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"nan?", ksi_nan_p, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"max", ksi_max, KSI_CALL_REST0, 1, env);
    ksi_defun(L"min", ksi_min, KSI_CALL_REST0, 1, env);
    ksi_defun(L"abs", ksi_abs, KSI_CALL_ARG1,  1, env);

    ksi_defun(L"+", ksi_plus, KSI_CALL_REST0, 0, env);
    ksi_defun(L"-", ksi_minus, KSI_CALL_REST0, 1, env);
    ksi_defun(L"*", ksi_multiply, KSI_CALL_REST0, 0, env);
    ksi_defun(L"/", ksi_divide, KSI_CALL_REST0, 1, env);

    ksi_defun(L"exact-div", ksi_exact_div, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"div", ksi_idiv, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"mod", ksi_imod, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"div-and-mod", ksi_idiv_and_mod, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"div-and-mod*", ksi_idiv_and_mod_who, KSI_CALL_ARG3, 3, env);

    ksi_defun(L"gcd", ksi_gcd, KSI_CALL_REST0, 0, env);
    ksi_defun(L"lcm", ksi_lcm, KSI_CALL_REST0, 0, env);

    ksi_defun(L"numerator", ksi_numerator, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"denominator", ksi_denominator, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"floor", ksi_floor, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"ceiling", ksi_ceiling, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"truncate", ksi_truncate, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"round", ksi_round, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"exp", ksi_exp, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"log", ksi_log, KSI_CALL_ARG2, 1, env);
    ksi_defun(L"sin", ksi_sin, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"cos", ksi_cos, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"tan", ksi_tan, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"sinh", ksi_sinh, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"cosh", ksi_cosh, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"tanh", ksi_tanh, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"asin", ksi_asin, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"acos", ksi_acos, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"atan", ksi_atan, KSI_CALL_ARG2, 1, env);
    ksi_defun(L"asinh", ksi_asinh, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"acosh", ksi_acosh, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"atanh", ksi_atanh, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"sqrt", ksi_sqrt, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"exact-integer-sqrt", ksi_exact_sqrt, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"expt", ksi_expt, KSI_CALL_ARG2, 2, env);

    ksi_defun(L"make-rectangular", ksi_make_rectangular, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"make-polar", ksi_make_polar, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"real-part", scm_real_part, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"imag-part", scm_imag_part, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"magnitude", ksi_abs, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"angle", scm_angle, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"number->string", ksi_number2string, KSI_CALL_ARG2, 1, env);
    ksi_defun(L"string->number", ksi_string2number, KSI_CALL_ARG2, 1, env);

    ksi_defun(L"lognot", ksi_lognot, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"logtest", ksi_logtest, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"logior", ksi_logior, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"logxor", ksi_logxor, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"logand", ksi_logand, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"logbit?", ksi_logbit_p, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"ash", ksi_ash, KSI_CALL_ARG2, 2, env);


    env = ksi_get_lib_env(L"ksi", L"core", L"char", 0);
    ksi_defun(L"char?", ksi_char_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"char=?", ksi_char_eq_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"char<?", ksi_char_lt_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"char>?", ksi_char_gt_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"char<=?", ksi_char_le_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"char>=?", ksi_char_ge_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"char-ci=?", ksi_char_ci_eq_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"char-ci<?", ksi_char_ci_lt_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"char-ci>?", ksi_char_ci_gt_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"char-ci<=?", ksi_char_ci_le_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"char-ci>=?", ksi_char_ci_ge_p, KSI_CALL_REST0, 0, env);

    ksi_defun(L"char-alphabetic?", ksi_char_alpha_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"char-numeric?", ksi_char_digit_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"char-whitespace?", ksi_char_space_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"char-upper-case?", ksi_char_upper_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"char-lower-case?", ksi_char_lower_p, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"integer->char", ksi_integer2char, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"char->integer", ksi_char2integer, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"char-upcase", ksi_char_upcase, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"char-downcase", ksi_char_downcase, KSI_CALL_ARG1, 1, env);


    env = ksi_get_lib_env(L"ksi", L"core", L"string", 0);
    ksi_defun(L"string?", ksi_string_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"make-string", scm_make_string, KSI_CALL_ARG2, 1, env);
    ksi_defun(L"string-length", ksi_string_length, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"string-ref", ksi_string_ref, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"string-set!", ksi_string_set_x, KSI_CALL_ARG3, 3, env);
    ksi_defun(L"substring", ksi_substring, KSI_CALL_ARG3, 2, env);
    ksi_defun(L"string-copy", ksi_string_copy, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"string-fill!", ksi_string_fill_x, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"string", ksi_new_string, KSI_CALL_REST0, 0, env);
    ksi_defun(L"string-append", ksi_string_append, KSI_CALL_REST0, 0, env);
    ksi_defun(L"string-for-each", ksi_vector_for_each, KSI_CALL_REST2, 2, env);

    ksi_defun(L"string->list", ksi_string2list, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"list->string", ksi_list2string, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"symbol->string", ksi_symbol2string, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"string->symbol", ksi_string2symbol, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"string->keyword", ksi_string2keyword, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"keyword->string", ksi_keyword2string, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"string=?", ksi_string_eq_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"string-ci=?", ksi_string_ci_eq_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"string<?", ksi_string_ls_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"string>?", ksi_string_gt_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"string<=?", ksi_string_le_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"string>=?", ksi_string_ge_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"string-ci<?", ksi_string_ci_ls_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"string-ci>?", ksi_string_ci_gt_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"string-ci<=?", ksi_string_ci_le_p, KSI_CALL_REST0, 0, env);
    ksi_defun(L"string-ci>=?", ksi_string_ci_ge_p, KSI_CALL_REST0, 0, env);

    // non RNRS string utils
    ksi_defun(L"string-index", ksi_string_index, KSI_CALL_ARG3, 2, env);
    ksi_defun(L"string-rindex", ksi_string_rindex, KSI_CALL_ARG3, 2, env);
    ksi_defun(L"string-upcase!", ksi_string_upcase_x, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"string-downcase!", ksi_string_downcase_x, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"string-capitalize!", ksi_string_capitalize_x, KSI_CALL_ARG1, 1, env);


    env = ksi_get_lib_env(L"ksi", L"core", L"bytevector", 0);
    ksi_defun(L"native-endianness", ksi_native_endianness, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"bytevector?", ksi_bytevector_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"make-bytevector", ksi_make_bytevector, KSI_CALL_ARG2, 1, env);
    ksi_defun(L"bytevector-length", ksi_bytevector_length, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"bytevector=?", ksi_bytevector_eqv_p, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"bytevector-fill!", ksi_bytevector_fill_x, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"bytevector-copy!", ksi_bytevector_copy_x, KSI_CALL_ARG5, 5, env);
    ksi_defun(L"bytevector-copy", ksi_bytevector_copy, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"bytevector-u8-ref", ksi_bytevector_u8_ref, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"bytevector-u8-set!", ksi_bytevector_u8_set_x, KSI_CALL_ARG3, 3, env);
    ksi_defun(L"bytevector-s8-ref", ksi_bytevector_s8_ref, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"bytevector-s8-set!", ksi_bytevector_s8_set_x, KSI_CALL_ARG3, 3, env);
    ksi_defun(L"bytevector-float-ref", ksi_bytevector_float_ref, KSI_CALL_ARG3, 2, env);
    ksi_defun(L"bytevector-float-set!", ksi_bytevector_float_set_x, KSI_CALL_ARG4, 3, env);
    ksi_defun(L"bytevector-double-ref", ksi_bytevector_double_ref, KSI_CALL_ARG3, 2, env);
    ksi_defun(L"bytevector-double-set!", ksi_bytevector_double_set_x, KSI_CALL_ARG4, 3, env);


    env = ksi_get_lib_env(L"ksi", L"core", L"vector", 0);
    ksi_defsym(L"vector?", ksi_data->vectorp_proc, env);
    ksi_defsym(L"vector", ksi_data->vector_proc, env);
    ksi_defsym(L"list->vector", ksi_data->list2vector_proc, env);
    ksi_defun(L"vector->list", ksi_vector2list, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"make-vector", ksi_make_vector, KSI_CALL_ARG2, 1, env);
    ksi_defun(L"vector-length", ksi_vector_length, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"vector-ref", ksi_vector_ref, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"vector-set!", ksi_vector_set_x, KSI_CALL_ARG3, 3, env);
    ksi_defun(L"vector-fill!", ksi_vector_fill_x, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"vector-map", ksi_vector_map, KSI_CALL_REST2, 2, env);
    ksi_defun(L"vector-for-each", ksi_vector_for_each, KSI_CALL_REST2, 2, env);


    env = ksi_get_lib_env(L"ksi", L"core", L"system", 0);
    ksi_defun(L"copyright", ksi_copyright, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"banner", ksi_banner, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"version", ksi_scm_version, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"major-version", ksi_scm_major_version, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"minor-version", ksi_scm_minor_version, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"patch-level", ksi_scm_patch_level, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"cpu", ksi_scm_cpu, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"os", ksi_scm_os, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"host", ksi_scm_host, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"lib-path", lib_path, KSI_CALL_ARG0, 0, env);

    ksi_defun(L"getenv", ksi_getenv, KSI_CALL_ARG1, 0, env);
    ksi_defun(L"system", ksi_syscall, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"getcwd", ksi_getcwd, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"chdir", ksi_chdir, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"mkdir", ksi_mkdir, KSI_CALL_ARG2, 1, env);
    ksi_defun(L"rmdir", ksi_rmdir, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"stat", ksi_stat, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"file-exists?", ksi_file_exists, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"delete-file", ksi_delete_file, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"rename-file", ksi_rename_file, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"opendir", ksi_opendir, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"readdir", ksi_readdir, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"closedir", ksi_closedir, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"path-list->file-name", ksi_exp_fname, KSI_CALL_ARG2, 1, env);
    ksi_defun(L"split-file-name", ksi_split_fname, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"split-path", ksi_split_path, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"file-name-has-suffix?", ksi_has_suffix_p, KSI_CALL_ARG2, 2, env);


    env = ksi_get_lib_env(L"ksi", L"core", L"build", 0);
    ksi_defun(L"install-include-dir", inst_include_dir, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"install-lib-dir", inst_lib_dir, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"install-bin-dir", inst_bin_dir, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"install-info-dir", inst_info_dir, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"install-man-dir", inst_man_dir, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"build-cflags", build_cflags, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"build-libs", build_libs, KSI_CALL_ARG0, 0, env);


    env = ksi_get_lib_env(L"ksi", L"core", L"gc", 0);
    ksi_defun(L"gc-expand", gc_expand_heap, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"gc-set-size!", gc_set_heap_size, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"gc-size", gc_heap_size, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"gc-free", gc_free_size, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"gc-collections", gc_collections, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"gc-collect", gc_collect, KSI_CALL_ARG0, 0, env);


    env = ksi_get_lib_env(L"ksi", L"core", L"eval", 0);
    ksi_defsyntax(ksi_data->sym_defmacro, ksi_new_core(KSI_TAG_DEFSYNTAX), env, 1);
    ksi_defun(L"eval", ksi_eval, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"environment", ksi_environment, KSI_CALL_REST0, 0, env);
    ksi_defun(L"compile", ksi_comp, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"closure-environment", ksi_closure_env, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"closure-body", ksi_closure_body, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"closure?", ksi_closure_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"procedure-arity", ksi_procedure_arity, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"procedure-has-arity?", ksi_procedure_has_arity_p, KSI_CALL_ARG3, 2, env);

    ksi_defun(L"environment?", ksi_env_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"bound?", ksi_bound_p, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"exported?", ksi_exported_p, KSI_CALL_ARG3, 2, env);
    ksi_defun(L"environment-ref", ksi_var_ref, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"environment-set!", ksi_var_set, KSI_CALL_ARG3, 3, env);
    ksi_defun(L"environment-for-each", ksi_env_for_each, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"bound-identifier?", ksi_bound_identifier_p, KSI_CALL_ARG2, 2, env);

    ksi_defun(L"make-exn", ksi_scm_make_exn, KSI_CALL_ARG4,  3, env);
    ksi_defun(L"exn?", ksi_exn_p, KSI_CALL_ARG1,  1, env);
    ksi_defun(L"exn-type", ksi_exn_type, KSI_CALL_ARG1,  1, env);
    ksi_defun(L"exn-message", ksi_exn_message, KSI_CALL_ARG1,  1, env);
    ksi_defun(L"exn-value", ksi_exn_value, KSI_CALL_ARG1,  1, env);
    ksi_defun(L"exn-source", ksi_exn_source, KSI_CALL_ARG1,  1, env);
    ksi_defun(L"catch", ksi_catch, KSI_CALL_ARG3, 2, env);
    ksi_defun(L"catch-with-retry", ksi_catch_with_retry, KSI_CALL_ARG3, 2, env);
    ksi_defun(L"throw", ksi_throw, KSI_CALL_ARG2, 1, env);
    ksi_defun(L"exit", ksi_exit, KSI_CALL_ARG1, 0, env);

    ksi_defun(L"error", scm_error_proc, KSI_CALL_REST0, 0, env);
    ksi_defun(L"open-errlog", ksi_open_errlog, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"errlog-priority", ksi_errlog_priority, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"errlog", scm_errlog_proc, KSI_CALL_REST2, 2, env);
    ksi_defsym (L"errlog/error", ksi_long2num(KSI_ERRLOG_ERROR), env);
    ksi_defsym (L"errlog/warning", ksi_long2num(KSI_ERRLOG_WARNING), env);
    ksi_defsym (L"errlog/notice", ksi_long2num(KSI_ERRLOG_NOTICE), env);
    ksi_defsym (L"errlog/info", ksi_long2num(KSI_ERRLOG_INFO), env);
    ksi_defsym (L"errlog/debug", ksi_long2num(KSI_ERRLOG_DEBUG), env);
    ksi_defsym (L"errlog/all", ksi_long2num(KSI_ERRLOG_ALL), env);


    env = ksi_get_lib_env(L"ksi", L"core", L"time", 0);
    ksi_defun(L"current-time", ksi_scm_current_time, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"cpu-time", ksi_scm_cpu_time, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"eval-time", ksi_scm_eval_time, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"real-time", ksi_scm_real_time, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"gmtime", scm_gmtime, KSI_CALL_ARG1, 0, env);
    ksi_defun(L"localtime", scm_localtime, KSI_CALL_ARG1, 0, env);
    ksi_defun(L"strftime", scm_strftime, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"mktime", scm_mktime, KSI_CALL_ARG1, 1, env);


    env = ksi_get_lib_env(L"ksi", L"core", L"hashtables", 0);
    ksi_defun(L"make-eq-hashtable", ksi_make_eq_hashtab, KSI_CALL_ARG1, 0, env);
    ksi_defun(L"make-eqv-hashtable", ksi_make_eqv_hashtab, KSI_CALL_ARG1, 0, env);
    ksi_defun(L"make-hashtable", ksi_make_hashtab, KSI_CALL_ARG3, 2, env);
    ksi_defun(L"hashtable?", ksi_hashtab_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"hashtable-size", ksi_hashtab_size, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"hashtable-ref", ksi_hash_ref, KSI_CALL_ARG3, 2, env);
    ksi_defun(L"hashtable-set!", ksi_hash_set_x, KSI_CALL_ARG3, 3, env);
    ksi_defun(L"hashtable-delete!", ksi_hash_del_x, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"hashtable-contains?", ksi_hash_has_p, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"hashtable-for-each", ksi_hash_for_each, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"hashtable-copy", ksi_hash_copy, KSI_CALL_ARG2, 1, env);
    ksi_defun(L"hashtable-clear!", ksi_hash_clear, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"hashtable-equivalence-function", ksi_hash_eq_fun, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"hashtable-hash-function", ksi_hash_hash_fun, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"hashtable-mutable?", ksi_hash_mutable_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"equal-hash", ksi_equal_hash, KSI_CALL_ARG1, 1, env);


    env = ksi_get_lib_env(L"ksi", L"core", L"io", 0);
    ksi_defun(L"setlocale", ksi_setlocale, KSI_CALL_ARG2, 1, env);

    ksi_defun(L"current-input-port", ksi_current_input_port, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"current-output-port", ksi_current_output_port, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"current-error-port", ksi_current_error_port, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"set-current-input-port", ksi_set_current_input_port, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"set-current-output-port", ksi_set_current_output_port, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"set-current-error-port", ksi_set_current_error_port, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"null-port", ksi_null_port, KSI_CALL_ARG0, 0, env);

    ksi_defun(L"open-bytevector-input-port", ksi_open_bytevector_input_port, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"open-bytevector-output-port", ksi_open_bytevector_output_port, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"extract-bytevector-port-data", ksi_extract_bytevector_port_data, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"open-string-input-port", ksi_open_string_input_port, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"open-string-output-port", ksi_open_string_output_port, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"extract-string-port-data", ksi_extract_string_port_data, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"file-port?", ksi_fd_port_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"open-file-input-port", ksi_open_file_input_port, KSI_CALL_ARG3, 2, env);
    ksi_defun(L"open-file-output-port", ksi_open_file_output_port, KSI_CALL_ARG3, 2, env);
    ksi_defun(L"open-file-input/output-port", ksi_open_file_inout_port, KSI_CALL_ARG3, 2, env);

    ksi_defun(L"transcoded-port?", ksi_transcoded_port_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"transcoded-port", ksi_transcoded_port, KSI_CALL_ARG4, 2, env);
    ksi_defun(L"port-codec", ksi_port_codec, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"set-port-codec!", ksi_set_port_codec_x, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"port-eol-style", ksi_port_eol_style, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"port-error-handling-mode", ksi_port_error_handling_mode, KSI_CALL_ARG1, 1, env);

    ksi_defun(L"eof-object", ksi_eof_object, KSI_CALL_ARG0, 0, env);
    ksi_defun(L"eof-object?", ksi_eof_object_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"port?", ksi_port_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"textual-port?", ksi_text_port_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"binary-port?", ksi_bin_port_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"input-port?", ksi_input_port_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"output-port?", ksi_output_port_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"port-has-port-position?", ksi_port_can_getpos_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"port-position", ksi_port_getpos, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"port-has-set-port-position!?", ksi_port_can_setpos_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"set-port-position!", ksi_port_setpos, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"close-port", ksi_close_port, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"flush-output-port", ksi_flush_output_port, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"port-buffer-mode", ksi_port_buffer_mode, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"port-eof?", ksi_port_eof_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"get-u8", scm_get_u8, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"lookahead-u8", scm_lookahead_u8, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"get-bytevector-n!", ksi_get_bytevector, KSI_CALL_ARG4, 4, env);
    ksi_defun(L"put-u8", scm_put_u8, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"put-bytevector", ksi_put_bytevector, KSI_CALL_ARG4, 2, env);
    ksi_defun(L"get-char", scm_get_char, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"lookahead-char", scm_lookahead_char, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"get-string-n!", ksi_get_string, KSI_CALL_ARG4, 4, env);
    ksi_defun(L"get-line", ksi_get_line, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"get-datum", ksi_get_datum, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"put-char", scm_put_char, KSI_CALL_ARG2, 2, env);
    ksi_defun(L"put-string", ksi_put_string, KSI_CALL_ARG4, 2, env);
    ksi_defun(L"put-datum", ksi_get_datum, KSI_CALL_ARG2, 2, env);

    ksi_defun(L"object->string", ksi_object2string, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"format", scm_format_proc, KSI_CALL_REST2, 2, env);
    ksi_defun(L"write", ksi_write, KSI_CALL_ARG2, 1, env);
    ksi_defun(L"display", ksi_display, KSI_CALL_ARG2, 1, env);
    ksi_defun(L"newline", ksi_newline, KSI_CALL_ARG1, 0, env);
    ksi_defun(L"write-char", ksi_write_char, KSI_CALL_ARG2, 2, env);

    ksi_defun(L"read-char", ksi_read_char, KSI_CALL_ARG1, 0, env);
    ksi_defun(L"peek-char", ksi_read_char, KSI_CALL_ARG1, 0, env);
    ksi_defun(L"read", ksi_read, KSI_CALL_ARG1, 0, env);
    ksi_defun(L"annotation?", scm_annotation_p, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"annotation-source", scm_annotation_source, KSI_CALL_ARG1, 1, env);
    ksi_defun(L"annotation-expression", scm_annotation_expression, KSI_CALL_ARG1, 1, env);


    ksi_init_klos();
    ksi_init_dynl();
  }

  return data;
}

void
ksi_init_context (ksi_context_t context, void *top_stack)
{
    ksi_internal_data();

    memset(context, 0, sizeof *context);

    context->stack = top_stack;
    context->errlog_priority = KSI_ERRLOG_NOTICE;
/*        context->errlog_priority = KSI_ERRLOG_DEBUG;
 */

    context->errlog_port = 0;
    context->input_port = ksi_data->input_port;
    context->output_port = ksi_data->output_port;
    context->error_port = ksi_data->error_port;
}

ksi_context_t
ksi_set_current_context (ksi_context_t ctx)
{
    ksi_context_t old_ctx = ksi_current_context;
    ksi_current_context = ctx;
    return old_ctx;
}

void
ksi_term (void)
{
    if (!ksi_current_context)
        return;

    ksi_flush_output_port((ksi_obj) ksi_current_context->output_port);
    ksi_flush_output_port((ksi_obj) ksi_current_context->error_port);

    ksi_term_dynl();

    ksi_current_context = 0;
    ksi_gcollect(1);
}


/* End of code */
