/*
 * test.c
 *
 * Copyright (C) 2009-2011, ivan demakov.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Sun Mar 22 16:18:42 2009
 *
 */

#include "ksi.h"

#include <stdio.h>


int
main(int argc, char *argv[])
{
    struct Ksi_Context ctx;
    ksi_obj x;

    printf("libdir=%s\n", ksi_scheme_lib_dir());

    ksi_init_std_ports(0, 1, 2);
    ksi_init_context(&ctx, 0);
    ksi_set_current_context(&ctx);

    ksi_setlocale(ksi_str02sym(L"LC_ALL"), ksi_str02string(L""));

    x = ksi_nil;
    x = ksi_cons(ksi_str2bytevector("12ф", 3), x);
    x = ksi_cons(ksi_long2num(999), x);
    x = ksi_cons(ksi_double2num(1.2345), x);
    x = ksi_cons(ksi_make_vector(ksi_long2num(2), ksi_long2num(2)), x);
    x = ksi_cons(ksi_str02sym(L"hehe"), x);
    x = ksi_cons(ksi_str02string(L"фыва"), x);
    x = KSI_LIST2(ksi_str02sym(L"x"), x);

    wchar_t *msg = ksi_aprintf("obj=%ls", ksi_obj2str(x));
    printf("%ls\n", msg);

    ksi_env base_env = ksi_get_lib_env(L"ksi", L"core", L"syntax", 0);
    ksi_env io_env = ksi_get_lib_env(L"ksi", L"core", L"io", 0);
    ksi_env list_env = ksi_get_lib_env(L"ksi", L"core", L"list", 0);
    ksi_env env = ksi_top_level_env();
    ksi_import(base_env, ksi_str02sym(L"let"), env, 0);
    ksi_import(base_env, ksi_str02sym(L"if"), env, 0);
    ksi_import(base_env, ksi_str02sym(L"begin"), env, 0);
    ksi_import(list_env, ksi_str02sym(L"list"), env, 0);
    ksi_import(list_env, ksi_str02sym(L"pair?"), env, 0);
    ksi_import(list_env, ksi_str02sym(L"car"), env, 0);
    ksi_import(list_env, ksi_str02sym(L"cdr"), env, 0);
    ksi_import(io_env, ksi_str02sym(L"display"), env, 0);
    ksi_import(io_env, ksi_str02sym(L"newline"), env, 0);

    x = ksi_eval_string(L"(let loop ((a (list 7 8 9))) (if (pair? a) (begin (display (car a)) (loop (cdr a))) (newline)))", env, 0, 0, 0);
    printf("res=%ls\n", ksi_obj2str(x));

    return 0;
}

 /* End of code */
