/*
 * ksi_dynl.c
 * dynamic loading
 *
 * Copyright (C) 1997-2010, 2014, 2015, Ivan Demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Fri Aug 22 00:43:35 1997
 * Last Update:   Mon May 19 22:09:18 2014
 *
 */

#include "ksi_type.h"
#include "ksi_proc.h"
#include "ksi_printf.h"
#include "ksi_util.h"
#include "ksi_gc.h"


#if defined(unix) || defined(CYGWIN)
# define IS_DIR_SUFFIX(c) ((c) == '/')
#elif defined(MSDOS) || defined(WIN32) || defined(OS2)
# define IS_DIR_SUFFIX(c) ((c) == '/' || (c) == '\\')
#else
# define IS_DIR_SUFFIX(c) (0)
#endif


#define dynl_lib_list ksi_data->dynl_libs


#ifdef DYNAMIC_LINKING

#if defined(HAVE_LIBDL) || defined(HAVE_DLOPEN)

#include <dlfcn.h>
#include <ctype.h>


static inline const char*
ksi_dlerror ()
{
    const char* ptr = dlerror ();
    return ptr ? ptr : "dynamic linking failed";
}

static inline void*
ksi_dlopen (const char* fname, int mode)
{
    return dlopen (fname, mode);
}

static inline void
ksi_dlclose (void* handle)
{
    dlclose (handle);
}

static void*
ksi_dlsym (void* handle, const char* prefix, const char* name)
{
    int len = strlen(prefix) + strlen(name) + 2;
    char* buff = (char*) alloca(len);
    void* proc;

#if defined(USCORE) && !defined(DLSYM_ADDS_USCORE)
    buff[0] = '_';
    strcpy(buff+1, prefix);
    strcat(buff, name);
    proc = dlsym(handle, buff);
#else
    strcpy(buff, prefix);
    strcat(buff, name);
    proc = dlsym(handle, buff);
#endif
    return proc;
}

#elif defined(HAVE_LIBDLD)

#include <dld.h>

static inline const char*
ksi_dlerror ()
{
    const char* ptr = dld_strerror (dld_errno);
    return ptr ? ptr : "dynamic linking failed";
}

static inline void*
ksi_dlopen (const char* fname, int mode)
{
    int status = dld_link (fname);
    return status ? 0 : fname;
}

static inline void
ksi_dlclose (void* handle)
{
    int status = dld_unlink_by_file ((char*) handle, 1);
}

static void*
ksi_dlsym (void* handle, const char* prefix, const char* name)
{
    int len = strlen(prefix) + strlen(name) + 2;

    char* buff = (char*) alloca(len);
    void* proc;

    strcpy(buff, prefix);
    strcat(buff, name);
    proc = dld_get_func(buff);

    if (proc && !dld_function_executable_p (proc)) {
        proc = 0;
    }

    return proc;
}

#elif defined(MSWIN32)

static inline const char*
ksi_dlerror ()
{
    return ksi_get_last_error ("dynamic linking failed");
}

static inline void*
ksi_dlopen (const char* fname, int mode)
{
    return (void*) LoadLibrary (fname);
}

static inline void
ksi_dlclose (void* handle)
{
    FreeLibrary ((HINSTANCE) handle);
}

static void*
ksi_dlsym (void* handle, const char* prefix, const char* name)
{
    int len = strlen(prefix) + strlen(name) + 1;
    char* buff = (char*) alloca(len);
    void* proc;

    strcpy(buff, prefix);
    strcat(buff, name);
    proc = GetProcAddress((HINSTANCE) handle, buff);
    return proc;
}

#else

static inline const char*
ksi_dlerror ()
{
    return "dynamic linking not implemented";
}

static inline void*
ksi_dlopen (char* fname, int mode)
{
    return 0;
}

static inline void*
ksi_dlsym (void* handle, char* prefix, char* name)
{
    return 0;
}

#endif

#ifndef RTLD_LAZY
# define RTLD_LAZY 1
#endif
#ifndef RTLD_GLOBAL
# define RTLD_GLOBAL 0
#endif


struct Ksi_Func
{
    struct Ksi_EObj ko;

    struct Ksi_Dynl_Lib *module;
    const char *name;
    void *proc;
};


static const wchar_t *
func_print (struct Ksi_EObj *x, int slashify)
{
    struct Ksi_Func* func = (struct Ksi_Func*) x;

    if (func->proc)
        return ksi_aprintf("#<dynamic-func %s in %s>", func->name, func->module->name);
    else
        return ksi_aprintf("#<unlinked-func %s in %s>", func->name, func->module->name);
}

static struct Ksi_ETag tc_func =
{
    L"dynamic-func",
    ksi_default_tag_equal,
    func_print
};

#define DYNFUNC_P(x) (KSI_EXT_IS (x, &tc_func))


static char*
fname2pname (char* fname)
{
    char *module = fname, *ptr;
    for (ptr = fname; *ptr; ptr++)
        if (IS_DIR_SUFFIX(*ptr))
            module = ptr + 1;

    if (module[0] == 'l' && module[1] == 'i' && module[2] == 'b')
        module += 3;

    ptr = ksi_strdup(module);
    for (module = ptr; *ptr; ptr++) {
        if (*ptr == '.') {
            *ptr = '\0';
            break;
        }
        if (!isalpha(*ptr) && !isdigit(*ptr))
            *ptr = '_';
    }

    return module;
}

const wchar_t *
ksi_dynload_file (char* fname)
{
    struct Ksi_Dynl_Lib* lib;

    for (lib = ksi_data->dynl_libs; lib; lib = lib->next) {
        if (strcmp(fname, lib->name) == 0) {
            if (lib->count < 0)
                return L"";
            break;
        }
    }

    if (!lib) {
        lib = (struct Ksi_Dynl_Lib*) ksi_malloc(sizeof (*lib));
        lib->next = dynl_lib_list;
        dynl_lib_list = lib;
        lib->name = ksi_strdup(fname);
    }

    if (!lib->handle) {
        ksi_debug("try load dynamic library %s", fname);
        void* dl = ksi_dlopen(lib->name, RTLD_LAZY | RTLD_GLOBAL);
        if (!dl) {
            lib->count = -1;
            return L"";
        }
        lib->handle = dl;
        lib->count = 1;
    } else {
        lib->count += 1;
    }

    if (lib->count == 1) {
        /* library loaded first time, need initialization. */
        char *pname = fname2pname(lib->name);
        void (*init) () = (void (*)()) ksi_dlsym(lib->handle, "ksi_init_", pname);
        if (!init) {
            const char* msg = ksi_dlerror();
            wchar_t *ptr = ksi_aprintf("%s: ksi_init_%s", msg, pname);

            ksi_dlclose(lib->handle);
            lib->handle = 0;
            lib->count = -1;

            return ptr;
        }

        init();
    }

    return 0;
}


static ksi_obj
ksi_make_func (struct Ksi_Dynl_Lib* module, const char* name, void* proc)
{
  struct Ksi_Func* func;
  func = (struct Ksi_Func*) ksi_malloc (sizeof *func);
  func->ko.o.itag = KSI_TAG_EXTENDED;
  func->ko.etag = &tc_func;
  func->module = module;
  func->name = name;
  func->proc = proc;

  return (ksi_obj) func;
}


ksi_obj
ksi_dynamic_link (ksi_obj mod, ksi_obj sym)
{
    struct Ksi_Dynl_Lib* lib;
    const char *fname, *func;
    void *proc;
    char *ptr;

    KSI_CHECK(mod, KSI_STR_P(mod), "dynamic-link: invalid string in arg1");
    KSI_CHECK(sym, KSI_STR_P(sym), "dynamic-link: invalid string in arg2");

    fname = ksi_local(KSI_STR_PTR(mod));
    func = ksi_local(KSI_STR_PTR(sym));

#ifdef KSI_DL_SUFFIX
    if (!ksi_has_suffix(fname, KSI_DL_SUFFIX)) {
        ptr = (char*) ksi_malloc_data(strlen(fname) + strlen(KSI_DL_SUFFIX) + 1);
        strcpy(ptr, fname);
        strcat(ptr, KSI_DL_SUFFIX);
        fname = ptr;
    }
#endif

    fname = ksi_expand_file_name(fname);

    for (lib = dynl_lib_list; lib; lib = lib->next) {
        if (strcmp(fname, lib->name) == 0)
            break;
    }
    if (!lib) {
        lib = (struct Ksi_Dynl_Lib*) ksi_malloc (sizeof (*lib));
        lib->next = dynl_lib_list;
        dynl_lib_list = lib;
        ptr = ksi_strdup(fname);
        lib->name = ptr;
    }

    if (lib->handle == 0) {
        void* dl = ksi_dlopen(lib->name, RTLD_LAZY);
        if (!dl)
            ksi_exn_error(0, 0, "%s: dynamic-link: %s", fname, ksi_dlerror());
        lib->handle = dl;
    }

    proc = ksi_dlsym(lib->handle, "", func);
    if (!proc) {
        if (lib->count == 0) {
            ksi_dlclose(lib->handle);
            lib->handle = 0;
        }

        ksi_exn_error(0, 0, "%s: dynamic-link: %s", func, ksi_dlerror());
    }

    lib->count += 1;
    if (lib->count == 1) {
        /* library loaded first time, need initialization. */
        char* pname = fname2pname (lib->name);
        void (*init) () = (void (*)()) ksi_dlsym (lib->handle, "ksi_init_", pname);
        if (init)
            init ();
    }

    return ksi_make_func (lib, func, proc);
}

ksi_obj
ksi_dynamic_unlink (ksi_obj func)
{
    struct Ksi_Dynl_Lib* lib;
    KSI_CHECK (func, DYNFUNC_P (func), "dynamic-unlink: invalid dynamic-func");

    lib = ((struct Ksi_Func*) func) -> module;
    lib->count -= 1;
    if (lib->count == 0) {
        char* pname = fname2pname (lib->name);
        void (*term) () = (void (*)()) ksi_dlsym (lib->handle, "ksi_term_", pname);
        if (term)
            term ();

        ksi_dlclose (lib->handle);
        lib->handle = 0;
    }

    ((struct Ksi_Func*) func) -> proc = 0;
    return ksi_void;
}

ksi_obj
ksi_dynamic_call (ksi_obj func, ksi_obj args)
{
    int i, num, argc = ksi_list_len(args);
    char **argv;
    int (*proc) (int argc, char** argv);
    ksi_obj *vals;

    KSI_CHECK (func, DYNFUNC_P (func), "dynamic-call: invalid dynamic-func");
    KSI_CHECK (func, ((struct Ksi_Func*) func)->proc, "dynamic-call: unlinked dynamic-func");
    KSI_CHECK (args, argc >= 0, "dynamic-call: invalid list in arg2");

    argv = alloca(sizeof(char*) * (argc + 1));
    for (num = 0; num < argc; num++, args = KSI_CDR (args)) {
        if (KSI_KEY_P(KSI_CAR(args))) {
            char *p = ksi_local(KSI_KEY_PTR(KSI_CAR(args)));
            int len = strlen(p);
            argv[num] = ksi_malloc_data(len + 2);
            argv[num][0] = '-';
            memcpy(argv[num] + 1, p, len+1);
        } else {
            argv[num] = ksi_local(ksi_obj2str(KSI_CAR(args)));
        }
    }
    argv[num] = 0;

    proc = (int (*) (int, char**)) (((struct Ksi_Func*) func) -> proc);
    num = proc(argc, argv);

    if (num < 0)
        ksi_exn_error(0, 0, "%ls: error code %d", ksi_obj2str(func), num);

    if (num == 0)
        return ksi_void;

    vals = (ksi_obj*) alloca(num * sizeof *vals);
    for (i = 0; i < num; i++) {
        if (argv[num][0] == '-')
            vals[i] = ksi_str02key(ksi_utf(argv[num] + 1));
        else
            vals[i] = ksi_str02obj(ksi_utf(argv[num]));
    }
    return ksi_new_values(num, vals);
}


static struct Ksi_Prim_Def defs [] =
{
  { L"dynamic-link",    ksi_dynamic_link,       KSI_CALL_ARG2, 2 },
  { L"dynamic-unlink",  ksi_dynamic_unlink,     KSI_CALL_ARG1, 1 },
  { L"dynamic-call",    ksi_dynamic_call,       KSI_CALL_ARG2, 2 },

  { 0 }
};

void
ksi_init_dynl (void)
{
    ksi_env env = ksi_get_lib_env(L"ksi", L"core", L"dynamic-library", 0);
    ksi_reg_unit (defs, env);
}

void
ksi_term_dynl ()
{
    while (dynl_lib_list) {
        struct Ksi_Dynl_Lib* lib = dynl_lib_list;
        if (lib->count > 0) {
            char* pname = fname2pname(lib->name);
            ksi_debug("try term dynamic library %s (%s)", lib->name, pname);
            void (*term) () = (void (*)()) ksi_dlsym(lib->handle, "ksi_term_", pname);
            if (term) term();
            ksi_dlclose(lib->handle);
            lib->handle = 0;
        }
        dynl_lib_list = lib->next;
    }
}

#else

void
ksi_init_dynl (void)
{
}

void
ksi_term_dynl ()
{
}

#endif

 /* End of code */
