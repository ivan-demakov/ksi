/*
 * ksi_pair.c
 * pairs
 *
 * Copyright (C) 1997-2010, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Fri Apr 25 02:42:06 1997
 * Last Update:   Sat Aug 14 15:27:19 2010
 *
 */

#include "ksi_type.h"
#include "ksi_env.h"
#include "ksi_proc.h"
#include "ksi_gc.h"


ksi_obj
ksi_cons (ksi_obj car, ksi_obj cdr)
{
    ksi_pair x = ksi_malloc(sizeof(*x));
    x -> o.itag = KSI_TAG_PAIR;
    x -> car = car;
    x -> cdr = cdr;
    return (ksi_obj) x;
}

ksi_obj
ksi_xcons (ksi_obj cdr, ksi_obj car)
{
  ksi_pair x = ksi_malloc(sizeof(*x));
  x -> o.itag = KSI_TAG_PAIR;
  x -> car = car;
  x -> cdr = cdr;
  return (ksi_obj) x;
}

ksi_obj
ksi_cons_a (int argc, ksi_obj* argv)
{
  ksi_obj args = argv[--argc];

  while (--argc >= 0)
    args = ksi_cons(argv[argc], args);
  return args;
}

ksi_obj
ksi_acons (ksi_obj car, ksi_obj cdr, ksi_obj alist)
{
    ksi_pair x = ksi_malloc(sizeof(*x));
    ksi_pair y = ksi_malloc(sizeof(*y));

    x -> o.itag = KSI_TAG_PAIR;
    x -> car = car;
    x -> cdr = cdr;

    y -> o.itag = KSI_TAG_PAIR;
    y -> car = (ksi_obj) x;
    y -> cdr = alist;

    return (ksi_obj) y;
}

int
ksi_list_len (ksi_obj x)
{
    int len = 0;
    ksi_obj z = x;
    do {
        if (x == ksi_nil)
            return len;

        len++;
        if (!KSI_PAIR_P (x))
            return -len;

        x = KSI_CDR (x);

        if (x == ksi_nil)
            return len;

        len++;
        if (!KSI_PAIR_P (x))
            return -len;

        x = KSI_CDR (x);
        z = KSI_CDR (z);

    } while (x != z);

    return -1;
}

ksi_obj
ksi_pair_p (ksi_obj x)
{
    return KSI_PAIR_P(x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_null_p (ksi_obj x)
{
    return KSI_NULL_P(x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_list_p (ksi_obj x)
{
    return KSI_LIST_P(x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_car (ksi_obj o)
{
    KSI_CHECK(o, KSI_PAIR_P(o), "car: invalid pair");
    return KSI_CAR (o);
}

ksi_obj
ksi_cdr (ksi_obj o)
{
    KSI_CHECK(o, KSI_PAIR_P(o), "cdr: invalid pair");
    return KSI_CDR(o);
}

ksi_obj
ksi_set_car_x (ksi_obj o, ksi_obj v)
{
    KSI_CHECK(o, KSI_M_PAIR_P(o), "set-car!: invalid or constant pair in arg1");

    KSI_CAR(o) = v;
    return ksi_void;
}

ksi_obj
ksi_set_cdr_x (ksi_obj o, ksi_obj v)
{
    KSI_CHECK(o, KSI_M_PAIR_P(o), "set-cdr!: invalid or constant pair in arg1");

    KSI_CDR(o) = v;
    return ksi_void;
}

ksi_obj
ksi_length (ksi_obj o)
{
    int len;

    if (o == ksi_nil)
        return ksi_long2num(0);

    KSI_CHECK(o, KSI_PAIR_P(o), "length: invalid list");

    len = ksi_list_len(o);
    KSI_CHECK(o, len > 0, "length: improper or circular list");

    return ksi_long2num(len);
}


/*
 * ksi_append
 *	Обьединяет списки
 *
 * Scheme procedure
 *	append argN ...
 *
 * Side effects
 *	Все, за исключением последнего, аргументы копируются.
 *	Последний список присоединяется, установкой cdr в предпоследнем
 *	списке.
 */

ksi_obj
ksi_append (int argc, ksi_obj* argv)
{
    ksi_obj list = ksi_nil, *val = &list;

    if (argc <= 0)
        return list;

    while (--argc > 0) {
        ksi_obj l, x = *argv++;
        for (l = x; l != ksi_nil; l = KSI_CDR(l)) {
            KSI_CHECK(x, KSI_PAIR_P(l), "append: invalid list");
            *val = ksi_cons(KSI_CAR(l), ksi_nil);
            val = & KSI_CDR(*val);
        }
    }

    *val = *argv;
    return list;
}

/*
 * ksi_append_x
 *	Соединяет списки.
 *
 * Scheme procedure
 *	append! argN ...
 *
 * Side effects
 *	Модифицируются cdr последней пары всех, за исключением
 *	последнего аргументов.
 */

ksi_obj
ksi_append_x (int argc, ksi_obj* argv)
{
  ksi_obj list = ksi_nil, *val = &list;

  if (argc <= 0)
    return ksi_nil;

  while (--argc > 0) {
    ksi_obj l, x = *argv++;
    *val = x;
    for (l = x; l != ksi_nil; l = KSI_CDR (l)) {
      KSI_CHECK (x, KSI_PAIR_P (l), "append!: invalid list");
      val = & KSI_CDR (l);
    }
  }

  *val = *argv;
  return list;
}


/*
 * ksi_reverse
 *	Создается новый список, с обратным расположением эл-тов.
 *	Старый список не изменяется.
 *
 * Scheme procedure
 *	reverse arg
 *
 */

ksi_obj
ksi_reverse (ksi_obj arg)
{
  ksi_obj x, list = ksi_nil;
  for (x = arg; x != ksi_nil; x = KSI_CDR (x)) {
    KSI_CHECK (arg, KSI_PAIR_P (x), "reverse: invalid list");
    list = ksi_cons (KSI_CAR (x), list);
  }

  return list;
}


/*
 * ksi_reverse_x
 *	Меняет порядок элементов в списке на противоположный.
 *
 * Scheme procedure
 *	reverse! arg
 *
 */

ksi_obj
ksi_reverse_x (ksi_obj list)
{
  /*
   * (define (reverse! list new-tail)
   *   (if (not (pair? list))
   *      list
   *      (let ((old-tail (cdr list)))
   *        (set-cdr! list new-tail)
   *          (if (null? old-tail)
   *             list
   *             (reverse! old-tail list)))))
   */

  ksi_obj old_tail, new_tail = ksi_nil;

again:
  if (!KSI_PAIR_P (list))
    return list;

  old_tail = KSI_CDR (list);
  KSI_CDR (list) = new_tail;
  if (old_tail == ksi_nil)
    return list;

  new_tail = list;
  list = old_tail;
  goto again;
}


/*
 * ksi_list_tail
 *	Находит хвост списка LST, пропуская NUM эл-тов
 *
 * Scheme procedure
 *	list-tail lst num
 */

ksi_obj
ksi_list_tail (ksi_obj lst, ksi_obj num)
{
  int k;

  KSI_CHECK (lst, lst == ksi_nil || KSI_PAIR_P (lst), "list-tail: invalid list in arg1");
  KSI_CHECK (num, KSI_EINT_P (num), "list-tail: invalid integer in arg2");

  k = ksi_num2long (num, "list-tail");
  KSI_CHECK (num, k >= 0, "list-tail: negative index in arg2");

  while (--k >= 0) {
    KSI_CHECK (num, KSI_PAIR_P (lst), "list-tail: too big index in arg2");
    lst = KSI_CDR (lst);
  }

  return lst;
}

/*
 * ksi_list_head
 *	Копирует NUM элементов списка LST
 *
 * Scheme procedure
 *	list-head num lst
 */

ksi_obj
ksi_list_head (ksi_obj lst, ksi_obj num)
{
  int k;
  ksi_obj res = ksi_nil, *loc = &res;

  KSI_CHECK (lst, lst == ksi_nil || KSI_PAIR_P (lst), "list-head: invalid list in arg1");
  KSI_CHECK (num, KSI_UINT_P (num), "list-head: invalid index in arg2");

  k = ksi_num2ulong (num, "list-head");

  while (--k >= 0) {
      KSI_CHECK (num, KSI_PAIR_P (lst), "list-head: too big index in arg2");
      *loc = ksi_cons(KSI_CAR(lst), ksi_nil);
      loc = & KSI_CDR (*loc);
      lst = KSI_CDR (lst);
    }

  return res;
}


/*
 * ksi_list_ref
 *	Находит NUM-ный эл-т в списке LST
 *
 * Scheme procedure
 *	list-ref arg1 arg2
 */

ksi_obj
ksi_list_ref (ksi_obj lst, ksi_obj num)
{
  int k;

  KSI_CHECK (lst, KSI_PAIR_P (lst), "list-ref: invalid list in arg1");
  KSI_CHECK (num, KSI_EINT_P (num), "list-ref: invalid index in arg2");

  k = ksi_num2long (num, "list-ref");
  KSI_CHECK (num, k >= 0, "list-ref: negative index in arg2");

  while (--k >= 0) {
    lst = KSI_CDR (lst);
    KSI_CHECK (num, KSI_PAIR_P (lst), "list-ref: too big index in arg2");
  }

  return KSI_CAR (lst);
}

/*
 * ksi_list_set_x
 *	Устанавливает NUM-ный эл-т в списке LST в VAL.
 *
 * Scheme procedure
 *	list-set! num lst val
 */

ksi_obj
ksi_list_set_x (ksi_obj lst, ksi_obj num, ksi_obj val)
{
  int k;

  KSI_CHECK (lst, KSI_PAIR_P (lst), "list-set!: invalid list in arg1");
  KSI_CHECK (num, KSI_EINT_P (num), "list-set!: invalid index in arg2");

  k = ksi_num2long (num, "list-set!");
  KSI_CHECK (num, k >= 0, "list-set!: negative index in arg2");

  while (--k >= 0) {
    lst = KSI_CDR (lst);
    KSI_CHECK (num, KSI_PAIR_P (lst), "list-set!: too big index in arg2");
  }

  KSI_CAR (lst) = val;
  return ksi_void;
}

ksi_obj
ksi_last_pair (ksi_obj z)
{
  ksi_obj res = z, x = z;

  if (z == ksi_nil)
    return ksi_nil;

  KSI_CHECK (z, KSI_PAIR_P (z), "last-pair: invalid pair in arg1");

  while (1) {
    x = KSI_CDR (x);
    if (!KSI_PAIR_P (x)) return res;
    res = x;
    x = KSI_CDR (x);
    if (!KSI_PAIR_P (x)) return res;
    res = x;
    z = KSI_CDR (z);
    KSI_CHECK (z, z != x, "last-pair: circular list in arg1");
  }

  return res;
}

/*
 * ksi_copy_list
 *	Копирует список и вектор "вширь" (рекурсия по cdr).
 *
 */

ksi_obj
ksi_copy_list (ksi_obj x)
{
  if (KSI_VEC_P (x))
    return ksi_copy_vector (x);

  if (KSI_PAIR_P (x)) {
    ksi_obj lst, *val = &lst;
    do {
      *val = ksi_cons (KSI_CAR (x), ksi_nil);
      val = & KSI_CDR (*val);
      x = KSI_CDR (x);
    } while (KSI_PAIR_P (x));

    *val = x;
    return lst;
  }

  return x;
}

/*
 * ksi_copy_tree
 *	Копирует список и вектор "вглубь" (рекурсия по car и cdr);
 */

ksi_obj
ksi_copy_tree (ksi_obj x)
{
    if (KSI_VEC_P(x)) {
        int i, len = KSI_VEC_LEN(x);
        ksi_obj vec = ksi_alloc_vector(len, KSI_TAG_VECTOR);
        for (i = 0; i < len; ++i) {
            KSI_VEC_REF(vec, i) = ksi_copy_tree(KSI_VEC_REF (x, i));
        }
        return (ksi_obj) vec;
    }

    if (KSI_PAIR_P(x)) {
        ksi_obj lst, *val = &lst;
        do {
            *val = ksi_cons(ksi_copy_tree(KSI_CAR(x)), ksi_nil);
            val = &KSI_CDR(*val);
            x = KSI_CDR(x);
        } while (KSI_PAIR_P(x));

        *val = x;
        return lst;
    }

    return x;
}


ksi_obj
ksi_memq (ksi_obj o, ksi_obj list)
{
    ksi_obj x = list;
    while (list != ksi_nil) {
        KSI_CHECK (list, KSI_PAIR_P (list), "memq: improper list in arg2");
        if (o == KSI_CAR (list))
            return list;
        list = KSI_CDR (list);
        if (list == ksi_nil)
            break;

        KSI_CHECK (list, KSI_PAIR_P (list), "memq: improper list in arg2");
        if (o == KSI_CAR (list))
            return list;
        list = KSI_CDR (list);
        x = KSI_CDR (x);
        if (x == list)
            break;
    }

    return ksi_false;
}

ksi_obj
ksi_memv (ksi_obj o, ksi_obj list)
{
    ksi_obj x = list;
    while (list != ksi_nil) {
        KSI_CHECK (list, KSI_PAIR_P (list), "memv: improper list in arg2");
        if (KSI_EQV_P (o, KSI_CAR (list)))
            return list;
        list = KSI_CDR (list);
        if (list == ksi_nil)
            break;

        KSI_CHECK (list, KSI_PAIR_P (list), "memv: improper list in arg2");
        if (KSI_EQV_P (o, KSI_CAR (list)))
            return list;
        list = KSI_CDR (list);
        x = KSI_CDR (x);
        if (x == list)
            break;
    }

    return ksi_false;
}

ksi_obj
ksi_member (ksi_obj o, ksi_obj list)
{
    ksi_obj x = list;

    while (list != ksi_nil) {
        KSI_CHECK(list, KSI_PAIR_P(list), "member: improper list in arg2");
        if (KSI_EQUAL_P(o, KSI_CAR(list)))
            return list;

        list = KSI_CDR(list);
        if (list == ksi_nil)
            break;

        KSI_CHECK(list, KSI_PAIR_P(list), "member: improper list in arg2");
        if (KSI_EQUAL_P(o, KSI_CAR(list)))
            return list;

        list = KSI_CDR(list);
        x = KSI_CDR(x);
        if (x == list)
            break;
    }

    return ksi_false;
}

ksi_obj
ksi_memp (ksi_obj proc, ksi_obj list)
{
    ksi_obj x = list;

    KSI_CHECK(proc, KSI_PROC_P(proc), "memp: invalid procedure in arg1");
    if (ksi_procedure_has_arity_p(proc, ksi_long2num(1), 0) == ksi_false)
        ksi_exn_error(0, proc, "memp: invalid arity of the procedure in arg1");

    while (list != ksi_nil) {
        KSI_CHECK(list, KSI_PAIR_P(list), "memp: improper list in arg2");
        if (KSI_TRUE_P(ksi_apply_1(proc, KSI_CAR(list))))
            return list;

        list = KSI_CDR(list);
        if (list == ksi_nil)
            break;

        KSI_CHECK(list, KSI_PAIR_P(list), "memp: improper list in arg2");
        if (KSI_TRUE_P(ksi_apply_1(proc, KSI_CAR(list))))
            return list;

        list = KSI_CDR(list);
        x = KSI_CDR(x);
        if (x == list)
            break;
    }

    return ksi_false;
}

ksi_obj
ksi_remq (ksi_obj o, ksi_obj list)
{
    ksi_obj res = ksi_nil, *loc = &res;
    while (list != ksi_nil) {
        KSI_CHECK (list, KSI_PAIR_P (list), "remq: improper list in arg2");
        if (o != KSI_CAR (list)) {
            *loc = ksi_cons(KSI_CAR(list), ksi_nil);
            loc = & KSI_CDR (*loc);
        }
        list = KSI_CDR (list);
    }

    return res;
}

ksi_obj
ksi_remv (ksi_obj o, ksi_obj list)
{
    ksi_obj res = ksi_nil, *loc = &res;
    while (list != ksi_nil) {
        KSI_CHECK (list, KSI_PAIR_P (list), "remv: improper list in arg2");
        if (!KSI_EQV_P (o, KSI_CAR(list))) {
            *loc = ksi_cons(KSI_CAR(list), ksi_nil);
            loc = & KSI_CDR (*loc);
        }
        list = KSI_CDR (list);
    }

    return res;
}

ksi_obj
ksi_remove (ksi_obj o, ksi_obj list)
{
    ksi_obj res = ksi_nil, *loc = &res;

    while (list != ksi_nil) {
        KSI_CHECK(list, KSI_PAIR_P(list), "remove: improper list in arg2");
        if (!KSI_EQUAL_P(o, KSI_CAR(list))) {
            *loc = ksi_cons(KSI_CAR(list), ksi_nil);
            loc = & KSI_CDR(*loc);
        }
        list = KSI_CDR(list);
    }

    return res;
}

ksi_obj
ksi_remp (ksi_obj proc, ksi_obj list)
{
    ksi_obj res = ksi_nil, *loc = &res;

    KSI_CHECK(proc, KSI_PROC_P(proc), "remp: invalid procedure in arg1");
    if (ksi_procedure_has_arity_p(proc, ksi_long2num(1), 0) == ksi_false)
        ksi_exn_error(0, proc, "remp: invalid arity of the procedure in arg1");

    while (list != ksi_nil) {
        KSI_CHECK(list, KSI_PAIR_P(list), "remp: improper list in arg2");
        if (KSI_FALSE_P(ksi_apply_1(proc, KSI_CAR(list)))) {
            *loc = ksi_cons(KSI_CAR(list), ksi_nil);
            loc = & KSI_CDR(*loc);
        }
        list = KSI_CDR(list);
    }

    return res;
}

ksi_obj
ksi_assq (ksi_obj o, ksi_obj list)
{
    while (list != ksi_nil) {
        ksi_obj x;
        KSI_CHECK (list, KSI_PAIR_P (list), "assq: improper list in arg2");
        x = KSI_CAR (list);
        KSI_CHECK (x, KSI_PAIR_P (x), "assq: invalid pair");
        if (o == KSI_CAR (x))
            return x;
        list = KSI_CDR (list);
    }
    return ksi_false;
}

ksi_obj
ksi_assv (ksi_obj o, ksi_obj list)
{
    while (list != ksi_nil) {
        ksi_obj x;
        KSI_CHECK (list, KSI_PAIR_P (list), "assv: improper list in arg2");
        x = KSI_CAR (list);
        KSI_CHECK (x, KSI_PAIR_P (x), "assv: invalid pair");
        if (KSI_EQV_P (o, KSI_CAR (x)))
            return x;
        list = KSI_CDR (list);
    }
    return ksi_false;
}

ksi_obj
ksi_assoc (ksi_obj o, ksi_obj list)
{
    while (list != ksi_nil) {
        ksi_obj x;
        KSI_CHECK(list, KSI_PAIR_P(list), "assoc: improper list in arg2");
        x = KSI_CAR(list);
        KSI_CHECK(x, KSI_PAIR_P(x), "assoc: invalid pair");
        if (KSI_EQUAL_P(o, KSI_CAR(x)))
            return x;
        list = KSI_CDR(list);
    }
    return ksi_false;
}

ksi_obj
ksi_assp (ksi_obj proc, ksi_obj list)
{
    KSI_CHECK(proc, KSI_PROC_P(proc), "assp: invalid procedure in arg1");
    if (ksi_procedure_has_arity_p(proc, ksi_long2num(1), 0) == ksi_false)
        ksi_exn_error(0, proc, "assp: invalid arity of the procedure in arg1");

    while (list != ksi_nil) {
        ksi_obj x;
        KSI_CHECK(list, KSI_PAIR_P(list), "assp: improper list in arg2");
        x = KSI_CAR(list);
        KSI_CHECK(x, KSI_PAIR_P(x), "assp: invalid pair");
        if (KSI_TRUE_P(ksi_apply_1(proc, KSI_CAR(x))))
            return x;
        list = KSI_CDR(list);
    }
    return ksi_false;
}

ksi_obj
ksi_assq_ref (ksi_obj list, ksi_obj key)
{
    while (KSI_PAIR_P (list)) {
        ksi_obj x = KSI_CAR (list);
        if (KSI_PAIR_P (x) && key == KSI_CAR (x))
            return KSI_CDR (x);
        list = KSI_CDR (list);
    }

    return ksi_false;
}

ksi_obj
ksi_assv_ref (ksi_obj list, ksi_obj key)
{
    while (KSI_PAIR_P (list)) {
        ksi_obj x = KSI_CAR (list);
        if (KSI_PAIR_P (x) && KSI_EQV_P (key, KSI_CAR (x)))
            return KSI_CDR (x);
        list = KSI_CDR (list);
    }

    return ksi_false;
}

ksi_obj
ksi_assoc_ref (ksi_obj list, ksi_obj key)
{
    while (KSI_PAIR_P(list)) {
        ksi_obj x = KSI_CAR(list);
        if (KSI_PAIR_P(x) && KSI_EQUAL_P(key, KSI_CAR(x)))
            return KSI_CDR(x);
        list = KSI_CDR(list);
    }

    return ksi_false;
}

ksi_obj
ksi_assp_ref (ksi_obj list, ksi_obj proc)
{
    KSI_CHECK(proc, KSI_PROC_P(proc), "assp-ref: invalid procedure in arg2");
    if (ksi_procedure_has_arity_p(proc, ksi_long2num(1), 0) == ksi_false)
        ksi_exn_error(0, proc, "assp-ref: invalid arity of the procedure in arg2");

    while (KSI_PAIR_P(list)) {
        ksi_obj x = KSI_CAR(list);
        if (KSI_PAIR_P(x) && KSI_TRUE_P(ksi_apply_1(proc, KSI_CAR(x))))
            return KSI_CDR(x);
        list = KSI_CDR(list);
    }

    return ksi_false;
}

ksi_obj
ksi_assq_set_x (ksi_obj alist, ksi_obj key, ksi_obj val)
{
    ksi_obj list;
    for (list = alist; KSI_PAIR_P (list); list = KSI_CDR (list)) {
        ksi_obj x = KSI_CAR (list);
        if (KSI_PAIR_P (x) && key == KSI_CAR (x)) {
            KSI_CDR (x) = val;
            return alist;
        }
    }

    return ksi_acons (key, val, alist);
}

ksi_obj
ksi_assv_set_x (ksi_obj alist, ksi_obj key, ksi_obj val)
{
    ksi_obj list;
    for (list = alist; KSI_PAIR_P(list); list = KSI_CDR(list)) {
        ksi_obj x = KSI_CAR (list);
        if (KSI_PAIR_P(x) && KSI_EQV_P(key, KSI_CAR(x))) {
            KSI_CDR(x) = val;
            return alist;
        }
    }

    return ksi_acons(key, val, alist);
}

ksi_obj
ksi_assoc_set_x (ksi_obj alist, ksi_obj key, ksi_obj val)
{
    ksi_obj list;
    for (list = alist; KSI_PAIR_P(list); list = KSI_CDR(list)) {
        ksi_obj x = KSI_CAR(list);
        if (KSI_PAIR_P(x) && KSI_EQUAL_P(key, KSI_CAR(x))) {
            KSI_CDR(x) = val;
            return alist;
        }
    }

    return ksi_acons(key, val, alist);
}

ksi_obj
ksi_assp_set_x (ksi_obj alist, ksi_obj proc, ksi_obj val)
{
    ksi_obj list;

    KSI_CHECK(proc, KSI_PROC_P(proc), "assp-set!: invalid procedure in arg2");
    if (ksi_procedure_has_arity_p(proc, ksi_long2num(1), 0) == ksi_false)
        ksi_exn_error(0, proc, "assp-set!: invalid arity of the procedure in arg2");

    for (list = alist; KSI_PAIR_P(list); list = KSI_CDR(list)) {
        ksi_obj x = KSI_CAR(list);
        if (KSI_PAIR_P(x) && KSI_TRUE_P(ksi_apply_1(proc, KSI_CAR(x)))) {
            KSI_CDR(x) = val;
            return alist;
        }
    }

    return ksi_acons(ksi_apply_1(proc, ksi_void), val, alist);
}

ksi_obj
ksi_assq_rem_x (ksi_obj alist, ksi_obj key)
{
    ksi_obj prev = 0, list;
    for (list = alist; KSI_PAIR_P (list); list = KSI_CDR (list)) {
        ksi_obj x = KSI_CAR (list);
        if (KSI_PAIR_P (x) && key == KSI_CAR (x)) {
            if (list == alist)
                return KSI_CDR (list);
            KSI_CDR (prev) = KSI_CDR (list);
            return alist;
        }
        prev = list;
    }

    return alist;
}

ksi_obj
ksi_assv_rem_x (ksi_obj alist, ksi_obj key)
{
    ksi_obj prev = 0, list;
    for (list = alist; KSI_PAIR_P (list); list = KSI_CDR (list)) {
        ksi_obj x = KSI_CAR (list);
        if (KSI_PAIR_P (x) && KSI_EQV_P (key, KSI_CAR (x))) {
            if (list == alist)
                return KSI_CDR (list);
            KSI_CDR (prev) = KSI_CDR (list);
            return alist;
        }
        prev = list;
    }

    return alist;
}

ksi_obj
ksi_assoc_rem_x (ksi_obj alist, ksi_obj key)
{
    ksi_obj prev = 0, list;
    for (list = alist; KSI_PAIR_P(list); list = KSI_CDR(list)) {
        ksi_obj x = KSI_CAR(list);
        if (KSI_PAIR_P(x) && KSI_EQUAL_P(key, KSI_CAR(x))) {
            if (list == alist)
                return KSI_CDR(list);
            KSI_CDR(prev) = KSI_CDR(list);
            return alist;
        }
        prev = list;
    }

    return alist;
}

ksi_obj
ksi_assp_rem_x (ksi_obj alist, ksi_obj proc)
{
    ksi_obj prev = 0, list;

    KSI_CHECK(proc, KSI_PROC_P(proc), "assp-rem!: invalid procedure in arg2");
    if (ksi_procedure_has_arity_p(proc, ksi_long2num(1), 0) == ksi_false)
        ksi_exn_error(0, proc, "assp-rem!: invalid arity of the procedure in arg2");

    for (list = alist; KSI_PAIR_P(list); list = KSI_CDR(list)) {
        ksi_obj x = KSI_CAR(list);
        if (KSI_PAIR_P(x) && KSI_TRUE_P(ksi_apply_1(proc, KSI_CAR(x)))) {
            if (list == alist)
                return KSI_CDR(list);
            KSI_CDR(prev) = KSI_CDR(list);
            return alist;
        }
        prev = list;
    }

    return alist;
}


ksi_obj
ksi_new_list (int argc, ksi_obj* argv)
{
  ksi_obj args = ksi_nil;
  while (--argc >= 0)
    args = ksi_cons(argv[argc], args);
  return args;
}

ksi_obj
ksi_make_list (ksi_obj len, ksi_obj init)
{
  int l;
  ksi_obj res = ksi_nil;

  KSI_CHECK (len, KSI_EINT_P (len), "make-list: invalid integer in arg1");

  if (!init)
    init = ksi_void;

  l = ksi_num2long (len, "make-list");
  while (--l >= 0)
    res = ksi_cons(init, res);

  return res;
}

ksi_obj
ksi_map (ksi_obj proc, int argc, ksi_obj* argv)
{
    ksi_obj res = ksi_nil, *loc = &res;

    KSI_CHECK(proc, KSI_PROC_P(proc), "map: invalid procedure in arg1");
    if (ksi_procedure_has_arity_p(proc, ksi_long2num(argc), 0) == ksi_false)
        ksi_exn_error(0, proc, "map: invalid arity of the procedure in arg1");

    if (argc == 1) {
        ksi_obj x, xs = argv[0];
        for (;;) {
            if (xs == ksi_nil)
                return res;
            if (KSI_PAIR_P(xs)) {
                x = KSI_CAR(xs);
                xs = KSI_CDR(xs);
                *loc = ksi_cons(ksi_apply_proc(proc, 1, &x), ksi_nil);
                loc = & KSI_CDR(*loc);
            } else {
                *loc = ksi_apply_proc(proc, 1, &xs);
                return res;
            }
        }
    } else  {
        int i;
        ksi_obj *av, *xs;
        xs = (ksi_obj*) alloca(argc * sizeof *argv);
        av = (ksi_obj*) alloca(argc * sizeof *argv);
        memcpy(xs, argv, argc * sizeof *argv);

        for (;;) {
            if (!KSI_PAIR_P(xs[0])) {
                if (xs[0] == ksi_nil)
                    return res;
                for (i = 1; i < argc; i++) {
                    if (xs[i] == ksi_nil || KSI_PAIR_P(xs[i]))
                        ksi_exn_error(0, argv[0], "map: improper list in arg2");
                }
                *loc = ksi_apply_proc(proc, argc, xs);
                return res;
            }

            for (i = 0; i < argc; i++) {
                if (xs[i] == ksi_nil)
                    return res;
                if (!KSI_PAIR_P(xs[i]))
                    ksi_exn_error(0, argv[i], "map: improper list in arg%d", i+2);
                av[i] = KSI_CAR(xs[i]);
                xs[i] = KSI_CDR(xs[i]);
            }

            *loc = ksi_cons(ksi_apply_proc(proc, argc, av), ksi_nil);
            loc = & KSI_CDR(*loc);
        }
    }
}

ksi_obj
ksi_for_each (ksi_obj proc, int argc, ksi_obj* argv)
{
    KSI_CHECK(proc, KSI_PROC_P(proc), "for-each: invalid procedure in arg1");
    if (ksi_procedure_has_arity_p(proc, ksi_long2num(argc), 0) == ksi_false)
        ksi_exn_error(0, proc, "for-each: invalid arity of the procedure in arg1");

    if (argc == 1) {
        ksi_obj x, xs = argv[0];
        for (;;) {
            if (!KSI_PAIR_P(xs))
                return ksi_void;
            x  = KSI_CAR(xs);
            xs = KSI_CDR(xs);
            ksi_apply_proc(proc, 1, &x);
        }
    } else {
        int i;
        ksi_obj *av, *xs;
        xs = (ksi_obj*) alloca(argc * sizeof *argv);
        av = (ksi_obj*) alloca(argc * sizeof *argv);
        memcpy(xs, argv, argc * sizeof *argv);

        for (;;) {
            for (i = 0; i < argc; i++) {
                if (!KSI_PAIR_P(xs[i]))
                    return ksi_void;
                av[i] = KSI_CAR(xs[i]);
                xs[i] = KSI_CDR(xs[i]);
            }

            ksi_apply_proc(proc, argc, av);
        }
    }
}

ksi_obj
ksi_fold_left (ksi_obj kons, ksi_obj knil, int argc, ksi_obj* argv)
{
    int i;
    ksi_obj *xs, *av;

    KSI_CHECK(kons, KSI_PROC_P(kons), "fold-left: invalid procedure in arg1");
    if (ksi_procedure_has_arity_p(kons, ksi_long2num(argc+1), 0) == ksi_false)
        ksi_exn_error(0, kons, "fold-left: invalid arity of the procedure in arg1");

    xs = (ksi_obj*) alloca(argc * sizeof *argv);
    av = (ksi_obj*) alloca((argc + 1) * sizeof *av);
    memcpy(xs, argv, argc * sizeof *argv);

    av[argc] = knil;
    for (;;) {
        for (i = 0; i < argc; i++) {
            if (!KSI_PAIR_P(xs[i]))
                return av[argc];

            av[i] = KSI_CAR(xs[i]);
            xs[i] = KSI_CDR(xs[i]);
        }

        av[argc] = ksi_apply_proc(kons, argc+1, av);
    }
}

ksi_obj
ksi_fold_right (ksi_obj kons, ksi_obj knil, int argc, ksi_obj* argv)
{
    int i;
    ksi_obj *av;

    KSI_CHECK(kons, KSI_PROC_P(kons), "fold-right: invalid procedure in arg1");
    if (ksi_procedure_has_arity_p(kons, ksi_long2num(argc+1), 0) == ksi_false)
        ksi_exn_error(0, kons, "fold-right: invalid arity of the procedure in arg1");

    av = (ksi_obj*) alloca((argc + 1) * sizeof *argv);
    for (i = 0; i < argc; i++) {
        if (!KSI_PAIR_P(argv[i]))
            return knil;
        av[i] = KSI_CDR(argv[i]);
    }

    av[argc] = ksi_fold_right(kons, knil, argc, av);
    for (i = 0; i < argc; i++)
        av[i] = KSI_CAR(argv[i]);

    return ksi_apply_proc(kons, argc+1, av);
}

/* End of code */
