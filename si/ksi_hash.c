/*
 * ksi_hash.c
 * hash
 *
 * Copyright (C) 1998-2010, 2014, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Mon Feb 16 02:36:03 1998
 * Last Update:   Fri Apr 25 22:46:53 2014
 *
 */

#include "ksi_hash.h"
#include "ksi_proc.h"
#include "ksi_klos.h"
#include "ksi_util.h"
#include "ksi_gc.h"


typedef struct _hashrec
{
    ksi_obj key;
    ksi_obj val;
} hashrec;


unsigned
ksi_hasher (ksi_obj x, unsigned n, unsigned d)
{
    if (n <= 1)
        return 0;

    if (x == ksi_false)
        return 0;
    if (x == ksi_true)
        return 1;
    if (x == ksi_nil)
        return 2 % n;
    if (x == ksi_void)
        return 3 % n;

    switch (x->itag) {
    case KSI_TAG_SYMBOL:
        return ksi_hash_str(KSI_SYM_PTR(x), KSI_SYM_LEN(x), n);

    case KSI_TAG_KEYWORD:
        return ksi_hash_str(KSI_KEY_PTR(x), KSI_KEY_LEN(x), n);

    case KSI_TAG_CHAR:
        return KSI_CHAR_CODE(x) % n;

    case KSI_TAG_STRING:
    case KSI_TAG_CONST_STRING:
        return ksi_hash_str(KSI_STR_PTR(x), KSI_STR_LEN(x), n);

    case KSI_TAG_BIGNUM:
        x = ksi_imod(x, ksi_ulong2num(n));
        return (unsigned) ksi_num2ulong(x, "<ksi_hasher>") % n;

    case KSI_TAG_FLONUM:
    {
        wchar_t *ptr = ksi_num2str(x, 10);
        return ksi_hash_str(ptr, (int)wcslen(ptr), n);
    }

    case KSI_TAG_PAIR:
    case KSI_TAG_CONST_PAIR:
        if (d && KSI_CDR(x) != ksi_nil)
            return (ksi_hasher(KSI_CAR(x), n, d/2) + ksi_hasher(KSI_CDR(x), n, d/2)) % n;
        return ksi_hasher(KSI_CAR(x), n, 0);

    case KSI_TAG_VECTOR:
    case KSI_TAG_CONST_VECTOR:
    {
        int len = KSI_VEC_LEN(x), i;
        unsigned h;
        if (len > 5) {
            i = d/2; h = 1;
            while (--i >= 0)
                h = ((h<<8) + ksi_hasher(KSI_VEC_REF(x, h % len), n, 2)) % n;
            return h;
        } else {
            i = len; h = n-1;
            while (--i >= 0)
                h = ((h<<8) + (ksi_hasher(KSI_VEC_REF(x, i), n, d / len))) % n;
            return h;
        }
    }

    case KSI_TAG_INSTANCE:
        return ksi_hash_inst(x, n, d);
    }

    return ((KSI_WORD) x) % n;
}

static unsigned
hash_rec (void *v, unsigned size, void *data)
{
    ksi_obj hash = KSI_HASHTAB_HASH(data);
    ksi_obj cmp = KSI_HASHTAB_CMP(data);
    ksi_obj val, key = ((hashrec*) v) -> key;

    if (!hash) {
        if (!cmp || cmp == ksi_data->eq_proc) {
            if (KSI_OBJ_IS(key, KSI_TAG_IMM) || KSI_SYM_P(key) || KSI_KEY_P(key))
                return ksi_hasher(key, size, 0);
            return ((KSI_WORD) key) % size;
        }
        if (cmp == ksi_data->eqv_proc) {
            if (KSI_OBJ_IS(key, KSI_TAG_IMM) || KSI_SYM_P(key) || KSI_KEY_P(key) || KSI_CHAR_P(key) || KSI_NUM_P(key))
                return ksi_hasher(key, size, 0);
            return ((KSI_WORD) key) % size;
        }
        return ksi_hasher(key, size, 100);
    }

    val = ksi_apply_1(hash, key);
    if (!KSI_UINT_P(val)) {
        ksi_exn_error(0, val, "hasher: %ls returned invalid non-negative exact integer for key %ls", ksi_obj2str(hash), ksi_obj2str(key));
    }

    return (unsigned) ksi_num2ulong(val, 0) % size;
}

static int
cmp_rec (void* v1, void* v2, void* data)
{
    ksi_obj cmp = KSI_HASHTAB_CMP(data);
    ksi_obj key1 = ((hashrec*) v1) -> key;
    ksi_obj key2 = ((hashrec*) v2) -> key;

    if (!cmp)
        return key1 != key2;

    return KSI_FALSE_P(ksi_apply_2(cmp, key1, key2));
}

static int
copy_iter (void *rec, void *data)
{
    ksi_obj key = ((hashrec *) rec) -> key;
    ksi_obj val = ((hashrec *) rec) -> val;
    ksi_hashtab tab = (ksi_hashtab) data;
    hashrec *p;

    p = ksi_malloc(sizeof(*p));
    p->key = key;
    p = (hashrec*) ksi_lookup_vtab(KSI_HASHTAB_TAB(tab), p, 1);
    p->val = val;

    return 0;
}

ksi_obj
ksi_new_hashtab (ksi_obj hash, ksi_obj cmp, int size, int is_mutable)
{
    ksi_hashtab tab = ksi_malloc(sizeof(*tab));
    tab->o.itag = KSI_TAG_HASHTAB;
    tab->table = ksi_new_valtab(size, hash_rec, cmp_rec, tab);
    tab->hash = hash;
    tab->cmp = cmp;
    tab->is_mutable = is_mutable;
    return (ksi_obj) tab;
}

ksi_obj
ksi_hash_copy (ksi_obj old, ksi_obj mutable)
{
    ksi_hashtab new;

    KSI_CHECK(old, KSI_HASHTAB_P(old), "hashtable-copy: invalid hashtable in arg1");

    new = ksi_malloc(sizeof(*new));
    new->o.itag = KSI_TAG_HASHTAB;
    new->hash = KSI_HASHTAB_HASH(old);
    new->cmp = KSI_HASHTAB_CMP(old);;
    new->is_mutable = (mutable ? KSI_TRUE_P(mutable) : 0);

    if (!new->is_mutable && !KSI_HASHTAB_MUTABLE(old)) {
        new->table = KSI_HASHTAB_TAB(old);
    } else {
        new->table = ksi_new_valtab(KSI_HASHTAB_TAB(old)->size, hash_rec, cmp_rec, new);
        ksi_iterate_vtab(KSI_HASHTAB_TAB(old), copy_iter, new);
    }

    return (ksi_obj) new;
}

ksi_obj
ksi_make_eq_hashtab (ksi_obj size)
{
    KSI_CHECK(size, !size || KSI_UINT_P(size), "make-eq-hashtable: invalid exact integer in arg1");

    return ksi_new_hashtab(0, 0, (size ? ksi_num2ulong(size, 0) : 0), 1);
}

ksi_obj
ksi_make_eqv_hashtab (ksi_obj size)
{
    KSI_CHECK(size, !size || KSI_UINT_P(size), "make-eqv-hashtable: invalid exact integer in arg1");

    return ksi_new_hashtab(0, ksi_data->eqv_proc, (size ? ksi_num2ulong(size, 0) : 0), 1);
}

ksi_obj
ksi_make_hashtab (ksi_obj hash, ksi_obj cmp, ksi_obj size)
{
    if (hash) {
        KSI_CHECK(hash, KSI_PROC_P(hash), "make-hashtable: invalid procedure in arg1");
        if (ksi_procedure_has_arity_p(hash, ksi_long2num(1), 0) == ksi_false)
            ksi_exn_error(0, hash, "make-hashtable: invalid arity of the hash procedure in arg1");
    }
    if (cmp) {
        KSI_CHECK(cmp, KSI_PROC_P(cmp), "make-hashtable: invalid procedure in arg2");
        if (ksi_procedure_has_arity_p(cmp, ksi_long2num(2), 0) == ksi_false)
            ksi_exn_error(0, hash, "make-hashtable: invalid arity of the equiv procedure in arg2");
    }

    KSI_CHECK(size, KSI_UINT_P(size), "make-hashtable: invalid exact integer in arg3");

    return ksi_new_hashtab(hash, cmp, (size ? ksi_num2ulong(size, 0) : 0), 1);
}

ksi_obj
ksi_hashtab_p (ksi_obj x)
{
    return KSI_HASHTAB_P(x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_hash_eq_fun (ksi_obj x)
{
    KSI_CHECK(x, KSI_HASHTAB_P(x), "hashtable-equivalence-function: invalid hashtable in arg1");
    return KSI_HASHTAB_CMP(x) ? KSI_HASHTAB_CMP(x) : ksi_data->eq_proc;
}

ksi_obj
ksi_hash_hash_fun (ksi_obj x)
{
    KSI_CHECK(x, KSI_HASHTAB_P(x), "hashtable-hash-function: invalid hashtable in arg1");
    return KSI_HASHTAB_HASH(x) ? KSI_HASHTAB_HASH(x) : ksi_false;
}

ksi_obj
ksi_hash_mutable_p (ksi_obj x)
{
    KSI_CHECK(x, KSI_HASHTAB_P(x), "hashtable-mutable?: invalid hashtable in arg1");
    return KSI_HASHTAB_MUTABLE(x) ? ksi_true : ksi_false;
}

ksi_obj
ksi_hashtab_size (ksi_obj tab)
{
    KSI_CHECK(tab, KSI_HASHTAB_P(tab), "hashtable-size: invalid hashtable in arg1");
    return ksi_long2num(KSI_HASHTAB_TAB(tab)->count);
}

ksi_obj
ksi_hash_ref (ksi_obj tab, ksi_obj key, ksi_obj def)
{
    hashrec r, *p;
    KSI_CHECK(tab, KSI_HASHTAB_P(tab), "hashtable-ref: invalid hashtable in arg1");

    r.key = key;
    p = (hashrec*) ksi_lookup_vtab(KSI_HASHTAB_TAB(tab), &r, 0);
    if (p) {
        return p->val;
    }
    return (def ? def : ksi_void);
}

ksi_obj
ksi_hash_has_p (ksi_obj tab, ksi_obj key)
{
    hashrec r, *p;
    KSI_CHECK(tab, KSI_HASHTAB_P(tab), "hashtable-contains?: invalid hashtable in arg1");

    r.key = key;
    p = (hashrec*) ksi_lookup_vtab(KSI_HASHTAB_TAB(tab), &r, 0);

    return (p ? ksi_true : ksi_false);
}

ksi_obj
ksi_hash_set_x (ksi_obj tab, ksi_obj key, ksi_obj val)
{
    hashrec *p;
    KSI_CHECK(tab, KSI_HASHTAB_P(tab), "hashtable-set!: invalid hashtable in arg1");
    KSI_CHECK(tab, KSI_HASHTAB_MUTABLE(tab), "hashtable-set!: hashtable is immutable in arg1");

    p = ksi_malloc(sizeof(*p));
    p->key = key;
    p = (hashrec*) ksi_lookup_vtab(KSI_HASHTAB_TAB(tab), p, 1);
    p->val = val;

    return ksi_void;
}

ksi_obj
ksi_hash_del_x (ksi_obj tab, ksi_obj key)
{
    hashrec r;
    KSI_CHECK(tab, KSI_HASHTAB_P(tab), "hashtable-delete!: invalid hashtable in arg1");
    KSI_CHECK(tab, KSI_HASHTAB_MUTABLE(tab), "hashtable-delete!: hashtable is immutable in arg1");

    r.key = key;
    ksi_remove_vtab(KSI_HASHTAB_TAB(tab), &r);

    return ksi_void;
}

ksi_obj
ksi_hash_clear (ksi_obj tab)
{
    KSI_CHECK(tab, KSI_HASHTAB_P(tab), "hashtable-clear!: invalid hashtable in arg1");
    KSI_CHECK(tab, KSI_HASHTAB_MUTABLE(tab), "hashtable-clear!: hashtable is immutable in arg1");

    ksi_clear_vtab(KSI_HASHTAB_TAB(tab));

    return ksi_void;
}

static int
hash_iter (void *rec, void *data)
{
    ksi_obj key = ((hashrec *) rec) -> key;
    ksi_obj val = ((hashrec *) rec) -> val;
    ksi_obj proc = (ksi_obj) data;

    ksi_apply_2 (proc, key, val);
    return 0;
}

ksi_obj
ksi_hash_for_each (ksi_obj proc, ksi_obj tab)
{
    KSI_CHECK(proc, KSI_PROC_P(proc), "hashtable-for-each: invalid procedure in arg1");
    KSI_CHECK(tab, KSI_HASHTAB_P(tab), "hashtable-for-each: invalid hashtable in arg2");

    ksi_iterate_vtab(KSI_HASHTAB_TAB(tab), hash_iter, proc);
    return ksi_void;
}

ksi_obj
ksi_equal_hash (ksi_obj x)
{
    return ksi_ulong2num(ksi_hasher(x, INT_MAX, 100));
}


 /* End of code */
