/*
 * ksi_env.c
 * environment
 *
 * Copyright (C) 1998-2010, 2015, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Sun Jan  4 20:31:17 1998
 * Last Update:   Tue Jun  2 16:47:26 2015
 *
 */

#include "ksi_env.h"
#include "ksi_proc.h"
#include "ksi_hash.h"
#include "ksi_util.h"
#include "ksi_gc.h"


ksi_obj
ksi_env_p (ksi_obj x)
{
    return (KSI_OBJ_IS(x, KSI_TAG_ENVIRON) ? ksi_true : ksi_false);
}

static unsigned
hash_envrec (void *r, unsigned n, void *data)
{
    ksi_obj sym = ((ksi_envrec) r) -> sym;
    return ksi_hash_str(KSI_SYM_PTR(sym), KSI_SYM_LEN(sym), n);
}

static int
cmp_envrec (void *r1, void *r2, void *data)
{
	ptrdiff_t x = (((ksi_envrec)r1)->sym) - (((ksi_envrec)r2)->sym);
	return (x < 0 ? -1 : (x == 0 ? 0 : 1));
}

ksi_env
ksi_new_env (int size, ksi_env parent)
{
    ksi_env env;
    env = ksi_malloc(sizeof(*env));
    env->o.itag = KSI_TAG_ENVIRON;
    env->valtab = ksi_new_valtab(size, hash_envrec, cmp_envrec, 0);
    env->parent = parent;
    env->name = ksi_void;
    env->exported = ksi_nil;

    return env;
}

ksi_env
ksi_top_level_env (void)
{
    ksi_env env = ksi_new_env(0, 0);
    return env;
}

static unsigned
hash_env (void *obj, unsigned num, void *unused)
{
    return ksi_hasher(KSI_ENV_NAME(obj), num, 100);
}

static int
cmp_env (void *x1, void *x2, void *unused)
{
    return !KSI_EQUAL_P(KSI_ENV_NAME(x1), KSI_ENV_NAME(x2));
}

ksi_env
ksi_lib_env (ksi_obj name, int append_new)
{
    ksi_env env;
    struct Ksi_Environ r;

    KSI_LOCK_W(ksi_data->lock);
    if (!ksi_data->envtab) {
        ksi_data->envtab = ksi_new_valtab(100, hash_env, cmp_env, 0);
    }
    KSI_UNLOCK_W(ksi_data->lock);

    r.name = name;
    env = (ksi_env) ksi_lookup_vtab(ksi_data->envtab, &r, 0);
    if (!env && append_new) {
        env = ksi_new_env(0, 0);
        env->name = name;
        env = (ksi_env) ksi_lookup_vtab(ksi_data->envtab, env, 1);
    }

    return env;
}

ksi_env
ksi_get_lib_env (const wchar_t *name, ...)
{
    va_list args;
    ksi_obj res = ksi_nil;

    va_start(args, name);
    while (name) {
        res = ksi_cons(ksi_str02sym(name), res);
        name = va_arg(args, const wchar_t *);
    }
    va_end(args);

    return ksi_lib_env(ksi_reverse_x(res), 1);
}

ksi_envrec
ksi_lookup_env (ksi_env env, ksi_obj sym)
{
    struct Ksi_EnvRec r;
    KSI_CHECK(sym, KSI_SYM_P(sym), "ksi_lookup_env: invalid symbol");

    r.sym = sym;
    for (;;) {
        ksi_envrec p = (ksi_envrec) ksi_lookup_vtab(env->valtab, &r, 0);
        if (p)
            return p;
        env = env->parent;
        if (!env)
            break;
    }
    return 0;
}

ksi_envrec
ksi_append_env (ksi_env env, ksi_obj sym, ksi_obj val)
{
    ksi_envrec p;
    KSI_CHECK(sym, KSI_SYM_P(sym), "ksi_append_env: invalid symbol");

    p = (ksi_envrec) ksi_malloc (sizeof *p);
    p->sym = sym;
    p->val = val;
    return (ksi_envrec) ksi_lookup_vtab (env->valtab, p, 1);
}

static ksi_envrec
ksi_define_helper (ksi_obj sym, ksi_obj val, ksi_env env, int local)
{
    struct Ksi_EnvRec r, *ptr;

    KSI_CHECK(sym, KSI_SYM_P(sym), "define: invalid symbol in arg1");
    KSI_CHECK((ksi_obj) env, KSI_ENV_P(env), "define: invalid environ in arg3");

    r.sym = sym;
    ptr = (ksi_envrec) ksi_lookup_vtab(env->valtab, &r, 0);
    if (ptr) {
        if (!local) {
            if (ptr->imported) {
                ksi_exn_error(0, sym, "define: cannot modify imported variable in %ls", ksi_obj2str((ksi_obj) env));
            }
        }
        if (ptr->exported) {
            ksi_exn_error(0, sym, "define: cannot modify exported variable in %ls", ksi_obj2str((ksi_obj) env));
        }
        ptr->val = val;
    } else {
        ptr = ksi_append_env (env, sym, val);
        if (KSI_TRUE_P(ksi_exported_p(env, sym, ksi_false)))
            ptr->exported = 1;
    }

    return ptr;
}

ksi_obj
ksi_define_local (ksi_obj sym, ksi_obj val, ksi_env env)
{
    /*ksi_envrec ptr =*/ ksi_define_helper(sym, val, env, 1);
    return ksi_void;
}

ksi_obj
ksi_define (ksi_obj sym, ksi_obj val, ksi_env env)
{
    /*ksi_envrec ptr =*/ ksi_define_helper(sym, val, env, 0);
    return ksi_void;
}

ksi_obj
ksi_bound_p (ksi_obj sym, ksi_env env)
{
    KSI_CHECK(sym, KSI_SYM_P(sym), "bound?: invalid symbol in arg1");
    KSI_CHECK((ksi_obj) env, KSI_ENV_P(env), "bound?: invalid environment in arg2");

    if (ksi_lookup_env(env, sym))
        return ksi_true;

    return ksi_false;
}

ksi_obj
ksi_var_p (ksi_env env, ksi_obj sym)
{
    ksi_envrec rec;

    KSI_CHECK((ksi_obj) env, KSI_ENV_P(env), "environment-var?: invalid environment in arg1");
    KSI_CHECK(sym, KSI_SYM_P(sym), "environment-var?: invalid symbol in arg2");

    if ((rec = ksi_lookup_env (env, sym)) != 0) {
        if (rec->imported) {
            return ksi_false;
        }
        if (rec->exported) {
            return ksi_false;
        }
        return ksi_true;
    }
    return ksi_false;
}

ksi_obj
ksi_var_ref (ksi_env env, ksi_obj sym)
{
    ksi_envrec rec;

    KSI_CHECK((ksi_obj) env, KSI_ENV_P(env), "environment-ref: invalid environment in arg1");
    KSI_CHECK(sym, KSI_SYM_P(sym), "environment-ref: invalid symbol in arg2");

    if ((rec = ksi_lookup_env (env, sym)) != 0)
        return rec->val;

    ksi_exn_error(0, sym, "environment-ref: variable unbound in %ls", ksi_obj2str((ksi_obj) env));
    return ksi_void;
}

ksi_obj
ksi_var_set (ksi_env env, ksi_obj sym, ksi_obj val)
{
    ksi_envrec rec;

    KSI_CHECK((ksi_obj) env, KSI_ENV_P(env), "environment-set!: invalid environment in arg1");
    KSI_CHECK(sym, KSI_SYM_P(sym), "environment-set!: invalid symbol in arg2");

    if ((rec = ksi_lookup_env (env, sym)) != 0) {
        if (rec->imported) {
            ksi_exn_error(0, sym, "environment-set!: cannot modify imported variable in %ls", ksi_obj2str((ksi_obj) env));
        }
        if (rec->exported) {
            ksi_exn_error(0, sym, "environment-set!: cannot modify exported variable in %ls", ksi_obj2str((ksi_obj) env));
        }
        rec->val = val;
        return ksi_void;
    }

    ksi_exn_error(0, sym, "environment-set!: variable unbound in %ls", ksi_obj2str((ksi_obj) env));
    return ksi_void;
}

ksi_obj
ksi_var_value (ksi_env env, ksi_obj sym, ksi_obj def)
{
    ksi_envrec rec;

    if (KSI_ENV_P(env) && KSI_SYM_P(sym)) {
        if ((rec = ksi_lookup_env(env, sym)) != 0)
            return rec->val;
    }
    return def;
}

ksi_obj
ksi_defsym (const wchar_t *name, ksi_obj val, ksi_env env)
{
    ksi_obj sym = ksi_str02sym(name);
    ksi_define(sym, val, env);
    ksi_export(env, sym, 0);
    return sym;
}

ksi_obj
ksi_defsyntax (ksi_obj sym, ksi_obj val, ksi_env env, int export)
{
    ksi_envrec rec = ksi_define_helper(sym, val, env, 0);
    if (export)
        ksi_export(env, sym, 0);
    rec->syntax = 1;
    if (KSI_PROC_P(val))
        rec->macro = 1;
    return sym;
}


ksi_obj
ksi_export (ksi_env env, ksi_obj insym, ksi_obj exsym)
{
    ksi_envrec rec;
    ksi_obj exp, x;

    if (!exsym)
        exsym = insym;

    KSI_CHECK((ksi_obj) env, KSI_ENV_P(env), "export: invalid environment in arg1");
    KSI_CHECK(insym, KSI_SYM_P(insym), "export: invalid symbol in arg2");
    KSI_CHECK(exsym, KSI_SYM_P(exsym), "export: invalid symbol in arg3");

    for (exp = env->exported; KSI_PAIR_P(exp); exp = KSI_CDR(exp)){
        x = KSI_CAR(exp);
        if (x == exsym)
            return ksi_void;
        if (KSI_PAIR_P(x) && KSI_CAR(x) == exsym)
            return ksi_void;
    }
    if (insym == exsym)
        x = insym;
    else
        x = ksi_cons(exsym, insym);

    env->exported = ksi_cons(x, env->exported);

    rec = ksi_lookup_env (env, insym);
    if (rec)
        rec->exported = 1;

    return ksi_void;
}

ksi_obj
ksi_exported_p (ksi_env env, ksi_obj sym, ksi_obj external)
{
    ksi_obj exp, x;
    if (!external)
        external = ksi_true;

    KSI_CHECK((ksi_obj) env, KSI_ENV_P(env), "exported?: invalid environment in arg1");
    KSI_CHECK(sym, KSI_SYM_P(sym), "exported?: invalid symbol in arg2");

    for (exp = env->exported; KSI_PAIR_P(exp); exp = KSI_CDR(exp)){
        x = KSI_CAR(exp);
        if (x == sym)
            return ksi_true;
        if (KSI_PAIR_P(x)) {
            x = (external == ksi_false ? KSI_CDR(x) : KSI_CAR(x));
            if (x == sym)
                return ksi_true;
        }
    }
    return ksi_false;
}

ksi_obj
ksi_import (ksi_env libenv, ksi_obj libsym, ksi_env cenv, ksi_obj insym)
{
    ksi_obj exp, x;
    ksi_envrec rec;

    if (!insym)
        insym = libsym;

    KSI_CHECK((ksi_obj) libenv, KSI_ENV_P(libenv), "import: invalid environment in arg1");
    KSI_CHECK(libsym, KSI_SYM_P(libsym), "import: invalid symbol in arg2");
    KSI_CHECK((ksi_obj) cenv, KSI_ENV_P (cenv), "import: invalid environment in arg3");
    KSI_CHECK(insym, KSI_SYM_P(insym), "import: invalid symbol in arg4");

    for (exp = libenv->exported; KSI_PAIR_P(exp); exp = KSI_CDR(exp)){
        x = KSI_CAR(exp);
        if (x == libsym)
            goto exported;
        if (KSI_PAIR_P(x) && KSI_CAR(x) == libsym) {
            libsym = KSI_CDR(x);
            goto exported;
        }
    }
    ksi_exn_error(0, libsym, "import: variable is not exported from %ls", ksi_obj2str((ksi_obj) libenv));

exported:
    if ((rec = ksi_lookup_env (libenv, libsym)) != 0) {
        ksi_envrec ptr = ksi_lookup_env (cenv, insym);
        if (ptr) {
            if (ptr->val != rec->val) {
                ksi_exn_error(0, insym, "import: variable already defined or imported in %ls", ksi_obj2str((ksi_obj) cenv));
            }
        } else {
            ptr = ksi_append_env (cenv, insym, rec->val);
            ptr->imported = 1;
            ptr->syntax = rec->syntax;
            ptr->macro = rec->macro;
            if (KSI_TRUE_P(ksi_exported_p(cenv, insym, ksi_false)))
                ptr->exported = 1;
        }
        return ksi_void;
    }

    ksi_exn_error(0, libsym, "import: exported variable is unbound in %ls", ksi_obj2str((ksi_obj) libenv));
    return ksi_void;
}


static int
iter_func (void *r, void *p)
{
    ksi_apply_2((ksi_obj) p, ((ksi_envrec) r) -> sym, ((ksi_envrec) r) -> val);
    return 0;
}

ksi_obj
ksi_env_for_each (ksi_obj proc, ksi_obj env)
{
    KSI_CHECK(env, KSI_ENV_P (env), "environment-for-each: invalid environ");
    ksi_iterate_vtab(((ksi_env) env) -> valtab, iter_func, proc);
    return ksi_void;
}


/* End of code */
