#
# makefile for Visual C
#

!include $(SRCDIR)\makedefs.mak

SUBDIRS=regexp store
BUILD_TARGETS=$(pp_FILES)

!include $(TOPDIR)\visual.mif

extdir=$(TOPDIR)\$(DISTDIR)\$(KSI_MAJOR_VERSION).$(KSI_MINOR_VERSION)

all: do_subdirs do_curdir

match.ko: match.scm
	set KSI_LIBRARY=$(SCMLIBDIR)
	$(BUILDDIR)\ksi.exe -b $(SCMLIBDIR)\precomp.scm -v --eval match.scm

clean: do_subdirs_clean do_curdir_clean

dist: do_subdirs_dist
	@for %i in ($(ext_DATA)) do copy $(SRCDIR)\%i $(extdir)
