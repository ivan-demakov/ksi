;;;
;;; struct.scm
;;; structures
;;;
;;; Copyright (C) 1998-2000, Ivan Demakov.
;;;
;;; Permission to use, copy, modify, and distribute this software and its
;;; documentation for any purpose, without fee, and without a written
;;; agreement is hereby granted, provided that the above copyright notice
;;; and this paragraph and the following two paragraphs appear in all copies.
;;; Modifications to this software may be copyrighted by their authors
;;; and need not follow the licensing terms described here, provided that
;;; the new terms are clearly indicated on the first page of each file where
;;; they apply.
;;;
;;; IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
;;; FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
;;; INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
;;; DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
;;; POSSIBILITY OF SUCH DAMAGE.
;;;
;;; THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
;;; INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
;;; AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
;;; ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
;;; TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
;;;
;;;
;;; Author:        Ivan Demakov <demakov@users.sourceforge.net>
;;; Creation date: Thu Apr 23 14:59:55 1998
;;; Last Update:   Tue Mar 21 02:39:50 2000
;;;
;;;
;;; $Id: struct.scm,v 1.1 2001/02/16 22:09:46 demakov Exp $
;;;
;;;
;;; Comments:
;;;
;;;


(define-syntax define-structure
  (lambda (x)
    (define construct-name
      (lambda (template-identifier . args)
	(datum->syntax-object
	 template-identifier
	 (string->symbol
	  (apply string-append
		 (map (lambda (x)
			(cond ((string? x) x)
			      ((number? x) (number->string x))
			      (else (symbol->string
				     (syntax-object->datum x)))))
		      args))))))

    (syntax-case x ()
      ((_ (name id1 ...))
       (syntax (define-structure (name id1 ...) ())))
      ((_ (name id1 ...) ((id2 init) ...))
       (with-syntax
	   ((tag (construct-name (syntax name) "<" (syntax name) ">"))
	    (constructor (construct-name (syntax name) "make-" (syntax name)))
	    (predicate (construct-name (syntax name) (syntax name) "?"))
	    ((access ...)
	     (map (lambda (x) (construct-name x (syntax name) "-" x))
		  (syntax (id1 ... id2 ...))))
	    ((assign ...)
	     (map (lambda (x)
		    (construct-name x "set-" (syntax name) "-" x "!"))
		  (syntax (id1 ... id2 ...))))
	    (structure-length
	     (+ (length (syntax (id1 ... id2 ...))) 1))
	    ((index ...)
	     (let f ((i 1) (ids (syntax (id1 ... id2 ...))))
	       (if (null? ids)
		   '()
		   (cons i (f (+ i 1) (cdr ids)))))))
	 (syntax (begin
		   (define constructor
		     (lambda (id1 ...)
		       (let* ((id2 init) ...)
			 (vector 'tag id1 ... id2 ...))))

		   (define predicate
		     (lambda (x)
		       (and (vector? x)
			    (= (vector-length x) structure-length)
			    (eq? (vector-ref x 0) 'tag))))

		   (define access
		     (lambda (x)
		       (vector-ref x index)))
		   ...

		   (define assign
		     (lambda (x update)
		       (vector-set! x index update)))
		   ...)))))))


;;; End of code
