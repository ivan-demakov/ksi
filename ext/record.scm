;;;
;;; record.scm
;;; record implementation.
;;;
;;; Copyright (C) 1997-2001, ivan demakov
;;;
;;; Permission to use, copy, modify, and distribute this software and its
;;; documentation for any purpose, without fee, and without a written agreement
;;; is hereby granted, provided that the above copyright notice and this
;;; paragraph and the following two paragraphs appear in all copies.
;;; Modifications to this software may be copyrighted by their authors
;;; and need not follow the licensing terms described here, provided that
;;; the new terms are clearly indicated on the first page of each file where
;;; they apply.
;;;
;;; IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
;;; FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
;;; INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
;;; DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
;;; POSSIBILITY OF SUCH DAMAGE.
;;;
;;; THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
;;; INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
;;; AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
;;; ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
;;; TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
;;;
;;;
;;; Author:        Ivan Demakov <demakov@users.sourceforge.net>
;;; Creation date: Sat Jun 28 14:38:53 1997
;;; Last Update:   Fri May 11 16:25:00 2001
;;;
;;;
;;; $Id: record.scm,v 1.4 2009/09/19 04:13:38 ksion Exp $
;;;
;;;
;;; Comments:
;;;
;;; Records are implemented on top of klos.
;;;


(define-method initialize-instance ((self <record>) initargs)
  self)


(define-method write-instance ((self <record>) file)
  (display "#s[\"" file)
  (display (class-name (class-of self)) file)
  (display "\"" file)
  (let loop ((slots (class-slots (class-of self))))
    (if (null? slots)
	(display "]" file)
	(begin
	  (if (slot-bound? self (caar slots))
	      (begin
		(display " " file)
		(display (caar slots) file)
		(display " " file)
		(write (slot-ref self (caar slots)) file)))
	  (loop (cdr slots))))))


(define-method instance-equal? ((x1 <record>) (x2 <record>))
  (let ((rtd1 (class-of x1))
	(rtd2 (class-of x2)))
    (and (eq? rtd1 rtd2)
	 (let loop ((slots (class-slots rtd1)))
	   (or (null? slots)
	       (let* ((slot (car slots))
		      (name (if (pair? slot) (car slot) slot))
		      (val1 (if (slot-bound? x1 name) (slot-ref x1 name)))
		      (val2 (if (slot-bound? x2 name) (slot-ref x2 name))))
		 (and (equal? val1 val2)
		      (loop (cdr slots)))))))))


(define (record:record-type? rtd)
  (is-a? rtd <record-rtd>))


(define (make-record-type type-name field-names)
  (if (not (symbol? type-name))
      (error "make-record-type: not symbol in type-name argument:" type-name))
  (if (or (not (list? field-names))
	  (has-duplicates? field-names)
	  (not (every symbol? field-names)))
      (error "make-record-type: illegal field-names argument:" field-names))
  (make <record-rtd>
	name:   type-name
	supers: (list <Record>)
	slots:  (map list field-names)))


(define (record-constructor rtd . field-names)
  (if (not (record:record-type? rtd))
      (error "record-constructor: illegal rtd argument:" rtd))
  (let* ((slots (class-slots rtd))
	 (fields (if (null? field-names) (map car slots) (car field-names)))
	 (len (length fields)))
    (if (or (has-duplicates? fields)
	    (not (every (lambda (x) (assq x slots)) fields)))
	(error "record-constructor: invalid field-names argument:" field-names))

    (lambda elts
      (if (not (= (length elts) len))
	  (error "record-constructor-~s ~a" (class-name rtd) "wrong number of arguments"))
      (let ((result (make rtd)))
	(for-each (lambda (field elt) (slot-set! result field elt))
		  fields elts)
	result))))


(define (record-predicate rtd)
  (if (not (record:record-type? rtd))
      (error "record-predicate: illegal rtd argument:" rtd))
  (lambda (rec)
    (is-a? rec rtd)))


(define (record-accessor rtd field-name)
  (if (not (record:record-type? rtd))
      (error "record-accessor: illegal rtd argument:" rtd))
  (if (not (assq field-name (class-slots rtd)))
      (error "record-accessor: invalid field-name argument:" field-name))
  (lambda (rec)
    (if (not (is-a? rec rtd))
	(error "record-accessor-~s wrong record type: ~s"
	       (class-name rtd) rec))
    (slot-ref rec field-name)))


(define (record-modifier rtd field-name)
  (if (not (record:record-type? rtd))
      (error "record-modifier: illegal rtd argument:" rtd))
  (if (not (assq field-name (class-slots rtd)))
      (error "record-modifier: invalid field-name argument:" field-name))
  (lambda (rec val)
    (if (not (is-a? rec rtd))
	(error "record-modifier-~s wrong record type: ~s"
	       (class-name rtd) rec))
    (slot-set! rec field-name val)))
	       

(define (record? rec)
  (is-a? rec <record>))


(define (record-type-descriptor rec)
  (if (not (record? rec))
      (error "recor-type-descriptor: invalid record argument:" rec))
  (class-of rec))


(define (record-type-name rtd)
  (if (not (record:record-type? rtd))
      (error "record-type-name: illegal rtd argument:" rtd))
  (symbol->string (class-name rtd)))


(define (record-type-field-names rtd)
  (if (not (record:record-type? rtd))
      (error "record-type-field-names: illegal rtd argument:" rtd))
  (map car (class-slots rtd)))


(define-syntax define-record-type
  (syntax-rules ()
    ((define-record-type type
       (constructor constructor-tag ...)
       predicate
       (field-tag accessor . more) ...)
     (begin
       (define type (make-record-type 'type '(field-tag ...)))
       (define constructor (record-constructor type '(constructor-tag ...)))
       (define predicate (record-predicate type))
       (define-record-field type field-tag accessor . more)
       ...))))

; An auxilliary macro for define field accessors and modifiers.
; This is needed only because modifiers are optional.

(define-syntax define-record-field
  (syntax-rules ()
    ((define-record-field type field-tag accessor)
     (define accessor (record-accessor type 'field-tag)))
    ((define-record-field type field-tag accessor modifier)
     (begin
       (define accessor (record-accessor type 'field-tag))
       (define modifier (record-modifier type 'field-tag))))))


;; example

; 1)
;
; (define rec (make-record-type 'rec '(a b)))
; (define make-rec (record-constructor rec))
; (define rec? (record-predicate rec))
; (define rec-a (record-accessor rec 'a))
; (define set-rec-b! (record-modifier rec 'b))
;
; (define r (make-rec 1 2))
; (if (rec? r) (set-rec-b! r (rec-a r)))

; 2)
;
; (define-record-type rec (make-rec a b) rec? (a rec-a) (b rec-b set-rec-b!))


;;; End of code
