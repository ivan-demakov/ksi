;;;
;;; comparse.scm
;;; command line parsing
;;;
;;; Copyright (C) 1999-2000, Ivan Demakov.
;;;
;;; Permission to use, copy, modify, and distribute this software and its
;;; documentation for any purpose, without fee, and without a written
;;; agreement is hereby granted, provided that the above copyright notice
;;; and this paragraph and the following two paragraphs appear in all copies.
;;; Modifications to this software may be copyrighted by their authors
;;; and need not follow the licensing terms described here, provided that
;;; the new terms are clearly indicated on the first page of each file where
;;; they apply.
;;;
;;; IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
;;; FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
;;; INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
;;; DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
;;; POSSIBILITY OF SUCH DAMAGE.
;;;
;;; THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
;;; INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
;;; AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
;;; ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
;;; TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
;;;
;;;
;;; Author:        Ivan Demakov <demakov@users.sourceforge.net>
;;; Creation date: Mon Nov 15 00:09:38 1999
;;; Last Update:   Mon Nov 29 19:57:58 1999
;;;
;;;
;;; $Id: comparse.scm,v 1.1 2001/02/16 22:09:46 demakov Exp $
;;;


(define (mk-word word)
  (list->string (reverse word)))

(define (alnum? x)
  (or (char-alphabetic? x) (char-numeric? x) (char=? #\_ x)))

(define (var-subst var word env)
  (if (null? var)
      (cons #\$ word)
      (let ((val (assoc (mk-word var) env)))
	(if val
	    (let loop ((i 0) (word word))
	      (if (>= i (string-length (cdr val)))
		  word
		  (loop (+ i 1) (cons (string-ref (cdr val) i) word))))
	    word))))

(define (parse-command-line str env)
  (let loop ((pos 0) (args '()) (word '()) (esc? #f) (quote? #f))
    (cond ((>= pos (string-length str))
	   (cond ((or esc? quote?)
		  (error 'parse-command-line "unexpected end of line: ~a" str))
		 ((null? word)
		  (reverse! args))
		 (else
		  (loop pos (cons (mk-word word) args) '() #f #f))))

	  (esc?
	   (loop (+ pos 1) args (cons (string-ref str pos) word) #f quote?))

	  ((char=? (string-ref str pos) #\\)
	   (loop (+ pos 1) args word #t quote?))

	  ((char=? (string-ref str pos) #\')
	   (if (and quote?
		    (null? word)
		    (or (= (+ pos 1) (string-length str))
			(char-whitespace? (string-ref str (+ pos 1)))))
	       (loop (+ pos 1) (cons "" args) '() #f #f)
	       (loop (+ pos 1) args word #f (not quote?))))

	  (quote?
	   (loop (+ pos 1) args (cons (string-ref str pos) word) #f #t))

	  ((char=? (string-ref str pos) #\$)
	   (let next ((pos (+ pos 1)) (var '()))
	     (if (or (>= pos (string-length str))
		     (not (alnum? (string-ref str pos))))
		 (loop pos args (var-subst var word env) #f #f)
		 (next (+ pos 1) (cons (string-ref str pos) var)))))

	  ((char-whitespace? (string-ref str pos))
	   (if (null? word)
	       (loop (+ pos 1) args word #f #f)
	       (loop (+ pos 1) (cons (mk-word word) args) '() #f #f)))

	  (else
	   (loop (+ pos 1) args (cons (string-ref str pos) word) #f #f)))))


;;; End of code
