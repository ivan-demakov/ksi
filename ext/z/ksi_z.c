/*
 * ksi_z.c
 * ksi interface for z library
 *
 * Copyright (C) 1999-2000, 2009, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Wed Mar 17 18:59:00 1999
 * Last Update:   Fri Dec  1 16:59:54 2000
 *
 */

/* Comments:
 *
 */

#include "ksi_z.h"

#include <zlib.h>

static char rcsid[] = "$Id: ksi_z.c,v 1.5 2009/09/07 12:41:46 ksion Exp $";


#ifdef MSDOS
#  define OS_CODE  0x00
#endif
#ifdef OS2
#  define OS_CODE  0x06
#endif
#ifdef MSWIN32
#  define OS_CODE  0x0b
#endif
#if defined(VAXC) || defined(VMS)
#  define OS_CODE  0x02
#endif
#ifdef AMIGA
#  define OS_CODE  0x01
#endif
#if defined(ATARI) || defined(atarist)
#  define OS_CODE  0x05
#endif
#if defined(MACOS) || defined(TARGET_OS_MAC)
#  define OS_CODE  0x07
#endif
#ifdef __50SERIES /* Prime/PRIMOS */
#  define OS_CODE  0x0F
#endif
#ifdef TOPS20
#  define OS_CODE  0x0a
#endif


#ifndef OS_CODE
#  define OS_CODE  0x03  /* assume Unix */
#endif

#define Z_BUFSIZE 4096

#define ASCII_FLAG   0x01 /* bit 0 set: file probably ascii text */
#define HEAD_CRC     0x02 /* bit 1 set: header CRC present */
#define EXTRA_FIELD  0x04 /* bit 2 set: extra field present */
#define ORIG_NAME    0x08 /* bit 3 set: original file name present */
#define COMMENT      0x10 /* bit 4 set: file comment present */
#define RESERVED     0xE0 /* bits 5..7: reserved */

static unsigned char gz_magic[2] = {0x1f, 0x8b}; /* gzip magic header */

#define GZPORT_P(x) (KSI_PORT_P(x) && (((ksi_port) (x))->ops == &gz_port_tag))


struct Ksi_GZPort
{
  struct Ksi_Port	kp;
  ksi_port		port;
  z_stream		z;
  unsigned char		*buf;
  unsigned		crc;
};


struct Z_List
{
  struct Z_List		*next;
  ksi_obj		data;
};

static struct Z_List *head;

static void
z_add (ksi_obj data)
{
  struct Z_List *curr = ksi_malloc (sizeof *curr);
  curr->next = head->next;
  curr->data = data;
  head->next = curr;
}

static void
z_del (ksi_obj data)
{
  struct Z_List *curr, *prev;
  for (prev = head, curr = head->next; curr; prev = curr, curr = curr->next)
    if (curr->data == data)
      {
	prev->next = curr->next;
	curr->next = 0;
	curr->data = 0;
	break;
      }
}

static void*
z_alloc (void *u, unsigned n, unsigned s)
{
  return ksi_malloc (n*s);
}

static void
z_free (void *u, void *p)
{
  ksi_free (p);
}

static void
write_header (struct Ksi_GZPort *p)
{
  ksi_port_putc (p->port, gz_magic[0]);
  ksi_port_putc (p->port, gz_magic[1]);
  ksi_port_putc (p->port, Z_DEFLATED);
  ksi_port_putc (p->port, 0); /* flags */
  ksi_port_putc (p->port, 0); /* date */
  ksi_port_putc (p->port, 0); /* date */
  ksi_port_putc (p->port, 0); /* date */
  ksi_port_putc (p->port, 0); /* date */
  ksi_port_putc (p->port, 0); /* xflags */
  ksi_port_putc (p->port, OS_CODE);
}

static void
write_int (struct Ksi_GZPort *p, unsigned x)
{
  ksi_port_putc (p->port, x & 0xff);
  ksi_port_putc (p->port, (x >> 8)  & 0xff);
  ksi_port_putc (p->port, (x >> 16) & 0xff);
  ksi_port_putc (p->port, (x >> 24) & 0xff);
}

static int
read_byte  (struct Ksi_GZPort *p)
{
  if (p->kp.eof)
    return -1;
  if (p->z.avail_in == 0)
    {
      int err = ksi_port_read ((ksi_obj) p->port, (char *) p->buf, Z_BUFSIZE);
      if (err <= 0)
	{
	  p->kp.eof = 1;
	  return -1;
	}

      p->z.avail_in = err;
      p->z.next_in  = p->buf;
    }

  p->z.avail_in--;
  return *p->z.next_in++;
}

static void
read_header (struct Ksi_GZPort *p, char *proc)
{
  int method; /* method byte */
  int flags;  /* flags byte */
  int len, c;

  /* Check the gzip magic header */
  if ((c = read_byte (p)) < 0)
    return;
  if (c != gz_magic[0])
    {
      p->z.avail_in++;
      p->z.next_in--;
      p->kp.unbuf = 1;
      return;
    }
  if ((c = read_byte (p)) < 0)
    {
      p->z.avail_in++;
      p->z.next_in--;
      p->kp.eof   = 0;
      p->kp.unbuf = 1;
      return;
    }
  if (c != gz_magic[1])
    {
      p->z.avail_in += 2;
      p->z.next_in  -= 2;
      p->kp.unbuf = 1;
      return;
    }

  method = read_byte (p);
  flags  = read_byte (p);
  if (method != Z_DEFLATED || (flags & RESERVED) != 0)
    ksi_exn_error ("i/o", (ksi_obj) p, "%s: invalid gzip header", proc);

  /* skip time, xflags and OS code: */
  for (len = 0; len < 6; len++)
    read_byte (p);

  /* skip the extra field */
  if ((flags & EXTRA_FIELD) != 0)
    {
      len  = read_byte (p);
      len += read_byte (p) << 8;
      /* len is garbage if EOF but the loop below will quit anyway */
      while (len-- != 0 && read_byte (p) >= 0) /**/;
    }

  /* skip the original file name */
  if ((flags & ORIG_NAME) != 0)
    while (read_byte (p) > 0) /**/;

  /* skip the file comment */
  if ((flags & COMMENT) != 0)
    while (read_byte (p) > 0) /**/;

  /* skip the header crc */
  if ((flags & HEAD_CRC) != 0)
    {
      read_byte (p);
      read_byte (p);
    }

  if (p->kp.eof)
    ksi_exn_error ("i/o", (ksi_obj) p, "%s: unexpected end of data", proc);
}

static unsigned
read_int (struct Ksi_GZPort *p)
{
  unsigned x;

  x  = read_byte (p);
  x += read_byte (p) << 8;
  x += read_byte (p) << 16;
  x += read_byte (p) << 24;
  return x;
}

static int
do_flush (struct Ksi_GZPort *p, int flush)
{
  int len, err, done = 0;
  unsigned char *buf;

  for (;;)
    {
      len = Z_BUFSIZE - p->z.avail_out;
      buf = p->buf;
      while (len > 0)
	{
          if ((err = ksi_port_write ((ksi_obj) p->port, (char *) buf, len)) < 0)
	    return err;
	  len -= err;
	  buf += err;
	}

      p->z.next_out  = p->buf;
      p->z.avail_out = Z_BUFSIZE;

      if (done) break;
      err = deflate (&p->z, flush);

      /* deflate has finished flushing only when it hasn't used up
       * all the available space in the output buffer: 
       */
      done = (p->z.avail_out != 0 || err == Z_STREAM_END);
   }
 
  return 0;
}

static const char*
gz_name (ksi_port x)
{
  struct Ksi_GZPort* p = (struct Ksi_GZPort*) x;
  return ksi_aprintf ("gzip-%s", p->port->ops->name (p->port));
}

static int
gz_close (ksi_port x)
{
  struct Ksi_GZPort *p = (struct Ksi_GZPort*) x;
  int err = 0;

  z_del ((ksi_obj) p);
  ksi_unregister_finalizer (p);

  if (p->kp.output)
    {
      err = do_flush (p, Z_FINISH);
      write_int (p, p->crc);
      write_int (p, p->z.total_in);

      if (p->port->ops->flush (p->port) < 0)
	err = -1;
    }

  p->kp.closed = 1;
  p->kp.input  = 0;
  p->kp.output = 0;

  if (!p->kp.is_ext)
    p->port->ops->close (p->port);

  return err;
}

static int
gz_read (ksi_port x, char *buf, int len)
{
  struct Ksi_GZPort *p = (struct Ksi_GZPort*) x;
  Byte *start;
  int err;

  if (p->kp.eof)
    return 0;

  if (p->kp.unbuf)
    {
      err = 0;
      while (p->z.avail_in > 0 && len > 0)
	{
	  *buf++ = *p->z.next_in++;
	  p->z.avail_in--;
	  len--;
	  err++;
	}

      return (len <= 0 ? err : err + ksi_port_read (p->port, buf, len));
    }

  start = (Byte*) buf;
  p->z.next_out  = (Byte*) buf;
  p->z.avail_out = len;
  while (p->z.avail_out > 0)
    {
      if (p->z.avail_in == 0)
	{
	  err = ksi_port_read (p->port, p->buf, Z_BUFSIZE);
	  if (err <= 0)
	    {
	      p->kp.eof = 1;
	      break;
	    }

	  p->z.next_in  = p->buf;
	  p->z.avail_in = err;
	}

      err = inflate (&p->z, Z_NO_FLUSH);
      if (err == Z_STREAM_END)
	{
	  p->crc = crc32 (p->crc, start, p->z.next_out - start);
	  start  = p->z.next_out;

	  if (p->crc != read_int (p))
	    ksi_exn_error ("i/o", (ksi_obj) p, "read-char: invalid crc");

	  read_int (p);
	  if (p->kp.eof)
	    ksi_exn_error ("i/o", (ksi_obj) p, "read-char: unexpected end of data");

	  read_header (p, "read-char");
	  if (p->kp.eof)
	    return len - p->z.avail_out;

	  if (p->kp.unbuf)
	    {
	      while (p->z.avail_in > 0 && p->z.avail_out > 0)
		{
		  *p->z.next_out++ = *p->z.next_in++;
		  p->z.avail_in--;
		  p->z.avail_out--;
		}
	      return len - p->z.avail_out;
	    }

	  inflateReset (&p->z);
	  p->crc = crc32 (0, Z_NULL, 0);
	}
    }

  p->crc = crc32 (p->crc, start, p->z.next_out - start);
  return len - p->z.avail_out;
}

static int
gz_write (ksi_port x, const char* str, int len)
{
  struct Ksi_GZPort* p = (struct Ksi_GZPort*) x;
  int err;

  p->z.next_in  = (Byte*) str;
  p->z.avail_in = len;

  while (p->z.avail_in != 0)
    {
      if (p->z.avail_out == 0)
	{
	  err = ksi_port_write (p->port, p->buf, Z_BUFSIZE);
	  if (err < 0)
	    return err;
	  if (err != Z_BUFSIZE)
	    {
	      if (err > 0)
		memmove (p->buf, p->buf+err, Z_BUFSIZE-err);

	      p->z.next_out  = p->buf + err;
	      p->z.avail_out = err;

	      p->crc = crc32 (p->crc, str, len - p->z.avail_in);
	      return (len - p->z.avail_in);
	    }

	  p->z.next_out  = p->buf;
	  p->z.avail_out = Z_BUFSIZE;
        }

      err = deflate (&p->z, Z_NO_FLUSH);
      if (err != Z_OK)
	ksi_exn_error ("i/o", (ksi_obj) p, "write-char: %s", p->z.msg);
    }

  p->crc = crc32 (p->crc, str, len);
  return len;
}

static int
gz_ready (ksi_port x)
{
  return 1;
}

static int
gz_flush (ksi_port x)
{
  struct Ksi_GZPort *p = (struct Ksi_GZPort*) x;
  if (p->kp.output)
    {
      int err = do_flush (p, Z_SYNC_FLUSH);
      return (err < 0 ? err : p->port->ops->flush (p->port));
    }

  return 0;
}

static int
gz_fd (ksi_port x)
{
  return -1;
}

static struct Ksi_Port_Tag gz_port_tag =
{
  gz_name,
  gz_close,
  gz_read,
  gz_write,
  gz_ready,
  gz_ready,
  gz_flush,
  gz_fd,
  gz_fd
};

static void
gz_finalizer (void *obj, void *data)
{
  struct Ksi_GZPort *p = (struct Ksi_GZPort*) obj;

  z_del ((ksi_obj) p);

  if (p->kp.output)
    {
      do_flush (p, Z_FINISH);
      write_int (p, p->crc);
      write_int (p, p->z.total_in);
      p->port->ops->flush (p->port);
    }

  if (!p->kp.is_ext)
    p->port->ops->close (p->port);
}

static ksi_port
ksi_new_gzport (ksi_obj port, char *mode, char *proc)
{
  struct Ksi_GZPort *x;
  int lev = Z_DEFAULT_COMPRESSION; /* compression level */
  int stg = Z_DEFAULT_STRATEGY;    /* compression strategy */
  int err;

  x = KSI_NEW_TYPED (struct Ksi_GZPort, port, buf);
  bzero (x, sizeof *x);

  if (*mode == 'r')
    {
      if (!KSI_INPUT_PORT_P (port))
	ksi_exn_error ("type", port, "%s: invalid input port", proc);
      x->kp.input = 1;
    }
  else if (*mode == 'w' || *mode == 'a')
    {
      if (!KSI_OUTPUT_PORT_P (port))
	ksi_exn_error ("type", port, "%s: invalid output port", proc);
      x->kp.output = 1;
    }
  else
    {
      ksi_exn_error ("range", ksi_str02string (mode),
		     "%s: invalid open mode", proc);
    }

  while (*++mode)
    {
      if ('0' <= *mode && *mode <= '9')
	lev = *mode - '0';
      else if (*mode == 'f')
	stg = Z_FILTERED;
      else if (*mode == 'h')
	stg = Z_HUFFMAN_ONLY;
    }

  x->kp.itag     = KSI_TAG_PORT;
  x->kp.ops      = &gz_port_tag;
  x->port        = (ksi_port) port;
  x->z.zalloc    = z_alloc;
  x->z.zfree     = z_free;
  x->z.avail_out = Z_BUFSIZE;
  x->buf         = (char*) ksi_malloc_data (Z_BUFSIZE);
  x->crc         = crc32 (0, Z_NULL, 0);

  if (x->kp.output)
    {
      x->z.next_out = (Byte*) x->buf;
      err = deflateInit2 (&x->z, lev, Z_DEFLATED, -MAX_WBITS, 8, stg);
      if (err != Z_OK)
	ksi_exn_error ("misc", ksi_void, "%s: %s", proc, x->z.msg);

      write_header (x);
    }
  else /* if (x->input) */
    {
      x->z.next_in = (Byte*) x->buf;
      err = inflateInit2 (&x->z, -MAX_WBITS);
      if (err != Z_OK)
	ksi_exn_error ("misc", ksi_void, "%s: %s", proc, x->z.msg);

      read_header (x, proc);
    }

  z_add ((ksi_obj) x);
  ksi_register_finalizer (x, gz_finalizer, 0);

  return (ksi_port) x;
}


ksi_obj
ksi_make_gzport (ksi_obj port, ksi_obj mode)
{
  ksi_port x;

  KSI_TYPE_CHECK (mode, KSI_STR_P (mode), "make-gzport: invalid string in arg2");
  x = ksi_new_gzport (port, KSI_STR_PTR (mode), "make-gzport");
  x->is_ext = 1;
  return (ksi_obj) x;
}


ksi_obj
ksi_open_gzfile (ksi_obj name, ksi_obj mode)
{
  ksi_obj x;

  KSI_TYPE_CHECK (name, KSI_STR_P (name), "open-gzfile: invalid string in arg1");
  KSI_TYPE_CHECK (mode, KSI_STR_P (mode), "open-gzfile: invalid string in arg2");

  x = ksi_open_file (name, mode);
  return (ksi_obj) ksi_new_gzport (x, KSI_STR_PTR (mode), "open-gzfile");
}


static struct Ksi_Prim_Def defs [] =
{
  {"make-gzport",	ksi_make_gzport,	KSI_CALL_ARG2, 2},
  {"open-gzfile",	ksi_open_gzfile,	KSI_CALL_ARG2, 2},

  {0}
};

void
ksi_init_zip (void)
{
  (void) rcsid;

  ksi_reg_unit (defs);

  head = (struct Z_List*) ksi_malloc_eternal (sizeof *head);
  bzero (head, sizeof *head);
}

void
ksi_term_zip (void)
{
  struct Z_List *curr = head;
  while ((curr = curr->next) != 0)
    {
      ksi_unregister_finalizer (curr->data);

      if (GZPORT_P (curr->data))
	{
	  struct Ksi_GZPort *p = (struct Ksi_GZPort *) curr->data;
	  if (p->kp.output)
	    {
	      do_flush (p, Z_FINISH);
	      write_int (p, p->crc);
	      write_int (p, p->z.total_in);
	      p->port->ops->flush (p->port);
	    }

	  if (!p->kp.is_ext)
	    p->port->ops->close (p->port);
	}
    }

  ksi_free (head);
  head = 0;
}


 /* End of code */
