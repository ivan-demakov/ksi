/* -*-mode:C++-*- */
/*
 * ksi_z.h
 * ksi interface for z library
 *
 * Copyright (C) 1999-2000, 2009, Ivan Demakov.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 *
 * IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 * DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *
 * Author:        Ivan Demakov <demakov@users.sourceforge.net>
 * Creation date: Wed Mar 17 18:57:30 1999
 *
 *
 * $Id: ksi_z.h,v 1.3 2009/09/07 12:41:46 ksion Exp $
 *
 */

#ifndef KSI_Z_H
#define KSI_Z_H

#include <ksi.h>

#if defined(_MSC_VER)
#  if defined(KSIDLL)
#    if defined(MAKE_z_LIB)
#      define Z_API __declspec(dllexport)
#    else
#      define Z_API __declspec(dllimport)
#    endif
#  endif
#endif

#ifndef Z_API
#  define Z_API extern
#endif


#ifdef __cplusplus
extern "C" {
#endif

Z_API
void
ksi_init_zip (void);

Z_API
void
ksi_term_zip (void);

Z_API
ksi_obj
ksi_make_gzport (ksi_obj port, ksi_obj mode);

Z_API
ksi_obj
ksi_open_gzfile (ksi_obj fname, ksi_obj mode);


#ifdef __cplusplus
}
#endif

#endif

 /* End of file */
