/*
 * ksi_regexp.c
 * regexp extension
 *
 * Copyright (C) 1998-2009, ivan demakov
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Sat Jul  4 05:54:47 1998
 *
 */

/* Comments:
 *
 */

#include <ksi.h>

#include "pcre.h"

static char rcsid[] = "$Id: ksi_regexp.c,v 1.5 2009/09/12 14:48:00 ksion Exp $";


static struct Ksi_RE
{
  const unsigned char *table;
} *re;

struct Ksi_Regexp
{
  unsigned		itag;
  ksi_etag		etag;
  const char*		pattern;
  pcre*			regexp;
  pcre_extra*		extra;
  int			subnum;
};

typedef struct Ksi_Regexp	*ksi_regexp;


#define KSI_REGEXP_P(x)	KSI_EXT_IS((x), &tc_regexp)


static int
ksi_regexp_tag_equal (ksi_etag tag, ksi_obj x1, ksi_obj x2, int deep)
{
  return 0;
}

static const char*
ksi_regexp_tag_print (ksi_etag tag, ksi_obj x, int slashify)
{
  return ksi_aprintf ("#<regexp %s>", ((ksi_regexp) x) -> pattern);
}

static struct Ksi_ETag tc_regexp =
{
  "regexp",
  ksi_regexp_tag_equal,
  ksi_regexp_tag_print
};


ksi_obj
ksi_regexp_p (ksi_obj x)
{
  return KSI_REGEXP_P (x) ? ksi_true : ksi_false;
}


/*
 * ksi_make_regexp -- Compile a regexp.
 */

ksi_obj
ksi_make_regexp (ksi_obj str, ksi_obj opt)
{
  ksi_regexp x;
  pcre *regexp;
  char *pattern;
  const char *errptr;
  int erroff, i, flg = 0;

  KSI_TYPE_CHECK (str, KSI_STR_P (str), "make-regexp: invalid string in arg1");
  if (opt != 0)
    {
      KSI_TYPE_CHECK (opt, KSI_STR_P (opt), "make-regexp: invalid string in arg2");
      for (i = 0; i < KSI_STR_LEN (opt); i++)
	switch (KSI_STR_PTR (opt) [i])
	  {
	  case 'i': flg |= PCRE_CASELESS;  break;
	  case 's': flg |= PCRE_DOTALL;    break;
	  case 'x': flg |= PCRE_EXTENDED;  break;
	  case 'm': flg |= PCRE_MULTILINE; break;
	  }
    }

  if (KSI_C_STR_P (str)) {
    pattern = KSI_STR_PTR (str);
  } else {
    pattern = ksi_malloc_data (KSI_STR_LEN (str) + 1);
    memcpy (pattern, KSI_STR_PTR (str), KSI_STR_LEN (str));
    pattern [KSI_STR_LEN (str)] = '\0';
  }
  regexp  = pcre_compile (pattern, flg, &errptr, &erroff, re->table);

  if (regexp == 0)
    ksi_exn_error ("misc", str, "make-regexp: %s", errptr);

  x = ksi_malloc (sizeof *x);
  x->itag    = KSI_TAG_EXTENDED;
  x->etag    = &tc_regexp;
  x->pattern = pattern;
  x->regexp  = regexp;
  x->extra   = pcre_study (regexp, 0, &errptr);
  pcre_fullinfo (regexp, x->extra, PCRE_INFO_CAPTURECOUNT, &x->subnum);

  return (ksi_obj) x;
}


/*
 * ksi_regexp_exec -- Match a compiled regexp with a string.
 *
 * Return `ksi_false' if the string does not match the regexp
 * or a vector of 3 elements if the string matches.
 * First elementh of that vector is the matched string itself,
 * second and third are vectors of starting and ending indexes
 * of substrings within the string.
 * The first substring is a substring that matched the whole
 * regular expression.  The others are those substrings that
 * matched parenthesized expressions numbered in left-to-right
 * order.
 */

ksi_obj
ksi_regexp_exec (ksi_obj reg, ksi_obj str, ksi_obj beg, ksi_obj end)
{
  int bof, eof;

  KSI_TYPE_CHECK (reg, KSI_REGEXP_P(reg), "regexp-exec: invalid regexp in arg1");
  KSI_TYPE_CHECK (str, KSI_STR_P(str), "regexp-exec: invalid string in arg2");

  if (beg == 0)
    bof = 0;
  else
    {
      KSI_TYPE_CHECK (beg, KSI_EINT_P(beg), "regexp-exec: invalid integer in arg3");
      bof = ksi_num2int (beg, "regexp-exec");
      KSI_RANGE_CHECK (beg, 0 <= bof, "regexp-exec: index out of range");
    }

  if (end == 0)
    eof = KSI_STR_LEN (str);
  else
    {
      KSI_TYPE_CHECK (end, KSI_EINT_P(end), "regexp-exec: invalid integer in arg4");
      eof = ksi_num2int (beg, "regexp-exec");
      KSI_RANGE_CHECK (end, bof <= eof, "regexp-exec: index out of range");

      if (eof > KSI_STR_LEN (str))
	eof = KSI_STR_LEN (str);
    }

  if (bof < eof)
    {
      ksi_regexp  exp = (ksi_regexp) reg;
      const char *ptr = KSI_STR_PTR (str) + bof;
      int         num = (exp->subnum + 1) * 3;
      int        *sub = (int*) alloca (num * sizeof *sub);

      num = pcre_exec (exp->regexp, exp->extra, ptr, eof - bof, 0, 0, sub, num);
      if (num > 0)
	{
	  int i;
	  ksi_obj v, v1, v2;

	  v1 = ksi_make_vector (ksi_int2num (num), ksi_false);
	  v2 = ksi_make_vector (ksi_int2num (num), ksi_false);
	  for (i = 0; i < num; i++)
	    if (sub[i*2] >= 0 && sub[i*2+1] >= 0)
	      {
		KSI_VEC_REF (v1, i) = ksi_int2num (sub[i*2]);
		KSI_VEC_REF (v2, i) = ksi_int2num (sub[i*2+1]);
	      }

	  v = ksi_make_vector (ksi_int2num (3), ksi_false);
	  KSI_VEC_REF (v, 0) = ksi_str2string (ptr, eof - bof);
	  KSI_VEC_REF (v, 1) = v1;
	  KSI_VEC_REF (v, 2) = v2;
	  return v;
	}
    }

  return ksi_false;
}


ksi_obj
ksi_string_match (ksi_obj pattern, ksi_obj str, ksi_obj opt)
{
  ksi_obj exp = ksi_make_regexp (pattern, opt);
  return ksi_regexp_exec (exp, str, 0, 0);
}


static struct Ksi_Prim_Def defs [] =
{
  { "regexp?",		ksi_regexp_p,		KSI_CALL_ARG1, 1 },
  { "make-regexp",	ksi_make_regexp,	KSI_CALL_ARG2, 1 },
  { "regexp-exec",	ksi_regexp_exec,	KSI_CALL_ARG4, 2 },
  { "string-match",	ksi_string_match,	KSI_CALL_ARG3, 2 },

  { 0 }
};

void
ksi_init_regexp ()
{
  (void) rcsid;

  pcre_malloc = ksi_malloc_data;
  pcre_free   = ksi_free;

  re = (struct Ksi_RE*) ksi_malloc_eternal (sizeof *re);
  re->table = pcre_maketables ();

  ksi_reg_unit (defs, 0);
}


void
ksi_term_regexp ()
{
  ksi_free (re);
  re = 0;
}


 /* End of code */
