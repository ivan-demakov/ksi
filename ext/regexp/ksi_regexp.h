/* -*-mode:C++-*- */
/*
 * ksi_regexp.h
 * regexp ksi interface
 *
 * Copyright (C) 1998-2000, 2009, Ivan Demakov.
 *
 * The software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the software; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        Ivan Demakov <ksion@users.sourceforge.net>
 * Creation date: Sat Jul  4 06:35:44 1998
 *
 *
 * $Id: ksi_regexp.h,v 1.4 2009/09/12 14:48:00 ksion Exp $
 *
 */

#ifndef KSI_REGEXP_H
#define KSI_REGEXP_H

#ifndef KSI_H
#  include "ksi.h"
#endif

#if defined(_MSC_VER)
#  if defined(KSIDLL)
#    if defined(MAKE_regexp_LIB)
#      define REGEXP_API __declspec(dllexport)
#    else
#      define REGEXP_API __declspec(dllimport)
#    endif
#  endif
#endif

#ifndef REGEXP_API
#  define REGEXP_API extern
#endif


#ifdef __cplusplus
extern "C" {
#endif

REGEXP_API
void
ksi_init_regexp (void);

REGEXP_API
void
ksi_term_regexp (void);

REGEXP_API
ksi_obj
ksi_regexp_p (ksi_obj x);

REGEXP_API
ksi_obj
ksi_make_regexp (ksi_obj str, ksi_obj opt);

REGEXP_API
ksi_obj
ksi_regexp_exec (ksi_obj x, ksi_obj str, ksi_obj start, ksi_obj end);

REGEXP_API
ksi_obj
ksi_string_match (ksi_obj pattern, ksi_obj string, ksi_obj opt);


#ifdef __cplusplus
}
#endif


#endif

 /* End of file */
