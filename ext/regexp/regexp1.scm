;;;
;;; regexp1.scm
;;; scheme part of regexp package
;;;
;;; Copyright (C) 1998-2000, Ivan Demakov.
;;; All rights reserved.
;;;
;;; Permission to use, copy, modify, and distribute this software and its
;;; documentation for any purpose, without fee, and without a written
;;; agreement is hereby granted, provided that the above copyright notice
;;; and this paragraph and the following two paragraphs appear in all copies.
;;; Modifications to this software may be copyrighted by their authors
;;; and need not follow the licensing terms described here, provided that
;;; the new terms are clearly indicated on the first page of each file where
;;; they apply.
;;;
;;; IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
;;; FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
;;; INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
;;; DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
;;; POSSIBILITY OF SUCH DAMAGE.
;;;
;;; THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
;;; INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
;;; AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
;;; ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
;;; TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
;;;
;;;
;;; Author:        Ivan Demakov <demakov@users.sourceforge.net>
;;; Creation date: Sat Jul  4 09:00:10 1998
;;; Last Update:   Sat Dec  2 06:12:29 2000
;;;
;;;
;;; $Id: regexp1.scm,v 1.2 2000/12/16 18:46:34 demakov Exp $
;;;
;;;
;;; Comments:
;;;
;;;

(use-module 'list-lib)

(define (match:start match . index)
  (let ((ind (if (pair? index) (car index) 0)))
    (vector-ref (vector-ref match 1) ind)))

(define (match:end match . index)
  (let ((ind (if (pair? index) (car index) 0)))
    (vector-ref (vector-ref match 2) ind)))

(define (match:substring match . index)
  (let* ((ind (if (pair? index) (car index) 0))
	 (start (vector-ref (vector-ref match 1) ind)))
    (and start (substring (vector-ref match 0)
			  start
			  (vector-ref (vector-ref match 2) ind)))))


(define (regexp-quote string)
  (let lp ((i (- (string-length string) 1))
	   (result '()))
    (if (< i 0)
	(list->string result)
	(lp (- i 1)
	    (let* ((c (string-ref string i))
		   (result (cons c result)))
	      (if (memv c '(#\\ #\^ #\$ #\. #\| #\? #\* #\+ #\( #\[ #\{ ))
		  (cons #\\ result)
		  result))))))


(define (regexp-substitute port match . items)
  (let ((str (vector-ref match 0))
	(beg (vector-ref match 1))
	(end (vector-ref match 2)))
    (define (range item)
      (cond ((integer? item) (values (vector-ref beg item)
				     (vector-ref end item)))
	    ((eq? 'pre item) (values 0 (vector-ref beg 0)))
	    ((eq? 'post item) (values (vector-ref end 0)
				      (string-length str)))
	    (else (error 'regexp-substitute "illegal substitution item: ~s"
			 item))))
    (if (eq? port #t)
	(set! port (current-output-port)))

    (if port
	(for-each (lambda (item)
		    (if (string? item)
			(display item port)
			(call-with-values
			 (lambda () (range item))
			 (lambda (s e) (display (substring str s e) port)))))
		  items)
	(let ((res (make-string
		    (reduce-init (lambda (i item)
				   (+ i (if (string? item)
					    (string-length item)
					    (call-with-values
					     (lambda () (range item))
					     (lambda (s e) (- e s))))))
				 0 items))))
	  (reduce-init (lambda (i item)
			 (if (string? item)
			     (begin (string-replace! res i item)
				    (+ i (string-length item)))
			     (call-with-values
			      (lambda () (range item))
			      (lambda (s e)
				(substring-replace! res i str s e)
				(+ i (- e s))))))
		       0 items)
	  res))))
			 

;;; End of code