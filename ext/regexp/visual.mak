#
# makefile for Visual C
#
# $Id: visual.mak,v 1.1 2000/07/25 19:01:27 demakov Exp $
#

!include $(SRCDIR)\makedefs.mak

CUR_LIB=$(BUILDDIR)\$(CUR_TARGET).lib
CUR_DLL=$(BUILDDIR)\$(CUR_TARGET).dll

BUILD_TARGETS=$(CUR_LIB) $(pp_FILES)

!include $(TOPDIR)\visual.mif

LIBS=$(BUILDDIR)\ksi.lib
OBJS=$(regexp_OBJS)
TARGET_CFLAGS=$(LIB_CFLAGS)

!include $(TOPDIR)\visual1.mif

extdir=$(TOPDIR)\$(DISTDIR)\$(KSI_MAJOR_VERSION).$(KSI_MINOR_VERSION)

all: do_curdir

clean: do_curdir_clean

dist:
	@for %i in ($(regexp_HEADERS)) do copy $(SRCDIR)\%i $(TOPDIR)\$(DISTDIR)\include
	copy $(CUR_LIB) $(TOPDIR)\$(DISTDIR)\lib
!ifdef MAKE_AS_DLL
	copy $(CUR_DLL) $(extdir)
!endif
	@for %i in ($(ext_DATA)) do copy $(SRCDIR)\%i $(extdir)
		