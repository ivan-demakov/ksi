;;;
;;; symprop.scm
;;; properties
;;;
;;; Copyright (C) 1998-2000, Ivan Demakov.
;;;
;;; Permission to use, copy, modify, and distribute this software and its
;;; documentation for any purpose, without fee, and without a written
;;; agreement is hereby granted, provided that the above copyright notice
;;; and this paragraph and the following two paragraphs appear in all copies.
;;; Modifications to this software may be copyrighted by their authors
;;; and need not follow the licensing terms described here, provided that
;;; the new terms are clearly indicated on the first page of each file where
;;; they apply.
;;;
;;; IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
;;; FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
;;; INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
;;; DOCUMENTATION, EVEN IF THE AUTHORS HAS BEEN ADVISED OF THE
;;; POSSIBILITY OF SUCH DAMAGE.
;;;
;;; THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIMS ANY WARRANTIES,
;;; INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
;;; AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
;;; ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAS NO OBLIGATIONS
;;; TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
;;;
;;;
;;; Author:        Ivan Demakov <demakov@users.sourceforge.net>
;;; Creation date: Sat Apr 18 13:08:38 1998
;;; Last Update:   Tue Mar 21 02:40:05 2000
;;;
;;;
;;; $Id: symprop.scm,v 1.1 2001/02/16 22:09:46 demakov Exp $
;;;
;;;
;;; Comments:
;;;

(define symbol-property-table (make-hash-table 128 hashv eqv?))

(define (symbol-properties symbol)
  (hash-ref symbol-property-table symbol '()))

(define (set-symbol-properties! symbol properties)
  (hash-set! symbol-property-table symbol properties))


(define (get-symbol-property symbol property)
  (assq-ref (symbol-properties symbol) property))

(define (set-symbol-property! symbol property value)
  (set-symbol-properties!
   symbol
   (assq-set! (symbol-properties symbol) property value)))


;;; End of code
